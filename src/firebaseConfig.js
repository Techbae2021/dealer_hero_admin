/** @format */

import firebase from 'firebase/app';
import 'firebase/auth';

// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: 'AIzaSyC7djelWQ_av3R-yC8cQ6jKRroRN-OuDVA',
  authDomain: 'dealershero-b7034.firebaseapp.com',
  projectId: 'dealershero-b7034',
  storageBucket: 'dealershero-b7034.appspot.com',
  messagingSenderId: '449717401474',
  appId: '1:449717401474:web:f67c586e92f8ce5b75419b',
  measurementId: 'G-S638E3F5WE',
};

firebase.initializeApp(firebaseConfig);

export default firebase;
