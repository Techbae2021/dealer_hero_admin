/** @format */

//import logo from './logo.svg';
import React, { useEffect } from 'react';
import Layout from './components/layout/layout';
import { BrowserRouter } from 'react-router-dom';
import './assets/scss/theme.scss';
import Routes from './routes/routes';
import MainProvider from './pages/context/Provider';
import { positions, Provider } from 'react-alert';
import AlertTemplate from 'react-alert-template-basic';
import NavToggleProvider from './components/layout/Context/Provider';
import mixpanel from 'mixpanel-browser';
import firebase from '../src/firebase';
import EmployeeProvider from './routes/Provider';
mixpanel.init('b1375502b36d5eae49963743736cd874');

function App() {
  const options = {
    timeout: 5000,
    position: positions.TOP_CENTER,
  };

  useEffect(() => {
    let messaging = firebase.messaging.isSupported() ? firebase.messaging() : null;
    if(messaging){
    Notification.requestPermission().then(() => {
        return messaging.getToken();
      })
      .then((token) => {
        console.log('Token : ', token);
        localStorage.setItem('ftoken$', token);
      })
      .catch((err) => {
        console.log(err);
      });
    }
  }, []);

  return (
    <>
      <Provider template={AlertTemplate} {...options}>
        <EmployeeProvider>
          <NavToggleProvider>
            <MainProvider>
              <BrowserRouter>
                <Layout>
                  <Routes></Routes>
                </Layout>
              </BrowserRouter>
            </MainProvider>
          </NavToggleProvider>
        </EmployeeProvider>
      </Provider>
    </>
  );
}

export default App;
