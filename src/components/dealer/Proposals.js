/** @format */

import React, { useState, useEffect } from 'react';
import { Row, Col, Table, Fade } from 'reactstrap';
import Instance from '../../Instance';
import moment from 'moment';
import {
  CardBody,
  Card,
  Modal,
  ModalHeader,ModalBody} from 'reactstrap';
import config from '../../config';

const ProposalDetails = ({dealerId,active}) => {
  const [customerView, setCustomerView] = useState(null);
  const [loader, setLoader] = useState(false);
  const [count, setCount] = useState(1);
  const [totalCount, settotalCount] = useState(0);
  const [page, setPage] = useState(null);

  useEffect(() => {
    setLoader(true);
    Instance.post(
      `/api/admin/dealer/proposals/${dealerId}`,
      {
        page: count,
      },
      {
        headers: { authorization: `Bearer ${localStorage.getItem('token$')}` },
      }
    )
      .then(({ data }) => {
        let temp = data?.results?.proposals;
        // console.log('proposals',temp)
        setPage(data?.results);
        setCustomerView(temp);
        setLoader(false);
        settotalCount(data?.results?.totalproposaldb);
      })
      .catch((err) => {
        setLoader(false);
        console.log('err', err);
      });
  }, [count,active]);

  const NextPage = () => {
    setCount(count + 1);
  };
  const PrevPage = () => {
    setCount(count - 1);
  };

  const [modalOpen, setModalOpen] = useState(false);
  const [modalView, setModalView] = useState('');

  const modalToggle = (value) => {
    setModalOpen(!modalOpen);
    setModalView(value);
  };

  return (
    <>
      {/* <Row className='page-title align-items-center'>
        <Col sm={4} xl={6}>
          <h4 className='mb-1 mt-0'>Dealer Proposal Details</h4>
        </Col>
      </Row> */}

      <div className='row'>
        <div className='col-12'>
          <div className='card'>
            <div className='p-3'>
              <div className='row'>
                <div className='col-6'>
                  <h4> All Proposal </h4>
                </div>
                <div className='col-md-6 text-right'>
                  <div className='d-inline-block align-middle float-lg-right'>
                    <div className='row'>
                      <div
                        className='col-8 align-self-center'
                        style={{ whiteSpace: 'nowrap' }}>
                        Showing {page?.startOfItem} - {page?.endOfItem} of{' '}
                        {totalCount}
                      </div>
                      {/* end col*/}
                      <div className='col-4'>
                        <div className='btn-group float-right'>
                          <button
                            type='button'
                            className='btn btn-white btn-sm'
                            style={{
                              display: page?.Previous === false ? 'none' : '',
                            }}>
                            <i
                              className='uil uil-angle-left'
                              onClick={PrevPage}
                            />
                          </button>
                          <button
                            type='button'
                            className='btn btn-primary btn-sm'
                            style={{
                              display: page?.Next === false ? 'none' : '',
                            }}>
                            <i
                              className='uil uil-angle-right'
                              onClick={NextPage}
                            />
                          </button>
                        </div>
                      </div>{' '}
                      {/* end col*/}
                    </div>
                  </div>
                </div>
              </div>
              <div>
              {loader ? (
                      <div className='d-flex justify-content-center'>
                        <div className='spinner-border' role='status'>
                          <span className='sr-only'>Loading...</span>
                        </div>
                      </div>
                  ) : (
                      <Fade>
                <Table hover responsive className='mt-4'>
                    <tbody>
                      {customerView &&
                        customerView.map((value, i) => (
                          <tr key={i}>
                            <td>{i + 1}</td>
                            <td>
                              {value?.proposal_name}
                              <br />
                              {value?.vehicleDetails[0]?.make}
                              <br />
                              {value?.vehicleDetails[0]?.model}
                              <br />
                              {value?.vehicleDetails[0]?.vBody}
                              <br />
                              {value?.sale_kms}
                              <br />
                              {value?.vehicleDetails[0]?.status}
                              <br />
                              {value?.vehicleDetails[0]?.condition}
                              <br />
                              {value?.vehicleDetails[0]?.sold_by}
                            </td>
                            <td>
                              {moment(new Date(value.createdAt)).format(
                                'DD/MM/YYYY'
                              )}
                              ,{new Date(value.createdAt).toLocaleTimeString()}
                            </td>

                            <td>
                              <div className='button-list'>
                                <button
                                  onClick={()=>modalToggle(value)}
                                  className='btn btn-success  btn-sm Button_Size'>
                                  View{' '}
                                </button>
                              </div>
                            </td>
                          </tr>
                        ))}
                    </tbody>
                </Table>
                </Fade>
                )}
              </div>
            </div>
          </div>
        </div>{' '}
        {/* end Col */}
      </div>
      <Modal isOpen={modalOpen} toggle={modalToggle}>
        <ModalHeader toggle={modalToggle}>Proposal Details</ModalHeader>
        <ModalBody>
          <div className='row'>
             <div className='col-12'>
                    <Card>
                    <CardBody>
                    <h4>Proposal Details:</h4>
                    <div className='form-row'>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                        <strong>Vehicle: </strong>
                        </label>{' '}
                        <span>{modalView.vehicleDetails?modalView.vehicleDetails[0]?.vBody:null}</span>
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                        <strong>Sale KMS: </strong>
                        </label>{' '}
                        <span>{modalView?.sale_kms}</span>
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                          <strong>Status: </strong>
                        </label>{' '}
                        <span>{modalView?.status}</span>
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                        <strong>Condition: </strong>
                        </label>{' '}
                        <span>{modalView?.condition}</span>
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                          <strong>Sold By: </strong>
                        </label>{' '}
                        <span>{modalView?.sold_by}</span>
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                          <strong>Note: </strong>
                        </label>{' '}
                        <span>{modalView?.note}</span>
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                          <strong>Vehicle Image: </strong>
                        </label>{' '}
                        <span>{modalView.vehicleDetails?<img style={{maxWidth:150}} src={`${config.baseImgUrl}/vehicle/${modalView?.vehicleDetails[0]?.featuredImage}`} alt="image"/>:null}</span>
                      </div>
                      
                      </div>
                    </CardBody>
                  </Card>
                  </div>
             </div>
             <div className='row'>
             <div className='col-12'>
                    <Card>
                    <CardBody>
                    <h4>Invoice Details:</h4>
                    <div className='form-row'>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                        <strong>Invoice To: </strong>
                        </label>{' '}
                        <span>{modalView.customer?`${modalView.customer[0]?.firstName} ${modalView.customer[0]?.lastName}`:null}</span>
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                        <strong>Deliver To: </strong>
                        </label>{' '}
                        <span>{modalView?.deliver_to}</span>
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                          <strong>Proposal Name: </strong>
                        </label>{' '}
                        <span>{modalView?.proposal_name}</span>
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                        <strong>Transfer To: </strong>
                        </label>{' '}
                        <span>{modalView?.transfer_to}</span>
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                          <strong>Date Sold: </strong>
                        </label>{' '}
                        <span>{modalView?.date_sold}</span>
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                          <strong>Exp. Delivery: </strong>
                        </label>{' '}
                        <span>{modalView?.exp_delivery}</span>
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                          <strong>Yard : </strong>
                        </label>{' '}
                        <span>{modalView.yard}</span>
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                          <strong>How Sold : </strong>
                        </label>{' '}
                        <span>{modalView.how_sold}</span>
                      </div>
                      {
                        modalView?.Invoice?(
                          <div className='col-3 form-group'>
                        <label className='col-form-label'>
                          <strong>Invoice: </strong>
                        </label>{' '}
                        <a href={`${config.baseImgUrl}/pdf/${modalView?.Invoice}`} target="_blank" className="btn btn-xs btn-info" download>Download</a>
                      </div>
                        ):null
                      }
                      </div>
                    </CardBody>
                  </Card>
                  </div>
             </div>
             <div className='row'>
             <div className='col-12'>
                    <Card>
                    <CardBody>
                    <h4>Financial Requirements:</h4>
                    <div className='form-row'>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                        <strong>Requires Finance: </strong>
                        </label>{' '}
                        <span>{modalView?.requires_finance}</span>
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                        <strong>Finance Amount: </strong>
                        </label>{' '}
                        <span>{modalView?.finance_amount}</span>
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                          <strong>Finance Provide: </strong>
                        </label>{' '}
                        <span>{modalView?.finance_provider}</span>
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                        <strong>Term of Loan: </strong>
                        </label>{' '}
                        <span>{modalView?.term_of_loan}</span>
                      </div>
                      </div>
                    </CardBody>
                  </Card>
                  </div>
             </div>
             <div className='row'>
             <div className='col-12'>
                    <Card>
                    <CardBody>
                    <h4>Inspection Reports:</h4>
                    <div className='form-row'>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                        <strong>Inspection No: </strong>
                        </label>{' '}
                        <span>{modalView?.inspection_no}</span>
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                        <strong>Inspection Date: </strong>
                        </label>{' '}
                        <span>{modalView?.inspection_date}</span>
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                          <strong>Inspector Licence: </strong>
                        </label>{' '}
                        <span>{modalView?.inspector_licence}</span>
                      </div>
                      </div>
                    </CardBody>
                  </Card>
                  </div>
             </div>
        </ModalBody>
      </Modal>
    </>
  );
};

export default ProposalDetails;