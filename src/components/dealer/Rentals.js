/** @format */

import React, { useState, useEffect } from 'react';
import { Row, Col, Table, CardImg, Fade } from 'reactstrap';
import Instance from '../../Instance';
import moment from 'moment';
import {
  CardBody,
  Card,
  Modal,
  ModalHeader, ModalBody
} from 'reactstrap';
import config from '../../config';

const RentalDetails = ({dealerId,active}) => {
  const [rentalView, setRentalView] = useState(null);
  const [loader, setLoader] = useState(false);
  const [count, setCount] = useState(1);
  const [totalCount, settotalCount] = useState(0);
  const [page, setPage] = useState(null);

  useEffect(() => {
    setLoader(true);
    Instance.post(
      `/api/admin/dealer/rentals/${dealerId}`,
      {
        page: count,
      },
      {
        headers: { authorization: `Bearer ${localStorage.getItem('token$')}` },
      }
    ).then(({ data }) => {
      let temp = data?.allData?.rental;
      setPage(data?.allData);
      setRentalView(temp);
      setLoader(false);
      settotalCount(data?.allData?.totalrentaldb)
    }).catch((err) => {
      setLoader(false);
      console.log('err', err);
    });
  }, [count,active]);

  const NextPage = () => {
    setCount(count + 1);
  };
  const PrevPage = () => {
    setCount(count - 1);
  };

  const [modalOpen, setModalOpen] = useState(false);
  const [modalView, setModalView] = useState('');

  const modalToggle = (value) => {
    setModalOpen(!modalOpen);
    setModalView(value);
  };

  return (
    <>
      {/* <Row className='page-title align-items-center'>
        <Col sm={4} xl={6}>
          <h4 className='mb-1 mt-0'>Dealer Rental Details</h4>
        </Col>
      </Row> */}

      <div className='row'>
        <div className='col-12'>
          <div className='card'>
            <div className='p-3'>
              <div className='row'>
                <div className='col-6'>
                  <h4> All Rental </h4>
                </div>
                <div className='col-md-6 text-right'>
                  <div className='d-inline-block align-middle float-lg-right'>
                    <div className='row'>
                      <div
                        className='col-8 align-self-center'
                        style={{ whiteSpace: 'nowrap' }}>
                        Showing {page?.startOfItem} - {page?.endOfItem} of{' '}
                        {totalCount}
                      </div>
                      {/* end col*/}
                      <div className='col-4'>
                        <div className='btn-group float-right'>
                          <button
                            type='button'
                            className='btn btn-white btn-sm'
                            style={{
                              display: page?.Previous === false ? 'none' : '',
                            }}>
                            <i
                              className='uil uil-angle-left'
                              onClick={PrevPage}
                            />
                          </button>
                          <button
                            type='button'
                            className='btn btn-primary btn-sm'
                            style={{
                              display: page?.Next === false ? 'none' : '',
                            }}>
                            <i
                              className='uil uil-angle-right'
                              onClick={NextPage}
                            />
                          </button>
                        </div>
                      </div>{' '}
                      {/* end col*/}
                    </div>
                  </div>
                </div>
              </div>
              <div>
                {loader ? (
                  <>
                    <div className='d-flex justify-content-center'>
                      <div className='spinner-border' role='status'>
                        <span className='sr-only'>Loading...</span>
                      </div>
                    </div>{' '}
                  </>
                ) : (
                    <Fade>
                  <Table hover responsive className='mt-4'>
                    <tbody>
                      {rentalView &&
                        rentalView.map((value, i) => (
                          <tr key={i}>
                            <td>{i + 1}</td>
                            <td>
                              {value.firstName}
                              <br />
                              {value.lastName}
                              <br />
                              {value.email}
                              <br />
                              {value.country}
                              <br />
                              {value.address}
                            </td>
                            <td>
                              {moment(new Date(value.createdAt)).format(
                                'DD/MM/YYYY'
                              )}
                              ,{new Date(value.createdAt).toLocaleTimeString()}
                            </td>
                            <td>
                              <div className='button-list'>
                                <button
                                  onClick={() => modalToggle(value)}
                                  className='btn btn-success  btn-sm Button_Size'>
                                  View{' '}
                                </button>
                              </div>
                            </td>
                          </tr>
                        ))}
                    </tbody>

                  </Table>
                  </Fade>
                )}
              </div>
            </div>
          </div>
        </div>{' '}
        {/* end Col */}
      </div>
      <Modal isOpen={modalOpen} toggle={modalToggle} >
        <ModalHeader toggle={modalToggle}>Rental Details</ModalHeader>
        <ModalBody>
          <div className='row'>
            <div className='col-12'>
              <Card>
                <CardBody>
                  <h4>CheckIn Details:</h4>
                  <div className='form-row'>
                    <div className='col-3 form-group'>
                      <label className='col-form-label'>
                        <strong>First name: </strong>
                      </label>{' '}
                      <span>{modalView?.firstName}</span>
                    </div>
                    <div className='col-3 form-group'>
                      <label className='col-form-label'>
                        <strong>Last name: </strong>
                      </label>{' '}
                      <span>{modalView?.lastName}</span>
                    </div>
                    <div className='col-3 form-group'>
                      <label className='col-form-label'>
                        <strong>Email: </strong>
                      </label>{' '}
                      <span>{modalView?.email}</span>
                    </div>
                    <div className='col-3 form-group'>
                      <label className='col-form-label'>
                        <strong>Phone: </strong>
                      </label>{' '}
                      <span>{modalView?.phone}</span>
                    </div>
                    <div className='col-3 form-group'>
                      <label className='col-form-label'>
                        <strong>Make: </strong>
                      </label>{' '}
                      <span>{modalView?.carMake}</span>
                    </div>
                    <div className='col-3 form-group'>
                      <label className='col-form-label'>
                        <strong>Model: </strong>
                      </label>{' '}
                      <span>{modalView?.carModel}</span>
                    </div>
                    <div className='col-3 form-group'>
                      <label className='col-form-label'>
                        <strong>Body: </strong>
                      </label>{' '}
                      <span>{modalView?.carBody}</span>
                    </div>
                    <div className='col-3 form-group'>
                      <label className='col-form-label'>
                        <strong>Sart Date: </strong>
                      </label>{' '}
                      <span>{modalView?.startDate}</span>
                    </div>
                    <div className='col-3 form-group'>
                      <label className='col-form-label'>
                        <strong>End Date: </strong>
                      </label>{' '}
                      <span>{modalView?.endDate}</span>
                    </div>
                    <div className='col-3 form-group'>
                      <label className='col-form-label'>
                        <strong>Price : </strong>
                      </label>{' '}
                      <span>{modalView?.rentalPrice}</span>
                    </div>
                    <div className='col-3 form-group'>
                      <label className='col-form-label'>
                        <strong>Fuel : </strong>
                      </label>{' '}
                      <span>{modalView?.fuel}</span>
                    </div>
                    <div className='col-3 form-group'>
                      <label className='col-form-label'>
                        <strong>KM : </strong>
                      </label>{' '}
                      <span>{modalView?.km}</span>
                    </div>
                    <div className='col-3 form-group'>
                      <label className='col-form-label'>
                        <strong>Odo Meter : </strong>
                      </label>{' '}
                      <span>{modalView?.odoMeter}</span>
                    </div>
                  </div>
                </CardBody>
              </Card>
            </div>
          </div>
          <div className='row'>
            <div className='col-12'>
              <Card>
                <CardBody>
                  <h4>Description:</h4>
                  <div className='form-row'>
                    <div className='col-3 form-group'>
                      <span>{modalView?.description}</span>
                    </div>
                  </div>
                </CardBody>
              </Card>
            </div>
          </div>

          <div className='row'>
            <div className='col-12'>
              <Card>
                <CardBody>
                  <h4>Features:</h4>
                  <div className='form-row'>
                    <div className='col-3 form-group'>
                      <label className='col-form-label'>
                        <strong>Child Booster: </strong>
                      </label>{' '}
                      {(modalView?.childBooster) ? <span className="fa fa-check-circle" style={{ color: 'green' }}></span> : <span className="fa fa-times-circle" style={{ color: 'red' }}></span>}
                    </div>
                    <div className='col-3 form-group'>
                      <label className='col-form-label'>
                        <strong>Baby Seat: </strong>
                      </label>{' '}
                      {(modalView?.babySeat) ? <span className="fa fa-check-circle" style={{ color: 'green' }}></span> : <span className="fa fa-times-circle" style={{ color: 'red' }}></span>}
                    </div>
                    <div className='col-3 form-group'>
                      <label className='col-form-label'>
                        <strong>GPS Unit: </strong>
                      </label>{' '}
                      {(modalView?.gpsUnit) ? <span className="fa fa-check-circle" style={{ color: 'green' }}></span> : <span className="fa fa-times-circle" style={{ color: 'red' }}></span>}
                    </div>
                    <div className='col-3 form-group'>
                      <label className='col-form-label'>
                        <strong>Head Light: </strong>
                      </label>{' '}
                      {(modalView?.headLight) ? <span className="fa fa-check-circle" style={{ color: 'green' }}></span> : <span className="fa fa-times-circle" style={{ color: 'red' }}></span>}
                    </div>
                    <div className='col-3 form-group'>
                      <label className='col-form-label'>
                        <strong>Break Light: </strong>
                      </label>{' '}
                      {(modalView?.breakLight) ? <span className="fa fa-check-circle" style={{ color: 'green' }}></span> : <span className="fa fa-times-circle" style={{ color: 'red' }}></span>}
                    </div>
                    <div className='col-3 form-group'>
                      <label className='col-form-label'>
                        <strong>Spare Tyre: </strong>
                      </label>{' '}
                      {(modalView?.spareTyre) ? <span className="fa fa-check-circle" style={{ color: 'green' }}></span> : <span className="fa fa-times-circle" style={{ color: 'red' }}></span>}
                    </div>
                    <div className='col-3 form-group'>
                      <label className='col-form-label'>
                        <strong>Lighter: </strong>
                      </label>{' '}
                      {(modalView?.lighter) ? <span className="fa fa-check-circle" style={{ color: 'green' }}></span> : <span className="fa fa-times-circle" style={{ color: 'red' }}></span>}
                    </div>
                    <div className='col-3 form-group'>
                      <label className='col-form-label'>
                        <strong>Jack: </strong>
                      </label>{' '}
                      {(modalView?.jack) ? <span className="fa fa-check-circle" style={{ color: 'green' }}></span> : <span className="fa fa-times-circle" style={{ color: 'red' }}></span>}
                    </div>
                    <div className='col-3 form-group'>
                      <label className='col-form-label'>
                        <strong>Toolkit: </strong>
                      </label>{' '}
                      {(modalView?.toolkit) ? <span className="fa fa-check-circle" style={{ color: 'green' }}></span> : <span className="fa fa-times-circle" style={{ color: 'red' }}></span>}
                    </div>
                    <div className='col-3 form-group'>
                      <label className='col-form-label'>
                        <strong>Floor Mates: </strong>
                      </label>{' '}
                      {(modalView?.floorMates) ? <span className="fa fa-check-circle" style={{ color: 'green' }}></span> : <span className="fa fa-times-circle" style={{ color: 'red' }}></span>}
                    </div>
                    <div className='col-3 form-group'>
                      <label className='col-form-label'>
                        <strong>Wiper: </strong>
                      </label>{' '}
                      {(modalView?.wiper) ? <span className="fa fa-check-circle" style={{ color: 'green' }}></span> : <span className="fa fa-times-circle" style={{ color: 'red' }}></span>}
                    </div>
                    <div className='col-3 form-group'>
                      <label className='col-form-label'>
                        <strong>Ac: </strong>
                      </label>{' '}
                      {(modalView?.ac) ? <span className="fa fa-check-circle" style={{ color: 'green' }}></span> : <span className="fa fa-times-circle" style={{ color: 'red' }}></span>}
                    </div>
                  </div>
                </CardBody>
              </Card>
            </div>
          </div>

          <Card>
            <CardBody>
              <h4>Car Images:</h4>
              <Row>
                {
                  modalView.images && modalView.images.map((img) => (
                    <Col xs="4">
                      <Card>
                        <CardBody>
                          <CardImg width="100%" src={`${config.baseImgUrl}/uploads/rental/${img}`} alt="car image" />
                        </CardBody>
                      </Card>
                    </Col>
                  ))
                }
              </Row>
            </CardBody>
          </Card>

          <Card>
            <CardBody>
              <h4>Damage Car:</h4>
              <Row>
                <Col xs="6">
                  <Card>
                    <CardBody>
                      <label className="text-center">Damage Car Pic 1</label>
                      <CardImg width="100%" src={`${config.baseImgUrl}/rental/${modalView.damageCar1}`} alt="damageCar1" />
                    </CardBody>
                  </Card>
                </Col>
                <Col xs="6">
                  <Card>
                    <CardBody>
                      <label className="text-center">Damage Car Pic 2</label>
                      <CardImg width="100%" src={`${config.baseImgUrl}/uploads/rental/${modalView.damageCar2}`} alt="damageCar2" />
                    </CardBody>
                  </Card>
                </Col>
              </Row>
            </CardBody>
          </Card>

          <Card>
            <CardBody>
              <h4>Signature:</h4>
              <Row>
                <Col xs="6">
                  <Card>
                    <CardBody>
                      <label className="text-center">Customer Sign</label>
                      <CardImg width="100%" src={`${config.baseImgUrl}/rental/${modalView.authoritySign}`} alt="authoritySign" />
                    </CardBody>
                  </Card>
                </Col>
                <Col xs="6">
                  <Card>
                    <CardBody>
                      <label className="text-center">Authority Sign</label>
                      <CardImg width="100%" src={`${config.baseImgUrl}/rental/${modalView.customerSign}`} alt="customerSign" />
                    </CardBody>
                  </Card>
                </Col>
              </Row>
            </CardBody>
          </Card>

          <div className='row'>
            <div className='col-12'>
              <Card>
                <CardBody>
                  <h4>Check-Out Details:</h4>
                  <Card>
                    <CardBody>
                      <div className='form-row'>
                        <div className='col-3 form-group'>
                          <label className='col-form-label'>
                            <strong>Make: </strong>
                          </label>{' '}
                          <span>{modalView.checkouts && modalView.checkouts.carMake}</span>
                        </div>
                        <div className='col-3 form-group'>
                          <label className='col-form-label'>
                            <strong>Model: </strong>
                          </label>{' '}
                          <span>{modalView.checkouts && modalView.checkouts.carModel}</span>
                        </div>
                        <div className='col-3 form-group'>
                          <label className='col-form-label'>
                            <strong>Body: </strong>
                          </label>{' '}
                          <span>{modalView.checkouts && modalView.checkouts.carBody}</span>
                        </div>
                        <div className='col-3 form-group'>
                          <label className='col-form-label'>
                            <strong>Body: </strong>
                          </label>{' '}
                          <span>{modalView.checkouts && modalView.checkouts.carBody}</span>
                        </div>
                        <div className='col-3 form-group'>
                          <label className='col-form-label'>
                            <strong>Odo Meter: </strong>
                          </label>{' '}
                          <span>{modalView.checkouts && modalView.checkouts.odoMeter}</span>
                        </div>
                        <div className='col-3 form-group'>
                          <label className='col-form-label'>
                            <strong>End Date: </strong>
                          </label>{' '}
                          <span>{modalView.checkouts && modalView.checkouts.endDate}</span>
                        </div>
                        <div className='col-3 form-group'>
                          <label className='col-form-label'>
                            <strong>Fuel: </strong>
                          </label>{' '}
                          <span>{modalView.checkouts && modalView.checkouts.fuel}</span>
                        </div>
                        <div className='col-3 form-group'>
                          <label className='col-form-label'>
                            <strong>KM: </strong>
                          </label>{' '}
                          <span>{modalView.checkouts && modalView.checkouts.km}</span>
                        </div>
                      </div>
                    </CardBody>
                  </Card>

                  <div className='row'>
                    <div className='col-12'>
                      <Card>
                        <CardBody>
                          <h4>Description:</h4>
                          <div className='form-row'>
                            <div className='col-3 form-group'>
                              <span>{modalView.checkouts && modalView.checkouts.description}</span>
                            </div>
                          </div>
                        </CardBody>
                      </Card>
                    </div>
                  </div>

                  <Card>
                    <CardBody>
                      <h4>Car Images:</h4>
                      <Row>
                        {
                          modalView.checkouts && modalView.checkouts.images.map((img) => (
                            <Col xs="4">
                              <Card>
                                <CardBody>
                                  <CardImg width="100%" src={`${config.baseImgUrl}/rental/${img}`} alt="car image" />
                                </CardBody>
                              </Card>
                            </Col>
                          ))
                        }
                      </Row>
                    </CardBody>
                  </Card>

                  <Card>
                    <CardBody>
                      <h4>Damage Car:</h4>
                      <Row>
                        <Col xs="6">
                          <Card>
                            <CardBody>
                              <label className="text-center">Damage Car Pic 1</label>
                              <CardImg width="100%" src={`${config.baseImgUrl}/rental/${modalView.checkouts && modalView.checkouts.damageCar1}`} alt="damageCar1" />
                            </CardBody>
                          </Card>
                        </Col>
                        <Col xs="6">
                          <Card>
                            <CardBody>
                              <label className="text-center">Damage Car Pic 2</label>
                              <CardImg width="100%" src={`${config.baseImgUrl}/rental/${modalView.checkouts && modalView.checkouts.damageCar2}`} alt="damageCar2" />
                            </CardBody>
                          </Card>
                        </Col>
                      </Row>
                    </CardBody>
                  </Card>

                  <Card>
                    <CardBody>
                      <h4>Signature:</h4>
                      <Row>
                        <Col xs="6">
                          <Card>
                            <CardBody>
                              <label className="text-center">Customer Sign</label>
                              <CardImg width="100%" src={`${config.baseImgUrl}/rental/${modalView.checkouts && modalView.checkouts.authoritySign}`} alt="authoritySign" />
                            </CardBody>
                          </Card>
                        </Col>
                        <Col xs="6">
                          <Card>
                            <CardBody>
                              <label className="text-center">Authority Sign</label>
                              <CardImg width="100%" src={`${config.baseImgUrl}/uploads/rental/${modalView.checkouts && modalView.checkouts.customerSign}`} alt="customerSign" />
                            </CardBody>
                          </Card>
                        </Col>
                      </Row>
                    </CardBody>
                  </Card>

                </CardBody>
              </Card>
            </div>
          </div>
        </ModalBody>
      </Modal>
    </>
  );
};

export default RentalDetails;