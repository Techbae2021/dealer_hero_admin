import React from 'react'
import { Fade } from 'reactstrap'
import config from '../../config'

const DealerDetails = ({ dealerView,active }) => {
    return (
        <Fade in={active}>
        <div className="container card">
            <div className="row pt-3 pb-3">
                <div className="col-md-2 text-center">
                    <img src={`${config.baseImgUrl}/profile/${dealerView?.dealerDetails?.img}`} style={{ width: 100, height: 100, borderRadius: 100 }} alt="dealer image" />
                </div>
                <div className="row">
                <div className="col-md-12">
                        <div className="row">
                            <div className="col-md-3">
                                <label className='col-form-label'>
                                    {' '}
                                    <strong> First Name : </strong>
                                    {dealerView?.dealerDetails?.firstName}
                                </label>
                            </div>
                            <div className="col-md-3">
                                <label className='col-form-label'>
                                    {' '}
                                    <strong> Last Name : </strong>
                                    {dealerView?.dealerDetails?.lastName}
                                </label>
                            </div>
                            <div className="col-md-3">
                                <label className='col-form-label'>
                                    {' '}
                                    <strong> Phone No: </strong>
                                    {dealerView?.dealerDetails?.mobile}
                                </label>
                            </div>
                            <div className="col-md-3">
                                <label className='col-form-label'>
                                    {' '}
                                    <strong> Email : </strong>
                                    {dealerView?.dealerDetails?.email}
                                </label>
                            </div>
                        </div>
                        <div className="row">
                        {
                            dealerView?.dealerDetails?.websiteId?(
                                <div className="col-md-3">
                                <label className='col-form-label'>
                                    {'  '}
                                    <strong>WebsiteId : </strong>
                                    {dealerView?.dealerDetails?.websiteId}
                                </label>
                            </div>
                            ):null
                        }
                            
                            {dealerView?.dealerDetails?.websiteUrl?(
                                <div className="col-md-3">
                                <a href={dealerView?.dealerDetails?.websiteUrl} className="btn btn-primary btn-sm" target="_blank"> <span className="fa fa-globe"></span> View Website</a>

                            </div>
                            ):null}
                        </div>
                </div>
            </div>
             </div>
           
        </div>
        </Fade>
    )
}

export default DealerDetails
