/** @format */
import React, { useState, useEffect} from 'react';
import { Row, Col, Table, Fade} from 'reactstrap';
import Instance from '../../Instance';
import moment from 'moment';
import '../../App.css'
import config from '../../config'
import InventoryModal from './InventoryModal';

const Inventory = ({dealerId,active}) => {

  const [dealerView, setDealerView] = useState(null);
  const [loader, setLoader] = useState(false);
  const [count, setCount] = useState(1);
  const [totalCount, settotalCount] = useState(0);
  const [page, setPage] = useState(null);

  
  //Inventory
  const [InventoryOpen, setInventoryOpen] = useState(false);
  const [InventoryView, setInventoryView] = useState('');

  const InventoryToggle = (value) => {
    setInventoryOpen(!InventoryOpen);
    setInventoryView(value);
  };

  let images = ['instock','returned','intransit','transfers','sold','onoffer']
  useEffect(() => {
    setLoader(true);
    Instance.post(
      `/api/admin/dealer/inventory/instock/${dealerId}`,
      {
        page: count,
      },
      {
        headers: { authorization: `Bearer ${localStorage.getItem('token$')}` },
      }
    ).then(({ data }) => {
        // console.log('DealerData', data);
        let temp = data?.results?.VehicleDetails;
        setPage(data?.results);
        setDealerView(temp);
        setLoader(false);
        settotalCount(data?.results?.totalAllSalesdb);
      })
      .catch((err) => {
        console.log('err', err);
      });
  }, [count,active]);

  const NextPage = () => {
    setCount(count + 1);
  };
  const PrevPage = () => {
    setCount(count - 1);
  };

  return (

    <React.Fragment>
      {/* <Row className='page-title align-items-center'>
        <Col sm={4} xl={6}>
          <h4 className='mb-1 mt-0'>Dealer Inventory Details</h4>
        </Col>
      </Row> */}
      <div className='row'>
        <div className='col-12'>
            <div className='card p-3'>
              <div className='row'>
                <div className='col-6'>
                  <h4> All Inventory </h4>
                </div>
                <div className='col-md-6 text-right'>
                  <div className='d-inline-block align-middle float-lg-right'>
                    <div className='row'>
                      <div
                        className='col-8 align-self-center'
                        style={{ whiteSpace: 'nowrap' }}>
                        Showing {page?.startOfItem} - {page?.endOfItem} of{' '}
                        {totalCount}
                      </div>
                      {/* end col*/}
                      <div className='col-4'>
                        <div className='btn-group float-right'>
                          <button
                            type='button'
                            className='btn btn-white btn-sm'
                            style={{
                              display: page?.Previous === false ? 'none' : '',
                            }}>
                            <i
                              className='uil uil-angle-left'
                              onClick={PrevPage}
                            />
                          </button>
                          <button
                            type='button'
                            className='btn btn-primary btn-sm'
                            style={{
                              display: page?.Next === false ? 'none' : '',
                            }}>
                            <i
                              className='uil uil-angle-right'
                              onClick={NextPage}
                            />
                          </button>
                        </div>
                      </div>{' '}
                      {/* end col*/}
                    </div>
                  </div>
                </div>
              </div>
              <div>
              {loader ? (
                    <>
                      <div className='d-flex justify-content-center'>
                        <div className='spinner-border' role='status'>
                          <span className='sr-only'>Loading...</span>
                        </div>
                      </div>{' '}
                    </>
                  ) : (
                    <Fade in={active}>
                <Table hover responsive className='mt-4'>
                    <tbody>
                      {dealerView &&
                        dealerView.map((value, i) => (
                          <tr key={i}>
                            <td>{i + 1}</td>
                            <td>
                             <div className="container image__container">
                              <img src={`${config.baseImgUrl}/vehicle/${value?.vehicleimages[0]?.images[0]}`} alt="Vehicle Image" className="border border-success rounded" style={{height:200,width:300}}/>
                             {
                              images.find(status=>status===value?.status)?(
                              <div className="top-left offer__label"><img src={`/flags/${images.find(status=>status===value?.status)}.png`} alt="Vehicle Image"/></div>
                              ):null
                             }
                            </div>
                            </td>
                            <td>
                             <strong>StockId :</strong> {value?.stockId}
                              <br />
                              <strong>Make :</strong> {value?.make}
                              <br />
                              <strong>Model :</strong> {value?.model}
                              <br />
                              <strong>Vehicle Body :</strong> {value?.vBody}
                              <br />
                              <strong>Vehicle Vin :</strong> {value?.vin}
                              <br />
                              <strong>Registration No :</strong> {value?.registrationNo}
                              <br />
                              <strong>Built :</strong> {value?.built}
                              <br />
                              <strong>YardName :</strong> {value?.yard?value?.yard[0]?.yardName:null}
                              <br />
                              <strong>Kilometers :</strong> {value?.kms}
                              <br />
                              <strong>RegistrationExpiry :</strong> {value?.registrationExpiry}
                              <br />
                            </td>
                            <td>
                              {moment(new Date(value.createdAt)).format(
                                'DD/MM/YYYY'
                              )}
                              ,{new Date(value.createdAt).toLocaleTimeString()}
                            </td>
                            <td>
                              <div className='button-list'>
                                <button
                                  onClick={()=>InventoryToggle(value)}
                                  className='btn btn-success  btn-sm Button_Size'>
                                  View{' '}
                                </button>
                              </div>
                            </td>
                          </tr>
                        ))}
                    </tbody>
                </Table>
                </Fade>
                )}
              </div>
            </div>
                      
        </div>{' '}
        {/* end Col */}
        
      </div>
   
      <InventoryModal InventoryView={InventoryView} InventoryOpen={InventoryOpen} InventoryToggle={InventoryToggle}/>

      </React.Fragment>
   
  );
};

export default Inventory;