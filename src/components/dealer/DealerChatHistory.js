/** @format */

import React, { useContext, useEffect, useState } from 'react';
import {
  Card,
  CardBody,
  Row,
  Col,
  CardHeader,
  Modal,
  ModalHeader,
  ModalBody,
} from 'reactstrap';
import socketIOClient from 'socket.io-client';
import SocketIOFileClient from 'socket.io-file-client';
import PerfectScrollbar from 'react-perfect-scrollbar';
import 'react-perfect-scrollbar/dist/css/styles.css';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import Instance from './../../Instance';
import Context2 from '../../routes/Context';
import config from "../../config";
import moment from 'moment';
import { saveAs } from 'file-saver';

/* Chat Item Avatar */

/* Chat Item Text */
const chatItemDefaultProps = {
  placement: '',
  children: PropTypes.object,
  className: '',
};

const ChatItem = ({ children, placement, className }) => {
  return (
    <li
      className={classNames(
        'clearfix',
        { odd: placement === 'left' },
        className
      )}>
      {children}
    </li>
  );
};

ChatItem.defaultProps = chatItemDefaultProps;

/**
 * Renders the ChatHistory
 */

const ChatHistory = (props) => {
  const height = props.height || '320px';
  const { employee } = useContext(Context2);
  const [profileview, setProfileView] = useState(null);
  const [loader, setLoader] = useState(false);
  const [message, setMessage] = useState('');
  const ENDPOINT = 'http://localhost:5001';
  const [io, setIO] = React.useState('');
  const [fileIo, setFileIO] = React.useState('');
  const [messageShow, setMessageShow] = useState([]);
  const [modalOpen, setModalOpen] = useState(false);
  const [modalView, setModalView] = useState('');
 
  const [scrollEl, setScrollEl] = useState();
 
  const modalToggle = (value) => {
    setModalOpen(!modalOpen);
    setModalView(value);
  };

  const disconnectChat=()=>{
    if(io){
      io.disconnect()
    }
  }

  //Getall Dealers
  useEffect(()=>{
    LoadChats(props?.dealerId)
  },[props?.active])

  useEffect(()=>{
    scrollBottom()
  },[messageShow?.length])

  const onChatSocketChange = (currDealerId) => {
    disconnectChat()
    if (currDealerId && currDealerId!="") {
       const socket = socketIOClient(ENDPOINT)
      const uploader = new SocketIOFileClient(socket);
      setFileIO(uploader)
      setIO(socket)
      const { _id, role } = employee
      let obj = {
        sender: {
          senderId: _id,
          role: role,
        },
        dealerId: currDealerId,
        message: message,
        dateTime: moment().format("hh:mm a L")
      }
      //joinRoom2 is for admin dealer chat
      socket.emit('joinRoom2', obj)


      //message2 is for admin dealer chat
      // Message from server
      socket.on('message2', msg => {
        setMessageShow((oldArr) => [...oldArr, msg])
        // console.log('message2',msg)
        scrollBottom()
      });
    }
  }


  const LoadChats=(dealerId)=>{
    if(dealerId){
    setMessageShow([])
    setLoader(true)
    Instance.get(`/api/admin/chats/${dealerId}`,{
      headers: { authorization: `Bearer ${localStorage.getItem('token$')}` }
    }).then(async({ data }) => {
        // console.log('messegeData', data);
        setLoader(false)
        scrollBottom();
       await setMessageShow(data?.chats)
        onChatSocketChange(dealerId)
      })
      .catch((err) => {
        setLoader(false)
        console.log('err', err);
      });
    }
  }

  const scrollBottom=()=>{
    if (scrollEl) {
      scrollEl.scrollTop = scrollEl?.scrollHeight;
    }
 }



  useEffect(() => {
        setProfileView(employee?._id);
        // console.log("AdminId",employee?._id)
  }, []);

  const Click = (e) => {
    e.preventDefault();
    if (io) {
      const { _id, role } = employee
      const msg = {
        sender: {
          senderId: _id,
          role: role
        },
        message: message,
        dateTime: moment().format("hh:mm a L")
      }
      // console.log('Your message', msg)

      io.emit('sendMessage2', (msg))
    }
    setMessage('')
  }

  const fileHandler = () => {
    var fileEl = document.getElementById('Photo');
    if (fileIo) {
      fileIo.upload(fileEl, {
        data: {
          /* Arbitrary data... */
        },
      });
    }
  };

  const getFileExt=(filename)=>(filename.substring(filename.lastIndexOf('.')+1, filename.length) || filename+"").toLowerCase() 

  return (
    <>
     <Modal isOpen={modalOpen} toggle={modalToggle} centered={true} style={{display:'block',marginRight:'auto',marginLeft:'auto',width:800}}>
        <ModalHeader toggle={modalToggle}>Image View</ModalHeader>
        <ModalBody>
          <img src={`${config.baseImgUrl}/chatImages/${modalView}`} style={{width:1000}} className="img-fluid" /> 
        </ModalBody>
      </Modal>
      <Card>
      <CardHeader>

          <div className='row '>
            <div className='col-md-8 text-left'>
              <span className='mb-2 header-title'> <i className="fa fa-circle text-success"></i> Chat</span>
            </div>
          </div>
      </CardHeader>
        <CardBody className='pb-1'>
        
          <div className='chat-conversation'>
           
            <PerfectScrollbar
            style={{ maxHeight: height, width: '100%' ,marginBottom:'10px'}}
            id="listScroll"
            containerRef={ref => {
                setScrollEl(ref);
              }}
            >
            
              <ul className={classNames('conversation-list', props.className)}>
              
              {loader?<>
                      <div class='text-center' style={{position: 'absolute',bottom:0}}>
                        <div class='spinner-border' role='status'>
                          <span class='sr-only'>Loading...</span>
                        </div>{" "}
                        <span>Getting all messages...</span>
                      </div>{' '}
                    </>
                :
                messageShow &&
                messageShow.map((value, i) => (
                  <div key={i} className={value?.senderDetails?._id===profileview?'odd2':''}>
                  {
                    <div className='chat-avatar'>
                    {
                      value?.senderDetails.img?<img src={`${config.baseImgUrl}/profile/${value?.senderDetails?.img}`} alt="Dealer" />:
                      <img src={`${config.baseImgUrl}/SuperAdminProfile/${value?.senderDetails?.profile}`} alt="Admin" />
                    
                    }
                    </div>
                  }
                  <div className='conversation-text2 mt-2' style={{width:'94%'}}>
                    <div
                    className={value?.senderDetails?._id === profileview? 'ctext-wrap right': 'left ctext-wrap'}
                    >
                        <strong><p>{(value?.senderDetails?._id === profileview)?'You':value?.senderDetails?.firstName}</p></strong>
                        <p>{value?.message}</p>
                        {value?.file?<div>
                        {(["webp","jpg", "jpeg", "bmp", "gif", "png"].includes(getFileExt(value?.file)))?
                        <>
                        <img src={`${config.baseImgUrl}/chatImages/${value?.file}`} className="img-fluid"/>
                        <br/><br/>
                        <span onClick={async()=>await saveAs(`${config.baseImgUrl}/chatImages/${value?.file}`,value?.file)} className="btn btn-xs btn-success">Download </span>{" "}
                        <span onClick={()=>modalToggle(value?.file)} className="btn btn-xs btn-info">View</span>
                        </>
                        :<span onClick={async()=>await saveAs(`${config.baseImgUrl}/chatImages/${value?.file}`,value?.file)} className="btn btn-xs btn-success"> File Download  </span>}
                        </div>:null}
                        <span>{value.dateTime}</span>
                    </div>
                  </div>
                </div>
                ))}      
              </ul>
            
            </PerfectScrollbar>
          
            <form onSubmit={(e)=>e.preventDefault()}>
            <Row form>
            
              <Col>
                <input
                  className='form-control'
                  value={message}
                  placeholder="Type a message here..."
                  onChange={(e) => setMessage(e.target.value)}
                ></input>
              </Col>
              <Col className='col-auto'>
                <label htmlFor='Photo' style={{ fontSize: '25px' }}>
                  <i class='fa fa-plus-square mt-2' aria-hidden='true'></i>
                </label>

                <input
                  type='file'
                  id='Photo'
                  className='form-control'
                  onChange={fileHandler}
                  hidden
                  multiple></input>
              </Col>
              <Col className='col-auto'>
                <button
                  type='submit'
                  className='btn btn-danger chat-send btn-block'
                  onClick={Click}
                >
                  Send
                </button>
              </Col>
            </Row>
            </form>
          </div>
        
        </CardBody>
      </Card>
    </>
  );
};

export default ChatHistory;