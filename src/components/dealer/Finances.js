/** @format */

import React, { useState, useEffect } from 'react';
import { Row, Col, Table, CardImg, Fade } from 'reactstrap';
import Instance from '../../Instance';
import moment from 'moment';
import {
    CardBody,
    Card,
    Modal,
    ModalHeader, ModalBody
} from 'reactstrap';
import config from '../../config';

const FinanceDetails = ({ dealerId, active }) => {
    const [financeView, setFinanceView] = useState(null);
    const [loader, setLoader] = useState(false);
    const [count, setCount] = useState(1);
    const [totalCount, settotalCount] = useState(0);
    const [page, setPage] = useState(null);

    useEffect(() => {
        setLoader(true);
        Instance.post(
            `/api/admin/dealer/finances/${dealerId}`,
            {
                page: count,
            },
            {
                headers: { authorization: `Bearer ${localStorage.getItem('token$')}` },
            }
        )
            .then(({ data }) => {
                let temp = data?.results?.allData;
                setPage(data?.results);
                setFinanceView(temp);
                setLoader(false);
                settotalCount(data?.results?.totalDatadb);
            })
            .catch((err) => {
                setLoader(false);
                console.log('err', err);
            });
    }, [count, active]);

    const NextPage = () => {
        setCount(count + 1);
    };
    const PrevPage = () => {
        setCount(count - 1);
    };

    const [modalOpen, setModalOpen] = useState(false);
    const [modalView, setModalView] = useState('');

    const modalToggle = (value) => {
        setModalOpen(!modalOpen);
        setModalView(value);
    };

    return (
        <>
            {/* <Row className='page-title align-items-center'>
                <Col sm={4} xl={6}>
                    <h4 className='mb-1 mt-0'>Dealer Finance Details</h4>
                </Col>
            </Row> */}

            <div className='row'>
                <div className='col-12'>
                    <div className='card'>
                        <div className='p-3'>
                            <div className='row'>
                                <div className='col-6'>
                                    <h4> All Finance </h4>
                                </div>
                                <div className='col-md-6 text-right'>
                                    <div className='d-inline-block align-middle float-lg-right'>
                                        <div className='row'>
                                            <div
                                                className='col-8 align-self-center'
                                                style={{ whiteSpace: 'nowrap' }}>
                                                Showing {page?.startOfItem} - {page?.endOfItem} of{' '}
                                                {totalCount}
                                            </div>
                                            {/* end col*/}
                                            <div className='col-4'>
                                                <div className='btn-group float-right'>
                                                    <button
                                                        type='button'
                                                        className='btn btn-white btn-sm'
                                                        style={{
                                                            display: page?.Previous === false ? 'none' : '',
                                                        }}>
                                                        <i
                                                            className='uil uil-angle-left'
                                                            onClick={PrevPage}
                                                        />
                                                    </button>
                                                    <button
                                                        type='button'
                                                        className='btn btn-primary btn-sm'
                                                        style={{
                                                            display: page?.Next === false ? 'none' : '',
                                                        }}>
                                                        <i
                                                            className='uil uil-angle-right'
                                                            onClick={NextPage}
                                                        />
                                                    </button>
                                                </div>
                                            </div>{' '}
                                            {/* end col*/}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div>
                                {loader ? (
                                    <>
                                        <div class='d-flex justify-content-center'>
                                            <div class='spinner-border' role='status'>
                                                <span class='sr-only'>Loading...</span>
                                            </div>
                                        </div>{' '}
                                    </>
                                ) : (
                                    <Fade in={active}>
                                        <Table hover responsive className='mt-4'>
                                            <tbody>
                                                {financeView &&
                                                    financeView.map((value, i) => (
                                                        <tr key={i}>
                                                            <td>{i + 1}</td>
                                                            <td>
                                                                {value?.firstName}
                                                                <br />
                                                                {value?.lastName}
                                                                <br />
                                                                {value?.email}
                                                                <br />
                                                                {value?.phone}
                                                                <br />
                                                                {value?.address}
                                                            </td>
                                                            <td>
                                                                {moment(new Date(value.createdAt)).format(
                                                                    'DD/MM/YYYY'
                                                                )}
                                                                ,{new Date(value.createdAt).toLocaleTimeString()}
                                                            </td>

                                                            <td>
                                                                <div className='button-list'>
                                                                    <button
                                                                        onClick={() => modalToggle(value)}
                                                                        className='btn btn-success  btn-sm Button_Size'>
                                                                        View{' '}
                                                                    </button>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    ))}
                                            </tbody>
                                        </Table>
                                    </Fade>
                                )}
                            </div>
                        </div>
                    </div>
                </div>{' '}
                {/* end Col */}
            </div>
            <Modal isOpen={modalOpen} toggle={modalToggle} >
                <ModalHeader toggle={modalToggle}>Employee Details</ModalHeader>
                <ModalBody>
                    <Card>
                        <CardBody>
                            <div className='form-row'>
                                <div className='col-3 form-group'>
                                    <label className='col-form-label'>
                                        <strong>First Name: </strong>
                                    </label>{' '}
                                    <span>{modalView?.firstName}</span>
                                </div>
                                <div className='col-3 form-group'>
                                    <label className='col-form-label'>
                                        <strong>Last Name: </strong>
                                    </label>{' '}
                                    <span>{modalView?.lastName}</span>
                                </div>
                                <div className='col-3 form-group'>
                                    <label className='col-form-label'>
                                        <strong>Email: </strong>
                                    </label>{' '}
                                    <span>{modalView?.email}</span>
                                </div>
                                <div className='col-3 form-group'>
                                    <label className='col-form-label'>
                                        <strong>phone: </strong>
                                    </label>{' '}
                                    <span>{modalView?.phone}</span>
                                </div>
                                <div className='col-3 form-group'>
                                    <label className='col-form-label'>
                                        <strong>Address: </strong>
                                    </label>{' '}
                                    <span>{modalView?.address}</span>
                                </div>
                                <div className='col-3 form-group'>
                                    <label className='col-form-label'>
                                        <strong>City: </strong>
                                    </label>{' '}
                                    <span>{modalView?.city}</span>
                                </div>
                                <div className='col-3 form-group'>
                                    <label className='col-form-label'>
                                        <strong>State: </strong>
                                    </label>{' '}
                                    <span>{modalView?.state}</span>
                                </div>
                                <div className='col-3 form-group'>
                                    <label className='col-form-label'>
                                        <strong>Postal Code: </strong>
                                    </label>{' '}
                                    <span>{modalView?.postalCode}</span>
                                </div>
                                <div className='col-3 form-group'>
                                    <label className='col-form-label'>
                                        <strong>Marital Status: </strong>
                                    </label>{' '}
                                    <span>{modalView?.maratilStatus}</span>
                                </div>
                                <div className='col-3 form-group'>
                                    <label className='col-form-label'>
                                        <strong>Residency: </strong>
                                    </label>{' '}
                                    <span>{modalView?.residency}</span>
                                </div>
                                <div className='col-3 form-group'>
                                    <label className='col-form-label'>
                                        <strong>Depandents: </strong>
                                    </label>{' '}
                                    <span>{modalView?.depandents}</span>
                                </div>
                                <div className='col-3 form-group'>
                                    <label className='col-form-label'>
                                        <strong>LicenseNo: </strong>
                                    </label>{' '}
                                    <span>{modalView?.licenseNo}</span>
                                </div>
                                <div className='col-3 form-group'>
                                    <label className='col-form-label'>
                                        <strong>License Expiery: </strong>
                                    </label>{' '}
                                    <span>{modalView?.licenseExpiery}</span>
                                </div>
                                <div className='col-3 form-group'>
                                    <label className='col-form-label'>
                                        <strong>Applying: </strong>
                                    </label>{' '}
                                    <span>{modalView?.applying}</span>
                                </div>
                                <div className='col-3 form-group'>
                                    <label className='col-form-label'>
                                        <strong>Dob: </strong>
                                    </label>{' '}
                                    <span>{modalView?.dob}</span>
                                </div>
                                <div className='col-3 form-group'>
                                    <label className='col-form-label'>
                                        <strong>Proposal Price: </strong>
                                    </label>{' '}
                                    <span>{modalView?.proposalPrice}</span>
                                </div>
                                <div className='col-3 form-group'>
                                    <label className='col-form-label'>
                                        <strong>Deposit: </strong>
                                    </label>{' '}
                                    <span>{modalView?.deposit}</span>
                                </div>
                                <div className='col-3 form-group'>
                                    <label className='col-form-label'>
                                        <strong>LicenseNo: </strong>
                                    </label>{' '}
                                    <span>{modalView?.licenseNo}</span>
                                </div>
                                <div className='col-3 form-group'>
                                    <label className='col-form-label'>
                                        <strong>Finance Amount: </strong>
                                    </label>{' '}
                                    <span>{modalView?.financeAmount}</span>
                                </div>
                            </div>
                        </CardBody>
                    </Card>
                    <div className='row'>
                        <div className='col-12'>
                            <Card>
                                <CardBody>
                                    <h4>Finance Income:</h4>
                                    <div className='form-row'>
                                        <div className='col-3 form-group'>
                                            <label className='col-form-label'>
                                                <strong>Work: </strong>
                                            </label>{' '}
                                            <span>{modalView?.financeIncomeDetails && modalView?.financeIncomeDetails[0]?.work}</span>
                                        </div>
                                        <div className='col-3 form-group'>
                                            <label className='col-form-label'>
                                                <strong>Employee FirstName: </strong>
                                            </label>{' '}
                                            <span>{modalView?.financeIncomeDetails && modalView?.financeIncomeDetails[0]?.empFirstName}</span>
                                        </div>
                                        <div className='col-3 form-group'>
                                            <label className='col-form-label'>
                                                <strong>Employee LastName: </strong>
                                            </label>{' '}
                                            <span>{modalView?.financeIncomeDetails && modalView?.financeIncomeDetails[0]?.empLastName}</span>
                                        </div>
                                        <div className='col-3 form-group'>
                                            <label className='col-form-label'>
                                                <strong>Employee PhoneNo: </strong>
                                            </label>{' '}
                                            <span>{modalView?.financeIncomeDetails && modalView?.financeIncomeDetails[0]?.empPhone}</span>
                                        </div>
                                        <div className='col-3 form-group'>
                                            <label className='col-form-label'>
                                                <strong>Employee Year: </strong>
                                            </label>{' '}
                                            <span>{modalView?.financeIncomeDetails && modalView?.financeIncomeDetails[0]?.empYear}</span>
                                        </div>
                                        <div className='col-3 form-group'>
                                            <label className='col-form-label'>
                                                <strong>Employee Occupation: </strong>
                                            </label>{' '}
                                            <span>{modalView?.financeIncomeDetails && modalView?.financeIncomeDetails[0]?.empOccupation}</span>
                                        </div>
                                        <div className='col-3 form-group'>
                                            <label className='col-form-label'>
                                                <strong>SalaryType: </strong>
                                            </label>{' '}
                                            <span>{modalView?.financeIncomeDetails && modalView?.financeIncomeDetails[0]?.salaryType}</span>
                                        </div>
                                        <div className='col-3 form-group'>
                                            <label className='col-form-label'>
                                                <strong>IncomeType: </strong>
                                            </label>{' '}
                                            <span>{modalView?.financeIncomeDetails && modalView?.financeIncomeDetails[0]?.incomeType}</span>
                                        </div>
                                        <div className='col-3 form-group'>
                                            <label className='col-form-label'>
                                                <strong>IncomeBeforeTax: </strong>
                                            </label>{' '}
                                            <span>{modalView?.financeIncomeDetails && modalView?.financeIncomeDetails[0]?.incomeBeforeTax}</span>
                                        </div>
                                        <div className='col-3 form-group'>
                                            <label className='col-form-label'>
                                                <strong>IncomeAmount: </strong>
                                            </label>{' '}
                                            <span>{modalView?.financeIncomeDetails && modalView?.financeIncomeDetails[0]?.incomeAmount}</span>
                                        </div>
                                        <div className='col-3 form-group'>
                                            <label className='col-form-label'>
                                                <strong>CreditCard: </strong>
                                            </label>{' '}
                                            <span>{modalView?.financeIncomeDetails && modalView?.financeIncomeDetails[0]?.creditCard}</span>
                                        </div>
                                        <div className='col-3 form-group'>
                                            <label className='col-form-label'>
                                                <strong>ExpanseType: </strong>
                                            </label>{' '}
                                            <span>{modalView?.financeIncomeDetails && modalView?.financeIncomeDetails[0]?.expanseType}</span>
                                        </div>
                                        <div className='col-3 form-group'>
                                            <label className='col-form-label'>
                                                <strong>ExpanseBeforeTax: </strong>
                                            </label>{' '}
                                            <span>{modalView?.financeIncomeDetails && modalView?.financeIncomeDetails[0]?.expanseBeforeTax}</span>
                                        </div>
                                        <div className='col-3 form-group'>
                                            <label className='col-form-label'>
                                                <strong>Expanse: </strong>
                                            </label>{' '}
                                            <span>{modalView?.financeIncomeDetails && modalView?.financeIncomeDetails[0]?.expanse}</span>
                                        </div>
                                        <div className='col-3 form-group'>
                                            <label className='col-form-label'>
                                                <strong>Comment: </strong>
                                            </label>{' '}
                                            <span>{modalView?.financeIncomeDetails && modalView?.financeIncomeDetails[0]?.comment}</span>
                                        </div>
                                    </div>
                                </CardBody>
                            </Card>
                        </div>
                    </div>

                    <div className='row'>
                        <div className='col-12'>
                            <Card>
                                <CardBody>
                                    <h4>Finance Declarations:</h4>
                                    <Row>
                                        <Col xs="4">
                                            <Card>
                                                <CardBody>
                                                    <CardImg width="100%" src={`${config.baseImgUrl}/finance/${modalView?.financeDeclarationDetails && modalView?.financeDeclarationDetails[0]?.licenseFront}`} alt="licenseFront" />
                                                </CardBody>
                                            </Card>
                                        </Col>
                                        <Col xs="4">
                                            <Card>
                                                <CardBody>
                                                    <CardImg width="100%" src={`${config.baseImgUrl}/finance/${modalView?.financeDeclarationDetails && modalView?.financeDeclarationDetails[0]?.licenseBack}`} alt="licenseBack" />
                                                </CardBody>
                                            </Card>
                                        </Col>
                                        <Col xs="4">
                                            <Card>
                                                <CardBody>
                                                    <CardImg width="100%" src={`${config.baseImgUrl}/finance/${modalView?.financeDeclarationDetails && modalView?.financeDeclarationDetails[0]?.passport}`} alt="passport" />
                                                </CardBody>
                                            </Card>
                                        </Col>
                                        <Col xs="4">
                                            <Card>
                                                <CardBody>
                                                    <CardImg width="100%" src={`${config.baseImgUrl}/finance/${modalView?.financeDeclarationDetails && modalView?.financeDeclarationDetails[0]?.medicare}`} alt="medicare" />
                                                </CardBody>
                                            </Card>
                                        </Col>
                                        <Col xs="4">
                                            <Card>
                                                <CardBody>
                                                    <CardImg width="100%" src={`${config.baseImgUrl}/finance/${modalView?.financeDeclarationDetails && modalView?.financeDeclarationDetails[0]?.bankStatement}`} alt="bankStatement" />
                                                </CardBody>
                                            </Card>
                                        </Col>
                                        <Col xs="4">
                                            <Card>
                                                <CardBody>
                                                    <CardImg width="100%" src={`${config.baseImgUrl}/finance/${modalView?.financeDeclarationDetails && modalView?.financeDeclarationDetails[0]?.otherImage}`} alt="otherImage" />
                                                </CardBody>
                                            </Card>
                                        </Col>
                                    </Row>
                                </CardBody>
                            </Card>
                        </div>
                    </div>
                </ModalBody>
            </Modal>
        </>
    );
};

export default FinanceDetails;