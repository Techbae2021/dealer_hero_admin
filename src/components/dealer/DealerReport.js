import moment from 'moment'
import React, { useState } from 'react'
import { Fade, Table } from 'reactstrap'
import config from '../../config'
import Instance from '../../Instance'

const DealerReport = ({ dealerId, active }) => {

    const [report, setReport] = useState([])
    const [loader, setLoader] = useState(false)
    const [dateBW, setDateBW] = useState({ startDate: '', endDate: '' })
    const [show, setShow] = useState(false)

    const handleDateChange = (e) => {
        setDateBW({ ...dateBW, [e.target.name]: e.target.value })
    }

    const onGenReport = (e) => {
        e.preventDefault();
        setShow(false)
        if (dateBW.startDate && dateBW.endDate) {
            setLoader(true)
            Instance.post(`/api/admin/dealer/genreport/${dealerId}`, dateBW, {
                headers: { authorization: `Bearer ${localStorage.getItem('token$')}` },
            }).then(({ data }) => {
                setShow(true)
                setLoader(false)
                if (data?.success && (data?.result && data?.result.length > 0)) {
                    setReport(data?.result)
                    // console.log({ data })
                } else {
                    setReport([])
                }
            }).catch((e) => {
                setShow(false)
                setLoader(false)
                console.log(e)
            })
        }
    }

    return (
        <Fade in={active}>
            <div className="container-fluid">
                <div className="row card">
                    <div className="col-md-12">
                        <h5>Generate Report</h5>
                        <form className="form-row" onSubmit={onGenReport}>
                            <div className="form-group col-md-6">
                                <input type="date" name="startDate" onChange={handleDateChange} value={dateBW?.startDate} className="form-control" />
                            </div>
                            {' '}
                            <div className="form-group col-md-6">
                                <input type="date" name="endDate" onChange={handleDateChange} value={dateBW?.endDate} className="form-control" />
                            </div>
                            {' '}
                            <div className='justify-content-center col-md-5 pb-3' style={{ display: 'block', margin: 'auto' }}>
                                <input type="submit" className="btn btn-primary btn-block" value="Generate" />
                            </div>
                        </form>
                    </div>
                </div>
                {
                    loader ? (
                        <div className="container">
                            <div className="row card">
                                <div className='d-flex justify-content-center align-items-center'>
                                    <div className='spinner-border' role='status'>
                                        <span className='sr-only'>Loading...</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    ) : (
                        show && (
                            <div className="row card py-3 px-2">
                                <h5>Dealer Report ({dateBW?.startDate} - {dateBW?.endDate})</h5>
                                <span>Total Result: {report.length}</span>
                                {report.length > 0 ? (
                                    <Table hover responsive>
                                        <thead>
                                            <tr>
                                                <th># StockId</th>
                                                <th>Image</th>
                                                <th>Make</th>
                                                <th>Model</th>
                                                <th>Body</th>
                                                <th>Transmission</th>
                                                <th>Fuel Type</th>
                                                <th>Year</th>
                                                <th>Advertising Price ($)</th>
                                                <th>Compiled</th>
                                                <th>Condition</th>
                                                <th>Condition Rating</th>
                                                <th>Country Of Manufacture</th>
                                                <th>Doors</th>
                                                <th>Drive Train</th>
                                                <th>Drive Way</th>
                                                <th>Engine</th>
                                                <th>Engine Capicity</th>
                                                <th>Engine Size</th>
                                                <th>Extended Color</th>
                                                <th>PDF Form</th>
                                                <th>PDF Form4</th>
                                                <th>Form Type</th>
                                                <th>Fuel Economy</th>
                                                <th>Fuel Savear Rating</th>
                                                <th>Glass Code</th>
                                                <th>IntColor</th>
                                                <th>Keyboard</th>
                                                <th>KMs</th>
                                                <th>Min Price</th>
                                                <th>Previous Registration</th>
                                                <th>Previous Price</th>
                                                <th>Redbook Code</th>
                                                <th>Registration No.</th>
                                                <th>Registration Seriel</th>
                                                <th>Registration State</th>
                                                <th>Safety List</th>
                                                <th>Safety Rating</th>
                                                <th>Seats</th>
                                                <th>Service History</th>
                                                <th>Status</th>
                                                <th>Supplier Stock</th>
                                                <th>Varient Series</th>
                                                <th>Youtube Url</th>
                                                <th>Acquisition Amount ($)</th>
                                                <th>Acquisition Date Acquired</th>
                                                <th>Acquisition Date Canceled</th>
                                                <th>Acquisition Date Of Issue</th>
                                                <th>Acquisition Driver Lic</th>
                                                <th>Acquisition Finance Provider</th>
                                                <th>Acquisition How Acquired</th>

                                                <th>Acquisition Inspector Licence</th>
                                                <th>Acquisition KMs Wrong By</th>
                                                <th>Acquisition KMs at Time of Purchase</th>
                                                <th>Acquisition load</th>
                                                <th>Acquisition Manufactur Waranty exp.</th>
                                                <th>Acquisition Modified Or Repired</th>
                                                <th>Acquisition PPSR</th>
                                                <th>Acquisition Purchase By</th>
                                                <th>Acquisition Purchase Price</th>
                                                <th>Acquisition Report Number</th>
                                                <th>Acquisition Address</th>
                                                <th>Acquisition Advertising Source</th>
                                                <th>Acquisition CIty</th>
                                                <th>Acquisition Country</th>
                                                <th>Acquisition Dealers Lic</th>
                                                <th>Acquisition Dob</th>
                                                <th>Acquisition First Name</th>
                                                <th>Acquisition Last Name</th>
                                                <th>Acquisition Mobile no.</th>
                                                <th>Acquisition State</th>
                                                <th>Acquisition Work Mobile no.</th>
                                                <th>Acquisition ABN.</th>
                                                <th>Acquisition Email.</th>
                                                <th>Acquisition PostCode</th>
                                                <th>Acquisition Statutory</th>
                                                <th>Acquisition TradedOnStock</th>
                                                <th>Acquisition Water Exposure</th>
                                                <th>Acquisition Written Off</th>
                                                <th>Instock Date/Time</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {report.map((r, i) => (
                                                <tr key={i}>
                                                    <td>{r?.stockId}</td>
                                                    <td><img src={`${config.baseImgUrl}/vehicle/${r?.featuredImage}`} style={{ width: 50, height: 50 }} /></td>
                                                    <td>{r?.make}</td>
                                                    <td>{r?.model}</td>
                                                    <td>{r?.vBody}</td>
                                                    <td>{r?.trasnmission}</td>
                                                    <td>{r?.fuelType}</td>
                                                    <td>{r?.yearofFirstreg}</td>
                                                    <td>${r?.advPrice}</td>
                                                    <td>{r?.compiled}</td>
                                                    <td>{r?.condition}</td>
                                                    <td>{r?.conditionRating}</td>
                                                    <td>{r?.countryofmanufacture}</td>
                                                    <td>{r?.doors}</td>
                                                    <td>{r?.driveTrain}</td>
                                                    <td>{r?.driveWay ? 'Yes' : 'No'}</td>
                                                    <td>{r?.engine}</td>
                                                    <td>{r?.engineCapicity}</td>
                                                    <td>{r?.engineSize}</td>
                                                    <td>{r?.extendedcolor}</td>
                                                    <td><a href={`${config.baseImgUrl}/pdf/${r?.form}`} target="_blank" className="btn btn-xs btn-info" download>Download</a></td>
                                                    <td>{r?.form4 ? <a href={`${config.baseImgUrl}/pdf/${r?.form4}`} target="_blank" className="btn btn-xs btn-info" download>Download</a> : '---'}</td>
                                                    <td>{r?.fromType}</td>
                                                    <td>{r?.fuelEconomy}</td>
                                                    <td>{r?.fuelsavearRating}</td>
                                                    <td>{r?.glasscode}</td>
                                                    <td>{r?.intColor}</td>
                                                    <td>{r?.keyboard}</td>
                                                    <td>{r?.kms}</td>
                                                    <td>{r?.minPrice}</td>
                                                    <td>{r?.prevRegistration}</td>
                                                    <td>{r?.previousPrice}</td>
                                                    <td>{r?.redbookCode}</td>
                                                    <td>{r?.registrationNo}</td>
                                                    <td>{r?.registrationSeriel}</td>
                                                    <td>{r?.registrationState}</td>
                                                    <td>{r?.safetyList}</td>
                                                    <td>{r?.safetyRating}</td>
                                                    <td>{r?.seats}</td>
                                                    <td>{r?.serviceHistory}</td>
                                                    <td>{r?.status}</td>
                                                    <td>{r?.supplierStock}</td>
                                                    <td>{r?.varientSeries}</td>
                                                    <td> <a href={r?.youtubeUrl}>Link</a> </td>
                                                    <td>${r?.acquisitionDetails?.amount}</td>
                                                    <td>{r?.acquisitionDetails?.dateAcquired}</td>
                                                    <td>{r?.acquisitionDetails?.dateResoCanceled}</td>
                                                    <td>{r?.acquisitionDetails?.dateofIssue}</td>
                                                    <td>{r?.acquisitionDetails?.driverLic}</td>
                                                    <td>{r?.acquisitionDetails?.financeProvider}</td>
                                                    <td>{r?.acquisitionDetails?.howAcquired}</td>
                                                    <td>{r?.acquisitionDetails?.inspectorLicence}</td>
                                                    <td>{r?.acquisitionDetails?.kMSWrongBy}</td>
                                                    <td>{r?.acquisitionDetails?.kmsatTimeofPurchase}</td>
                                                    <td>{r?.acquisitionDetails?.load}</td>
                                                    <td>{r?.acquisitionDetails?.manufacturWarantyexp}</td>
                                                    <td>{r?.acquisitionDetails?.modifiedOrRepired ? 'Yes' : 'No'}</td>
                                                    <td>{r?.acquisitionDetails?.ppsr}</td>
                                                    <td>{r?.acquisitionDetails?.purchaseBy}</td>
                                                    <td>${r?.acquisitionDetails?.purchasePrice}</td>
                                                    <td>{r?.acquisitionDetails?.reportNumber}</td>
                                                    <td>{r?.acquisitionDetails?.sAddress}</td>
                                                    <td>{r?.acquisitionDetails?.sAdvertisingsource}</td>
                                                    <td>{r?.acquisitionDetails?.sCIty}</td>
                                                    <td>{r?.acquisitionDetails?.sCountry}</td>
                                                    <td>{r?.acquisitionDetails?.sDealersLic}</td>
                                                    <td>{r?.acquisitionDetails?.sDob}</td>
                                                    <td>{r?.acquisitionDetails?.sFname}</td>
                                                    <td>{r?.acquisitionDetails?.sLname}</td>
                                                    <td>{r?.acquisitionDetails?.sMobile}</td>
                                                    <td>{r?.acquisitionDetails?.sState}</td>
                                                    <td>{r?.acquisitionDetails?.sWorkmobile}</td>
                                                    <td>{r?.acquisitionDetails?.sabn}</td>
                                                    <td>{r?.acquisitionDetails?.semail}</td>
                                                    
                                                    <td>{r?.acquisitionDetails?.spostcode}</td>
                                                    <td>{r?.acquisitionDetails?.statutory?"Yes":"No"}</td>
                                                    <td>{r?.acquisitionDetails?.tradedonStock}</td>
                                                    <td>{r?.acquisitionDetails?.waterExposure?"Yes":"No"}</td>
                                                    <td>{r?.acquisitionDetails?.writtenOff?"Yes":"No"}</td>
                                                    <td>{moment(new Date(r.createdAt)).format('DD/MM/YYYY')},{new Date(r.createdAt).toLocaleTimeString()}</td>
                                                </tr>
                                            ))}
                                        </tbody>
                                    </Table>
                                ) : <p>No data found!</p>}
                            </div>
                        )
                    )
                }
            </div>
        </Fade>
    )
}

export default DealerReport
