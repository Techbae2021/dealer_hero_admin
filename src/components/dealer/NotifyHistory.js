/** @format */

import React, { useState, useEffect } from 'react';
import { Col, Row, Table } from 'reactstrap';
import Instance from '../../Instance';
import moment from 'moment';
import { Link } from 'react-router-dom';
import { useHistory  } from 'react-router-dom';


const Notification = ({dealerId,active}) => {
  const [notificationView, setNotificationView] = useState(null);
  const [loader, setLoader] = useState(false);
  const [count, setCount] = useState(1);
  const [page, setPage] = useState(null);
  let history = useHistory();
  
  useEffect(() => {
    setLoader(true);
    Instance.post(`/api/admin/notifications/dealer/${dealerId}`,{page: count},
      {
        headers: { authorization: `Bearer ${localStorage.getItem('token$')}` },
      }
    ).then(({ data }) => {
    //   console.log('iData', data);
      let temp = data?.results?.Notifications;
      setNotificationView(temp);
      setPage(data?.results);
      setLoader(false);
    }).catch((err) => {
        setLoader(false);
        console.log('err', err);
      });
  }, [count,active]);


  const NextPage = () => {
    setCount(count + 1);
  };
  const PrevPage = () => {
    setCount(count - 1);
  };

  const handleNotify=(value)=>{
    // Instance.put(`/api/admin/notification/status/${value._id}`,{},{
    //   headers: { authorization: `Bearer ${localStorage.getItem('token$')}` },
    // }).then(()=>{
    //   // console.log('okoko')
    //   history.push(`${value.webRedirectUrl}`)
    // }).catch((e)=>console.log(e))
  }

  return (
    <>
      {/* <Row className='page-title align-items-center'>
        <Col sm={4} xl={6}>
          <h4 className='mb-1 mt-0'>Notification</h4>
        </Col>
      </Row> */}

      <div className='row'>
        {/* <div className='col-2 Lead_Board'>
          <div className='email-container bg-transparent'>
            {/* Left sidebar */}
        {/* <LeftMenu /> */}
        {/* End Left sidebar */}
        {/* </div>
        </div> */}
        <div className='col-12'>
          <div className='card'>
            <div className='p-3'>
              <div className='row'>
                <div className='col-6'>
                  <h4> All </h4>
                </div>
                <div className='col-8 align-self-center'>
                    Showing {page?.startOfItem} - {page?.endOfItem} of{' '}
                    {page?.totalNotificationsdb}
                  </div>
                  <div className='col-4'>
                    <div className='btn-group float-right'>
                      <button
                        type='button'
                        className='btn btn-white btn-sm'
                        onClick={PrevPage}
                        style={{
                          display: page?.Previous === false ? 'none' : '',
                        }}>
                        <i className='uil uil-angle-left'  />
                      </button>
                      <button
                        type='button'
                        onClick={NextPage} 
                        className='btn btn-primary btn-sm'
                        style={{ display: page?.Next === false ? 'none' : '' }}>
                        <i className='uil uil-angle-right'/>
                      </button>
                    </div>
                  </div>{' '}
                {/* <div className='col-md-6 text-right'>
                  <div className='d-inline-block align-middle float-lg-right'>
                    <div className='row'>
                      <div
                        className='col-8 align-self-center'
                        style={{ whiteSpace: 'nowrap' }}>
                        Showing {page?.startOfItem} - {page?.endOfItem} of{' '}
                        {page?.totalAllSalesdb}
                      </div>
               
                      <div className='col-4'>
                        <div className='btn-group float-right'>
                          <button
                            type='button'
                            className='btn btn-white btn-sm'
                            title='Previous Page'
                            style={{
                              display: page?.Previous === false ? 'none' : '',
                            }}>
                            <i
                              className='uil uil-angle-left'
                              onClick={PrevPage}
                            />
                          </button>
                          <button
                            type='button'
                            className='btn btn-primary btn-sm'
                            title='Previous Page'
                            style={{
                              display: page?.Next === false ? 'none' : '',
                            }}>
                            <i
                              className='uil uil-angle-right'
                              onClick={NextPage}
                            />
                          </button>
                        </div>
                      </div>{' '}
                
                    </div>
                  </div>
                </div> */}
              </div>

              <div>
                {loader ? (
                  <>
                    <div className='d-flex justify-content-center'>
                      <div className='spinner-border' role='status'>
                        <span className='sr-only'>Loading...</span>
                      </div>
                    </div>{' '}
                  </>
                ) : (
                  <>
                    <Table hover responsive className='mt-4'>
                      <tbody>
                        {notificationView &&
                          notificationView.map((value, i) => (
                            <tr key={i}>
                              <td>{i + 1}</td>
                              <td className='ml-4'>
                                  <>
                                    {value?.title}
                                    <br />
                                    {value?.message}
                                  </>
                              </td>
                              <td>
                                {moment(new Date(value.createdAt)).format(
                                  'DD/MM/YYYY'
                                )}
                                ,
                                {new Date(value.createdAt).toLocaleTimeString()}
                              </td>
                            </tr>
                          ))}
                      </tbody>
                    </Table>
                  </>
                )}
              </div>
            </div>
          </div>
        </div>{' '}
        {/* end Col */}
      </div>
    </>
  );
};

export default Notification;
