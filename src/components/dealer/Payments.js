/** @format */

import React, { useState, useEffect } from 'react';
import { Row, Col, Table, Fade } from 'reactstrap';
import Instance from '../../Instance';
import moment from 'moment';
import {
  CardBody,
  Card,
  Modal,
  ModalHeader,ModalBody} from 'reactstrap';
import config from '../../config';

const Payments = ({dealerId,active}) => {
  const [PaymentsView, setPaymentsView] = useState([{
      _id:"6565456446546545450",
      payment_name:"Akshay"
  }]);
  const [loader, setLoader] = useState(false);
  const [count, setCount] = useState(1);
//   const [totalCount, settotalCount] = useState(0);
//   const [page, setPage] = useState(null);

//   useEffect(() => {
//     setLoader(true);
//     Instance.post(
//       `/api/admin/dealer/proposals/${dealerId}`,
//       {
//         page: count,
//       },
//       {
//         headers: { authorization: `Bearer ${localStorage.getItem('token$')}` },
//       }
//     )
//       .then(({ data }) => {
//         let temp = data?.results?.proposals;
//         console.log('proposals',temp)
//         setPage(data?.results);
//         setPaymentsView(temp);
//         setLoader(false);
//         settotalCount(data?.results?.totalproposaldb);
//       })
//       .catch((err) => {
//         setLoader(false);
//         console.log('err', err);
//       });
//   }, [count,active]);

//   const NextPage = () => {
//     setCount(count + 1);
//   };
//   const PrevPage = () => {
//     setCount(count - 1);
//   };

  const [modalOpen, setModalOpen] = useState(false);
  const [modalView, setModalView] = useState('');

  const modalToggle = (value) => {
    setModalOpen(!modalOpen);
    setModalView(value);
  };

  return (
    <>
      {/* <Row className='page-title align-items-center'>
        <Col sm={4} xl={6}>
          <h4 className='mb-1 mt-0'>Dealer Proposal Details</h4>
        </Col>
      </Row> */}

      <div className='row'>
        <div className='col-12'>
          <div className='card'>
            <div className='p-3'>
              <div className='row'>
                <div className='col-6'>
                  <h4> All Payments </h4>
                </div>
                <div className='col-md-6 text-right'>
                  <div className='d-inline-block align-middle float-lg-right'>
                    <div className='row'>
                      <div
                        className='col-8 align-self-center'
                        style={{ whiteSpace: 'nowrap' }}>
                        Showing 0 - 0 of{' '}0
                        {/* Showing {page?.startOfItem} - {page?.endOfItem} of{' '} */}
                        {/* {totalCount} */}
                      </div>
                      {/* end col*/}
                      <div className='col-4'>
                        <div className='btn-group float-right'>
                          {/* <button
                            type='button'
                            className='btn btn-white btn-sm'
                            style={{
                              display: page?.Previous === false ? 'none' : '',
                            }}>
                            <i
                              className='uil uil-angle-left'
                              onClick={PrevPage}
                            />
                          </button> */}
                          {/* <button
                            type='button'
                            className='btn btn-primary btn-sm'
                            style={{
                              display: page?.Next === false ? 'none' : '',
                            }}>
                            <i
                              className='uil uil-angle-right'
                              onClick={NextPage}
                            />
                          </button> */}
                        </div>
                      </div>{' '}
                      {/* end col*/}
                    </div>
                  </div>
                </div>
              </div>
              <div>
              {loader ? (
                      <div className='d-flex justify-content-center'>
                        <div className='spinner-border' role='status'>
                          <span className='sr-only'>Loading...</span>
                        </div>
                      </div>
                  ) : (
                      <Fade in={active}>
                <Table hover responsive className='mt-4'>
                    <tbody>
                      {PaymentsView &&
                        PaymentsView.map((value, i) => (
                          <tr key={i}>
                            <td>{i + 1}</td>
                            <td>
                              {value.payment_name}
                            </td>
                            <td>
                              DateTime
                              {/* {moment(new Date(value.createdAt)).format(
                                'DD/MM/YYYY'
                              )}
                              ,{new Date(value.createdAt).toLocaleTimeString()} */}
                            </td>

                            <td>
                              <div className='button-list'>
                                <button
                                  onClick={()=>modalToggle(value)}
                                  className='btn btn-success  btn-sm Button_Size'>
                                  View{' '}
                                </button>
                              </div>
                            </td>
                          </tr>
                        ))}
                    </tbody>
                </Table>
                </Fade>
                )}
              </div>
            </div>
          </div>
        </div>{' '}
        {/* end Col */}
      </div>
      <Modal isOpen={modalOpen} toggle={modalToggle}>
        <ModalHeader toggle={modalToggle}>Payments Details</ModalHeader>
        <ModalBody>
         <h4>Payments</h4>
        </ModalBody>
      </Modal>
    </>
  );
};

export default Payments;