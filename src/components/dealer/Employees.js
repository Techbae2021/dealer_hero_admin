/** @format */

import React, { useState, useEffect } from 'react';
import { Row, Col, Table, Fade } from 'reactstrap';
import Instance from '../../Instance';
import { useParams } from 'react-router-dom';
import moment from 'moment';
import {
  CardBody,
  Card,
  Modal,
  ModalHeader, ModalBody
} from 'reactstrap';

const EmployeeDetails = ({dealerId,active}) => {
  const params = useParams()
  const [employeeView, setEmpView] = useState(null);
  const [loader, setLoader] = useState(false);
  const [count, setCount] = useState(1);
  const [totalCount, settotalCount] = useState(0);
  const [page, setPage] = useState(null);

  useEffect(() => {
    setLoader(true);
    Instance.post(
      `/api/admin/dealer/employees/${dealerId}`,
      {
        page: count,
      },
      {
        headers: { authorization: `Bearer ${localStorage.getItem('token$')}` },
      }
    )
      .then(({ data }) => {
        let temp = data?.results?.employee;
        setPage(data?.results);
        setEmpView(temp);
        setLoader(false);
        settotalCount(data?.results?.totalEmployeedb);
      })
      .catch((err) => {
        setLoader(false);
        console.log('err', err);
      });
  }, [count,active]);

  const NextPage = () => {
    setCount(count + 1);
  };
  const PrevPage = () => {
    setCount(count - 1);
  };

  const [modalOpen, setModalOpen] = useState(false);
  const [modalView, setModalView] = useState('');

  const modalToggle = (value) => {
    setModalOpen(!modalOpen);
    setModalView(value);
  };

  return (
    <>
      {/* <Row className='page-title align-items-center'>
        <Col sm={4} xl={6}>
          <h4 className='mb-1 mt-0'>Dealer Employee Details</h4>
        </Col>
      </Row> */}

      <div className='row'>
        <div className='col-12'>
          <div className='card'>
            <div className='p-3'>
              <div className='row'>
                <div className='col-6'>
                  <h4> All Employee </h4>
                </div>
                <div className='col-md-6 text-right'>
                  <div className='d-inline-block align-middle float-lg-right'>
                    <div className='row'>
                      <div
                        className='col-8 align-self-center'
                        style={{ whiteSpace: 'nowrap' }}>
                        Showing {page?.startOfItem} - {page?.endOfItem} of{' '}
                        {totalCount}
                      </div>
                      {/* end col*/}
                      <div className='col-4'>
                        <div className='btn-group float-right'>
                          <button
                            type='button'
                            className='btn btn-white btn-sm'
                            style={{
                              display: page?.Previous === false ? 'none' : '',
                            }}>
                            <i
                              className='uil uil-angle-left'
                              onClick={PrevPage}
                            />
                          </button>
                          <button
                            type='button'
                            className='btn btn-primary btn-sm'
                            style={{
                              display: page?.Next === false ? 'none' : '',
                            }}>
                            <i
                              className='uil uil-angle-right'
                              onClick={NextPage}
                            />
                          </button>
                        </div>
                      </div>{' '}
                      {/* end col*/}
                    </div>
                  </div>
                </div>
              </div>
              <div>
              {loader ? (
                    <>
                      <div class='d-flex justify-content-center'>
                        <div class='spinner-border' role='status'>
                          <span class='sr-only'>Loading...</span>
                        </div>
                      </div>{' '}
                    </>
                  ) : (
                      <Fade>
                <Table hover responsive className='mt-4'>
                    <tbody>
                      {employeeView &&
                        employeeView.map((value, i) => (
                          <tr key={i}>
                            <td>{i + 1}</td>
                            <td>
                              {value.firstName}
                              <br />
                              {value.lastName}
                              <br />
                              {value.email}
                            </td>
                            <td>
                              {moment(new Date(value.createdAt)).format(
                                'DD/MM/YYYY'
                              )}
                              ,{new Date(value.createdAt).toLocaleTimeString()}
                            </td>

                            <td>
                              <div className='button-list'>
                                <button
                                  onClick={() => modalToggle(value)}
                                  className='btn btn-success  btn-sm Button_Size'>
                                  View{' '}
                                </button>
                              </div>
                            </td>
                          </tr>
                        ))}
                    </tbody>
                </Table>
                </Fade>
                )}
              </div>
            </div>
          </div>
        </div>{' '}
        {/* end Col */}
      </div>
      <Modal isOpen={modalOpen} toggle={modalToggle} >
        <ModalHeader toggle={modalToggle}>Employee Details</ModalHeader>
        <ModalBody>
          <Card>
            <CardBody>
              <div className='form-row'>
                <div className='col-3 form-group'>
                  <label className='col-form-label'>
                    <strong>First Name: </strong>
                  </label>{' '}
                  <span>{modalView?.firstName}</span>
                </div>
                <div className='col-3 form-group'>
                  <label className='col-form-label'>
                    <strong>Last Name: </strong>
                  </label>{' '}
                  <span>{modalView?.lastName}</span>
                </div>
                <div className='col-3 form-group'>
                  <label className='col-form-label'>
                    <strong>Department: </strong>
                  </label>{' '}
                  <span>{modalView?.department}</span>
                </div>
                <div className='col-3 form-group'>
                  <label className='col-form-label'>
                    <strong>Email: </strong>
                  </label>{' '}
                  <span>{modalView?.email}</span>
                </div>
                <div className='col-3 form-group'>
                  <label className='col-form-label'>
                    <strong>Mobile: </strong>
                  </label>{' '}
                  <span>{modalView?.mobile}</span>
                </div>
                <div className='col-3 form-group'>
                  <label className='col-form-label'>
                    <strong>User Type: </strong>
                  </label>{' '}
                  <span>{modalView?.userType}</span>
                </div>
              </div>
            </CardBody>
          </Card>
          <div className='row'>
            <div className='col-12'>
              <Card>
                <CardBody>
                  <h4>Permission:</h4>
                  <span><strong>Lead:</strong></span>
                  <div className='form-row'>
                    <div className='col-3 form-group'>
                      <label className='col-form-label'>
                        <strong>Create: </strong>
                      </label>{' '}
                      {(modalView?.createLead) ? <span className="fa fa-check-circle" style={{ color: 'green' }}></span> : <span className="fa fa-times-circle" style={{ color: 'red' }}></span>}
                    </div>
                    <div className='col-3 form-group'>
                      <label className='col-form-label'>
                        <strong>Edit: </strong>
                      </label>{' '}
                      {(modalView?.editLead) ? <span className="fa fa-check-circle" style={{ color: 'green' }}></span> : <span className="fa fa-times-circle" style={{ color: 'red' }}></span>}
                    </div>
                    <div className='col-3 form-group'>
                      <label className='col-form-label'>
                        <strong>Delete: </strong>
                      </label>{' '}
                      {(modalView?.deleteLead) ? <span className="fa fa-check-circle" style={{ color: 'green' }}></span> : <span className="fa fa-times-circle" style={{ color: 'red' }}></span>}
                    </div>
                    <div className='col-3 form-group'>
                      <label className='col-form-label'>
                        <strong>Add Note: </strong>
                      </label>{' '}
                      {(modalView?.leadNote) ? <span className="fa fa-check-circle" style={{ color: 'green' }}></span> : <span className="fa fa-times-circle" style={{ color: 'red' }}></span>}
                    </div>
                    <div className='col-3 form-group'>
                      <label className='col-form-label'>
                        <strong>Send Mail: </strong>
                      </label>{' '}
                      {(modalView?.leadMail) ? <span className="fa fa-check-circle" style={{ color: 'green' }}></span> : <span className="fa fa-times-circle" style={{ color: 'red' }}></span>}
                    </div>
                    <div className='col-3 form-group'>
                      <label className='col-form-label'>
                        <strong>Set Reminder: </strong>
                      </label>{' '}
                      {(modalView?.leadReminder) ? <span className="fa fa-check-circle" style={{ color: 'green' }}></span> : <span className="fa fa-times-circle" style={{ color: 'red' }}></span>}
                    </div>
                    <div className='col-3 form-group'>
                      <label className='col-form-label'>
                        <strong>Lead Board: </strong>
                      </label>{' '}
                      {(modalView?.leadBoard) ? <span className="fa fa-check-circle" style={{ color: 'green' }}></span> : <span className="fa fa-times-circle" style={{ color: 'red' }}></span>}
                    </div>
                  </div>
                  <span><strong>Inventory:</strong></span>
                  <div className='form-row'>
                    <div className='col-3 form-group'>
                      <label className='col-form-label'>
                        <strong>Create Stock: </strong>
                      </label>{' '}
                      {(modalView?.createStock) ? <span className="fa fa-check-circle" style={{ color: 'green' }}></span> : <span className="fa fa-times-circle" style={{ color: 'red' }}></span>}
                    </div>
                    <div className='col-3 form-group'>
                      <label className='col-form-label'>
                        <strong>Create Costing: </strong>
                      </label>{' '}
                      {(modalView?.createCosting) ? <span className="fa fa-check-circle" style={{ color: 'green' }}></span> : <span className="fa fa-times-circle" style={{ color: 'red' }}></span>}
                    </div>
                    <div className='col-3 form-group'>
                      <label className='col-form-label'>
                        <strong>Sell Vehicle: </strong>
                      </label>{' '}
                      {(modalView?.sellVehicle) ? <span className="fa fa-check-circle" style={{ color: 'green' }}></span> : <span className="fa fa-times-circle" style={{ color: 'red' }}></span>}
                    </div>
                    <div className='col-3 form-group'>
                      <label className='col-form-label'>
                        <strong>Vehicle Edit: </strong>
                      </label>{' '}
                      {(modalView?.vehicleEdit) ? <span className="fa fa-check-circle" style={{ color: 'green' }}></span> : <span className="fa fa-times-circle" style={{ color: 'red' }}></span>}
                    </div>
                    <div className='col-3 form-group'>
                      <label className='col-form-label'>
                        <strong>Acquition Edit: </strong>
                      </label>{' '}
                      {(modalView?.accquitionEdit) ? <span className="fa fa-check-circle" style={{ color: 'green' }}></span> : <span className="fa fa-times-circle" style={{ color: 'red' }}></span>}
                    </div>
                    <div className='col-3 form-group'>
                      <label className='col-form-label'>
                        <strong>Costing Edit: </strong>
                      </label>{' '}
                      {(modalView?.costingEdit) ? <span className="fa fa-check-circle" style={{ color: 'green' }}></span> : <span className="fa fa-times-circle" style={{ color: 'red' }}></span>}
                    </div>
                    <div className='col-3 form-group'>
                      <label className='col-form-label'>
                        <strong>Advertising Edit: </strong>
                      </label>{' '}
                      {(modalView?.advertisingEdit) ? <span className="fa fa-check-circle" style={{ color: 'green' }}></span> : <span className="fa fa-times-circle" style={{ color: 'red' }}></span>}
                    </div>
                    <div className='col-3 form-group'>
                      <label className='col-form-label'>
                        <strong>Vehicle Note: </strong>
                      </label>{' '}
                      {(modalView?.vehicleNote) ? <span className="fa fa-check-circle" style={{ color: 'green' }}></span> : <span className="fa fa-times-circle" style={{ color: 'red' }}></span>}
                    </div>
                    <div className='col-3 form-group'>
                      <label className='col-form-label'>
                        <strong>Vehicle Reminder: </strong>
                      </label>{' '}
                      {(modalView?.vehicleReminder) ? <span className="fa fa-check-circle" style={{ color: 'green' }}></span> : <span className="fa fa-times-circle" style={{ color: 'red' }}></span>}
                    </div>
                    <div className='col-3 form-group'>
                      <label className='col-form-label'>
                        <strong>Valuation Add: </strong>
                      </label>{' '}
                      {(modalView?.valuationAdd) ? <span className="fa fa-check-circle" style={{ color: 'green' }}></span> : <span className="fa fa-times-circle" style={{ color: 'red' }}></span>}
                    </div>
                    <div className='col-3 form-group'>
                      <label className='col-form-label'>
                        <strong>Valuation Edit: </strong>
                      </label>{' '}
                      {(modalView?.valuationEdit) ? <span className="fa fa-check-circle" style={{ color: 'green' }}></span> : <span className="fa fa-times-circle" style={{ color: 'red' }}></span>}
                    </div>
                  </div>
               
                  <span><strong>Deal:</strong></span>
                  <div className='form-row'>
                    <div className='col-3 form-group'>
                      <label className='col-form-label'>
                        <strong>Create Proposal: </strong>
                      </label>{' '}
                      {(modalView?.createProposal) ? <span className="fa fa-check-circle" style={{ color: 'green' }}></span> : <span className="fa fa-times-circle" style={{ color: 'red' }}></span>}
                    </div>
                    <div className='col-3 form-group'>
                      <label className='col-form-label'>
                        <strong>Edit Proposal: </strong>
                      </label>{' '}
                      {(modalView?.proposalEdit) ? <span className="fa fa-check-circle" style={{ color: 'green' }}></span> : <span className="fa fa-times-circle" style={{ color: 'red' }}></span>}
                    </div>
                    <div className='col-3 form-group'>
                      <label className='col-form-label'>
                        <strong>Edit Trade-In: </strong>
                      </label>{' '}
                      {(modalView?.tradeInEdit) ? <span className="fa fa-check-circle" style={{ color: 'green' }}></span> : <span className="fa fa-times-circle" style={{ color: 'red' }}></span>}
                    </div>
                    <div className='col-3 form-group'>
                      <label className='col-form-label'>
                        <strong>Edit Sell Item: </strong>
                      </label>{' '}
                      {(modalView?.sellItemEdit) ? <span className="fa fa-check-circle" style={{ color: 'green' }}></span> : <span className="fa fa-times-circle" style={{ color: 'red' }}></span>}
                    </div>
                    <div className='col-3 form-group'>
                      <label className='col-form-label'>
                        <strong>Proposal Finalize: </strong>
                      </label>{' '}
                      {(modalView?.proposalFinalize) ? <span className="fa fa-check-circle" style={{ color: 'green' }}></span> : <span className="fa fa-times-circle" style={{ color: 'red' }}></span>}
                    </div>
                    <div className='col-3 form-group'>
                      <label className='col-form-label'>
                        <strong>Proposal Lost: </strong>
                      </label>{' '}
                      {(modalView?.proposalLost) ? <span className="fa fa-check-circle" style={{ color: 'green' }}></span> : <span className="fa fa-times-circle" style={{ color: 'red' }}></span>}
                    </div>
                    <div className='col-3 form-group'>
                      <label className='col-form-label'>
                        <strong>Proposal Note: </strong>
                      </label>{' '}
                      {(modalView?.proposalNote) ? <span className="fa fa-check-circle" style={{ color: 'green' }}></span> : <span className="fa fa-times-circle" style={{ color: 'red' }}></span>}
                    </div>
                    <div className='col-3 form-group'>
                      <label className='col-form-label'>
                        <strong>Proposal Reminder: </strong>
                      </label>{' '}
                      {(modalView?.proposalReminder) ? <span className="fa fa-check-circle" style={{ color: 'green' }}></span> : <span className="fa fa-times-circle" style={{ color: 'red' }}></span>}
                    </div>
                    <div className='col-3 form-group'>
                      <label className='col-form-label'>
                        <strong>Create Brokered: </strong>
                      </label>{' '}
                      {(modalView?.createBrokered) ? <span className="fa fa-check-circle" style={{ color: 'green' }}></span> : <span className="fa fa-times-circle" style={{ color: 'red' }}></span>}
                    </div>
                    <div className='col-3 form-group'>
                      <label className='col-form-label'>
                        <strong>Edit Brokered: </strong>
                      </label>{' '}
                      {(modalView?.brokeredEdit) ? <span className="fa fa-check-circle" style={{ color: 'green' }}></span> : <span className="fa fa-times-circle" style={{ color: 'red' }}></span>}
                    </div>
                    <div className='col-3 form-group'>
                      <label className='col-form-label'>
                        <strong>Edit Brokered Trade-In: </strong>
                      </label>{' '}
                      {(modalView?.bTradeInEdit) ? <span className="fa fa-check-circle" style={{ color: 'green' }}></span> : <span className="fa fa-times-circle" style={{ color: 'red' }}></span>}
                    </div>
                    <div className='col-3 form-group'>
                      <label className='col-form-label'>
                        <strong>Edit Brokered Sell Item: </strong>
                      </label>{' '}
                      {(modalView?.bSellItemEdit) ? <span className="fa fa-check-circle" style={{ color: 'green' }}></span> : <span className="fa fa-times-circle" style={{ color: 'red' }}></span>}
                    </div>
                    <div className='col-3 form-group'>
                      <label className='col-form-label'>
                        <strong>Brokered Finalize: </strong>
                      </label>{' '}
                      {(modalView?.brokeredFinalize) ? <span className="fa fa-check-circle" style={{ color: 'green' }}></span> : <span className="fa fa-times-circle" style={{ color: 'red' }}></span>}
                    </div>
                    <div className='col-3 form-group'>
                      <label className='col-form-label'>
                        <strong>Brokered Lost: </strong>
                      </label>{' '}
                      {(modalView?.brokeredLost) ? <span className="fa fa-check-circle" style={{ color: 'green' }}></span> : <span className="fa fa-times-circle" style={{ color: 'red' }}></span>}
                    </div>
                    <div className='col-3 form-group'>
                      <label className='col-form-label'>
                        <strong>Brokered Note: </strong>
                      </label>{' '}
                      {(modalView?.brokeredNote) ? <span className="fa fa-check-circle" style={{ color: 'green' }}></span> : <span className="fa fa-times-circle" style={{ color: 'red' }}></span>}
                    </div>
                    <div className='col-3 form-group'>
                      <label className='col-form-label'>
                        <strong>Brokered Reminder: </strong>
                      </label>{' '}
                      {(modalView?.brokeredReminder) ? <span className="fa fa-check-circle" style={{ color: 'green' }}></span> : <span className="fa fa-times-circle" style={{ color: 'red' }}></span>}
                    </div>
                  </div>
               
                  <span><strong>Customer:</strong></span>
                  <div className='form-row'>
                    <div className='col-3 form-group'>
                      <label className='col-form-label'>
                        <strong>Create Customer: </strong>
                      </label>{' '}
                      {(modalView?.createCustomer) ? <span className="fa fa-check-circle" style={{ color: 'green' }}></span> : <span className="fa fa-times-circle" style={{ color: 'red' }}></span>}
                    </div>
                    <div className='col-3 form-group'>
                      <label className='col-form-label'>
                        <strong>Edit Customer: </strong>
                      </label>{' '}
                      {(modalView?.customerEdit) ? <span className="fa fa-check-circle" style={{ color: 'green' }}></span> : <span className="fa fa-times-circle" style={{ color: 'red' }}></span>}
                    </div>
                    <div className='col-3 form-group'>
                      <label className='col-form-label'>
                        <strong>Delete Customer: </strong>
                      </label>{' '}
                      {(modalView?.customerDelete) ? <span className="fa fa-check-circle" style={{ color: 'green' }}></span> : <span className="fa fa-times-circle" style={{ color: 'red' }}></span>}
                    </div>
                  </div>
               
                  <span><strong>Employee:</strong></span>
                  <div className='form-row'>
                    <div className='col-3 form-group'>
                      <label className='col-form-label'>
                        <strong>Create Employee: </strong>
                      </label>{' '}
                      {(modalView?.addEmp) ? <span className="fa fa-check-circle" style={{ color: 'green' }}></span> : <span className="fa fa-times-circle" style={{ color: 'red' }}></span>}
                    </div>
                    <div className='col-3 form-group'>
                      <label className='col-form-label'>
                        <strong>Delete Employee: </strong>
                      </label>{' '}
                      {(modalView?.empDelete) ? <span className="fa fa-check-circle" style={{ color: 'green' }}></span> : <span className="fa fa-times-circle" style={{ color: 'red' }}></span>}
                    </div>
                  </div>
               
                  <span><strong>Other:</strong></span>
                  <div className='form-row'>
                    <div className='col-3 form-group'>
                      <label className='col-form-label'>
                        <strong>Purchase Delete: </strong>
                      </label>{' '}
                      {(modalView?.purchaseDelete) ? <span className="fa fa-check-circle" style={{ color: 'green' }}></span> : <span className="fa fa-times-circle" style={{ color: 'red' }}></span>}
                    </div>
                    <div className='col-3 form-group'>
                      <label className='col-form-label'>
                        <strong>Sales Delete: </strong>
                      </label>{' '}
                      {(modalView?.salesDelete) ? <span className="fa fa-check-circle" style={{ color: 'green' }}></span> : <span className="fa fa-times-circle" style={{ color: 'red' }}></span>}
                    </div>
                    <div className='col-3 form-group'>
                      <label className='col-form-label'>
                        <strong>PPSR Delete: </strong>
                      </label>{' '}
                      {(modalView?.ppsrDelete) ? <span className="fa fa-check-circle" style={{ color: 'green' }}></span> : <span className="fa fa-times-circle" style={{ color: 'red' }}></span>}
                    </div>
                  </div>
               
                  <span><strong>Setting:</strong></span>
                  <div className='form-row'>
                    <div className='col-3 form-group'>
                      <label className='col-form-label'>
                        <strong>Setting 1: </strong>
                      </label>{' '}
                      {(modalView?.setting1) ? <span className="fa fa-check-circle" style={{ color: 'green' }}></span> : <span className="fa fa-times-circle" style={{ color: 'red' }}></span>}
                    </div>
                    <div className='col-3 form-group'>
                      <label className='col-form-label'>
                        <strong>Setting 2: </strong>
                      </label>{' '}
                      {(modalView?.setting2) ? <span className="fa fa-check-circle" style={{ color: 'green' }}></span> : <span className="fa fa-times-circle" style={{ color: 'red' }}></span>}
                    </div>
                    <div className='col-3 form-group'>
                      <label className='col-form-label'>
                        <strong>Setting 3: </strong>
                      </label>{' '}
                      {(modalView?.setting3) ? <span className="fa fa-check-circle" style={{ color: 'green' }}></span> : <span className="fa fa-times-circle" style={{ color: 'red' }}></span>}
                    </div>
                    <div className='col-3 form-group'>
                      <label className='col-form-label'>
                        <strong>Setting 4: </strong>
                      </label>{' '}
                      {(modalView?.setting4) ? <span className="fa fa-check-circle" style={{ color: 'green' }}></span> : <span className="fa fa-times-circle" style={{ color: 'red' }}></span>}
                    </div>
                    <div className='col-3 form-group'>
                      <label className='col-form-label'>
                        <strong>Setting 5: </strong>
                      </label>{' '}
                      {(modalView?.setting5) ? <span className="fa fa-check-circle" style={{ color: 'green' }}></span> : <span className="fa fa-times-circle" style={{ color: 'red' }}></span>}
                    </div>
                    <div className='col-3 form-group'>
                      <label className='col-form-label'>
                        <strong>Setting 6: </strong>
                      </label>{' '}
                      {(modalView?.setting6) ? <span className="fa fa-check-circle" style={{ color: 'green' }}></span> : <span className="fa fa-times-circle" style={{ color: 'red' }}></span>}
                    </div>
                  </div>
               
                </CardBody>
              </Card>
            </div>
          </div>
        </ModalBody>
      </Modal>
    </>
  );
};

export default EmployeeDetails;