/** @format */

import React, { useState, useContext } from 'react';
import Instance from '../../Instance';
import { useAlert } from 'react-alert';

const CNoteEdit = ({note,onSuccess}) => {
  const alert = useAlert();
  const [isLoading,setLoading] = useState(false)

  const [errors, setErrors] = useState({
    note: false
  });

  const [noteDetails, setNoteDetails] = useState(note?note:{});

  const handelChange = (event) => {
    setNoteDetails({
      ...noteDetails,
      [event.target.id]: event.target.value,
    });

    if (event.target.value === '') {
      setErrors({ ...errors, [event.target.id]: true });
    } else {
      setErrors({ ...errors, [event.target.id]: false });
    }
  };

  const OnUpdated = () => {
    setLoading(true)
    Instance.put(
      `/api/admin/updateDealerNote/${noteDetails?._id}`,
      {
        note: noteDetails.note,
      },
      {
        headers: { authorization: `Bearer ${localStorage.getItem('token$')}` },
      }
    )
      .then(({ data }) => {
        setLoading(false)
        onSuccess(true);
        alert.success('Note Edited');
      }).catch((err) => {
        setLoading(false)
        console.log('Err', err);
      });
  };


  const HandleValidation = () => {
    if (noteDetails.note === '') {
      setErrors({ note: true });
    } else {
      OnUpdated();
    }
  };

  return (
    <>
      {/* <Row className='page-title align-items-center'>
        <Col sm={4} xl={6}>
          <h4 className='mb-1 mt-0'>Proposal Note</h4>
        </Col>
      </Row> */}

      <div className='row'>
        <div className='col-12'>
          <div className='card p-3'>
            <h4 className='mb-4'> Note Edit</h4>
            <div className='form-row'>
              <div className='form-group col-md-12'>
                <label>Note</label>
                <textarea
                  type='text'
                  style={{height:150}}
                  // className='form-control'
                  placeholder='Note'
                  id='note'
                  value={noteDetails.note}
                  onChange={handelChange}
                  name='note'
                  className={`form-control ${errors.note ? 'is-invalid' : ''}`}
                />
                  {errors?.note && (
                  <div className='invalid-feedback'>Required</div>
                )}
              </div>
            </div>
            <div className='form-row'>
              <div className='form-group col-md-2'>
              <button
                 disabled={isLoading}
                  className='btn btn-primary btn-block'
                  onClick={(e) => HandleValidation(e)}>
                    {isLoading?'Updating...':'Update'}
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default CNoteEdit;