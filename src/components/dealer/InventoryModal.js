import React from 'react'
import { Card, CardBody, Modal, ModalBody, ModalHeader } from 'reactstrap'
import config from '../../config'

const InventoryModal = ({InventoryView,InventoryOpen,InventoryToggle}) => {
    return (
        <Modal isOpen={InventoryOpen} toggle={InventoryToggle} >
        <ModalHeader toggle={InventoryToggle}>Vehicle Details</ModalHeader>
        <ModalBody>
            <div className='row'>
             <div className='col-12'>
                    <Card>
                    <CardBody>
                    <h4>Vehicle Specification:</h4>
                    <div className='form-row'>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                        <strong>Make: </strong>
                        </label>{' '}
                        <span>{InventoryView?.make}</span>
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                        <strong>Model: </strong>
                        </label>{' '}
                        <span>{InventoryView?.model}</span>
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                          <strong>Body: </strong>
                        </label>{' '}
                        <span>{InventoryView?.vBody}</span>
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                        <strong>Trasnmission: </strong>
                        </label>{' '}
                        <span>{InventoryView?.trasnmission}</span>
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                          <strong>Engine: </strong>
                        </label>{' '}
                        <span>{InventoryView?.engine}</span>
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                          <strong>Compiled: </strong>
                        </label>{' '}
                        <span>{InventoryView?.compiled}</span>
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                          <strong>Varient & Series: </strong>
                        </label>{' '}
                        <span>{InventoryView?.varientSeries}</span>
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                          <strong>Built: </strong>
                        </label>{' '}
                        <span>{InventoryView?.built}</span>
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                          <strong>Vin: </strong>
                        </label>{' '}
                        <span>{InventoryView?.vin}</span>
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                          <strong>YoutubeUrl: </strong>
                        </label>{' '}
                        <span>{InventoryView?.youtubeUrl}</span>
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                        <strong>Kms: </strong>
                        </label>{' '}
                        <span>{InventoryView?.kms}</span>
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                          <strong>Advertising Price: </strong>
                        </label>{' '}
                        <span>{InventoryView?.advPrice}</span>
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                          <strong>Drive Way: </strong>
                        </label>{' '}
                        <span>{InventoryView?.driveWay?'Yes':'No'}</span>
                      </div>
                      {
                        InventoryView?.form?(
                          <div className='col-3 form-group'>
                        <label className='col-form-label'>
                          <strong>Form: </strong>
                        </label>{' '}
                        <a href={`${config.baseImgUrl}/pdf/${InventoryView?.form}`} target="_blank" className="btn btn-xs btn-info" download>Download</a>
                      </div>
                        ):null
                      }
                      {
                        InventoryView?.form4?(
                          <div className='col-3 form-group'>
                        <label className='col-form-label'>
                          <strong>Form4: </strong>
                        </label>{' '}
                        <a href={`${config.baseImgUrl}/pdf/${InventoryView?.form4}`} target="_blank" className="btn btn-xs btn-info" download>Download</a>
                      </div>
                        ):null
                      }
                      
                      </div>
                    </CardBody>
                  </Card>
                  </div>
             </div>
             <div className='row'>
             <div className='col-12'>
                    <Card>
                    <CardBody>
                    <h4>Registration Details:</h4>
                    <div className='form-row'>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                        <strong>Registration State:</strong>
                        </label>{' '}
                        <span>{InventoryView?.registrationState}</span>
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                        <strong>Registration No: </strong>
                        </label>{' '}
                        <span>{InventoryView?.registrationNo}</span>
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                        <strong>Registration Expiery: </strong>
                        </label>{' '}
                        <span>{InventoryView?.registrationExpiry}</span>
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                        <strong>Registration Seriel: </strong>
                        </label>{' '}
                        <span>{InventoryView?.registrationSeriel}</span>
                      </div>
                     </div>
                    </CardBody>
                  </Card>
                  </div>
             </div>
             <div className='row'>
             <div className='col-12'>
                    <Card>
                    <CardBody>
                    <h4>Location & Status:</h4>
                    <div className='form-row'>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                        <strong>Yard:</strong>
                        </label>{' '}
                        <span>{InventoryView?.yard?InventoryView?.yard[0]?.yardName:null}</span>
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                        <strong>Form Type: </strong>
                        </label>{' '}
                        <span>{InventoryView?.fuelType}</span>
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                        <strong>Condition: </strong>
                        </label>{' '}
                        <span>{InventoryView?.condition}</span>
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                        <strong>Condition Rating: </strong>
                        </label>{' '}
                        <span>{InventoryView?.conditionRating}</span>
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                        <strong>Keyboard: </strong>
                        </label>{' '}
                        <span>{InventoryView?.keyboard}</span>
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                        <strong>Status: </strong>
                        </label>{' '}
                        <span>{InventoryView?.status}</span>
                      </div>
                     </div>
                    </CardBody>
                  </Card>
                  </div>
             </div>
             <div className='row'>
             <div className='col-12'>
                    <Card>
                    <CardBody>
                    <h4>Extended Details:</h4>
                    <div className='form-row'>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                        <strong>Extended Color:</strong>
                        </label>{' '}
                        <span>{InventoryView?.extendedcolor}</span>
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                        <strong>Int. Color: </strong>
                        </label>{' '}
                        <span>{InventoryView?.intColor}</span>
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                        <strong>Doors: </strong>
                        </label>{' '}
                        <span>{InventoryView?.doors}</span>
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                        <strong>Seats: </strong>
                        </label>{' '}
                        <span>{InventoryView?.seats}</span>
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                        <strong>Drivetrain: </strong>
                        </label>{' '}
                        <span>{InventoryView?.driveTrain}</span>
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                        <strong>Engine Size: </strong>
                        </label>{' '}
                        <span>{InventoryView?.engineSize}</span>
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                        <strong>Engine Capacity: </strong>
                        </label>{' '}
                        <span>{InventoryView?.engineCapicity}</span>
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                        <strong>Service History: </strong>
                        </label>{' '}
                        <span>{InventoryView?.serviceHistory}</span>
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                        <strong>Country of Manufacture: </strong>
                        </label>{' '}
                        <span>{InventoryView?.countryofmanufacture}</span>
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                        <strong>Redbook Code: </strong>
                        </label>{' '}
                        <span>{InventoryView?.redbookCode}</span>
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                        <strong>Glasses Code: </strong>
                        </label>{' '}
                        <span>{InventoryView?.glasscode}</span>
                      </div>
                     </div>
                    </CardBody>
                  </Card>
                  </div>
             </div>
             <div className='row'>
             <div className='col-12'>
                    <Card>
                    <CardBody>
                    <h4>Fuel Details:</h4>
                    <div className='form-row'>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                        <strong>Fuel Type:</strong>
                        </label>{' '}
                        <span>{InventoryView?.fuelType}</span>
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                        <strong>Fuel Savear Rating: </strong>
                        </label>{' '}
                        <span>{InventoryView?.fuelsavearRating}</span>
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                        <strong>Safety Rating: </strong>
                        </label>{' '}
                        <span>{InventoryView?.safetyRating}</span>
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                        <strong>Safety Lists: </strong>
                        </label>{' '}
                        <span>{InventoryView?.safetyList}</span>
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                        <strong>Fuel Economy: </strong>
                        </label>{' '}
                        <span>{InventoryView?.fuelEconomy}</span>
                      </div>
                     </div>
                    </CardBody>
                  </Card>
                  </div>
             </div>
             <div className='row'>
             <div className='col-12'>
                    <Card>
                    <CardBody>
                    <h4>Previous Details:</h4>
                    <div className='form-row'>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                        <strong>Previous Price:</strong>
                        </label>{' '}
                        <span>{InventoryView?.previousPrice}</span>
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                        <strong>Prev Registration:</strong>
                        </label>{' '}
                        <span>{InventoryView?.prevRegistration}</span>
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                        <strong>Year First Registered: </strong>
                        </label>{' '}
                        <span>{InventoryView?.yearofFirstreg}</span>
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                        <strong>Suppliers Stock: </strong>
                        </label>{' '}
                        <span>{InventoryView?.supplierStock}</span>
                      </div>
                     </div>
                    </CardBody>
                  </Card>
                  </div>
             </div>
             <div className='row'>
             <div className='col-12'>
                    <Card>
                    <CardBody>
                    <h4>Features:</h4>
                    <div className='form-row'>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                        <strong>4WD:</strong>
                        </label>{' '}
                        {(InventoryView.vehiclefeatures && InventoryView?.vehiclefeatures[0]?.WD)?<span className="fa fa-check-circle" style={{color:'green'}}></span>:<span className="fa fa-times-circle" style={{color:'red'}}></span>}
                       </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                        <strong>AA APPRAISED:</strong>
                        </label>{' '}
                        {(InventoryView.vehiclefeatures && InventoryView?.vehiclefeatures[0]?.aaAppraised)?<span className="fa fa-check-circle" style={{color:'green'}}></span>:<span className="fa fa-times-circle" style={{color:'red'}}></span>}
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                        <strong>AA INSPECTION RPT: </strong>
                        </label>{' '}
                        {InventoryView.vehiclefeatures && InventoryView?.vehiclefeatures[0]?.aaInspectionRpt?<span className="fa fa-check-circle" style={{color:'green'}}></span>:<span className="fa fa-times-circle" style={{color:'red'}}></span>}
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                        <strong>ABS BREAKS: </strong>
                        </label>{' '}
                        {InventoryView.vehiclefeatures && InventoryView?.vehiclefeatures[0]?.absBreaks?<span className="fa fa-check-circle" style={{color:'green'}}></span>:<span className="fa fa-times-circle" style={{color:'red'}}></span>}
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                        <strong>BODY KIT: </strong>
                        </label>{' '}
                        {InventoryView.vehiclefeatures && InventoryView?.vehiclefeatures[0]?.bodyKit?<span className="fa fa-check-circle" style={{color:'green'}}></span>:<span className="fa fa-times-circle" style={{color:'red'}}></span>}
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                        <strong>AIR CONDITIONING: </strong>
                        </label>{' '}
                        {InventoryView.vehiclefeatures && InventoryView?.vehiclefeatures[0]?.airConditioning?<span className="fa fa-check-circle" style={{color:'green'}}></span>:<span className="fa fa-times-circle" style={{color:'red'}}></span>}
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                        <strong>ALARM: </strong>
                        </label>{' '}
                        {InventoryView.vehiclefeatures && InventoryView?.vehiclefeatures[0]?.alarm?<span className="fa fa-check-circle" style={{color:'green'}}></span>:<span className="fa fa-times-circle" style={{color:'red'}}></span>}
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                        <strong>ALLOY WHEEL: </strong>
                        </label>{' '}
                        {InventoryView.vehiclefeatures && InventoryView?.vehiclefeatures[0]?.alloyWheel?<span className="fa fa-check-circle" style={{color:'green'}}></span>:<span className="fa fa-times-circle" style={{color:'red'}}></span>}
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                        <strong>CENTRAL LOCKING: </strong>
                        </label>{' '}
                        {InventoryView.vehiclefeatures && InventoryView?.vehiclefeatures[0]?.centralLooking?<span className="fa fa-check-circle" style={{color:'green'}}></span>:<span className="fa fa-times-circle" style={{color:'red'}}></span>}
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                        <strong>CANOPY: </strong>
                        </label>{' '}
                        {InventoryView.vehiclefeatures && InventoryView?.vehiclefeatures[0]?.canopy?<span className="fa fa-check-circle" style={{color:'green'}}></span>:<span className="fa fa-times-circle" style={{color:'red'}}></span>}
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                        <strong>CD PLAYER: </strong>
                        </label>{' '}
                        {InventoryView.vehiclefeatures && InventoryView?.vehiclefeatures[0]?.cdPlayer?<span className="fa fa-check-circle" style={{color:'green'}}></span>:<span className="fa fa-times-circle" style={{color:'red'}}></span>}
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                        <strong>CD STEKER: </strong>
                        </label>{' '}
                        {InventoryView.vehiclefeatures && InventoryView?.vehiclefeatures[0]?.cdSteker?<span className="fa fa-check-circle" style={{color:'green'}}></span>:<span className="fa fa-times-circle" style={{color:'red'}}></span>}
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                        <strong>DVD: </strong>
                        </label>{' '}
                        {InventoryView.vehiclefeatures && InventoryView?.vehiclefeatures[0]?.dvd?<span className="fa fa-check-circle" style={{color:'green'}}></span>:<span className="fa fa-times-circle" style={{color:'red'}}></span>}
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                        <strong>CLIMATE CONTROL: </strong>
                        </label>{' '}
                        {InventoryView.vehiclefeatures && InventoryView?.vehiclefeatures[0]?.climateControl?<span className="fa fa-check-circle" style={{color:'green'}}></span>:<span className="fa fa-times-circle" style={{color:'red'}}></span>}
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                        <strong>CRUISE CONTROL: </strong>
                        </label>{' '}
                        {InventoryView.vehiclefeatures && InventoryView?.vehiclefeatures[0]?.cruiseControl?<span className="fa fa-check-circle" style={{color:'green'}}></span>:<span className="fa fa-times-circle" style={{color:'red'}}></span>}
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                        <strong>DRIVER AIRBAG: </strong>
                        </label>{' '}
                        {InventoryView.vehiclefeatures && InventoryView?.vehiclefeatures[0]?.driverAirbag?<span className="fa fa-check-circle" style={{color:'green'}}></span>:<span className="fa fa-times-circle" style={{color:'red'}}></span>}
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                        <strong>ELECTRIC WINDOWS: </strong>
                        </label>{' '}
                        {InventoryView.vehiclefeatures && InventoryView?.vehiclefeatures[0]?.electricWindows?<span className="fa fa-check-circle" style={{color:'green'}}></span>:<span className="fa fa-times-circle" style={{color:'red'}}></span>}
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                        <strong>ELECTRIC ARIEL: </strong>
                        </label>{' '}
                        {InventoryView.vehiclefeatures && InventoryView?.vehiclefeatures[0]?.electricAriel?<span className="fa fa-check-circle" style={{color:'green'}}></span>:<span className="fa fa-times-circle" style={{color:'red'}}></span>}
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                        <strong>ELECTRIC MIRRORS: </strong>
                        </label>{' '}
                        {InventoryView.vehiclefeatures && InventoryView?.vehiclefeatures[0]?.electricMirrors?<span className="fa fa-check-circle" style={{color:'green'}}></span>:<span className="fa fa-times-circle" style={{color:'red'}}></span>}
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                        <strong>ELECTRIC SEATS: </strong>
                        </label>{' '}
                        {InventoryView.vehiclefeatures && InventoryView?.vehiclefeatures[0]?.electricSeats?<span className="fa fa-check-circle" style={{color:'green'}}></span>:<span className="fa fa-times-circle" style={{color:'red'}}></span>}
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                        <strong>FUEL INJECTION: </strong>
                        </label>{' '}
                        {InventoryView.vehiclefeatures && InventoryView?.vehiclefeatures[0]?.fuelInjection?<span className="fa fa-check-circle" style={{color:'green'}}></span>:<span className="fa fa-times-circle" style={{color:'red'}}></span>}
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                        <strong>EXHAUST BREAK: </strong>
                        </label>{' '}
                        {InventoryView.vehiclefeatures && InventoryView?.vehiclefeatures[0]?.exhaustBreak?<span className="fa fa-check-circle" style={{color:'green'}}></span>:<span className="fa fa-times-circle" style={{color:'red'}}></span>}
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                        <strong>FACTROY ALLOYS: </strong>
                        </label>{' '}
                        {InventoryView.vehiclefeatures && InventoryView?.vehiclefeatures[0]?.factroyAlloys?<span className="fa fa-check-circle" style={{color:'green'}}></span>:<span className="fa fa-times-circle" style={{color:'red'}}></span>}
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                        <strong>FOG LIGHTS: </strong>
                        </label>{' '}
                        {InventoryView.vehiclefeatures && InventoryView?.vehiclefeatures[0]?.fogLights?<span className="fa fa-check-circle" style={{color:'green'}}></span>:<span className="fa fa-times-circle" style={{color:'red'}}></span>}
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                        <strong>PASSENGER AIRBAG: </strong>
                        </label>{' '}
                        {InventoryView.vehiclefeatures && InventoryView?.vehiclefeatures[0]?.passengerAirbag?<span className="fa fa-check-circle" style={{color:'green'}}></span>:<span className="fa fa-times-circle" style={{color:'red'}}></span>}
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                        <strong>GPS: </strong>
                        </label>{' '}
                        {InventoryView.vehiclefeatures && InventoryView?.vehiclefeatures[0]?.gps?<span className="fa fa-check-circle" style={{color:'green'}}></span>:<span className="fa fa-times-circle" style={{color:'red'}}></span>}
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                        <strong>LATHER INTERIOR: </strong>
                        </label>{' '}
                        {InventoryView.vehiclefeatures && InventoryView?.vehiclefeatures[0]?.latherInterior?<span className="fa fa-check-circle" style={{color:'green'}}></span>:<span className="fa fa-times-circle" style={{color:'red'}}></span>}
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                        <strong>PARKING SENSORS: </strong>
                        </label>{' '}
                        {InventoryView.vehiclefeatures && InventoryView?.vehiclefeatures[0]?.parkingSensors?<span className="fa fa-check-circle" style={{color:'green'}}></span>:<span className="fa fa-times-circle" style={{color:'red'}}></span>}
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                        <strong>REMOTE LOCKING: </strong>
                        </label>{' '}
                        {InventoryView.vehiclefeatures && InventoryView?.vehiclefeatures[0]?.remoteLocking?<span className="fa fa-check-circle" style={{color:'green'}}></span>:<span className="fa fa-times-circle" style={{color:'red'}}></span>}
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                        <strong>POWER SEETING: </strong>
                        </label>{' '}
                        {InventoryView.vehiclefeatures && InventoryView?.vehiclefeatures[0]?.powerSeeting?<span className="fa fa-check-circle" style={{color:'green'}}></span>:<span className="fa fa-times-circle" style={{color:'red'}}></span>}
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                        <strong>RADIO: </strong>
                        </label>{' '}
                        {InventoryView.vehiclefeatures && InventoryView?.vehiclefeatures[0]?.radio?<span className="fa fa-check-circle" style={{color:'green'}}></span>:<span className="fa fa-times-circle" style={{color:'red'}}></span>}
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                        <strong>RAIN WIPERS: </strong>
                        </label>{' '}
                        {InventoryView.vehiclefeatures && InventoryView?.vehiclefeatures[0]?.rainWipers?<span className="fa fa-check-circle" style={{color:'green'}}></span>:<span className="fa fa-times-circle" style={{color:'red'}}></span>}
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                        <strong>SUNROOF: </strong>
                        </label>{' '}
                        {InventoryView.vehiclefeatures && InventoryView?.vehiclefeatures[0]?.sunRoof?<span className="fa fa-check-circle" style={{color:'green'}}></span>:<span className="fa fa-times-circle" style={{color:'red'}}></span>}
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                        <strong>REVERSEING CAMERA: </strong>
                        </label>{' '}
                        {InventoryView.vehiclefeatures && InventoryView?.vehiclefeatures[0]?.reverseingCamera?<span className="fa fa-check-circle" style={{color:'green'}}></span>:<span className="fa fa-times-circle" style={{color:'red'}}></span>}
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                        <strong>SMART KEY: </strong>
                        </label>{' '}
                        {InventoryView.vehiclefeatures && InventoryView?.vehiclefeatures[0]?.smartkey?<span className="fa fa-check-circle" style={{color:'green'}}></span>:<span className="fa fa-times-circle" style={{color:'red'}}></span>}
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                        <strong>SPOT LIGHTS: </strong>
                        </label>{' '}
                        {InventoryView.vehiclefeatures && InventoryView?.vehiclefeatures[0]?.spotLights?<span className="fa fa-check-circle" style={{color:'green'}}></span>:<span className="fa fa-times-circle" style={{color:'red'}}></span>}
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                        <strong>TURBO: </strong>
                        </label>{' '}
                        {InventoryView.vehiclefeatures && InventoryView?.vehiclefeatures[0]?.turbo?<span className="fa fa-check-circle" style={{color:'green'}}></span>:<span className="fa fa-times-circle" style={{color:'red'}}></span>}
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                        <strong>TINTED WINDOWS: </strong>
                        </label>{' '}
                        {InventoryView.vehiclefeatures && InventoryView?.vehiclefeatures[0]?.tintedWindows?<span className="fa fa-check-circle" style={{color:'green'}}></span>:<span className="fa fa-times-circle" style={{color:'red'}}></span>}
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                        <strong>TOBER: </strong>
                        </label>{' '}
                        {InventoryView.vehiclefeatures && InventoryView?.vehiclefeatures[0]?.tober?<span className="fa fa-check-circle" style={{color:'green'}}></span>:<span className="fa fa-times-circle" style={{color:'red'}}></span>}
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                        <strong>TRACTION CONTROL: </strong>
                        </label>{' '}
                        {InventoryView.vehiclefeatures && InventoryView?.vehiclefeatures[0]?.tractionControl?<span className="fa fa-check-circle" style={{color:'green'}}></span>:<span className="fa fa-times-circle" style={{color:'red'}}></span>}
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                        <strong>ADAPTIVE CURLSE GEER: </strong>
                        </label>{' '}
                        {InventoryView.vehiclefeatures && InventoryView?.vehiclefeatures[0]?.adaptiveCurlseGeer?<span className="fa fa-check-circle" style={{color:'green'}}></span>:<span className="fa fa-times-circle" style={{color:'red'}}></span>}
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                        <strong>XENON HEADLIGHT: </strong>
                        </label>{' '}
                        {InventoryView.vehiclefeatures && InventoryView?.vehiclefeatures[0]?.xenonHeadLight?<span className="fa fa-check-circle" style={{color:'green'}}></span>:<span className="fa fa-times-circle" style={{color:'red'}}></span>}
                      </div>
                     </div>
                    </CardBody>
                  </Card>
                  </div>
             </div>
             <div className='row'>
             <div className='col-12'>
                    <Card>
                    <CardBody>
                    <h4>Description :</h4>
                    <div className='form-row'>
                      <div className='col-12 form-group'>
                        <span>{InventoryView?.description}</span>
                      </div>
                     </div>
                    </CardBody>
                  </Card>
                  </div>
             </div>
        </ModalBody>
      </Modal>
    )
}

export default InventoryModal
