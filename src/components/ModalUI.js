import {Modal,Button} from 'react-bootstrap';
import React from 'react';

const ModalUI = (props) =>{
    return(
        <>
        <Modal show={props.show} onHide={props.handleClose}
        >
        <Modal.Header closeButton>
          <Modal.Title>{Modal.title}</Modal.Title>
        </Modal.Header>
        <Modal.Body>{props.children}</Modal.Body>
        <Modal.Footer>
       
        </Modal.Footer>
      </Modal>
        </>
    )
}

export default ModalUI;