import React from 'react'
import Instance from '../../Instance'
import { useHistory } from 'react-router-dom'

const AuthUser = ({ children }) => {
    const history = useHistory()
    const [isLoading, setLoading] = React.useState(true)
    React.useEffect(() => {
        let token = localStorage.getItem('token$')
        Instance.get(`/api/admin/getProfileDetails`, {
            headers: { authorization: `Bearer ${token ? token : ''}` },
        }).then((data) => {
            setLoading(false)
        }).catch((err) => {
            localStorage.removeItem("token$")
            history.push('/')
            setLoading(false)
            console.log('err', err)
        })
    }, [])
    return <>{isLoading ?
        <div className='col-12'>
            <div className='d-flex justify-content-center mt-5'>
                <div className='spinner-border' role='status'>
                    <span className='sr-only'>Loading...</span>
                </div>
            </div>
        </div> : children}</>
}

export default AuthUser
