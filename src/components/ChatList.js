/** @format */

import React, { useContext, useEffect, useState } from 'react';
import {
  Card,
  CardBody,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Row,
  Col,
  Media,
  UncontrolledButtonDropdown,
  CardHeader,
  Modal,
  ModalHeader,
  ModalBody,
} from 'reactstrap';
import socketIOClient from 'socket.io-client';
import SocketIOFileClient from 'socket.io-file-client';
import PerfectScrollbar from 'react-perfect-scrollbar';
import 'react-perfect-scrollbar/dist/css/styles.css';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import Instance from './../Instance';
import { useAlert } from 'react-alert';
import Context2 from '../routes/Context';
import config from '../config';
import moment from 'moment';
import { saveAs } from 'file-saver';
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';

/* Chat Item Avatar */

/* Chat Item Text */
let socket;
/* Chat Item */
const chatItemDefaultProps = {
  placement: '',
  children: PropTypes.object,
  className: '',
};

const ChatItem = ({ children, placement, className }) => {
  return (
    <li
      className={classNames(
        'clearfix',
        { odd: placement === 'left' },
        className
      )}>
      {children}
    </li>
  );
};

ChatItem.defaultProps = chatItemDefaultProps;

/**
 * Renders the ChatList
 */

const ChatList = (props) => {
  const height = props.height || '320px';
  const alert = useAlert();
  const { employee } = useContext(Context2);

  const [view, setView] = useState(false);
  const [roomName, setRoomName] = useState({
    profile_id: '',
    user: '',
    room: 1,
  });

  const [scrollEl, setScrollEl] = useState();

  const [profileview, setProfileView] = useState(null);
  const [loader, setLoader] = useState(false);


  const [message, setMessage] = useState('');
  const list = [];

  const ENDPOINT = 'http://localhost:5001';
  const [io, setIO] = React.useState('');
  const [fileIo, setFileIO] = React.useState('');
  const [messageShow, setMessageShow] = useState([]);

  const [modalOpen, setModalOpen] = useState(false);
  const [modalView, setModalView] = useState('');
 
  const modalToggle = (value) => {
    setModalOpen(!modalOpen);
    setModalView(value);
  };

  const disconnectChat=()=>{
    if(io){
      io.disconnect()
    }
  }

   //auto Suggest
  const [dealersList,setDealers]=useState([])
  const [currentDealer,setCurrentDealer]=useState({})

 

  //Getall Dealers
  useEffect(()=>{
    Instance.get(`/api/admin/getAlldealersList`,{
      headers: { authorization: `Bearer ${localStorage.getItem('token$')}` }
    }).then(async({ data }) => {
          if(data?.success && data?.dealers && data?.dealers.length>0){
            // console.log("data?.success",data?.dealers)
            setDealers(data?.dealers)
            //Get Initial Chats Data
            LoadChats(data?.dealers[0])
          }
      }).catch((err) => {
        console.log('err', err);
      });
  },[])

  const onChatSocketChange = (currDealerId) => {
    disconnectChat()
  
    if (currDealerId && currDealerId!="") {
       const socket = socketIOClient(ENDPOINT)
      // React.useEffect(()=>{
      const uploader = new SocketIOFileClient(socket);
      setFileIO(uploader)
      setIO(socket)
      const { _id, role } = employee
      // let profilepic= "https://img.freepik.com/free-vector/businessman-character-avatar-isolated_24877-60111.jpg?size=192&ext=jpg"

      let obj = {
        sender: {
          senderId: _id,
          role: role,
        },
        dealerId: currDealerId,
        message: message,
        dateTime: moment().format("hh:mm a L")
      }
      //joinRoom2 is for admin dealer chat
      socket.emit('joinRoom2', obj)


      //message2 is for admin dealer chat
      // Message from server
      socket.on('message2', msg => {
        setMessageShow((oldArr) => [...oldArr, msg])
        // console.log('message2',msg)
        scrollBottom()
      });
    }
  }


  const LoadChats=(dealer)=>{
    if(dealer){
    setCurrentDealer(dealer)
    setMessageShow([])
    setLoader(true)
    Instance.get(`/api/admin/chats/${dealer?._id}`,{
      headers: { authorization: `Bearer ${localStorage.getItem('token$')}` }
    }).then(async({ data }) => {
        // console.log('messegeData', data);
        setLoader(false)
       await setMessageShow(data?.chats)
        onChatSocketChange(dealer?._id)
      })
      .catch((err) => {
        setLoader(false)
        console.log('err', err);
      });
    }
  }

  // useEffect(() => {
  //   let DATA = JSON.parse(localStorage.getItem('data$'));
  //   let profile_id;
  //   let username;
  //   let room;
  //   if (DATA) {
  //     setView(true);
  //     profile_id = DATA?.profile_id;
  //     username = DATA?.user;
  //     room = DATA?.room;
  //   } else {
  //     setView(false);
  //     profile_id = roomName?.profile_id;
  //     username = roomName?.user;
  //     room = roomName?.room;
  //   }

  //   // const { profile_id, username, room } = roomName;
  //   socket.emit('joinRoom', { profile_id, username, room });
  // }, [roomName, socket]);

  // useEffect(() => {
  //   socket.on('message', (msg) => {
  //     setMessageShow([...messageShow, msg]);
  //   });

  //   return () => {
  //     socket.off();
  //   };
  // }, [messageShow, socket]);

 const scrollBottom=()=>{
    if (scrollEl) {
      if(messageShow?.length>1){
      scrollEl.scrollTop = scrollEl?.scrollHeight;
      }
    }
 }

 useEffect(()=>{
  scrollBottom()
},[messageShow?.length])

  useEffect(() => {
    
    // Instance.get(`/api/user/view`, {
    //   headers: { authorization: `Bearer ${localStorage.getItem('token$')}` },
    // })
    //   .then(({ data }) => {
    //     console.log('profileData', data);
        setProfileView(employee?._id);
        // console.log("AdminId",employee?._id)
      // })
      // .catch((err) => {
      //   console.log('err', err);
      // });
  }, []);

  // const LogOut = (msg) => {
  //   localStorage.removeItem('data$');
  //   setView(false);
  //   alert.success('You have successfully Log Out');
  // };

  const Click = (e) => {
    e.preventDefault();
    if (io) {
      const { _id, role } = employee
      const msg = {
        sender: {
          senderId: _id,
          role: role
        },
        message: message,
        dateTime: moment().format("hh:mm a L")
      }
      // console.log('Your message', msg)

      io.emit('sendMessage2', (msg))
    }
    setMessage('')
  }

  const fileHandler = () => {
    var fileEl = document.getElementById('Photo');
    if (fileIo) {
      fileIo.upload(fileEl, {
        data: {
          /* Arbitrary data... */
        },
      });
    }
  };

  const getFileExt=(filename)=>(filename.substring(filename.lastIndexOf('.')+1, filename.length) || filename+"").toLowerCase() 

  return (
    <>
     <Modal isOpen={modalOpen} toggle={modalToggle} centered={true} style={{display:'block',marginRight:'auto',marginLeft:'auto',width:800}}>
        <ModalHeader toggle={modalToggle}>Image View</ModalHeader>
        <ModalBody>
          <img src={`${config.baseImgUrl}/chatImages/${modalView}`} style={{width:1000}} className="img-fluid" /> 
        </ModalBody>
      </Modal>
      <Card>
      <CardHeader>
      {/* <UncontrolledDropdown className='mt-2 float-right'>
            <DropdownToggle
              tag='button'
              className='btn btn-link arrow-none p-0 text-muted'>
              <i className='uil uil-ellipsis-v'></i>
            </DropdownToggle>
            <DropdownMenu right>
              <DropdownItem>
                <i className='uil uil-edit-alt mr-2'></i>Add
              </DropdownItem>
              <DropdownItem>
                <i className='uil uil-edit-alt mr-2'></i>Add
              </DropdownItem>
              <DropdownItem>
                <i className='uil uil-edit-alt mr-2'></i>Add
              </DropdownItem>
              <DropdownItem>
                <i className='uil uil-edit-alt mr-2'></i>Add
              </DropdownItem>
              <DropdownItem>
                <i className='uil uil-edit-alt mr-2'></i>Add
              </DropdownItem>
              <DropdownItem>
              <i className='uil uil-exit mr-2'></i>Remove from Team
            </DropdownItem>
            <DropdownItem divider />
            <DropdownItem className='text-danger'>
              <i className='uil uil-trash mr-2'></i>Delete
            </DropdownItem>
            </DropdownMenu>
          </UncontrolledDropdown> */}

          <div className='row '>
            <div className='col-md-8 text-left'>
              <span className='mb-2 header-title'> <i className="fa fa-circle text-success"></i> Chat</span>
             
            </div>
            <div className='col-md-4 text-right' style={{ fontSize: '25px' }}>
            <i title="close" style={{cursor:'pointer'}} className="fa fa-window-close" onClick={()=>{props?.chatToggle();disconnectChat()}}></i>
            {/* <i
              class='fa fa-sign-out'
              aria-hidden='true'
              style={{ display: view === false ? 'none' : 'block' }}
              onClick={LogOut}></i> */}
          </div>
          </div>
          <div className="row">
          {dealersList?.length>0 && 
            <Autocomplete
                id="combo-box-demo"
                options={dealersList}
                getOptionSelected={(option, value) => option._id === value._id}
                getOptionLabel={(option) => option.firstName}
                value={currentDealer}
                onChange={(e,v)=>LoadChats(v)}
                color="secondary"
                renderOption={(option) => (
                  <React.Fragment>
                  <div className='media user-profile '>
                    <img
                    style={{width:32,height:32}}
                      src={`${config.baseImgUrl}/profile/${option?.img}`}
                      alt=''
                      className='rounded-circle align-self-center'
                    />
                    <div className='media-body text-left'>
                      <h6 className='pro-user-name ml-2 my-0'>
                        <span>
                          {option?.firstName} {option?.lastName}
                        </span>
                        <span className='pro-user-desc text-muted d-block mt-1'>
                          {option?.email}
                        </span>
                      </h6>
                    </div>
                  </div>
                  </React.Fragment>
                )}
                size="small"
                style={{ width: 270 }}
                renderInput={(params) => <TextField {...params} label="Dealer" variant="outlined" color="secondary" fullWidth />}
              />
            }
          </div>
      </CardHeader>
        <CardBody className='pt-2 pb-1'>
        
          <div className='chat-conversation'>
           
            <PerfectScrollbar
            style={{ maxHeight: height, width: '100%' ,marginBottom:'10px'}}
            containerRef={ref => {
                setScrollEl(ref);
              }}
            id="listScroll"
            >
              <ul className={classNames('conversation-list', props.className)}>
              {loader?<>
                      <div class='text-center' style={{position: 'absolute',bottom:0}}>
                        <div class='spinner-border' role='status'>
                          <span class='sr-only'>Loading...</span>
                        </div>{" "}
                        <span>Getting all messages...</span>
                      </div>{' '}
                    </>
                :
                messageShow &&
                messageShow.map((value, i) => (
                  <div key={i} className={value?.senderDetails?._id===profileview?'odd':''}>
                  {
                    <div className='chat-avatar'>
                    {
                      value?.senderDetails.img?<img src={`${config.baseImgUrl}/profile/${value?.senderDetails?.img}`} alt="Dealer" />:
                      <img src={`${config.baseImgUrl}/SuperAdminProfile/${value?.senderDetails?.profile}`} alt="Admin" />
                    
                    }
                    </div>
                  }
                  <div className='conversation-text mt-2'>
                    <div
                    className={value?.senderDetails?._id === profileview? 'ctext-wrap right': 'left ctext-wrap'}
                    >
                        <strong><p>{(value?.senderDetails?._id === profileview)?'You':value?.senderDetails?.firstName}</p></strong>
                        <p>{value?.message}</p>
                        {value?.file?<div>
                        {(["webp","jpg", "jpeg", "bmp", "gif", "png"].includes(getFileExt(value?.file)))?
                        <>
                        <img src={`${config.baseImgUrl}/chatImages/${value?.file}`} className="img-fluid"/>
                        <br/><br/>
                        <span onClick={async()=>await saveAs(`${config.baseImgUrl}/chatImages/${value?.file}`,value?.file)} className="btn btn-xs btn-success">Download </span>{" "}
                        <span onClick={()=>modalToggle(value?.file)} className="btn btn-xs btn-info">View</span>
                        </>
                        :<span onClick={async()=>await saveAs(`${config.baseImgUrl}/chatImages/${value?.file}`,value?.file)} className="btn btn-xs btn-success"> File Download  </span>}
                        </div>:null}
                        <span>{value.dateTime}</span>
                    </div>
                  </div>
                </div>
                ))}
                {/* </ChatItem> */}
              
              </ul>
            
            </PerfectScrollbar>
          
            <form onSubmit={(e)=>e.preventDefault()}>
            <Row form>
            
              <Col>
                <input
                  className='form-control'
                  placeholder="Type a message here..."
                  value={message}
                  onChange={(e) => setMessage(e.target.value)}
                ></input>
              </Col>
              <Col className='col-auto'>
                <label htmlFor='Photo' style={{ fontSize: '25px' }}>
                  <i class='fa fa-plus-square mt-2' aria-hidden='true'></i>
                </label>

                <input
                  type='file'
                  id='Photo'
                  className='form-control'
                  onChange={fileHandler}
                  hidden
                  multiple></input>
              </Col>
              <Col className='col-auto'>
                <button
                  type='submit'
                  className='btn btn-danger chat-send btn-block'
                  onClick={Click}
                >
                  Send
                </button>
              </Col>
            </Row>
            </form>
          </div>

          {/* <div style={{ display: view === true ? 'none' : 'block' }}>
          <h5 className='mb-3 header-title'>Chat System</h5>
          <div className='form-group'>
            <label htmlFor='exampleInputEmail1'>Email address</label>
            <input
              type='email'
              className='form-control'
              aria-describedby='emailHelp'
              id='user'
              onChange={handelChange}
            />
          </div>
          <div className='form-group'>
            <label htmlFor='exampleInputEmail2'>Phone Number</label>
            <input
              type='number'
              className='form-control'
              aria-describedby='emailHelp'
              id='profile_id'
              onChange={handelChange}
            />
          </div>
          <button type='submit' class='btn btn-primary' onClick={Submit}>
            Submit
          </button>
        </div> */}
        
        </CardBody>
      </Card>

     
    </>
  );
};

export default ChatList;
