/** @format */

import React, { useState, useRef, useEffect, useContext } from 'react';
import { Link } from 'react-router-dom';
//import profilePic from '../../assets/images/users/avatar-7.jpg';
//import avatar1 from '../../assets/images/users/avatar-1.jpg';
import './topbar.css';
import DealerLogo from '../../assets/Dealer_Hero.png';
import { useAlert } from 'react-alert';
import NavToggleContext from './Context/Context';
import {
  UncontrolledDropdown,
  DropdownMenu,
  DropdownToggle,
  DropdownItem,
} from 'reactstrap';
import * as FeatherIcon from 'react-feather';
import { useHistory,useLocation  } from 'react-router-dom';
import Context2 from '../../routes/Context';
import env from "react-dotenv";
import Instance from '../../Instance';
import moment from 'moment'
import config from '../../config';

const Topbar = () => {
 
  let history = useHistory();
  let location  = useLocation();
  const alert = useAlert();
  const { employee } = useContext(Context2);
  const { btnToggle, setBtnToggle } = useContext(NavToggleContext);
  const toggleDropdown2 = () => {
    document.getElementById('yutube').classList.remove('show');
  };

  const toggleDropdown3 = () => {
    document.getElementById('notify').classList.remove('show');
  };

  const [notifications,setNotifications] =React.useState([])
  const [unreaded,setUnreaded] =React.useState(false)

  const HandleLogout = () => {
    localStorage.removeItem('token$');
    alert.success('You have successfully Log Out');
    return history.push('/');
  };

  const ShowNav = () => {
    setBtnToggle(!btnToggle);
  };

  useEffect(()=>{
    Instance.get('/api/admin/notificationslastfive',{
      headers: { authorization: `Bearer ${localStorage.getItem('token$')}` },
    }).then(({data})=>{
      // console.log('notification',data)
      setNotifications(data?.notifications)
      if(data?.notifications && data?.notifications.length>0){
       setUnreaded(true)
      }else{
        setUnreaded(false)
      }
    }).catch((e)=>console.log('err',e))
  },[location])

  useEffect(() => {
    // console.log('YYRREE', document.getElementById('lol').style.display);

    if (btnToggle) {
      document.getElementById('lol').style.display = 'block';
    } else if (
      document.getElementById('lol').style.display != '' &&
      !btnToggle
    ) {
      document.getElementById('lol').style.display = 'none';
    }
    // switch (document.getElementById('lol').style.display) {
    //   case 'block':
    //     document.getElementById('lol').style.display = 'none';
    //     break;
    //   case 'none':
    //     document.getElementById('lol').style.display = 'block';
    //     break;
    // }
  }, [btnToggle]);

  const clearAllNotify=()=>{
    Instance.put(`/api/admin/clearAllNotify`,{},{
      headers: { authorization: `Bearer ${localStorage.getItem('token$')}` },
    }).then(({data})=>{
      // console.log('data',data)

      if(data?.success){
        alert.success(data?.message);
        setNotifications([])
        setUnreaded(false)
        toggleDropdown3()
      }
    }).catch((e)=>console.log(e))
  }

  const handleNotify=(value)=>{
    Instance.put(`/api/admin/notification/status/${value._id}`,{},{
      headers: { authorization: `Bearer ${localStorage.getItem('token$')}` },
    }).then(()=>{
      // console.log('okoko')
      toggleDropdown3()
      history.push(`${value.webRedirectUrl}`)
    }).catch((e)=>console.log(e))
  }

  return (
    <>
      {/* Topbar Start */}
      <div className='navbar navbar-expand flex-column flex-md-row navbar-custom' style={{zIndex:1}}>
        <div className='container-fluid'>
          {/* LOGO */}
          <Link to='/' className='navbar-brand mr-0 mr-md-2 logo'>
            <span className='logo-lg'>
              <img src={DealerLogo} alt='' height={50} />

              {/* <span className='d-inline h5 ml-1 text-logo'>
                <div class='dropdown'>
                  <span
                    class='btn btn-secondary dropdown-toggle'
                    type='button'
                    id='dropdownMenuButton'
                    data-toggle='dropdown'
                    aria-haspopup='true'
                    aria-expanded='false'>
                    Yard
                  </span>
                  <div
                    class='dropdown-menu'
                    aria-labelledby='dropdownMenuButton'>
                    <a class='dropdown-item' href='#'>
                      Action
                    </a>
                    <a class='dropdown-item' href='#'>
                      Another action
                    </a>
                    <a class='dropdown-item' href='#'>
                      Something else here
                    </a>
                  </div>
                </div>
              </span> */}
            </span>
            {/* <span className='logo-sm'>
              <img src={logo} alt='' height={24} />
            </span> */}
          </Link>

          <ul className='navbar-nav bd-navbar-nav flex-row list-unstyled menu-left mb-0'>
            <li
              className='d-flex Logo_Header_Show'
              style={{ placeItems: 'center' }}>
              <img src={DealerLogo} alt='' height={50} />
              <button className='button-menu-mobile open-left disable-btn'>
                <i
                  className='fa fa-bars'
                  style={{ marginLeft: '-50px' }}
                  aria-hidden='true'
                  onClick={ShowNav}
                />
              </button>
            </li>
          </ul>

          <ul className='navbar-nav flex-row ml-auto d-flex list-unstyled topnav-menu float-right mb-0'>
            <li className='dropdown notification-list'
              title={`${notifications?.length} new unread notifications`}>
              <UncontrolledDropdown className='dropdown notification-list align-self-center profile-dropdown'>

                <DropdownToggle
                  tag='a'
                  // data-toggle='dropdown'
                  className='nav-link dropdown-toggle'>
                  
                  {unreaded?<span className='noti-icon-badge'></span>:null}
                  <FeatherIcon.Bell />
                  
                </DropdownToggle>

                <DropdownMenu id="notify" className='dropdown-menu dropdown-menu-right dropdown-lg'>
                  <div className='dropdown-item noti-title border-bottom'>
                    <h5 className='m-0 font-size-16'>
                    {unreaded?(
                      <span className='float-right'>
                        <a href className='text-dark' style={{cursor:'pointer'}}>
                          <small onClick={clearAllNotify}>Clear All</small>
                        </a>
                      </span>
                    ):null}
                      Notification
                    </h5>
                  </div>

                  <div>
                    <div className='slimscroll noti-scroll'>
                    {
                      notifications && notifications.map((n,i)=>(
                        <span style={{cursor:'pointer'}} key={i} onClick={()=>handleNotify(n)} className='dropdown-item notify-item border-bottom'>
                        <>
                        <div className='notify-icon bg-primary'>
                          <i className='uil uil-user-plus' />
                        </div>
                        <p className='notify-details'>
                          {n?.title}.
                          <small className='text-muted'>{moment(n?.createdAt).fromNow()}</small>
                        </p>
                        </>
                      </span>
                      ))
                    }
                    </div>
                    {/* All*/}
                    <Link
                      to='/notification'
                      onClick={()=>toggleDropdown3()}
                      className='dropdown-item text-center text-primary notify-item notify-all border-top'>
                      View all
                      <i className='fi-arrow-right' />
                    </Link>
                  </div>
                </DropdownMenu>
              </UncontrolledDropdown>
            </li>

            <li className='dropdown'>
              <UncontrolledDropdown className='dropdown notification-list align-self-center profile-dropdown'>
                <DropdownToggle
                  tag='a'
                  // data-toggle='dropdown'
                  className='nav-link dropdown-toggle nav-user mr-0'>
                  <div className='media user-profile '>
                    <img
                      src={`${config.baseImgUrl}/SuperAdminProfile/${employee?.profile}`}
                      alt=''
                      className='rounded-circle align-self-center'
                    />
                    <div className='media-body text-left'>
                      <h6 className='pro-user-name ml-2 my-0'>
                        <span>
                          {employee?.firstName} {employee?.lastName}
                        </span>
                        <span className='pro-user-desc text-muted d-block mt-1'>
                          Admin
                        </span>
                      </h6>
                    </div>
                    <span
                      data-feather='chevron-down'
                      className='ml-2 align-self-center'
                    />
                  </div>
                </DropdownToggle>

                <DropdownMenu
                  id='yutube'
                  right
                  className='dropdown-menu profile-dropdown-items dropdown-menu-right'>
                  <Link
                    to='/myprofile'
                    onClick={toggleDropdown2}
                    className='dropdown-item notify-item'>
                    <FeatherIcon.User className='icon-dual icon-xs mr-2' />
                    <span>My Account</span>
                  </Link>
                  <Link
                    to='/'
                    onClick={toggleDropdown2}
                    className='dropdown-item notify-item'>
                    <FeatherIcon.Gift className='icon-dual icon-xs mr-2' />
                    <span>Billing</span>
                  </Link>

                  <DropdownItem divider />
                  <Link to='#' onClick={HandleLogout} className='dropdown-item notify-item'>
                    <FeatherIcon.LogOut className='icon-dual icon-xs mr-2' />
                    <span>Logout</span>
                  </Link>
                </DropdownMenu>
              </UncontrolledDropdown>
            </li>
          </ul>
        </div>
      </div>
      {/* end Topbar */}
    </>
  );
};

export default Topbar;
