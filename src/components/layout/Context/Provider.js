/** @format */

import React, { useState } from 'react';
import Context from './Context';

export default function NavToggleProvider(props) {
  const [btnToggle, setBtnToggle] = useState(false);

  return (
    <Context.Provider
      value={{
        btnToggle,
        setBtnToggle,
      }}>
      {props.children}
    </Context.Provider>
  );
}
