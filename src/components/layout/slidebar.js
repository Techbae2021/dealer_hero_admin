/** @format */

import React, { useContext, useEffect } from 'react';
//import { Link } from 'react-router-dom';
import Navbar from './navbar';
import './topbar.css';
import NavToggleContext from './Context/Context';

//import avatar7 from '../../assets/images/users/avatar-7.jpg';

// import {
//   UncontrolledDropdown,
//   DropdownMenu,
//   DropdownToggle,
//   DropdownItem,
// } from 'reactstrap';
// import * as FeatherIcon from 'react-feather';

const Sidebar = () => {
  const { btnToggle, setBtnToggle } = useContext(NavToggleContext);

  useEffect(() => {
    // console.log('YYRREE');
    switch (document.getElementById('lol').style.display) {
      case 'block':
        document.getElementById('lol').style.display = 'none';
        break;
      case 'none':
        document.getElementById('lol').style.display = 'block';
        break;
    }
  }, []);
  return (
    <>
      {/* ========== Left Sidebar Start ========== */}
      <div id='lol' className='left-side-menu'>
        {/* <div className='media user-profile mt-2 mb-2'>
          <img
            src={avatar7}
            className='avatar-sm rounded-circle mr-2'
            alt='Shreyu'
          />
          <img
            src={avatar7}
            className='avatar-xs rounded-circle mr-2'
            alt='Shreyu'
          />
          <div className='media-body'>
            <h6 className='pro-user-name mt-0 mb-0'>Nik Patel</h6>
            <span className='pro-user-desc'>Administrator</span>
          </div>
          <UncontrolledDropdown className='align-self-center profile-dropdown-menu'>
            <DropdownToggle
              data-toggle='dropdown'
              tag='button'
              className='btn btn-link p-0 dropdown-toggle mr-0'>
              <FeatherIcon.ChevronDown />
            </DropdownToggle>
            <DropdownMenu
              right
              className='topbar-dropdown-menu profile-dropdown-items'>
              <Link to='/' className='dropdown-item notify-item'>
                <FeatherIcon.User className='icon-dual icon-xs mr-2' />
                <span>My Account</span>
              </Link>
              <Link to='/' className='dropdown-item notify-item'>
                <FeatherIcon.Settings className='icon-dual icon-xs mr-2' />
                <span>Billing</span>
              </Link>

              <DropdownItem divider />
              <Link to='/account/logout' className='dropdown-item notify-item'>
                <FeatherIcon.LogOut className='icon-dual icon-xs mr-2' />
                <span>Logout</span>
              </Link>
            </DropdownMenu>
          </UncontrolledDropdown>
        </div> */}
        <div className='sidebar-content' style={{ overflow: 'auto' }}>
          {/*- Sidemenu */}
          <div id='sidebar-menu' className='slimscroll-menu'>
            <Navbar />
          </div>
          {/* End Sidebar */}
          {/* <div className='clearfix' /> */}
        </div>
        {/* Sidebar -left */}
      </div>
      {/* Left Sidebar End */}
    </>
  );
};

export default Sidebar;
