/** @format */

import React from 'react';
import Header from './topbar';
import Sidebar from './slidebar';
import Footer from './footer';

import { matchPath, useLocation } from 'react-router-dom';

const Layout = ({ children }) => {
  const location = useLocation();

  if (location.pathname === '/') {
    return (
      <>
        <main>{children}</main>
      </>
    );
  }

  if (location.pathname === '/signup') {
    return (
      <>
        <main>{children}</main>
      </>
    );
  }
  if (location.pathname === '/forgotpassword') {
    return (
      <>
        <main>{children}</main>
      </>
    );
  }

  if (matchPath(location.pathname, { path: '/updatepassword/:id' })) {
    return (
      <>
        <main>{children}</main>
      </>
    );
  }

  if (location.pathname === '/broker/login') {
    return (
      <>
        <main>{children}</main>
      </>
    );
  }
  if (location.pathname === '/broker/forgotpassword') {
    return (
      <>
        <main>{children}</main>
      </>
    );
  }
  if (location.pathname === '/broker/dashboard') {
    return (
      <>
        <main>{children}</main>
      </>
    );
  }
  if (location.pathname === '/broker/finance') {
    return (
      <>
        <main>{children}</main>
      </>
    );
  }
  if (location.pathname === '/broker/details') {
    return (
      <>
        <main>{children}</main>
      </>
    );
  }
  if (location.pathname === '/broker/forgot/password') {
    return (
      <>
        <main>{children}</main>
      </>
    );
  }
  if (location.pathname === '/broker/profile') {
    return (
      <>
        <main>{children}</main>
      </>
    );
  }
  if (location.pathname === '/broker/change/password') {
    return (
      <>
        <main>{children}</main>
      </>
    );
  }
  if (matchPath(location.pathname, { path: '/broker/update/password' })) {
    return (
      <>
        <main>{children}</main>
      </>
    );
  }

  if (location.pathname === '/emp/login') {
    return (
      <>
        <main>{children}</main>
      </>
    );
  }
  if (location.pathname === '/emp/forgotpassword') {
    return (
      <>
        <main>{children}</main>
      </>
    );
  }
  if (matchPath(location.pathname, { path: '/emp/updatepassword' })) {
    return (
      <>
        <main>{children}</main>
      </>
    );
  }
  return (
    <>
      <Header />
      <Sidebar />
      <div className='content-page'>
        <div className='content'>
          <div className='container-fluid'>
            <main>{children}</main>
          </div>
        </div>
      </div>

      <Footer />
    </>
  );
};

export default Layout;
