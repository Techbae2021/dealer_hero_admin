/** @format */

import React from 'react';
import { Link } from 'react-router-dom';

const footer = () => {
  return (
    <>
      {/* Footer Start */}
      <footer className='footer'>
        <div className='container-fluid'>
          <div className='row'>
            <div className='col-12'>
              2021 © Dealer Hero. All Rights Reserved. Design by
              <i className='uil uil-heart text-danger font-size-12' />{' '}
              <Link href='techbae.com.au'>Techbae Team</Link>
            </div>
          </div>
        </div>
      </footer>
      {/* end Footer */}
    </>
  );
};

export default footer;
