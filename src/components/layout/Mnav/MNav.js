/** @format */

import { useRef, useEffect, useContext } from 'react';

import NavToggleContext from '../Context/Context';

// this is for outside click -> aoutmatically disapper
export const useSideToggleRef = () => {
  const ref = useRef(null);

  // this is context state toggle, use can use useState for ref lik -> const [visible , setVisible] = useState(false)
  const { btnToggle, setBtnToggle } = useContext(NavToggleContext);

  useEffect(() => {
    // this is to find when documnet will click a event called handleClick will trigger
    document.addEventListener('click', handleClick, true);

    // this is for clean up useEffect
    return () => {
      document.addEventListener('click', handleClick, true);
    };
  }, [ref]);

  const handleClick = (e) => {
    // this is to check if ref is a sidebar for example or other div
    if (ref.current && !ref.current.contains(e.target)) {
      // !ref.current.contains(e.target) ===>  that ensures that the clicked div is toogle div or outsider div
      setBtnToggle(false);
    }
  };

  return { btnToggle, setBtnToggle, ref };
};
