/** @format */

import React, { useState, useContext } from 'react';
import chart from '../../assets/chart-line-solid.svg';
import lead from '../../assets/users-solid.svg';
import inventory from '../../assets/car-solid.svg';
// import deal from '../../assets/notes-medical-solid.svg';
// import rental from '../../assets/retweet-solid.svg';
// import finance from '../../assets/money-bill-wave-solid.svg';
import customer from '../../assets/user-solid.svg';
// import test from '../../assets/clipboard-regular.svg';
import account from '../../assets/wallet-solid.svg';
// import action from '../../assets/car-alt-solid.svg';
import shop from '../../assets/shopping-cart-solid.svg';
import pipeline from '../../assets/bars-solid.svg';
// import emp from '../../assets/user-plus-solid.svg';
import email from '../../assets/mobile-alt-solid.svg';
// import reports from '../../assets/clipboard-regular.svg';
import setting from '../../assets/user-cog-solid.svg';
import { Link } from 'react-router-dom';
import MetisMenu from '@metismenu/react';
import './topbar.css';

const Navbar = () => {
  const [iconspin, SetIconSpin] = useState({
    dashboardSpin: false,
    leadSpin: false,
    inventorySpin: false,
    dealSpin: false,
    rentalSpin: false,
    financeSpin: false,
    customerSpin: false,
    accountSpin: false,
    // auctionSpin: false,
    shopSpin: false,
    pipelineSpin: false,
    empSpin: false,
    emailSpin: false,
    reportSpin: false,
    settingSpin: false,
    dealerCustomerSpin: false,
    // testSpin: false,
  });

  return (
    <>
      <React.Fragment>
        {/* <PerfectScrollbar> */}
        <MetisMenu>
          {/* TODO */}
          {/* <li className='menu-title'>Navigation</li> */}

          <li
            className='mt-2'
            onMouseOver={() => {
              SetIconSpin({ dashboardSpin: true });
            }}
            onMouseLeave={() => {
              SetIconSpin({ dashboardSpin: false });
            }}>
            <Link exact to='/dashboard'>
              <span className='badge badge-success float-right'></span>

              <img
                src={chart}
                style={{ height: '1rem', width: '1rem' }}
                alt='Chart'
                className={iconspin.dashboardSpin === true ? 'fa-spin' : ''}
              />
              <span> DASHBOARD </span>
            </Link>
          </li>

          <li
            onMouseOver={() => {
              SetIconSpin({ leadSpin: true });
            }}
            onMouseLeave={() => {
              SetIconSpin({ leadSpin: false });
            }}>
            <Link to='/leads'>
              {' '}
              <img
                src={lead}
                style={{ height: '1rem', width: '1rem' }}
                alt='lead'
                className={iconspin.leadSpin === true ? 'fa-spin' : ''}
              />
              <span> LEAD </span>
            </Link>
          </li>
          <li
            onMouseOver={() => {
              SetIconSpin({ customerSpin: true });
            }}
            onMouseLeave={() => {
              SetIconSpin({ customerSpin: false });
            }}>
            <Link to='/dealer'>
              {' '}
              <img
                src={customer}
                style={{ height: '1rem', width: '1rem' }}
                alt='lead'
                className={iconspin.customerSpin === true ? 'fa-spin' : ''}
              />
              <span> DEALER </span>
            </Link>
          </li>
          <li
            onMouseOver={() => {
              SetIconSpin({ dealerCustomerSpin: true });
            }}
            onMouseLeave={() => {
              SetIconSpin({ dealerCustomerSpin: false });
            }}>
            <Link to='/customer'>
              {' '}
              <img
                src={customer}
                style={{ height: '1rem', width: '1rem' }}
                alt='lead'
                className={
                  iconspin.dealerCustomerSpin === true ? 'fa-spin' : ''
                }
              />
              <span> CUSTOMER </span>
            </Link>
          </li>

          <li
            onMouseOver={() => {
              SetIconSpin({ accountSpin: true });
            }}
            onMouseLeave={() => {
              SetIconSpin({ accountSpin: false });
            }}>
            <Link to='/payment'>
              {' '}
              <img
                src={account}
                style={{ height: '1rem', width: '1rem' }}
                alt='lead'
                className={iconspin.accountSpin === true ? 'fa-spin' : ''}
              />
              <span> PAYMENT </span>
            </Link>
          </li>

          {/* <li
            onMouseOver={() => {
              SetIconSpin({ auctionSpin: true });
            }}
            onMouseLeave={() => {
              SetIconSpin({ auctionSpin: false });
            }}>
            <Link to='/' className='has-arrow'>
              <img
                src={action}
                style={{ height: '1rem', width: '1rem' }}
                alt='action'
                className={iconspin.auctionSpin === true ? 'fa-spin' : ''}
              />
              <span> AUCTION </span>{' '}
            </Link>
            <ul className='nav-second-level'>
              <li>
                {' '}
                <Link to=''> Auctions</Link>{' '}
              </li>
              <li>
                {' '}
                <Link to=''> Wholesale Vehicles </Link>{' '}
              </li>
              <li>
                {' '}
                <Link to=''> Private Vehicles </Link>{' '}
              </li>
              <li>
                {' '}
                <Link to=''> Stock Locator </Link>{' '}
              </li>
            </ul>
          </li> */}

          <li
            onMouseOver={() => {
              SetIconSpin({ shopSpin: true });
            }}
            onMouseLeave={() => {
              SetIconSpin({ shopSpin: false });
            }}>
            <Link to='/subscription'>
              <img
                src={shop}
                style={{ height: '1rem', width: '1rem' }}
                alt='shop'
                className={iconspin.shopSpin === true ? 'fa-spin' : ''}
              />
              <span> SUBSCRIPTION </span>{' '}
            </Link>
          </li>

          <li
            onMouseOver={() => {
              SetIconSpin({ pipelineSpin: true });
            }}
            onMouseLeave={() => {
              SetIconSpin({ pipelineSpin: false });
            }}>
            <Link to='/pipeline'>
              <img
                src={pipeline}
                style={{ height: '1rem', width: '1rem' }}
                alt='pipeline'
                className={iconspin.pipelineSpin === true ? 'fa-spin' : ''}
              />{' '}
              <span> PIPELINE </span>
            </Link>
          </li>

          <li
            onMouseOver={() => {
              SetIconSpin({ emailSpin: true });
            }}
            onMouseLeave={() => {
              SetIconSpin({ emailSpin: false });
            }}>
            <Link to='/email' className='has-arrow'>
              <img
                src={email}
                style={{ height: '1rem', width: '1rem' }}
                alt='email'
                className={iconspin.emailSpin === true ? 'fa-spin' : ''}
              />
              <span> EMAIL </span>{' '}
            </Link>
            <ul className='nav-second-level'>
              <li>
                {' '}
                <Link to='/email'>Inbox</Link>{' '}
              </li>
            </ul>
          </li>

          <li
            onMouseOver={() => {
              SetIconSpin({ settingSpin: true });
            }}
            onMouseLeave={() => {
              SetIconSpin({ settingSpin: false });
            }}>
            <Link to='/' className='has-arrow'>
              <img
                src={setting}
                style={{ height: '1rem', width: '1rem' }}
                alt='setting'
                className={iconspin.settingSpin === true ? 'fa-spin' : ''}
              />
              <span> SETTING</span>{' '}
            </Link>
            <ul className='nav-second-level'>
              <li>
                <Link to='/setting/header'> UI Setting </Link>{' '}
              </li>
            </ul>
          </li>

          {/* <li>  
              <Link to="/" className="has-arrow"><span> Email </span> </Link>
              <ul className="nav-second-level" >
                  <li>
                      <Link to="">Inbox </Link>
                  </li>
                  <li>
                  <Link to="">Inbox </Link>
                  </li>
                  <li>
                  <Link to="">Inbox </Link>
                  </li>
              </ul>
          </li> */}
        </MetisMenu>
        {/* </PerfectScrollbar> */}
      </React.Fragment>
    </>
  );
};

export default Navbar;
