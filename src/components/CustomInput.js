/** @format */

import React from "react";

const CustomInput = (props) => {
  return (
    <>
    {props.label?<label className="col-form-label">{props.label}</label>:null}
      <input
        type={props.type}
        className={`form-control ${props.error ? "is-invalid" : ""}`}
        name={props.name}
        id={props.id}
        value={props.value}
        onChange={props.handleChange}
        autoComplete='off'
        placeholder={props?.placeholder}
      />
      {props.error && <div className="invalid-feedback">Required</div>}
    </>
  );
};

export default CustomInput
