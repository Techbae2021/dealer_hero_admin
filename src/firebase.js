/** @format */

// /** @format */

import firebase from 'firebase';

// import * as firebase from 'firebase';
import 'firebase/firestore';

const config = {
  apiKey: 'AIzaSyCyBbGR7kAjirwe4ybZcLpynC2ITbiAdio',
  authDomain: 'dealershero-e16bc.firebaseapp.com',
  projectId: 'dealershero-e16bc',
  storageBucket: 'dealershero-e16bc.appspot.com',
  messagingSenderId: '545296404168',
  appId: '1:545296404168:web:8b771910560b67c2c39ffd',
};

export default firebase;
