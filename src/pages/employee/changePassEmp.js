/** @format */

import React, { useState } from 'react';
import { Row, Col } from 'reactstrap';
import { useHistory, useParams } from 'react-router-dom';
import Instance from '../../Instance';
import { useAlert } from 'react-alert';
import CustomInput from '../../components/CustomInput'

const ChangePassEmp = () => {
  let history = useHistory();
  const alert = useAlert();
  const params = useParams();

  const [empDetails, setEmpDetails] = useState({
    oldPassword: '',
    password: '',
    confirmPassword: '',
  });

  const handelChange = (event) => {
    setEmpDetails({
      ...empDetails,
      [event.target.id]: event.target.value,
    });

    if (event.target.value === "") {
      setErrors({ ...errors, [event.target.name]: true });
    } else {
      setErrors({ ...errors, [event.target.name]: false });
    }
  };

  const OnUpdated = () => {
    // e.preventDefault();
    Instance.put(
      `/api/user/employee/changePassword/${params.id}`,
      {
        oldPassword: empDetails.oldPassword,
        password: empDetails.password,
        confirmPassword: empDetails.confirmPassword,
      },
      {
        headers: { authorization: `Bearer ${localStorage.getItem('token$')}` },
      }
    )
      .then(({ data }) => {
        // console.log('save', data);
        alert.success('Password Changed');
        history.push(`/emp`);
      })
      .catch((err) => {
        console.log('Err', err);
        alert.error(err?.response?.data?.message);
      });
  };

  const [errors, setErrors] = useState({
    oldPassword: '',
    password: '',
    confirmPassword: '',
  });

  const [isValid, setisValid] = useState(true);

  const HandleValidation = () => {
    let shouldsubmit = true;
    let tempError = errors;

    for (const [key, value] of Object.entries(empDetails)) {
      if (value == "") {
        tempError[key] = true;
        if (shouldsubmit) {
          shouldsubmit = false;
        }
      } else {
        tempError[key] = false;
      }
    }
    // console.log(tempError);
    setisValid(shouldsubmit);
    setErrors(tempError);

    if (shouldsubmit) {
      OnUpdated();
    }
  };

  return (
    <>
      <Row className='page-title align-items-center'>
        <Col sm={4} xl={6}>
          <h4 className='mb-1 mt-0'>Employee</h4>
        </Col>
      </Row>
      <div className='col-12'>
        <div className='card p-3'>
          <h4 className='mb-4'> Change Password </h4>
          <div className='form-row'>
          {[
                { label: "Old Password", name: "oldPassword", type: "password" },
                { label: "New Password", name: "password", type: "password" },
                { label: "Confirm Password", name: "confirmPassword", type: "password" },
              ].map((item, i) => (
                <div className="form-group col-md-4" key={i}>
                  <CustomInput
                    label={item.label}
                    type={item.type}
                    error={errors[item.name]}
                    name={item.name}
                    id={item.name}
                    value={empDetails[item.name]}
                    handleChange={(e) => {
                      handelChange(e);
                    }}
                  />
                </div>
              ))}
          </div>

          <div
            className='form-row'
            style={{ display: 'grid', placeItems: 'center' }}>
            <div className='form-group col-md-2'>
              <button className='btn btn-primary btn-block' onClick={HandleValidation}>
                Save
              </button>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default ChangePassEmp;
