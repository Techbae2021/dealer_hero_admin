/** @format */

import React, { useEffect, useState, useContext } from 'react';
import { Row, Col, CustomInput } from 'reactstrap';
import Leftsidebar from './leftSidebar';
import { useHistory, useParams } from 'react-router-dom';
import Instance from './../../Instance';
import moment from 'moment';
import Context from '../context/Context';
import { EMP_DETAILS, EMP_PERMISSION_DETAILS } from '../context/action.type';

const EmpDetails = () => {
  let history = useHistory();
  const params = useParams();

  const [empView, setempView] = useState(null);
  const [loader, setLoader] = useState(false);
  const { dispatchDetails } = useContext(Context);

  useEffect(() => {
    setLoader(true);
    Instance.get(`/api/user/employee/${params.id}`, {
      headers: { authorization: `Bearer ${localStorage.getItem('token$')}` },
    })
      .then(({ data }) => {
        // console.log('empData', data);
        setempView(data?.employeeDetails);
        setLoader(false);
      })
      .catch((err) => {
        console.log('err', err);
      });
  }, []);

  const EmpEdit = (empView) => {
    dispatchDetails({
      type: EMP_DETAILS,
      payload: empView,
    });
    history.push(`/emp/edit/${empView?._id}`);
  };

  const PermissionEdit = (empView) => {
    dispatchDetails({
      type: EMP_PERMISSION_DETAILS,
      payload: empView,
    });
    history.push(`/permission/edit/${empView?._id}`);
  };

  const EmpPass = (empView) => {
    history.push(`/change/emp/pass/${empView?._id}`);
  };

  return (
    <>
      <Row className='page-title align-items-center'>
        <Col sm={4} xl={6}>
          <h4 className='mb-1 mt-0'>Employee</h4>
        </Col>
      </Row>

      <div className='row'>
        <div className='col-2 Lead_Board'>
          <div className='email-container bg-transparent'>
            {/* Left sidebar */}
            <Leftsidebar />
            {/* End Left sidebar */}
          </div>
        </div>

        <div className='col-10'>
          <div className='card p-3'>
            <div className='row'>
              <div className='col-md-6'>
                <h4 className='mb-4'> Employee Details </h4>
              </div>
              <div className='col-md-6 text-right'>
                <svg
                  xmlns='http://www.w3.org/2000/svg'
                  width='2em'
                  height='2em'
                  fill='currentColor'
                  class='bi bi-pencil-square'
                  viewBox='0 0 16 16'
                  onClick={() => EmpEdit(empView)}>
                  <path d='M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456l-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z' />
                  <path
                    fill-rule='evenodd'
                    d='M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z'
                  />
                </svg>
                <svg
                  xmlns='http://www.w3.org/2000/svg'
                  width='2em'
                  height='2em'
                  fill='currentColor'
                  class='bi bi-file-lock2 ml-2'
                  viewBox='0 0 16 16'
                  onClick={() => EmpPass(empView)}>
                  <path d='M8 5a1 1 0 0 1 1 1v1H7V6a1 1 0 0 1 1-1zm2 2.076V6a2 2 0 1 0-4 0v1.076c-.54.166-1 .597-1 1.224v2.4c0 .816.781 1.3 1.5 1.3h3c.719 0 1.5-.484 1.5-1.3V8.3c0-.627-.46-1.058-1-1.224z' />
                  <path d='M4 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h8a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H4zm0 1h8a1 1 0 0 1 1 1v12a1 1 0 0 1-1 1H4a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1z' />
                </svg>
              </div>
            </div>
            {loader ? (
              <>
                <div class='d-flex justify-content-center'>
                  <div class='spinner-border' role='status'>
                    <span class='sr-only'>Loading...</span>
                  </div>
                </div>{' '}
              </>
            ) : (
              <>
                <div className='row'>
                  <div className='col-md-3'>
                    <p>
                      <strong>First Name : </strong>
                      {empView?.firstName}
                    </p>
                  </div>
                  <div className='col-md-3'>
                    <p>
                      <strong>Last Name : </strong>
                      {empView?.lastName}
                    </p>
                  </div>
                  <div className='col-md-3'>
                    <p>
                      <strong>Department : </strong> {empView?.department}
                    </p>
                  </div>
                  <div className='col-md-3'>
                    <p>
                      <strong>Email : </strong> {empView?.email}
                    </p>
                  </div>
                  <div className='col-md-3'>
                    <p>
                      <strong>Mobile : </strong>
                      {empView?.mobile}
                    </p>
                  </div>
                  <div className='col-md-3'>
                    <p>
                      <strong>User Type : </strong> {empView?.userType}
                    </p>
                  </div>
                </div>
              </>
            )}
          </div>

          <div className='card'>
            <div className='card p-3'>
              <div>
                <div className='row'>
                  <div className='col-md-6'>
                    <h4>Permission</h4>
                  </div>
                  <div className='col-md-6 text-right'>
                    <svg
                      xmlns='http://www.w3.org/2000/svg'
                      width='2em'
                      height='2em'
                      fill='currentColor'
                      class='bi bi-pencil-square'
                      viewBox='0 0 16 16'
                      onClick={() => PermissionEdit(empView)}>
                      <path d='M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456l-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z' />
                      <path
                        fill-rule='evenodd'
                        d='M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z'
                      />
                    </svg>
                  </div>
                </div>
                {loader ? (
                  <>
                    <div class='d-flex justify-content-center'>
                      <div class='spinner-border' role='status'>
                        <span class='sr-only'>Loading...</span>
                      </div>
                    </div>{' '}
                  </>
                ) : (
                  <>
                    <h6>Lead:</h6>

                    <div className='row'>
                      <div className='form-group  col-md-3'>
                        <CustomInput
                          type='switch'
                          id='1'
                          name='customSwitch'
                          label='Create'
                          checked={empView?.createLead}
                        />
                      </div>
                      <div className='form-group  col-md-3'>
                        <CustomInput
                          type='switch'
                          id='2'
                          name='customSwitch'
                          label='Edit'
                          checked={empView?.editLead}
                        />
                      </div>
                      <div className='form-group  col-md-3'>
                        <CustomInput
                          type='switch'
                          id='3'
                          name='customSwitch'
                          label='Delite'
                          checked={empView?.deleteLead}
                        />
                      </div>
                      <div className='form-group  col-md-3'>
                        <CustomInput
                          type='switch'
                          id='4'
                          name='customSwitch'
                          label='Add Note'
                          checked={empView?.leadNote}
                        />
                      </div>

                      <div className='form-group  col-md-3'>
                        <CustomInput
                          type='switch'
                          id='5'
                          name='customSwitch'
                          label='Send Mail'
                          checked={empView?.leadMail}
                        />
                      </div>
                      <div className='form-group  col-md-3'>
                        <CustomInput
                          type='switch'
                          id='6'
                          name='customSwitch'
                          label='Set Reminder'
                          checked={empView?.leadReminder}
                        />
                      </div>
                      <div className='form-group  col-md-3'>
                        <CustomInput
                          type='switch'
                          id='7'
                          name='customSwitch'
                          label='Lead Board'
                          checked={empView?.leadBoard}
                        />
                      </div>
                    </div>

                    <h6>Inventory:</h6>
                    <div className='row'>
                      <div className='form-group  col-md-3'>
                        <CustomInput
                          type='switch'
                          id='8'
                          name='customSwitch1'
                          label='Create Stock'
                          checked={empView?.createStock}
                        />
                      </div>
                      <div className='form-group  col-md-3'>
                        <CustomInput
                          type='switch'
                          id='9'
                          name='customSwitch1'
                          label='Create Costing'
                          checked={empView?.createCosting}
                        />
                      </div>
                      <div className='form-group  col-md-3'>
                        <CustomInput
                          type='switch'
                          id='10'
                          name='customSwitch1'
                          label='Sell Vehicle'
                          checked={empView?.sellVehicle}
                        />
                      </div>
                      <div className='form-group  col-md-3'>
                        <CustomInput
                          type='switch'
                          id='11'
                          name='customSwitch1'
                          label='Vehicle Edit'
                          checked={empView?.vehicleEdit}
                        />
                      </div>

                      <div className='form-group  col-md-3'>
                        <CustomInput
                          type='switch'
                          id='12'
                          name='customSwitch1'
                          label='Acquition Edit'
                          checked={empView?.accquitionEdit}
                        />
                      </div>
                      <div className='form-group  col-md-3'>
                        <CustomInput
                          type='switch'
                          id='13'
                          name='customSwitch1'
                          label='Costing Edit'
                          checked={empView?.costingEdit}
                        />
                      </div>
                      <div className='form-group  col-md-3'>
                        <CustomInput
                          type='switch'
                          id='14'
                          name='customSwitch1'
                          label='Advertising Edit'
                          checked={empView?.advertisingEdit}
                        />
                      </div>
                      <div className='form-group  col-md-3'>
                        <CustomInput
                          type='switch'
                          id='15'
                          name='customSwitch1'
                          label='Vehicle Note'
                          checked={empView?.vehicleNote}
                        />
                      </div>
                      <div className='form-group  col-md-3'>
                        <CustomInput
                          type='switch'
                          id='16'
                          name='customSwitch1'
                          label='Vehicle Reminder'
                          checked={empView?.vehicleReminder}
                        />
                      </div>
                      <div className='form-group  col-md-3'>
                        <CustomInput
                          type='switch'
                          id='17'
                          name='customSwitch1'
                          label='Valuation Add'
                          checked={empView?.valuationAdd}
                        />
                      </div>
                      <div className='form-group  col-md-3'>
                        <CustomInput
                          type='switch'
                          id='18'
                          name='customSwitch1'
                          label='Valuation Edit'
                          checked={empView?.valuationEdit}
                        />
                      </div>
                    </div>

                    <h6>Deal:</h6>
                    <div className='row'>
                      <div className='form-group  col-md-3'>
                        <CustomInput
                          type='switch'
                          id='19'
                          name='customSwitch2'
                          label='Create Proposal'
                          checked={empView?.createProposal}
                        />
                      </div>
                      <div className='form-group  col-md-3'>
                        <CustomInput
                          type='switch'
                          id='20'
                          name='customSwitch2'
                          label='Edit Proposal'
                          checked={empView?.proposalEdit}
                        />
                      </div>
                      <div className='form-group  col-md-3'>
                        <CustomInput
                          type='switch'
                          id='21'
                          name='customSwitch2'
                          label='Edit Trade-In'
                          checked={empView?.tradeInEdit}
                        />
                      </div>
                      <div className='form-group  col-md-3'>
                        <CustomInput
                          type='switch'
                          id='22'
                          name='customSwitch2'
                          label='Edit Sell Item'
                          checked={empView?.sellItemEdit}
                        />
                      </div>

                      <div className='form-group  col-md-3'>
                        <CustomInput
                          type='switch'
                          id='23'
                          name='customSwitch2'
                          label='Proposal Finalize'
                          checked={empView?.proposalFinalize}
                        />
                      </div>
                      <div className='form-group  col-md-3'>
                        <CustomInput
                          type='switch'
                          id='24'
                          name='customSwitch2'
                          label='Proposal Lost'
                          checked={empView?.proposalLost}
                        />
                      </div>
                      <div className='form-group  col-md-3'>
                        <CustomInput
                          type='switch'
                          id='25'
                          name='customSwitch2'
                          label='Proposal Note'
                          checked={empView?.proposalNote}
                        />
                      </div>
                      <div className='form-group  col-md-3'>
                        <CustomInput
                          type='switch'
                          id='26'
                          name='customSwitch2'
                          label='Proposal Reminder'
                          checked={empView?.proposalReminder}
                        />
                      </div>
                      <div className='form-group  col-md-3'>
                        <CustomInput
                          type='switch'
                          id='27'
                          name='customSwitch2'
                          label='Create Brokered'
                          checked={empView?.createBrokered}
                        />
                      </div>
                      <div className='form-group  col-md-3'>
                        <CustomInput
                          type='switch'
                          id='28'
                          name='customSwitch2'
                          label='Edit Brokered'
                          checked={empView?.brokeredEdit}
                        />
                      </div>
                      <div className='form-group  col-md-3'>
                        <CustomInput
                          type='switch'
                          id='29'
                          name='customSwitch2'
                          label='Edit Brokered Trade-In'
                          checked={empView?.bTradeInEdit}
                        />
                      </div>
                      <div className='form-group  col-md-3'>
                        <CustomInput
                          type='switch'
                          id='30'
                          name='customSwitch2'
                          label='Edit Brokered Sell Item'
                          checked={empView?.bSellItemEdit}
                        />
                      </div>

                      <div className='form-group  col-md-3'>
                        <CustomInput
                          type='switch'
                          id='31'
                          name='customSwitch2'
                          label='Brokered Finalize'
                          checked={empView?.brokeredFinalize}
                        />
                      </div>
                      <div className='form-group  col-md-3'>
                        <CustomInput
                          type='switch'
                          id='32'
                          name='customSwitch2'
                          label='Brokered Lost'
                          checked={empView?.brokeredLost}
                        />
                      </div>
                      <div className='form-group  col-md-3'>
                        <CustomInput
                          type='switch'
                          id='33'
                          name='customSwitch2'
                          label='Brokered Note'
                          checked={empView?.brokeredNote}
                        />
                      </div>
                      <div className='form-group  col-md-3'>
                        <CustomInput
                          type='switch'
                          id='34'
                          name='customSwitch2'
                          label='Brokered Reminder'
                          checked={empView?.brokeredReminder}
                        />
                      </div>
                    </div>

                    <h6>Customer:</h6>
                    <div className='row'>
                      <div className='form-group  col-md-3'>
                        <CustomInput
                          type='switch'
                          id='35'
                          name='customSwitch66'
                          label='Create Customer'
                          checked={empView?.createCustomer}
                        />
                      </div>
                      <div className='form-group  col-md-3'>
                        <CustomInput
                          type='switch'
                          id='36'
                          name='customSwitch45'
                          label='Edit Customer'
                          checked={empView?.customerEdit}
                        />
                      </div>
                      <div className='form-group  col-md-3'>
                        <CustomInput
                          type='switch'
                          id='37'
                          name='customSwitch125'
                          label='Delete Customer'
                          checked={empView?.customerDelete}
                        />
                      </div>
                    </div>

                    <h6>Employee:</h6>
                    <div className='row'>
                      <div className='form-group  col-md-3'>
                        <CustomInput
                          type='switch'
                          id='38'
                          name='customSwitch3545'
                          label='Create Employee'
                          checked={empView?.addEmp}
                        />
                      </div>
                      <div className='form-group  col-md-3'>
                        <CustomInput
                          type='switch'
                          id='39'
                          name='customSwitch5555'
                          label='Delete Employee'
                          checked={empView?.empDelete}
                        />
                      </div>
                    </div>

                    <h6>Other:</h6>
                    <div className='row'>
                      <div className='form-group  col-md-3'>
                        <CustomInput
                          type='switch'
                          id='40'
                          name='customSwitch2343'
                          label='Purchase Delete'
                          checked={empView?.purchaseDelete}
                        />
                      </div>
                      <div className='form-group  col-md-3'>
                        <CustomInput
                          type='switch'
                          id='41'
                          name='customSwitch2333'
                          label='Sales Delete'
                          checked={empView?.salesDelete}
                        />
                      </div>
                      <div className='form-group  col-md-3'>
                        <CustomInput
                          type='switch'
                          id='42'
                          name='customSwitch15544'
                          label='PPSR Delete'
                          checked={empView?.ppsrDelete}
                        />
                      </div>
                    </div>

                    <h6>Setting:</h6>
                    <div className='row'>
                      <div className='form-group  col-md-3'>
                        <CustomInput
                          type='switch'
                          id='43'
                          name='customSwitch3'
                          label='Setting 1'
                          checked={empView?.setting1}
                        />
                      </div>
                      <div className='form-group  col-md-3'>
                        <CustomInput
                          type='switch'
                          id='44'
                          name='customSwitch3'
                          label='Setting 2'
                          checked={empView?.setting2}
                        />
                      </div>
                      <div className='form-group  col-md-3'>
                        <CustomInput
                          type='switch'
                          id='45'
                          name='customSwitch3'
                          label='Setting 3'
                          checked={empView?.setting3}
                        />
                      </div>
                      <div className='form-group  col-md-3'>
                        <CustomInput
                          type='switch'
                          id='46'
                          name='customSwitch3'
                          label='Setting 4'
                          checked={empView?.setting4}
                        />
                      </div>
                      <div className='form-group  col-md-3'>
                        <CustomInput
                          type='switch'
                          id='47'
                          name='customSwitch3'
                          label='Setting 5'
                          checked={empView?.setting5}
                        />
                      </div>
                      <div className='form-group  col-md-3'>
                        <CustomInput
                          type='switch'
                          id='48'
                          name='customSwitch3'
                          label='Setting 6'
                          checked={empView?.setting6}
                        />
                      </div>
                    </div>
                  </>
                )}
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default EmpDetails;
