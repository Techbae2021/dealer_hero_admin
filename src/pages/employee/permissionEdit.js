/** @format */

import React, { useState, useContext } from 'react';
import { Row, Col, CustomInput } from 'reactstrap';
import { useHistory, Redirect } from 'react-router-dom';
import Instance from '../../Instance';
import Context from '../context/Context';
import { useAlert } from 'react-alert';

const PermissionEdit = () => {
  let history = useHistory();
  const alert = useAlert();
  const { details } = useContext(Context);
  const [toggle, setToggle] = useState({
    createLead: details != null ? details.createLead : '',
    editLead: details != null ? details.editLead : '',
    deleteLead: details != null ? details.deleteLead : '',
    leadNote: details != null ? details.leadNote : '',
    leadMail: details != null ? details.leadMail : '',
    leadReminder: details != null ? details.leadReminder : '',
    leadBoard: details != null ? details.leadBoard : '',
    createStock: details != null ? details.createStock : '',
    createCosting: details != null ? details.createCosting : '',
    sellVehicle: details != null ? details.sellVehicle : '',
    vehicleEdit: details != null ? details.vehicleEdit : '',
    costingEdit: details != null ? details.costingEdit : '',
    accquitionEdit: details != null ? details.accquitionEdit : '',
    advertisingEdit: details != null ? details.advertisingEdit : '',
    vehicleNote: details != null ? details.vehicleNote : '',
    vehicleReminder: details != null ? details.vehicleReminder : '',
    valuationEdit: details != null ? details.valuationEdit : '',
    valuationAdd: details != null ? details.valuationAdd : '',
    createProposal: details != null ? details.createProposal : '',
    proposalEdit: details != null ? details.proposalEdit : '',
    tradeInEdit: details != null ? details.tradeInEdit : '',
    sellItemEdit: details != null ? details.sellItemEdit : '',
    proposalFinalize: details != null ? details.proposalFinalize : '',
    proposalLost: details != null ? details.proposalLost : '',
    proposalReminder: details != null ? details.proposalReminder : '',
    proposalNote: details != null ? details.proposalNote : '',
    createBrokered: details != null ? details.createBrokered : '',
    brokeredEdit: details != null ? details.brokeredEdit : '',
    bTradeInEdit: details != null ? details.bTradeInEdit : '',
    bSellItemEdit: details != null ? details.bSellItemEdit : '',
    brokeredFinalize: details != null ? details.brokeredFinalize : '',
    brokeredLost: details != null ? details.brokeredLost : '',
    brokeredReminder: details != null ? details.brokeredReminder : '',
    brokeredNote: details != null ? details.brokeredNote : '',
    empDelete: details != null ? details.empDelete : '',
    addEmp: details != null ? details.addEmp : '',
    createCustomer: details != null ? details.createCustomer : '',
    customerEdit: details != null ? details.customerEdit : '',
    customerDelete: details != null ? details.customerDelete : '',
    purchaseDelete: details != null ? details.purchaseDelete : '',
    salesDelete: details != null ? details.salesDelete : '',
    ppsrDelete: details != null ? details.ppsrDelete : '',
    setting1: details != null ? details.setting1 : '',
    setting2: details != null ? details.setting2 : '',
    setting3: details != null ? details.setting3 : '',
    setting4: details != null ? details.setting4 : '',
    setting5: details != null ? details.setting5 : '',
    setting6: details != null ? details.setting6 : '',
  });

  const handelSelect = (e) => {
    if (e.target.id === 'leadSelect-all' && e.target.checked) {
      setToggle({
        ...toggle,
        createLead: true,
        editLead: true,
        deleteLead: true,
        leadNote: true,
        leadMail: true,
        leadReminder: true,
        leadBoard: true,
      });
    } else if (e.target.id === 'leadSelect-all' && !e.target.checked) {
      setToggle({
        ...toggle,
        createLead: false,
        editLead: false,
        deleteLead: false,
        leadNote: false,
        leadMail: false,
        leadReminder: false,
        leadBoard: false,
      });
    }
    if (e.target.id === 'iSelect-all' && e.target.checked) {
      setToggle({
        ...toggle,
        createStock: true,
        createCosting: true,
        sellVehicle: true,
        vehicleEdit: true,
        costingEdit: true,
        accquitionEdit: true,
        advertisingEdit: true,
        vehicleNote: true,
        vehicleReminder: true,
        valuationEdit: true,
        valuationAdd: true,
      });
    } else if (e.target.id === 'iSelect-all' && !e.target.checked) {
      setToggle({
        ...toggle,
        createStock: false,
        createCosting: false,
        sellVehicle: false,
        vehicleEdit: false,
        costingEdit: false,
        accquitionEdit: false,
        advertisingEdit: false,
        vehicleNote: false,
        vehicleReminder: false,
        valuationEdit: false,
        valuationAdd: false,
      });
    }
    if (e.target.id === 'dSelect-all' && e.target.checked) {
      setToggle({
        ...toggle,
        createProposal: true,
        proposalEdit: true,
        tradeInEdit: true,
        sellItemEdit: true,
        proposalFinalize: true,
        proposalLost: true,
        proposalReminder: true,
        proposalNote: true,
        createBrokered: true,
        brokeredEdit: true,
        bTradeInEdit: true,
        bSellItemEdit: true,
        brokeredFinalize: true,
        brokeredLost: true,
        brokeredReminder: true,
        brokeredNote: true,
      });
    } else if (e.target.id === 'dSelect-all' && !e.target.checked) {
      setToggle({
        ...toggle,
        createProposal: false,
        proposalEdit: false,
        tradeInEdit: false,
        sellItemEdit: false,
        proposalFinalize: false,
        proposalLost: false,
        proposalReminder: false,
        proposalNote: false,
        createBrokered: false,
        brokeredEdit: false,
        bTradeInEdit: false,
        bSellItemEdit: false,
        brokeredFinalize: false,
        brokeredLost: false,
        brokeredReminder: false,
        brokeredNote: false,
      });
    }
    if (e.target.id === 'sSelect-all' && e.target.checked) {
      setToggle({
        ...toggle,
        setting1: true,
        setting2: true,
        setting3: true,
        setting4: true,
        setting5: true,
        setting6: true,
      });
    } else if (e.target.id === 'sSelect-all' && !e.target.checked) {
      setToggle({
        ...toggle,
        setting1: false,
        setting2: false,
        setting3: false,
        setting4: false,
        setting5: false,
        setting6: false,
      });
    }
  };

  const OnUpdated = (e) => {
    e.preventDefault();
    Instance.put(
      `/api/user/employee/permission/update/${details._id}`,
      {
        createLead: toggle.createLead,
        editLead: toggle.editLead,
        deleteLead: toggle.deleteLead,
        leadNote: toggle.leadNote,
        leadMail: toggle.leadMail,
        leadReminder: toggle.leadReminder,
        leadBoard: toggle.leadBoard,
        createStock: toggle.createStock,
        createCosting: toggle.createCosting,
        sellVehicle: toggle.sellVehicle,
        vehicleEdit: toggle.vehicleEdit,
        costingEdit: toggle.costingEdit,
        accquitionEdit: toggle.accquitionEdit,
        advertisingEdit: toggle.advertisingEdit,
        vehicleNote: toggle.vehicleNote,
        vehicleReminder: toggle.vehicleReminder,
        valuationEdit: toggle.valuationEdit,
        valuationAdd: toggle.valuationAdd,
        createProposal: toggle.createProposal,
        proposalEdit: toggle.proposalEdit,
        tradeInEdit: toggle.tradeInEdit,
        sellItemEdit: toggle.sellItemEdit,
        proposalFinalize: toggle.proposalFinalize,
        proposalLost: toggle.proposalLost,
        proposalReminder: toggle.proposalReminder,
        proposalNote: toggle.proposalNote,
        createBrokered: toggle.createBrokered,
        brokeredEdit: toggle.brokeredEdit,
        bTradeInEdit: toggle.bTradeInEdit,
        bSellItemEdit: toggle.bSellItemEdit,
        brokeredFinalize: toggle.brokeredFinalize,
        brokeredLost: toggle.brokeredLost,
        brokeredReminder: toggle.brokeredReminder,
        brokeredNote: toggle.brokeredNote,
        empDelete: toggle.empDelete,
        addEmp: toggle.addEmp,
        createCustomer: toggle.createCustomer,
        customerEdit: toggle.customerEdit,
        customerDelete: toggle.customerDelete,
        purchaseDelete: toggle.purchaseDelete,
        salesDelete: toggle.salesDelete,
        ppsrDelete: toggle.ppsrDelete,
        setting1: toggle.setting1,
        setting2: toggle.setting2,
        setting3: toggle.setting3,
        setting4: toggle.setting4,
        setting5: toggle.setting5,
        setting6: toggle.setting6,
      },
      {
        headers: { authorization: `Bearer ${localStorage.getItem('token$')}` },
      }
    )
      .then(({ data }) => {
        // console.log('save', data);
        alert.success('Permission Edited');
        history.push(`/emp/detail/${details._id}`);
      })
      .catch((err) => {
        console.log('Err', err);
      });
  };

  if (details == null) {
    return <Redirect to={`/emp/detail/${details._id}`} />;
  }

  return (
    <>
      <Row className='page-title align-items-center'>
        <Col sm={4} xl={6}>
          <h4 className='mb-1 mt-0'>Employee</h4>
        </Col>
      </Row>

      <div className='row'>
        <div className='col-12'>
          <div className='card p-3'>
            <h4>Edit Permission</h4>
            <h6>Lead:</h6>
            <div className='row'>
              <div className='form-group  col-md-3'>
                <CustomInput
                  type='switch'
                  id='leadSelect-all'
                  name='customSwitch'
                  label='Select All'
                  onChange={(e) => {
                    handelSelect(e);
                  }}
                />
              </div>
              <div className='form-group  col-md-3'>
                <CustomInput
                  type='switch'
                  id='1'
                  name='customSwitch'
                  label='Create'
                  checked={toggle.createLead}
                  onChange={(e) => {
                    if (e.target.checked) {
                      setToggle({ ...toggle, createLead: true });
                    } else {
                      setToggle({ ...toggle, createLead: false });
                    }
                  }}
                />
              </div>
              <div className='form-group  col-md-3'>
                <CustomInput
                  type='switch'
                  id='2'
                  name='customSwitch'
                  label='Edit'
                  checked={toggle.editLead}
                  onChange={(e) => {
                    if (e.target.checked) {
                      setToggle({ ...toggle, editLead: true });
                    } else {
                      setToggle({ ...toggle, editLead: false });
                    }
                  }}
                />
              </div>
              <div className='form-group  col-md-3'>
                <CustomInput
                  type='switch'
                  id='3'
                  name='customSwitch'
                  label='Delite'
                  checked={toggle.deleteLead}
                  onChange={(e) => {
                    if (e.target.checked) {
                      setToggle({ ...toggle, deleteLead: true });
                    } else {
                      setToggle({ ...toggle, deleteLead: false });
                    }
                  }}
                />
              </div>
              <div className='form-group  col-md-3'>
                <CustomInput
                  type='switch'
                  id='4'
                  name='customSwitch'
                  label='Add Note'
                  checked={toggle.leadNote}
                  onChange={(e) => {
                    if (e.target.checked) {
                      setToggle({ ...toggle, leadNote: true });
                    } else {
                      setToggle({ ...toggle, leadNote: false });
                    }
                  }}
                />
              </div>

              <div className='form-group  col-md-3'>
                <CustomInput
                  type='switch'
                  id='5'
                  name='customSwitch'
                  label='Send Mail'
                  checked={toggle.leadMail}
                  onChange={(e) => {
                    if (e.target.checked) {
                      setToggle({ ...toggle, leadMail: true });
                    } else {
                      setToggle({ ...toggle, leadMail: false });
                    }
                  }}
                />
              </div>
              <div className='form-group  col-md-3'>
                <CustomInput
                  type='switch'
                  id='6'
                  name='customSwitch'
                  label='Set Reminder'
                  checked={toggle.leadReminder}
                  onChange={(e) => {
                    if (e.target.checked) {
                      setToggle({ ...toggle, leadReminder: true });
                    } else {
                      setToggle({ ...toggle, leadReminder: false });
                    }
                  }}
                />
              </div>
              <div className='form-group  col-md-3'>
                <CustomInput
                  type='switch'
                  id='7'
                  name='customSwitch'
                  label='Lead Board'
                  checked={toggle.leadBoard}
                  onChange={(e) => {
                    if (e.target.checked) {
                      setToggle({ ...toggle, leadBoard: true });
                    } else {
                      setToggle({ ...toggle, leadBoard: false });
                    }
                  }}
                />
              </div>
            </div>

            <h6>Inventory:</h6>
            <div className='row'>
              <div className='form-group  col-md-3'>
                <CustomInput
                  type='switch'
                  id='iSelect-all'
                  name='customSwitch1'
                  label='Select All'
                  onChange={(e) => {
                    handelSelect(e);
                  }}
                />
              </div>
              <div className='form-group  col-md-3'>
                <CustomInput
                  type='switch'
                  id='8'
                  name='customSwitch1'
                  label='Create Stock'
                  checked={toggle.createStock}
                  onChange={(e) => {
                    if (e.target.checked) {
                      setToggle({ ...toggle, createStock: true });
                    } else {
                      setToggle({ ...toggle, createStock: false });
                    }
                  }}
                />
              </div>
              <div className='form-group  col-md-3'>
                <CustomInput
                  type='switch'
                  id='9'
                  name='customSwitch1'
                  label='Create Costing'
                  checked={toggle.createCosting}
                  onChange={(e) => {
                    if (e.target.checked) {
                      setToggle({ ...toggle, createCosting: true });
                    } else {
                      setToggle({ ...toggle, createCosting: false });
                    }
                  }}
                />
              </div>
              <div className='form-group  col-md-3'>
                <CustomInput
                  type='switch'
                  id='10'
                  name='customSwitch1'
                  label='Sell Vehicle'
                  checked={toggle.sellVehicle}
                  onChange={(e) => {
                    if (e.target.checked) {
                      setToggle({ ...toggle, sellVehicle: true });
                    } else {
                      setToggle({ ...toggle, sellVehicle: false });
                    }
                  }}
                />
              </div>
              <div className='form-group  col-md-3'>
                <CustomInput
                  type='switch'
                  id='11'
                  name='customSwitch1'
                  label='Vehicle Edit'
                  checked={toggle.vehicleEdit}
                  onChange={(e) => {
                    if (e.target.checked) {
                      setToggle({ ...toggle, vehicleEdit: true });
                    } else {
                      setToggle({ ...toggle, vehicleEdit: false });
                    }
                  }}
                />
              </div>

              <div className='form-group  col-md-3'>
                <CustomInput
                  type='switch'
                  id='12'
                  name='customSwitch1'
                  label='Acquition Edit'
                  checked={toggle.accquitionEdit}
                  onChange={(e) => {
                    if (e.target.checked) {
                      setToggle({ ...toggle, accquitionEdit: true });
                    } else {
                      setToggle({ ...toggle, accquitionEdit: false });
                    }
                  }}
                />
              </div>
              <div className='form-group  col-md-3'>
                <CustomInput
                  type='switch'
                  id='13'
                  name='customSwitch1'
                  label='Costing Edit'
                  checked={toggle.costingEdit}
                  onChange={(e) => {
                    if (e.target.checked) {
                      setToggle({ ...toggle, costingEdit: true });
                    } else {
                      setToggle({ ...toggle, costingEdit: false });
                    }
                  }}
                />
              </div>
              <div className='form-group  col-md-3'>
                <CustomInput
                  type='switch'
                  id='14'
                  name='customSwitch1'
                  label='Advertising Edit'
                  checked={toggle.advertisingEdit}
                  onChange={(e) => {
                    if (e.target.checked) {
                      setToggle({ ...toggle, advertisingEdit: true });
                    } else {
                      setToggle({ ...toggle, advertisingEdit: false });
                    }
                  }}
                />
              </div>
              <div className='form-group  col-md-3'>
                <CustomInput
                  type='switch'
                  id='15'
                  name='customSwitch1'
                  label='Vehicle Note'
                  checked={toggle.vehicleNote}
                  onChange={(e) => {
                    if (e.target.checked) {
                      setToggle({ ...toggle, vehicleNote: true });
                    } else {
                      setToggle({ ...toggle, vehicleNote: false });
                    }
                  }}
                />
              </div>
              <div className='form-group  col-md-3'>
                <CustomInput
                  type='switch'
                  id='16'
                  name='customSwitch1'
                  label='Vehicle Reminder'
                  checked={toggle.vehicleReminder}
                  onChange={(e) => {
                    if (e.target.checked) {
                      setToggle({ ...toggle, vehicleReminder: true });
                    } else {
                      setToggle({ ...toggle, vehicleReminder: false });
                    }
                  }}
                />
              </div>
              <div className='form-group  col-md-3'>
                <CustomInput
                  type='switch'
                  id='17'
                  name='customSwitch1'
                  label='Valuation Add'
                  checked={toggle.valuationAdd}
                  onChange={(e) => {
                    if (e.target.checked) {
                      setToggle({ ...toggle, valuationAdd: true });
                    } else {
                      setToggle({ ...toggle, valuationAdd: false });
                    }
                  }}
                />
              </div>
              <div className='form-group  col-md-3'>
                <CustomInput
                  type='switch'
                  id='18'
                  name='customSwitch1'
                  label='Valuation Edit'
                  checked={toggle.valuationEdit}
                  onChange={(e) => {
                    if (e.target.checked) {
                      setToggle({ ...toggle, valuationEdit: true });
                    } else {
                      setToggle({ ...toggle, valuationEdit: false });
                    }
                  }}
                />
              </div>
            </div>

            <h6>Deal:</h6>
            <div className='row'>
              <div className='form-group  col-md-3'>
                <CustomInput
                  type='switch'
                  id='dSelect-all'
                  name='customSwitch2'
                  label='Select All'
                  onChange={(e) => {
                    handelSelect(e);
                  }}
                />
              </div>
              <div className='form-group  col-md-3'>
                <CustomInput
                  type='switch'
                  id='19'
                  name='customSwitch2'
                  label='Create Proposal'
                  checked={toggle.createProposal}
                  onChange={(e) => {
                    if (e.target.checked) {
                      setToggle({ ...toggle, createProposal: true });
                    } else {
                      setToggle({ ...toggle, createProposal: false });
                    }
                  }}
                />
              </div>
              <div className='form-group  col-md-3'>
                <CustomInput
                  type='switch'
                  id='20'
                  name='customSwitch2'
                  label='Edit Proposal'
                  checked={toggle.proposalEdit}
                  onChange={(e) => {
                    if (e.target.checked) {
                      setToggle({ ...toggle, proposalEdit: true });
                    } else {
                      setToggle({ ...toggle, proposalEdit: false });
                    }
                  }}
                />
              </div>
              <div className='form-group  col-md-3'>
                <CustomInput
                  type='switch'
                  id='21'
                  name='customSwitch2'
                  label='Edit Trade-In'
                  checked={toggle.tradeInEdit}
                  onChange={(e) => {
                    if (e.target.checked) {
                      setToggle({ ...toggle, tradeInEdit: true });
                    } else {
                      setToggle({ ...toggle, tradeInEdit: false });
                    }
                  }}
                />
              </div>
              <div className='form-group  col-md-3'>
                <CustomInput
                  type='switch'
                  id='22'
                  name='customSwitch2'
                  label='Edit Sell Item'
                  checked={toggle.sellItemEdit}
                  onChange={(e) => {
                    if (e.target.checked) {
                      setToggle({ ...toggle, sellItemEdit: true });
                    } else {
                      setToggle({ ...toggle, sellItemEdit: false });
                    }
                  }}
                />
              </div>

              <div className='form-group  col-md-3'>
                <CustomInput
                  type='switch'
                  id='23'
                  name='customSwitch2'
                  label='Proposal Finalize'
                  checked={toggle.proposalFinalize}
                  onChange={(e) => {
                    if (e.target.checked) {
                      setToggle({ ...toggle, proposalFinalize: true });
                    } else {
                      setToggle({ ...toggle, proposalFinalize: false });
                    }
                  }}
                />
              </div>
              <div className='form-group  col-md-3'>
                <CustomInput
                  type='switch'
                  id='24'
                  name='customSwitch2'
                  label='Proposal Lost'
                  checked={toggle.proposalLost}
                  onChange={(e) => {
                    if (e.target.checked) {
                      setToggle({ ...toggle, proposalLost: true });
                    } else {
                      setToggle({ ...toggle, proposalLost: false });
                    }
                  }}
                />
              </div>
              <div className='form-group  col-md-3'>
                <CustomInput
                  type='switch'
                  id='25'
                  name='customSwitch2'
                  label='Proposal Note'
                  checked={toggle.proposalNote}
                  onChange={(e) => {
                    if (e.target.checked) {
                      setToggle({ ...toggle, proposalNote: true });
                    } else {
                      setToggle({ ...toggle, proposalNote: false });
                    }
                  }}
                />
              </div>
              <div className='form-group  col-md-3'>
                <CustomInput
                  type='switch'
                  id='26'
                  name='customSwitch2'
                  label='Proposal Reminder'
                  checked={toggle.proposalReminder}
                  onChange={(e) => {
                    if (e.target.checked) {
                      setToggle({ ...toggle, proposalReminder: true });
                    } else {
                      setToggle({ ...toggle, proposalReminder: false });
                    }
                  }}
                />
              </div>
              <div className='form-group  col-md-3'>
                <CustomInput
                  type='switch'
                  id='27'
                  name='customSwitch2'
                  label='Create Brokered'
                  checked={toggle.createBrokered}
                  onChange={(e) => {
                    if (e.target.checked) {
                      setToggle({ ...toggle, createBrokered: true });
                    } else {
                      setToggle({ ...toggle, createBrokered: false });
                    }
                  }}
                />
              </div>
              <div className='form-group  col-md-3'>
                <CustomInput
                  type='switch'
                  id='28'
                  name='customSwitch2'
                  label='Edit Brokered'
                  checked={toggle.brokeredEdit}
                  onChange={(e) => {
                    if (e.target.checked) {
                      setToggle({ ...toggle, brokeredEdit: true });
                    } else {
                      setToggle({ ...toggle, brokeredEdit: false });
                    }
                  }}
                />
              </div>
              <div className='form-group  col-md-3'>
                <CustomInput
                  type='switch'
                  id='29'
                  name='customSwitch2'
                  label='Edit Brokered Trade-In'
                  checked={toggle.bTradeInEdit}
                  onChange={(e) => {
                    if (e.target.checked) {
                      setToggle({ ...toggle, bTradeInEdit: true });
                    } else {
                      setToggle({ ...toggle, bTradeInEdit: false });
                    }
                  }}
                />
              </div>
              <div className='form-group  col-md-3'>
                <CustomInput
                  type='switch'
                  id='30'
                  name='customSwitch2'
                  label='Edit Brokered Sell Item'
                  checked={toggle.bSellItemEdit}
                  onChange={(e) => {
                    if (e.target.checked) {
                      setToggle({ ...toggle, bSellItemEdit: true });
                    } else {
                      setToggle({ ...toggle, bSellItemEdit: false });
                    }
                  }}
                />
              </div>

              <div className='form-group  col-md-3'>
                <CustomInput
                  type='switch'
                  id='31'
                  name='customSwitch2'
                  label='Brokered Finalize'
                  checked={toggle.brokeredFinalize}
                  onChange={(e) => {
                    if (e.target.checked) {
                      setToggle({ ...toggle, brokeredFinalize: true });
                    } else {
                      setToggle({ ...toggle, brokeredFinalize: false });
                    }
                  }}
                />
              </div>
              <div className='form-group  col-md-3'>
                <CustomInput
                  type='switch'
                  id='32'
                  name='customSwitch2'
                  label='Brokered Lost'
                  checked={toggle.brokeredLost}
                  onChange={(e) => {
                    if (e.target.checked) {
                      setToggle({ ...toggle, brokeredLost: true });
                    } else {
                      setToggle({ ...toggle, brokeredLost: false });
                    }
                  }}
                />
              </div>
              <div className='form-group  col-md-3'>
                <CustomInput
                  type='switch'
                  id='33'
                  name='customSwitch2'
                  label='Brokered Note'
                  checked={toggle.brokeredNote}
                  onChange={(e) => {
                    if (e.target.checked) {
                      setToggle({ ...toggle, brokeredNote: true });
                    } else {
                      setToggle({ ...toggle, brokeredNote: false });
                    }
                  }}
                />
              </div>
              <div className='form-group  col-md-3'>
                <CustomInput
                  type='switch'
                  id='34'
                  name='customSwitch2'
                  label='Brokered Reminder'
                  checked={toggle.brokeredReminder}
                  onChange={(e) => {
                    if (e.target.checked) {
                      setToggle({ ...toggle, brokeredReminder: true });
                    } else {
                      setToggle({ ...toggle, brokeredReminder: false });
                    }
                  }}
                />
              </div>
            </div>

            <h6>Customer:</h6>
            <div className='row'>
              <div className='form-group  col-md-3'>
                <CustomInput
                  type='switch'
                  id='35'
                  name='customSwitch66'
                  label='Create Customer'
                  checked={toggle.createCustomer}
                  onChange={(e) => {
                    if (e.target.checked) {
                      setToggle({ ...toggle, createCustomer: true });
                    } else {
                      setToggle({ ...toggle, createCustomer: false });
                    }
                  }}
                />
              </div>
              <div className='form-group  col-md-3'>
                <CustomInput
                  type='switch'
                  id='36'
                  name='customSwitch45'
                  label='Edit Customer'
                  checked={toggle.customerEdit}
                  onChange={(e) => {
                    if (e.target.checked) {
                      setToggle({ ...toggle, customerEdit: true });
                    } else {
                      setToggle({ ...toggle, customerEdit: false });
                    }
                  }}
                />
              </div>
              <div className='form-group  col-md-3'>
                <CustomInput
                  type='switch'
                  id='37'
                  name='customSwitch125'
                  label='Delete Customer'
                  checked={toggle.customerDelete}
                  onChange={(e) => {
                    if (e.target.checked) {
                      setToggle({ ...toggle, customerDelete: true });
                    } else {
                      setToggle({ ...toggle, customerDelete: false });
                    }
                  }}
                />
              </div>
            </div>

            <h6>Employee:</h6>
            <div className='row'>
              <div className='form-group  col-md-3'>
                <CustomInput
                  type='switch'
                  id='38'
                  name='customSwitch3545'
                  label='Create Employee'
                  checked={toggle.addEmp}
                  onChange={(e) => {
                    if (e.target.checked) {
                      setToggle({ ...toggle, addEmp: true });
                    } else {
                      setToggle({ ...toggle, addEmp: false });
                    }
                  }}
                />
              </div>
              <div className='form-group  col-md-3'>
                <CustomInput
                  type='switch'
                  id='39'
                  name='customSwitch5555'
                  label='Delete Employee'
                  checked={toggle.empDelete}
                  onChange={(e) => {
                    if (e.target.checked) {
                      setToggle({ ...toggle, empDelete: true });
                    } else {
                      setToggle({ ...toggle, empDelete: false });
                    }
                  }}
                />
              </div>
            </div>

            <h6>Other:</h6>
            <div className='row'>
              <div className='form-group  col-md-3'>
                <CustomInput
                  type='switch'
                  id='40'
                  name='customSwitch2343'
                  label='Purchase Delete'
                  checked={toggle.purchaseDelete}
                  onChange={(e) => {
                    if (e.target.checked) {
                      setToggle({ ...toggle, purchaseDelete: true });
                    } else {
                      setToggle({ ...toggle, purchaseDelete: false });
                    }
                  }}
                />
              </div>
              <div className='form-group  col-md-3'>
                <CustomInput
                  type='switch'
                  id='41'
                  name='customSwitch2333'
                  label='Sales Delete'
                  checked={toggle.salesDelete}
                  onChange={(e) => {
                    if (e.target.checked) {
                      setToggle({ ...toggle, salesDelete: true });
                    } else {
                      setToggle({ ...toggle, salesDelete: false });
                    }
                  }}
                />
              </div>
              <div className='form-group  col-md-3'>
                <CustomInput
                  type='switch'
                  id='42'
                  name='customSwitch15544'
                  label='PPSR Delete'
                  checked={toggle.ppsrDelete}
                  onChange={(e) => {
                    if (e.target.checked) {
                      setToggle({ ...toggle, ppsrDelete: true });
                    } else {
                      setToggle({ ...toggle, ppsrDelete: false });
                    }
                  }}
                />
              </div>
            </div>

            <h6>Setting:</h6>
            <div className='row'>
              <div className='form-group  col-md-3'>
                <CustomInput
                  type='switch'
                  id='sSelect-all'
                  name='customSwitch3'
                  label='Select All'
                  onChange={(e) => {
                    handelSelect(e);
                  }}
                />
              </div>
              <div className='form-group  col-md-3'>
                <CustomInput
                  type='switch'
                  id='43'
                  name='customSwitch3'
                  label='Setting 1'
                  checked={toggle.setting1}
                  onChange={(e) => {
                    if (e.target.checked) {
                      setToggle({ ...toggle, setting1: true });
                    } else {
                      setToggle({ ...toggle, setting1: false });
                    }
                  }}
                />
              </div>
              <div className='form-group  col-md-3'>
                <CustomInput
                  type='switch'
                  id='44'
                  name='customSwitch3'
                  label='Setting 2'
                  checked={toggle.setting2}
                  onChange={(e) => {
                    if (e.target.checked) {
                      setToggle({ ...toggle, setting2: true });
                    } else {
                      setToggle({ ...toggle, setting2: false });
                    }
                  }}
                />
              </div>
              <div className='form-group  col-md-3'>
                <CustomInput
                  type='switch'
                  id='45'
                  name='customSwitch3'
                  label='Setting 3'
                  checked={toggle.setting3}
                  onChange={(e) => {
                    if (e.target.checked) {
                      setToggle({ ...toggle, setting3: true });
                    } else {
                      setToggle({ ...toggle, setting3: false });
                    }
                  }}
                />
              </div>
              <div className='form-group  col-md-3'>
                <CustomInput
                  type='switch'
                  id='46'
                  name='customSwitch3'
                  label='Setting 4'
                  checked={toggle.setting4}
                  onChange={(e) => {
                    if (e.target.checked) {
                      setToggle({ ...toggle, setting4: true });
                    } else {
                      setToggle({ ...toggle, setting4: false });
                    }
                  }}
                />
              </div>
              <div className='form-group  col-md-3'>
                <CustomInput
                  type='switch'
                  id='47'
                  name='customSwitch3'
                  label='Setting 5'
                  checked={toggle.setting5}
                  onChange={(e) => {
                    if (e.target.checked) {
                      setToggle({ ...toggle, setting5: true });
                    } else {
                      setToggle({ ...toggle, setting5: false });
                    }
                  }}
                />
              </div>
              <div className='form-group  col-md-3'>
                <CustomInput
                  type='switch'
                  id='48'
                  name='customSwitch3'
                  label='Setting 6'
                  checked={toggle.setting6}
                  onChange={(e) => {
                    if (e.target.checked) {
                      setToggle({ ...toggle, setting6: true });
                    } else {
                      setToggle({ ...toggle, setting6: false });
                    }
                  }}
                />
              </div>
            </div>

            <div
              className='form-row'
              style={{ display: 'grid', placeItems: 'center' }}>
              <div className='form-group col-md-2'>
                <button
                  className='btn btn-primary btn-block'
                  onClick={OnUpdated}>
                  Save
                </button>
              </div>
            </div>
          </div>
        </div>{' '}
        {/* end Col */}
      </div>
    </>
  );
};

export default PermissionEdit;
