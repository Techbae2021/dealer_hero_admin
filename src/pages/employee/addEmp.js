/** @format */

import React, { useEffect, useState } from 'react';
import { Row, Col, CustomInput } from 'reactstrap';
import { default as Input } from '../../components/CustomInput';
import { useHistory } from 'react-router-dom';
import Instance from '../../Instance';
import { useAlert } from 'react-alert';

const AddEmp = () => {
  let history = useHistory();
  const alert = useAlert();

  const [toggle, setToggle] = useState({
    createLead: false,
    editLead: false,
    deleteLead: false,
    leadNote: false,
    leadMail: false,
    leadReminder: false,
    leadBoard: false,
    createStock: false,
    createCosting: false,
    sellVehicle: false,
    vehicleEdit: false,
    costingEdit: false,
    accquitionEdit: false,
    advertisingEdit: false,
    vehicleNote: false,
    vehicleReminder: false,
    valuationEdit: false,
    valuationAdd: false,
    createProposal: false,
    proposalEdit: false,
    tradeInEdit: false,
    sellItemEdit: false,
    proposalFinalize: false,
    proposalLost: false,
    proposalReminder: false,
    proposalNote: false,
    createBrokered: false,
    brokeredEdit: false,
    bTradeInEdit: false,
    bSellItemEdit: false,
    brokeredFinalize: false,
    brokeredLost: false,
    brokeredReminder: false,
    brokeredNote: false,
    empDelete: false,
    addEmp: false,
    createCustomer: false,
    customerEdit: false,
    customerDelete: false,
    purchaseDelete: false,
    salesDelete: false,
    ppsrDelete: false,
    setting1: false,
    setting2: false,
    setting3: false,
    setting4: false,
    setting5: false,
    setting6: false,
  });

  const handelSelect = (e) => {
    if (e.target.id === 'leadSelect-all' && e.target.checked) {
      setToggle({
        ...toggle,
        createLead: true,
        editLead: true,
        deleteLead: true,
        leadNote: true,
        leadMail: true,
        leadReminder: true,
        leadBoard: true,
      });
    } else if (e.target.id === 'leadSelect-all' && !e.target.checked) {
      setToggle({
        ...toggle,
        createLead: false,
        editLead: false,
        deleteLead: false,
        leadNote: false,
        leadMail: false,
        leadReminder: false,
        leadBoard: false,
      });
    }
    if (e.target.id === 'iSelect-all' && e.target.checked) {
      setToggle({
        ...toggle,
        createStock: true,
        createCosting: true,
        sellVehicle: true,
        vehicleEdit: true,
        costingEdit: true,
        accquitionEdit: true,
        advertisingEdit: true,
        vehicleNote: true,
        vehicleReminder: true,
        valuationEdit: true,
        valuationAdd: true,
      });
    } else if (e.target.id === 'iSelect-all' && !e.target.checked) {
      setToggle({
        ...toggle,
        createStock: false,
        createCosting: false,
        sellVehicle: false,
        vehicleEdit: false,
        costingEdit: false,
        accquitionEdit: false,
        advertisingEdit: false,
        vehicleNote: false,
        vehicleReminder: false,
        valuationEdit: false,
        valuationAdd: false,
      });
    }
    if (e.target.id === 'dSelect-all' && e.target.checked) {
      setToggle({
        ...toggle,
        createProposal: true,
        proposalEdit: true,
        tradeInEdit: true,
        sellItemEdit: true,
        proposalFinalize: true,
        proposalLost: true,
        proposalReminder: true,
        proposalNote: true,
        createBrokered: true,
        brokeredEdit: true,
        bTradeInEdit: true,
        bSellItemEdit: true,
        brokeredFinalize: true,
        brokeredLost: true,
        brokeredReminder: true,
        brokeredNote: true,
      });
    } else if (e.target.id === 'dSelect-all' && !e.target.checked) {
      setToggle({
        ...toggle,
        createProposal: false,
        proposalEdit: false,
        tradeInEdit: false,
        sellItemEdit: false,
        proposalFinalize: false,
        proposalLost: false,
        proposalReminder: false,
        proposalNote: false,
        createBrokered: false,
        brokeredEdit: false,
        bTradeInEdit: false,
        bSellItemEdit: false,
        brokeredFinalize: false,
        brokeredLost: false,
        brokeredReminder: false,
        brokeredNote: false,
      });
    }
    if (e.target.id === 'sSelect-all' && e.target.checked) {
      setToggle({
        ...toggle,
        setting1: true,
        setting2: true,
        setting3: true,
        setting4: true,
        setting5: true,
        setting6: true,
      });
    } else if (e.target.id === 'sSelect-all' && !e.target.checked) {
      setToggle({
        ...toggle,
        setting1: false,
        setting2: false,
        setting3: false,
        setting4: false,
        setting5: false,
        setting6: false,
      });
    }
  };

  const [empDetails, setEmpDetails] = useState({
    firstName: '',
    lastName: '',
    department: '',
    email: '',
    mobile: '',
    userType: '',
    password: '',
    confirmPassword: '',
  });

  const handelChange = (event) => {
    setEmpDetails({
      ...empDetails,
      [event.target.id]: event.target.value,
    });

    if (event.target.value === '') {
      setErrors({ ...errors, [event.target.name]: true });
    } else {
      setErrors({ ...errors, [event.target.name]: false });
    }
  };

  const OnSubmit = () => {
    // e.preventDefault();
    Instance.post(
      `/api/user/employee`,
      {
        firstName: empDetails.firstName,
        lastName: empDetails.lastName,
        department: empDetails.department,
        email: empDetails.email,
        mobile: empDetails.mobile,
        userType: empDetails.userType,
        password: empDetails.password,
        confirmPassword: empDetails.confirmPassword,
        createLead: toggle.createLead,
        editLead: toggle.editLead,
        deleteLead: toggle.deleteLead,
        leadNote: toggle.leadNote,
        leadMail: toggle.leadMail,
        leadReminder: toggle.leadReminder,
        leadBoard: toggle.leadBoard,
        createStock: toggle.createStock,
        createCosting: toggle.createCosting,
        sellVehicle: toggle.sellVehicle,
        vehicleEdit: toggle.vehicleEdit,
        costingEdit: toggle.costingEdit,
        accquitionEdit: toggle.accquitionEdit,
        advertisingEdit: toggle.advertisingEdit,
        vehicleNote: toggle.vehicleNote,
        vehicleReminder: toggle.vehicleReminder,
        valuationEdit: toggle.valuationEdit,
        valuationAdd: toggle.valuationAdd,
        createProposal: toggle.createProposal,
        proposalEdit: toggle.proposalEdit,
        tradeInEdit: toggle.tradeInEdit,
        sellItemEdit: toggle.sellItemEdit,
        proposalFinalize: toggle.proposalFinalize,
        proposalLost: toggle.proposalLost,
        proposalReminder: toggle.proposalReminder,
        proposalNote: toggle.proposalNote,
        createBrokered: toggle.createBrokered,
        brokeredEdit: toggle.brokeredEdit,
        bTradeInEdit: toggle.bTradeInEdit,
        bSellItemEdit: toggle.bSellItemEdit,
        brokeredFinalize: toggle.brokeredFinalize,
        brokeredLost: toggle.brokeredLost,
        brokeredReminder: toggle.brokeredReminder,
        brokeredNote: toggle.brokeredNote,
        empDelete: toggle.empDelete,
        addEmp: toggle.addEmp,
        createCustomer: toggle.createCustomer,
        customerEdit: toggle.customerEdit,
        customerDelete: toggle.customerDelete,
        purchaseDelete: toggle.purchaseDelete,
        salesDelete: toggle.salesDelete,
        ppsrDelete: toggle.ppsrDelete,
        setting1: toggle.setting1,
        setting2: toggle.setting2,
        setting3: toggle.setting3,
        setting4: toggle.setting4,
        setting5: toggle.setting5,
        setting6: toggle.setting6,
      },
      {
        headers: { authorization: `Bearer ${localStorage.getItem('token$')}` },
      }
    )
      .then(({ data }) => {
        // console.log('save', data);
        alert.success('Employee Created');
        history.push('/emp');
      })
      .catch((err) => {
        console.log('Err', err.response.data.message);
        alert.success('Some of the fields might be invalid..!');
      });
  };

  const [errors, setErrors] = useState({
    firstName: false,
    lastName: false,
    department: false,
    email: false,
    mobile: false,
    userType: false,
    password: false,
    confirmPassword: false,
  });

  const [isValid, setisValid] = useState(true);

  const HandleValidation = () => {
    let shouldsubmit = true;
    let tempError = errors;

    for (const [key, value] of Object.entries(empDetails)) {
      if (value == '') {
        tempError[key] = true;
        if (shouldsubmit) {
          shouldsubmit = false;
        }
      } else {
        tempError[key] = false;
      }
    }
    // console.log(tempError);
    setisValid(shouldsubmit);
    setErrors(tempError);

    if (shouldsubmit) {
      OnSubmit();
    }
  };

  return (
    <>
      <Row className='page-title align-items-center'>
        <Col sm={4} xl={6}>
          <h4 className='mb-1 mt-0'>Employee</h4>
        </Col>
      </Row>

      <div className='row'>
        <div className='col-12'>
          <div className='card p-3'>
            <h4 className='mb-4'> Create Employee </h4>

            <div className='form-row'>
              {[
                { label: 'First Name', name: 'firstName', type: 'text' },
                { label: 'Last Name', name: 'lastName', type: 'text' },
                { label: 'Email', name: 'email', type: 'text' },
                { label: 'Phone', name: 'mobile', type: 'text' },
                { label: 'Password', name: 'password', type: 'text' },
                {
                  label: 'Confirm Password',
                  name: 'confirmPassword',
                  type: 'text',
                },
              ].map((item, i) => (
                <div className='form-group col-md-4' key={i}>
                  <Input
                    label={item.label}
                    type={item.type}
                    error={errors[item.name]}
                    name={item.name}
                    id={item.name}
                    value={empDetails[item.name]}
                    handleChange={(e) => {
                      handelChange(e);
                    }}
                  />
                </div>
              ))}

              <div className='form-group col-md-4'>
                <label>Department</label>
                <select
                  className={`form-control ${
                    errors.department ? 'is-invalid' : ''
                  }`}
                  id='department'
                  name='department'
                  value={empDetails.department}
                  onChange={handelChange}>
                  <option>Select Department</option>
                  <option value='Manager'>Manager</option>
                  <option value='Employee'>Employee</option>
                </select>
                {errors.department && (
                  <div className='invalid-feedback'>Required</div>
                )}
              </div>
              <div className='form-group col-md-4'>
                <label>User Type</label>
                <select
                  className={`form-control ${
                    errors.userType ? 'is-invalid' : ''
                  }`}
                  id='userType'
                  name='userType'
                  value={empDetails.userType}
                  onChange={handelChange}>
                  <option>Select User Type</option>
                  <option value='1'>Employee-1</option>
                  <option value='2'>Employee-2</option>
                  <option value='3'>Employee-3</option>
                  <option value='4'>Employee-3</option>
                </select>
                {errors.userType && (
                  <div className='invalid-feedback'>Required</div>
                )}
              </div>
            </div>

            <h4>Permission</h4>
            <h6>Lead:</h6>
            <div className='row'>
              <div className='form-group  col-md-3'>
                <CustomInput
                  type='switch'
                  id='leadSelect-all'
                  name='customSwitch'
                  label='Select All'
                  onChange={(e) => {
                    handelSelect(e);
                  }}
                />
              </div>
              <div className='form-group  col-md-3'>
                <CustomInput
                  type='switch'
                  id='1'
                  name='customSwitch'
                  label='Create'
                  checked={toggle.createLead}
                  onChange={(e) => {
                    if (e.target.checked) {
                      setToggle({ ...toggle, createLead: true });
                    } else {
                      setToggle({ ...toggle, createLead: false });
                    }
                  }}
                />
              </div>
              <div className='form-group  col-md-3'>
                <CustomInput
                  type='switch'
                  id='2'
                  name='customSwitch'
                  label='Edit'
                  checked={toggle.editLead}
                  onChange={(e) => {
                    if (e.target.checked) {
                      setToggle({ ...toggle, editLead: true });
                    } else {
                      setToggle({ ...toggle, editLead: false });
                    }
                  }}
                />
              </div>
              <div className='form-group  col-md-3'>
                <CustomInput
                  type='switch'
                  id='3'
                  name='customSwitch'
                  label='Delite'
                  checked={toggle.deleteLead}
                  onChange={(e) => {
                    if (e.target.checked) {
                      setToggle({ ...toggle, deleteLead: true });
                    } else {
                      setToggle({ ...toggle, deleteLead: false });
                    }
                  }}
                />
              </div>
              <div className='form-group  col-md-3'>
                <CustomInput
                  type='switch'
                  id='4'
                  name='customSwitch'
                  label='Add Note'
                  checked={toggle.leadNote}
                  onChange={(e) => {
                    if (e.target.checked) {
                      setToggle({ ...toggle, leadNote: true });
                    } else {
                      setToggle({ ...toggle, leadNote: false });
                    }
                  }}
                />
              </div>

              <div className='form-group  col-md-3'>
                <CustomInput
                  type='switch'
                  id='5'
                  name='customSwitch'
                  label='Send Mail'
                  checked={toggle.leadMail}
                  onChange={(e) => {
                    if (e.target.checked) {
                      setToggle({ ...toggle, leadMail: true });
                    } else {
                      setToggle({ ...toggle, leadMail: false });
                    }
                  }}
                />
              </div>
              <div className='form-group  col-md-3'>
                <CustomInput
                  type='switch'
                  id='6'
                  name='customSwitch'
                  label='Set Reminder'
                  checked={toggle.leadReminder}
                  onChange={(e) => {
                    if (e.target.checked) {
                      setToggle({ ...toggle, leadReminder: true });
                    } else {
                      setToggle({ ...toggle, leadReminder: false });
                    }
                  }}
                />
              </div>
              <div className='form-group  col-md-3'>
                <CustomInput
                  type='switch'
                  id='7'
                  name='customSwitch'
                  label='Lead Board'
                  checked={toggle.leadBoard}
                  onChange={(e) => {
                    if (e.target.checked) {
                      setToggle({ ...toggle, leadBoard: true });
                    } else {
                      setToggle({ ...toggle, leadBoard: false });
                    }
                  }}
                />
              </div>
            </div>

            <h6>Inventory:</h6>
            <div className='row'>
              <div className='form-group  col-md-3'>
                <CustomInput
                  type='switch'
                  id='iSelect-all'
                  name='customSwitch1'
                  label='Select All'
                  onChange={(e) => {
                    handelSelect(e);
                  }}
                />
              </div>
              <div className='form-group  col-md-3'>
                <CustomInput
                  type='switch'
                  id='8'
                  name='customSwitch1'
                  label='Create Stock'
                  checked={toggle.createStock}
                  onChange={(e) => {
                    if (e.target.checked) {
                      setToggle({ ...toggle, createStock: true });
                    } else {
                      setToggle({ ...toggle, createStock: false });
                    }
                  }}
                />
              </div>
              <div className='form-group  col-md-3'>
                <CustomInput
                  type='switch'
                  id='9'
                  name='customSwitch1'
                  label='Create Costing'
                  checked={toggle.createCosting}
                  onChange={(e) => {
                    if (e.target.checked) {
                      setToggle({ ...toggle, createCosting: true });
                    } else {
                      setToggle({ ...toggle, createCosting: false });
                    }
                  }}
                />
              </div>
              <div className='form-group  col-md-3'>
                <CustomInput
                  type='switch'
                  id='10'
                  name='customSwitch1'
                  label='Sell Vehicle'
                  checked={toggle.sellVehicle}
                  onChange={(e) => {
                    if (e.target.checked) {
                      setToggle({ ...toggle, sellVehicle: true });
                    } else {
                      setToggle({ ...toggle, sellVehicle: false });
                    }
                  }}
                />
              </div>
              <div className='form-group  col-md-3'>
                <CustomInput
                  type='switch'
                  id='11'
                  name='customSwitch1'
                  label='Vehicle Edit'
                  checked={toggle.vehicleEdit}
                  onChange={(e) => {
                    if (e.target.checked) {
                      setToggle({ ...toggle, vehicleEdit: true });
                    } else {
                      setToggle({ ...toggle, vehicleEdit: false });
                    }
                  }}
                />
              </div>

              <div className='form-group  col-md-3'>
                <CustomInput
                  type='switch'
                  id='12'
                  name='customSwitch1'
                  label='Acquition Edit'
                  checked={toggle.accquitionEdit}
                  onChange={(e) => {
                    if (e.target.checked) {
                      setToggle({ ...toggle, accquitionEdit: true });
                    } else {
                      setToggle({ ...toggle, accquitionEdit: false });
                    }
                  }}
                />
              </div>
              <div className='form-group  col-md-3'>
                <CustomInput
                  type='switch'
                  id='13'
                  name='customSwitch1'
                  label='Costing Edit'
                  checked={toggle.costingEdit}
                  onChange={(e) => {
                    if (e.target.checked) {
                      setToggle({ ...toggle, costingEdit: true });
                    } else {
                      setToggle({ ...toggle, costingEdit: false });
                    }
                  }}
                />
              </div>
              <div className='form-group  col-md-3'>
                <CustomInput
                  type='switch'
                  id='14'
                  name='customSwitch1'
                  label='Advertising Edit'
                  checked={toggle.advertisingEdit}
                  onChange={(e) => {
                    if (e.target.checked) {
                      setToggle({ ...toggle, advertisingEdit: true });
                    } else {
                      setToggle({ ...toggle, advertisingEdit: false });
                    }
                  }}
                />
              </div>
              <div className='form-group  col-md-3'>
                <CustomInput
                  type='switch'
                  id='15'
                  name='customSwitch1'
                  label='Vehicle Note'
                  checked={toggle.vehicleNote}
                  onChange={(e) => {
                    if (e.target.checked) {
                      setToggle({ ...toggle, vehicleNote: true });
                    } else {
                      setToggle({ ...toggle, vehicleNote: false });
                    }
                  }}
                />
              </div>
              <div className='form-group  col-md-3'>
                <CustomInput
                  type='switch'
                  id='16'
                  name='customSwitch1'
                  label='Vehicle Reminder'
                  checked={toggle.vehicleReminder}
                  onChange={(e) => {
                    if (e.target.checked) {
                      setToggle({ ...toggle, vehicleReminder: true });
                    } else {
                      setToggle({ ...toggle, vehicleReminder: false });
                    }
                  }}
                />
              </div>
              <div className='form-group  col-md-3'>
                <CustomInput
                  type='switch'
                  id='17'
                  name='customSwitch1'
                  label='Valuation Add'
                  checked={toggle.valuationAdd}
                  onChange={(e) => {
                    if (e.target.checked) {
                      setToggle({ ...toggle, valuationAdd: true });
                    } else {
                      setToggle({ ...toggle, valuationAdd: false });
                    }
                  }}
                />
              </div>
              <div className='form-group  col-md-3'>
                <CustomInput
                  type='switch'
                  id='18'
                  name='customSwitch1'
                  label='Valuation Edit'
                  checked={toggle.valuationEdit}
                  onChange={(e) => {
                    if (e.target.checked) {
                      setToggle({ ...toggle, valuationEdit: true });
                    } else {
                      setToggle({ ...toggle, valuationEdit: false });
                    }
                  }}
                />
              </div>
            </div>

            <h6>Deal:</h6>
            <div className='row'>
              <div className='form-group  col-md-3'>
                <CustomInput
                  type='switch'
                  id='dSelect-all'
                  name='customSwitch2'
                  label='Select All'
                  onChange={(e) => {
                    handelSelect(e);
                  }}
                />
              </div>
              <div className='form-group  col-md-3'>
                <CustomInput
                  type='switch'
                  id='19'
                  name='customSwitch2'
                  label='Create Proposal'
                  checked={toggle.createProposal}
                  onChange={(e) => {
                    if (e.target.checked) {
                      setToggle({ ...toggle, createProposal: true });
                    } else {
                      setToggle({ ...toggle, createProposal: false });
                    }
                  }}
                />
              </div>
              <div className='form-group  col-md-3'>
                <CustomInput
                  type='switch'
                  id='20'
                  name='customSwitch2'
                  label='Edit Proposal'
                  checked={toggle.proposalEdit}
                  onChange={(e) => {
                    if (e.target.checked) {
                      setToggle({ ...toggle, proposalEdit: true });
                    } else {
                      setToggle({ ...toggle, proposalEdit: false });
                    }
                  }}
                />
              </div>
              <div className='form-group  col-md-3'>
                <CustomInput
                  type='switch'
                  id='21'
                  name='customSwitch2'
                  label='Edit Trade-In'
                  checked={toggle.tradeInEdit}
                  onChange={(e) => {
                    if (e.target.checked) {
                      setToggle({ ...toggle, tradeInEdit: true });
                    } else {
                      setToggle({ ...toggle, tradeInEdit: false });
                    }
                  }}
                />
              </div>
              <div className='form-group  col-md-3'>
                <CustomInput
                  type='switch'
                  id='22'
                  name='customSwitch2'
                  label='Edit Sell Item'
                  checked={toggle.sellItemEdit}
                  onChange={(e) => {
                    if (e.target.checked) {
                      setToggle({ ...toggle, sellItemEdit: true });
                    } else {
                      setToggle({ ...toggle, sellItemEdit: false });
                    }
                  }}
                />
              </div>

              <div className='form-group  col-md-3'>
                <CustomInput
                  type='switch'
                  id='23'
                  name='customSwitch2'
                  label='Proposal Finalize'
                  checked={toggle.proposalFinalize}
                  onChange={(e) => {
                    if (e.target.checked) {
                      setToggle({ ...toggle, proposalFinalize: true });
                    } else {
                      setToggle({ ...toggle, proposalFinalize: false });
                    }
                  }}
                />
              </div>
              <div className='form-group  col-md-3'>
                <CustomInput
                  type='switch'
                  id='24'
                  name='customSwitch2'
                  label='Proposal Lost'
                  checked={toggle.proposalLost}
                  onChange={(e) => {
                    if (e.target.checked) {
                      setToggle({ ...toggle, proposalLost: true });
                    } else {
                      setToggle({ ...toggle, proposalLost: false });
                    }
                  }}
                />
              </div>
              <div className='form-group  col-md-3'>
                <CustomInput
                  type='switch'
                  id='25'
                  name='customSwitch2'
                  label='Proposal Note'
                  checked={toggle.proposalNote}
                  onChange={(e) => {
                    if (e.target.checked) {
                      setToggle({ ...toggle, proposalNote: true });
                    } else {
                      setToggle({ ...toggle, proposalNote: false });
                    }
                  }}
                />
              </div>
              <div className='form-group  col-md-3'>
                <CustomInput
                  type='switch'
                  id='26'
                  name='customSwitch2'
                  label='Proposal Reminder'
                  checked={toggle.proposalReminder}
                  onChange={(e) => {
                    if (e.target.checked) {
                      setToggle({ ...toggle, proposalReminder: true });
                    } else {
                      setToggle({ ...toggle, proposalReminder: false });
                    }
                  }}
                />
              </div>
              <div className='form-group  col-md-3'>
                <CustomInput
                  type='switch'
                  id='27'
                  name='customSwitch2'
                  label='Create Brokered'
                  checked={toggle.createBrokered}
                  onChange={(e) => {
                    if (e.target.checked) {
                      setToggle({ ...toggle, createBrokered: true });
                    } else {
                      setToggle({ ...toggle, createBrokered: false });
                    }
                  }}
                />
              </div>
              <div className='form-group  col-md-3'>
                <CustomInput
                  type='switch'
                  id='28'
                  name='customSwitch2'
                  label='Edit Brokered'
                  checked={toggle.brokeredEdit}
                  onChange={(e) => {
                    if (e.target.checked) {
                      setToggle({ ...toggle, brokeredEdit: true });
                    } else {
                      setToggle({ ...toggle, brokeredEdit: false });
                    }
                  }}
                />
              </div>
              <div className='form-group  col-md-3'>
                <CustomInput
                  type='switch'
                  id='29'
                  name='customSwitch2'
                  label='Edit Brokered Trade-In'
                  checked={toggle.bTradeInEdit}
                  onChange={(e) => {
                    if (e.target.checked) {
                      setToggle({ ...toggle, bTradeInEdit: true });
                    } else {
                      setToggle({ ...toggle, bTradeInEdit: false });
                    }
                  }}
                />
              </div>
              <div className='form-group  col-md-3'>
                <CustomInput
                  type='switch'
                  id='30'
                  name='customSwitch2'
                  label='Edit Brokered Sell Item'
                  checked={toggle.bSellItemEdit}
                  onChange={(e) => {
                    if (e.target.checked) {
                      setToggle({ ...toggle, bSellItemEdit: true });
                    } else {
                      setToggle({ ...toggle, bSellItemEdit: false });
                    }
                  }}
                />
              </div>

              <div className='form-group  col-md-3'>
                <CustomInput
                  type='switch'
                  id='31'
                  name='customSwitch2'
                  label='Brokered Finalize'
                  checked={toggle.brokeredFinalize}
                  onChange={(e) => {
                    if (e.target.checked) {
                      setToggle({ ...toggle, brokeredFinalize: true });
                    } else {
                      setToggle({ ...toggle, brokeredFinalize: false });
                    }
                  }}
                />
              </div>
              <div className='form-group  col-md-3'>
                <CustomInput
                  type='switch'
                  id='32'
                  name='customSwitch2'
                  label='Brokered Lost'
                  checked={toggle.brokeredLost}
                  onChange={(e) => {
                    if (e.target.checked) {
                      setToggle({ ...toggle, brokeredLost: true });
                    } else {
                      setToggle({ ...toggle, brokeredLost: false });
                    }
                  }}
                />
              </div>
              <div className='form-group  col-md-3'>
                <CustomInput
                  type='switch'
                  id='33'
                  name='customSwitch2'
                  label='Brokered Note'
                  checked={toggle.brokeredNote}
                  onChange={(e) => {
                    if (e.target.checked) {
                      setToggle({ ...toggle, brokeredNote: true });
                    } else {
                      setToggle({ ...toggle, brokeredNote: false });
                    }
                  }}
                />
              </div>
              <div className='form-group  col-md-3'>
                <CustomInput
                  type='switch'
                  id='34'
                  name='customSwitch2'
                  label='Brokered Reminder'
                  checked={toggle.brokeredReminder}
                  onChange={(e) => {
                    if (e.target.checked) {
                      setToggle({ ...toggle, brokeredReminder: true });
                    } else {
                      setToggle({ ...toggle, brokeredReminder: false });
                    }
                  }}
                />
              </div>
            </div>

            <h6>Customer:</h6>
            <div className='row'>
              <div className='form-group  col-md-3'>
                <CustomInput
                  type='switch'
                  id='35'
                  name='customSwitch66'
                  label='Create Customer'
                  checked={toggle.createCustomer}
                  onChange={(e) => {
                    if (e.target.checked) {
                      setToggle({ ...toggle, createCustomer: true });
                    } else {
                      setToggle({ ...toggle, createCustomer: false });
                    }
                  }}
                />
              </div>
              <div className='form-group  col-md-3'>
                <CustomInput
                  type='switch'
                  id='36'
                  name='customSwitch45'
                  label='Edit Customer'
                  checked={toggle.customerEdit}
                  onChange={(e) => {
                    if (e.target.checked) {
                      setToggle({ ...toggle, customerEdit: true });
                    } else {
                      setToggle({ ...toggle, customerEdit: false });
                    }
                  }}
                />
              </div>
              <div className='form-group  col-md-3'>
                <CustomInput
                  type='switch'
                  id='37'
                  name='customSwitch125'
                  label='Delete Customer'
                  checked={toggle.customerDelete}
                  onChange={(e) => {
                    if (e.target.checked) {
                      setToggle({ ...toggle, customerDelete: true });
                    } else {
                      setToggle({ ...toggle, customerDelete: false });
                    }
                  }}
                />
              </div>
            </div>

            <h6>Employee:</h6>
            <div className='row'>
              <div className='form-group  col-md-3'>
                <CustomInput
                  type='switch'
                  id='38'
                  name='customSwitch3545'
                  label='Create Employee'
                  checked={toggle.addEmp}
                  onChange={(e) => {
                    if (e.target.checked) {
                      setToggle({ ...toggle, addEmp: true });
                    } else {
                      setToggle({ ...toggle, addEmp: false });
                    }
                  }}
                />
              </div>
              <div className='form-group  col-md-3'>
                <CustomInput
                  type='switch'
                  id='39'
                  name='customSwitch5555'
                  label='Delete Employee'
                  checked={toggle.empDelete}
                  onChange={(e) => {
                    if (e.target.checked) {
                      setToggle({ ...toggle, empDelete: true });
                    } else {
                      setToggle({ ...toggle, empDelete: false });
                    }
                  }}
                />
              </div>
            </div>

            {/* <h6>Other:</h6>
            <div className="row">
              <div className="form-group  col-md-3">
                <CustomInput
                  type="switch"
                  id="40"
                  name="customSwitch2343"
                  label="Purchase Delete"
                  checked={toggle.purchaseDelete}
                  onChange={(e) => {
                    if (e.target.checked) {
                      setToggle({ ...toggle, purchaseDelete: true });
                    } else {
                      setToggle({ ...toggle, purchaseDelete: false });
                    }
                  }}
                />
              </div>
              <div className="form-group  col-md-3">
                <CustomInput
                  type="switch"
                  id="41"
                  name="customSwitch2333"
                  label="Sales Delete"
                  checked={toggle.salesDelete}
                  onChange={(e) => {
                    if (e.target.checked) {
                      setToggle({ ...toggle, salesDelete: true });
                    } else {
                      setToggle({ ...toggle, salesDelete: false });
                    }
                  }}
                />
              </div>
              <div className="form-group  col-md-3">
                <CustomInput
                  type="switch"
                  id="42"
                  name="customSwitch15544"
                  label="PPSR Delete"
                  checked={toggle.ppsrDelete}
                  onChange={(e) => {
                    if (e.target.checked) {
                      setToggle({ ...toggle, ppsrDelete: true });
                    } else {
                      setToggle({ ...toggle, ppsrDelete: false });
                    }
                  }}
                />
              </div>
            </div> */}

            <h6>Setting:</h6>
            <div className='row'>
              <div className='form-group  col-md-3'>
                <CustomInput
                  type='switch'
                  id='sSelect-all'
                  name='customSwitch3'
                  label='Select All'
                  onChange={(e) => {
                    handelSelect(e);
                  }}
                />
              </div>
              <div className='form-group  col-md-3'>
                <CustomInput
                  type='switch'
                  id='43'
                  name='customSwitch3'
                  label='General Setting'
                  checked={toggle.setting1}
                  onChange={(e) => {
                    if (e.target.checked) {
                      setToggle({ ...toggle, setting1: true });
                    } else {
                      setToggle({ ...toggle, setting1: false });
                    }
                  }}
                />
              </div>
              <div className='form-group  col-md-3'>
                <CustomInput
                  type='switch'
                  id='44'
                  name='customSwitch3'
                  label='UI Setting'
                  checked={toggle.setting2}
                  onChange={(e) => {
                    if (e.target.checked) {
                      setToggle({ ...toggle, setting2: true });
                    } else {
                      setToggle({ ...toggle, setting2: false });
                    }
                  }}
                />
              </div>
              {/* <div className='form-group  col-md-3'>
                <CustomInput
                  type='switch'
                  id='45'
                  name='customSwitch3'
                  label='Setting 3'
                  checked={toggle.setting3}
                  onChange={(e) => {
                    if (e.target.checked) {
                      setToggle({ ...toggle, setting3: true });
                    } else {
                      setToggle({ ...toggle, setting3: false });
                    }
                  }}
                />
              </div>
              <div className='form-group  col-md-3'>
                <CustomInput
                  type='switch'
                  id='46'
                  name='customSwitch3'
                  label='Setting 4'
                  checked={toggle.setting4}
                  onChange={(e) => {
                    if (e.target.checked) {
                      setToggle({ ...toggle, setting4: true });
                    } else {
                      setToggle({ ...toggle, setting4: false });
                    }
                  }}
                />
              </div>
              <div className='form-group  col-md-3'>
                <CustomInput
                  type='switch'
                  id='47'
                  name='customSwitch3'
                  label='Setting 5'
                  checked={toggle.setting5}
                  onChange={(e) => {
                    if (e.target.checked) {
                      setToggle({ ...toggle, setting5: true });
                    } else {
                      setToggle({ ...toggle, setting5: false });
                    }
                  }}
                />
              </div>
              <div className='form-group  col-md-3'>
                <CustomInput
                  type='switch'
                  id='48'
                  name='customSwitch3'
                  label='Setting 6'
                  checked={toggle.setting6}
                  onChange={(e) => {
                    if (e.target.checked) {
                      setToggle({ ...toggle, setting6: true });
                    } else {
                      setToggle({ ...toggle, setting6: false });
                    }
                  }}
                />
              </div> */}
            </div>

            <div
              className='form-row'
              style={{ display: 'grid', placeItems: 'center' }}>
              <div className='form-group col-md-2'>
                <button
                  className='btn btn-primary btn-block'
                  onClick={HandleValidation}>
                  Save
                </button>
              </div>
            </div>
          </div>
        </div>{' '}
        {/* end Col */}
      </div>
    </>
  );
};

export default AddEmp;
