/** @format */

import React, { useState, useEffect, useContext } from 'react';
import { Row, Col, Table } from 'reactstrap';
import LeftMenu from './leftSidebar';
import { useHistory } from 'react-router-dom';
import { Button } from 'reactstrap';
import Instance from '../../Instance';
import moment from 'moment';
import { useAlert } from 'react-alert';
import Context2 from '../../routes/Context';

const Emp = () => {
  let history = useHistory();
  const alert = useAlert();
  const { employee } = useContext(Context2);
  const [empView, setEmpView] = useState(null);
  const [loader, setLoader] = useState(false);
  const [refresh, setRefresh] = useState(false);
  const [count, setCount] = useState(1);
  const [page, setPage] = useState(null);

  useEffect(() => {
    setLoader(true);
    Instance.post(
      `/api/user/employees`,
      {
        page: count,
      },
      {
        headers: { authorization: `Bearer ${localStorage.getItem('token$')}` },
      }
    )
      .then(({ data }) => {
        // console.log('empData', data);
        let temp = data?.results?.allPurchases;
        setPage(data?.results);
        setEmpView(data?.results?.employee);
        setLoader(false);
      })
      .catch((err) => {
        console.log('err', err);
      });
  }, [count, refresh]);

  const NextPage = () => {
    setCount(count + 1);
  };
  const PrevPage = () => {
    setCount(count - 1);
  };

  const EmpDelete = (value) => {
    Instance.delete(`/api/user/employee/${value._id}`, {
      headers: { authorization: `Bearer ${localStorage.getItem('token$')}` },
    })
      .then(() => {
        // console.log('Success');
        setRefresh(!refresh);
        alert.success('Employee Deleted');
      })
      .catch((err) => {
        console.log('Error', err);
      });
  };

  const ViewDetails = (value) => {
    history.push(`/emp/detail/${value._id}`);
  };

  return (
    <>
      <Row className='page-title align-items-center'>
        <Col sm={4} xl={6}>
          <h4 className='mb-1 mt-0'>Employee</h4>
        </Col>
      </Row>

      <div className='row'>
        <div className='col-2 Lead_Board'>
          <div className='email-container bg-transparent'>
            {/* Left sidebar */}
            <LeftMenu />
            {/* End Left sidebar */}
          </div>
        </div>
        <div className='col-10'>
          <div className='card'>
            <div className='card p-3'>
              <div className='row'>
                <div className='col-6'>
                  <h4> All Employee </h4>
                </div>
                <div className='col-md-6 text-right'>
                  <div className='d-inline-block align-middle float-lg-right'>
                    <div className='row'>
                      <div className='col-8 align-self-center' style={{ whiteSpace:"nowrap"}}>
                        Showing {page?.startOfItem} - {page?.endOfItem} of{' '}
                        {page?.totalEmployeedb}
                      </div>
                      {/* end col*/}
                      <div className='col-4'>
                        <div className='btn-group float-right'>
                          <button
                            type='button'
                            onClick={PrevPage}
                            className='btn btn-white btn-sm'
                            style={{
                              display: page?.Previous === false ? 'none' : '',
                            }}>
                            <i
                              className='uil uil-angle-left'
                          
                            />
                          </button>
                          <button
                            type='button'
                            onClick={NextPage}
                            className='btn btn-primary btn-sm'
                            style={{
                              display: page?.Next === false ? 'none' : '',
                            }}>
                            <i
                              className='uil uil-angle-right'
                             
                            />
                          </button>
                        </div>
                      </div>{' '}
                      {/* end col*/}
                    </div>
                  </div>
                </div>
              </div>
              <div>
                {loader ? (
                  <>
                    <div class='d-flex justify-content-center'>
                      <div class='spinner-border' role='status'>
                        <span class='sr-only'>Loading...</span>
                      </div>
                    </div>{' '}
                  </>
                ) : (
                  <>
                    <Table hover responsive className='mt-4'>
                      {empView != null &&
                        empView.map((value, i) => (
                          <tbody key={i}>
                            <tr>
                              <td>
                                <a>{i + 1}</a>
                              </td>
                              <td>
                                {value.firstName} {value.lastName},{value.email}
                              </td>
                              <td>
                                {' '}
                                {moment(new Date(value.createdAt)).format(
                                  'DD/MM/YYYY'
                                )}
                                ,
                                {new Date(value.createdAt).toLocaleTimeString()}
                              </td>

                              <td>
                                <div className='button-list'>
                                  <a
                                    className='btn btn-primary  btn-sm'
                                    onClick={() => ViewDetails(value)}>
                                    VIEW
                                  </a>
                                  {employee?.empDelete === true ? (
                                    <Button
                                      className='btn btn-primary  btn-sm'
                                      onClick={() => EmpDelete(value)}>
                                      DELITE
                                    </Button>
                                  ) : (
                                    ''
                                  )}
                                </div>
                              </td>
                            </tr>
                          </tbody>
                        ))}
                    </Table>
                  </>
                )}
              </div>
            </div>
          </div>
        </div>{' '}
        {/* end Col */}
      </div>
      {/* End row */}
    </>
  );
};

export default Emp;
