/** @format */

import React, { useState, useEffect, useContext } from 'react';
import { Link } from 'react-router-dom';
import Instance from '../../Instance';
import Context2 from '../../routes/Context';

const LeftSidebar = () => {
  const [empCount, setEmpCount] = useState(null);
  const [refresh, setRefresh] = useState(false);
  const { employee } = useContext(Context2);

  useEffect(() => {
    Instance.get(`/api/user/employees/summary`, {
      headers: { authorization: `Bearer ${localStorage.getItem('token$')}` },
    })
      .then(({ data }) => {
        // console.log('empCounting', data);
        setEmpCount(data.totalEmployess);
        setRefresh(!refresh);
      })
      .catch((err) => {
        console.log('err', err);
      });
  }, []);

  return (
    <>
      <div>
        {/* Left sidebar */}
        <div className='p-3 card'>
          {employee?.addEmp === true ? (
            <Link to='/add/emp' className='btn btn-danger btn-block mb-3'>
              {' '}
              Create Employee{' '}
            </Link>
          ) : (
            ''
          )}

          <div className='mail-list '>
            <Link to='/emp' className='list-group-item border-0'>
              <i className='uil uil-receipt-alt font-size-15 mr-1' />
              Employee
              <span className='badge badge-danger float-right ml-2 mt-1'>
                {empCount}
              </span>
            </Link>
          </div>
        </div>

        {/* End Left sidebar */}
      </div>
    </>
  );
};

export default LeftSidebar;
