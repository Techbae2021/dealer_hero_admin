/** @format */

import React, { useState, useContext } from 'react';
import { Row, Col } from 'reactstrap';
import { useHistory, Redirect } from 'react-router-dom';
import Instance from '../../Instance';
import Context from '../context/Context';
import { useAlert } from 'react-alert';

const EditEmp = () => {
  let history = useHistory();
  const alert = useAlert();
  const { details } = useContext(Context);
  // console.log(details)

  const [empDetails, setEmpDetails] = useState({
    firstName: details != null ? details.firstName : '',
    lastName: details != null ? details.lastName : '',
    department: details != null ? details.department : '',
    email: details != null ? details.email : '',
    mobile: details != null ? details.mobile : '',
    userType: details != null ? details.userType : '',
  });

  const handelChange = (event) => {
    setEmpDetails({
      ...empDetails,
      [event.target.id]: event.target.value,
    });
  };

  const OnUpdated = (e) => {
    e.preventDefault();
    Instance.put(
      `/api/user/employee/details/${details._id}`,
      {
        firstName: empDetails.firstName,
        lastName: empDetails.lastName,
        department: empDetails.department,
        email: empDetails.email,
        mobile: empDetails.mobile,
        userType: empDetails.userType,
      },
      {
        headers: { authorization: `Bearer ${localStorage.getItem('token$')}` },
      }
    )
      .then(({ data }) => {
        // console.log('save', data);
        alert.success('Employee Edited');
        history.push(`/emp/detail/${details._id}`);
      })
      .catch((err) => {
        console.log('Err', err);
      });
  };

  if (details == null) {
    return <Redirect to={`/emp/detail/${details._id}`} />;
  }

  return (
    <>
      <Row className='page-title align-items-center'>
        <Col sm={4} xl={6}>
          <h4 className='mb-1 mt-0'>Employee</h4>
        </Col>
      </Row>

      <div className='row'>
        <div className='col-12'>
          <div className='card p-3'>
            <h4 className='mb-4'> Edit Employee </h4>

            <div className='form-row'>
              <div className='form-group col-md-4'>
                <label>First Name</label>
                <input
                  type='text'
                  className='form-control'
                  placeholder='First Name'
                  id='firstName'
                  value={empDetails.firstName}
                  onChange={handelChange}></input>
              </div>
              <div className='form-group col-md-4'>
                <label>Last Name</label>
                <input
                  type='text'
                  className='form-control'
                  placeholder='Last Name'
                  id='lastName'
                  value={empDetails.lastName}
                  onChange={handelChange}></input>
              </div>
              <div className='form-group col-md-4'>
                <label>Department</label>
                <select
                  className='form-control'
                  id='department'
                  value={empDetails.department}
                  onChange={handelChange}>
                  <option>Select Department</option>
                  <option value='Manager'>Manager</option>
                  <option value='Employee'>Employee</option>
                </select>
              </div>
              <div className='form-group col-md-4'>
                <label>Email</label>
                <input
                  type='text'
                  className='form-control'
                  placeholder='Email'
                  id='email'
                  value={empDetails.email}
                  onChange={handelChange}
                  disabled></input>
              </div>
              <div className='form-group col-md-4'>
                <label>Mobile</label>
                <input
                  type='text'
                  className='form-control'
                  placeholder='Mobile'
                  id='mobile'
                  value={empDetails.mobile}
                  onChange={handelChange}></input>
              </div>

              <div className='form-group col-md-4'>
                <label>User Type</label>
                <select
                  className='form-control'
                  id='userType'
                  value={empDetails.userType}
                  onChange={handelChange}>
                  <option>Select User Type</option>
                  <option value='1'>Employee-1</option>
                  <option value='2'>Employee-2</option>
                  <option value='3'>Employee-3</option>
                  <option value='4'>Employee-3</option>
                </select>
              </div>
            </div>

            <div
              className='form-row'
              style={{ display: 'grid', placeItems: 'center' }}>
              <div className='form-group col-md-2'>
                <button
                  className='btn btn-primary btn-block'
                  onClick={OnUpdated}>
                  {' '}
                  Save{' '}
                </button>
              </div>
            </div>
          </div>
        </div>{' '}
        {/* end Col */}
      </div>
    </>
  );
};

export default EditEmp;
