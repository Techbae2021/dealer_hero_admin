/** @format */

import React, { useState, useEffect} from 'react';
import { Row, Col, Table } from 'reactstrap';
import CSidebar from './dSidebar';
import Instance from '../../Instance';
import { useHistory } from 'react-router-dom';
import moment from 'moment';
import { useAlert } from 'react-alert';
import AuthUser from '../../components/auth/AuthUser';

const Dealer = () => {
  const alert = useAlert();
  let history = useHistory();
  const [dealerView, setDealerView] = useState(null);
  const [loader, setLoader] = useState(false);
  const [refresh, setRefresh] = useState(false);
  const [count, setCount] = useState(1);
  const [totalCount, settotalCount] = useState(0);
  const [page, setPage] = useState(null);

  useEffect(() => {
    setLoader(true)
    Instance.post(
      `/api/admin/getDealers`,
      {
        page: count,
      },
      {
        headers: { authorization: `Bearer ${localStorage.getItem('token$')}` },
      }
    ).then(({ data }) => {
        // console.log('dealerData', data);
        let temp = data?.results?.dealers;
        setPage(data?.results);
        setDealerView(temp);
        setLoader(false);
        settotalCount(data?.results?.totalIndb);
      }).catch((err) => {
        setLoader(false);
        console.log('err', err);
      })
  }, [count, refresh]);

  const NextPage = () => {
    setCount(count + 1);
  };
  const PrevPage = () => {
    setCount(count - 1);
  };

  const ViewDetails = (value) => {
    history.push(`/dealer/view/${value._id}`);
  };

  const Delete = (value) => {
    setLoader(true)
    Instance.delete(
      `/api/admin/deleteDealer/${value._id}`,
      {
        headers: { authorization: `Bearer ${localStorage.getItem('token$')}` },
      }).then(() => {
        setLoader(false)
        alert.success('Dealer Deleted');
        setRefresh(!refresh)
      }).catch((err) => {
        setLoader(false)
        alert.error('Somthing went wrong');
        console.log('Error', err);
      });
  };

  return (
    <AuthUser>
      <Row className='page-title align-items-center'>
        <Col sm={4} xl={6}>
          <h4 className='mb-1 mt-0'>Dealer</h4>
        </Col>
      </Row>

      <div className='row'>
        <div className='col-2 Lead_Board'>
          <div className='email-container bg-transparent'>
            {/* Left sidebar */}
            <CSidebar reload={refresh}/>
            {/* End Left sidebar */}
          </div>
        </div>
        <div className='col-10'>
          <div className='card'>
            <div className='card p-3'>
              <div className='row'>
                <div className='col-6'>
                  <h4> All Dealers </h4>
                </div>
                <div className='col-md-6 text-right'>
                  <div className='d-inline-block align-middle float-lg-right'>
                    <div className='row'>
                      <div
                        className='col-8 align-self-center'
                        style={{ whiteSpace: 'nowrap' }}>
                        Showing {page?.startOfItem} - {page?.endOfItem} of{' '}
                        {totalCount}
                      </div>
                      {/* end col*/}
                      <div className='col-4'>
                        <div className='btn-group float-right'>
                          <button
                            type='button'
                            className='btn btn-white btn-sm'
                            onClick={PrevPage}
                            style={{
                              display: page?.Previous === false ? 'none' : '',
                            }}>
                            <i
                              className='uil uil-angle-left'
                             
                            />
                          </button>
                          <button
                            type='button'
                            onClick={NextPage}
                            className='btn btn-primary btn-sm'
                            style={{
                              display: page?.Next === false ? 'none' : '',
                            }}>
                            <i className='uil uil-angle-right'
                             
                            />
                          </button>
                        </div>
                      </div>{' '}
                      {/* end col*/}
                    </div>
                  </div>
                </div>
              </div>
              <div>
              {loader ? (
                    <>
                      <div className='d-flex justify-content-center'>
                        <div className='spinner-border' role='status'>
                          <span className='sr-only'>Loading...</span>
                        </div>
                      </div>{' '}
                    </>
                  ) : (
                <Table hover responsive className='mt-4'>
                    <tbody>
                      {dealerView &&
                        dealerView.map((value, i) => (
                          <tr key={i}>
                            <td>{i + 1}</td>
                            <td>
                              {value.firstName}
                              <br />
                              {value.lastName}
                              <br />
                              {value.email}
                              <br />
                              {value.country}
                              <br />
                              {value.address}
                            </td>
                            <td>
                              {moment(new Date(value.createdAt)).format(
                                'DD/MM/YYYY'
                              )}
                              ,{new Date(value.createdAt).toLocaleTimeString()}
                            </td>
                            <td>
                              <div className='button-list'>
                                <button
                                  onClick={() => ViewDetails(value)}
                                  className='btn btn-success  btn-sm Button_Size'>
                                  View{' '}
                                </button>
                                  <button
                                    onClick={() => Delete(value)}
                                    type='button'
                                    className='btn btn-danger  btn-sm Button_Size'>
                                    Delete{' '}
                                  </button>
                              </div>
                            </td>
                          </tr>
                        ))}
                    </tbody>
                </Table>
                )}
              </div>
            </div>
          </div>
        </div>{' '}
        {/* end Col */}
      </div>
    </AuthUser>
  );
};

export default Dealer;
