/** @format */

import React, { useEffect, useState } from 'react';
import {
  Row,
  Col,
  CardBody,
  Card,
  Nav,
  NavItem,
  NavLink,
  TabPane,
  TabContent,
} from 'reactstrap';
import TopBar from './topBar';
import { useHistory, useParams } from 'react-router-dom';
import Instance from '../../Instance';
import AuthUser from '../../components/auth/AuthUser';
import DealerDetails from '../../components/dealer/DealerDetails';
import Inventory from '../../components/dealer/Inventory';
import Proposals from '../../components/dealer/Proposals';
import Rentals from '../../components/dealer/Rentals';
import Employees from '../../components/dealer/Employees';
import Finances from '../../components/dealer/Finances';
import Subscriptions from '../../components/dealer/Subscriptions';
import Payments from '../../components/dealer/Payments';
import DealerChatHistory from '../../components/dealer/DealerChatHistory';
import Emails from '../../components/dealer/Emails';
import Notes from '../../components/dealer/Notes';
import Reminders from '../../components/dealer/Reminders';
import DealerReport from '../../components/dealer/DealerReport';
import Notify from '../../components/dealer/NotifyHistory';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import { Box } from '@material-ui/core';

const AntTabs = withStyles({
  root: {
    borderBottom: '1px solid #e8e8e8',
  },
  indicator: {
    backgroundColor: '#1890ff',
  },
})(Tabs);

const AntTab = withStyles((theme) => ({
  root: {
    textTransform: 'none',
    minWidth: 72,
    fontWeight: theme.typography.fontWeightRegular,
    marginRight: theme.spacing(4),
    fontFamily: [
      '-apple-system',
      'BlinkMacSystemFont',
      '"Segoe UI"',
      'Roboto',
      '"Helvetica Neue"',
      'Arial',
      'sans-serif',
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"',
    ].join(','),
    '&:hover': {
      color: '#40a9ff',
      opacity: 1,
    },
    '&$selected': {
      color: '#1890ff',
      fontWeight: theme.typography.fontWeightMedium,
    },
    '&:focus': {
      color: '#40a9ff',
    },
  },
  selected: {},
}))((props) => <Tab disableRipple {...props} />);


function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`scrollable-auto-tabpanel-${index}`}
      aria-labelledby={`scrollable-auto-tab-${index}`}
      {...other}
    >
      {value === index && <Box p={3}>
        {children}
        </Box>}
    </div>
  );
}

const useStyles1 = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    width: '100%',
    backgroundColor: theme.palette.background.paper,
  },
}));

const CDetails = () => {
  const classes1 = useStyles1();
  const params = useParams();
  const [dealerView, setDealerView] = useState(null);
  const [loader, setLoader] = useState(false);
  const [total, setTotal] = useState({
    email:0,
    note:0,
    reminder:0
  });

  useEffect(() => {
    setLoader(true);
    Instance.get(`/api/admin/dealer/details/${params.id}`, {
      headers: { authorization: `Bearer ${localStorage.getItem('token$')}` },
    }).then(({ data }) => {
      // console.log('data', data)
      setDealerView(data);
      setTotal({
        email:data?.totalEmails,
        note:data?.totalNotes,
        reminder:data?.totalReminders
      })
      setLoader(false);
    }).catch((err) => {
      setLoader(false);
      console.log('err', err);
    });
  }, []);

  //TabHandle
  const [value, setValue] = React.useState(0);
  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  function a11yProps(index) {
    return {
      id: `scrollable-force-tab-${index}`,
      'aria-controls': `scrollable-force-tabpanel-${index}`,
    };
  }


  return (
    <AuthUser>
      <Row className='page-title align-items-center'>
        <Col sm={8}>
          <h4 className='mb-1 mt-0'> Dealer Details</h4>
        </Col>
        {/* <Col sm={4}>
          <TopBar id={params.id} />
        </Col> */}
      </Row>

      <Row>
      <div className="container-fluid">

        <Card>
          <CardBody>
            <div className={classes1.root}>
            <AntTabs 
              value={value}
              onChange={handleChange}
              indicatorColor="primary"
              textColor="primary"
              variant="scrollable"
              scrollButtons="auto"
              aria-label="scrollable auto tabs example"
              >
              <AntTab  label={(<span>Details</span>)}  {...a11yProps(0)}/>
              <AntTab {...a11yProps(1)} label={<span>Chat History </span>}/>
              <AntTab {...a11yProps(2)} label={<span>Emails  <span className="badge badge-pill badge-primary">{total?.email}</span></span>}/>
              <AntTab {...a11yProps(3)} label={<span>Notes  <span className="badge badge-pill badge-primary">{total?.note}</span></span>}/>
              <AntTab {...a11yProps(4)} label={<span>Reminders  <span className="badge badge-pill badge-primary">{total?.reminder}</span></span>}/>
              <AntTab {...a11yProps(5)} label={<span>Inventory <span className="badge badge-pill badge-primary">{dealerView?.totalVehicles}</span> </span>}/>
              <AntTab {...a11yProps(6)} label={<span>Proposals <span className="badge badge-pill badge-primary">{dealerView?.totalProposals}</span> </span>}/>
              <AntTab {...a11yProps(7)} label={<span>Rentals <span className="badge badge-pill badge-primary">{dealerView?.totalRentals}</span> </span>}/>
              <AntTab {...a11yProps(8)} label={<span>Employees <span className="badge badge-pill badge-primary">{dealerView?.totalEmployees}</span> </span>}/>
              <AntTab {...a11yProps(9)} label={<span>Finances <span className="badge badge-pill badge-primary">{dealerView?.totalFinances}</span> </span>}/>
              <AntTab  {...a11yProps(10)} label={<span>Subscriptions <span className="badge badge-pill badge-primary">{dealerView?.totalSubscriptions}</span> </span>}/>
              <AntTab {...a11yProps(11)} label={<span>Payments  </span>}/>
              <AntTab {...a11yProps(12)} label={<span>Report</span>}/>
              <AntTab {...a11yProps(13)} label={<span>Notifications History <span className="badge badge-pill badge-primary">{dealerView?.totalNotifications}</span></span>}/>
            </AntTabs>
            </div>
            <div className="row">
            {/* details */}
            <TabPanel value={value} index={0} className="col-md-12">
              {loader ? (
                    <div className="container">
                      <div className="row">
                        <div className='d-flex justify-content-center align-items-center'>
                          <div className='spinner-border' role='status'>
                            <span className='sr-only'>Loading...</span>
                          </div>
                        </div>
                      </div>
                    </div>
                  ) : <DealerDetails active={value === 0} dealerView={dealerView} />
                  }
            </TabPanel>
            {/* endOfDetails */}

              {/* tab Payments start */}
              <TabPanel value={value} index={1} className="col-md-12">
                    <DealerChatHistory active={value === 1} dealerId={params.id} />
                </TabPanel>
                {/* tab Payments end */}

                {/* tab Email start */}
                <TabPanel value={value} index={2} className="col-md-12">
                  <Emails active={value === 2} dealerId={params.id} totalEmailcallback={(e)=>setTotal({...total,email:e})} />
                </TabPanel>
                {/* tab Email end */}

                {/* tab Note start */}
                <TabPanel value={value} index={3} className="col-md-12">
                  <Notes active={value === 3} dealerId={params.id} totalNotecallback={(e)=>setTotal({...total,note:e})} />
                </TabPanel>
                {/* tab Note end */}
            
                  {/* tab Reminder start */}
                  <TabPanel value={value} index={4} className="col-md-12">
                        <Reminders active={value === 4} dealerId={params.id} totalRemindercallback={(e)=>setTotal({...total,reminder:e})} />
                  </TabPanel>
                {/* tab Reminder end */}

                {/* tab Inventory start */}
                <TabPanel value={value} index={5} className="col-md-12">
                  <Inventory active={value === 5} dealerId={params.id} />
                </TabPanel>
                {/* tab Inventory end */}

                {/* tab Proposals start */}
                <TabPanel value={value} index={6} className="col-md-12">
                  <Proposals active={value === 6} dealerId={params.id} />
                </TabPanel>
                {/* tab Proposals end */}

                {/* tab Rentals start */}
                <TabPanel value={value} index={7} className="col-md-12">
                  <Rentals active={value === 7} dealerId={params.id} />
                </TabPanel>
                {/* tab Rentals end */}

                {/* tab Employees start */}
                <TabPanel value={value} index={8} className="col-md-12">
                  <Employees active={value === 8} dealerId={params.id} />
                </TabPanel>
                {/* tab Employees end */}

                {/* tab Finance start */}
                <TabPanel value={value} index={9} className="col-md-12">
                  <Finances active={value === 9} dealerId={params.id} />
                </TabPanel>
                {/* tab Finance end */}

                 {/* tab Subscriptions start */}
                 <TabPanel value={value} index={10} className="col-md-12">
                  <Subscriptions active={value === 10} dealerId={params.id} />
                </TabPanel>
                {/* tab Subscriptions end */}

                 {/* tab Payments start */}
                 <TabPanel value={value} index={11} className="col-md-12">
                  <Payments active={value === 11} dealerId={params.id} />
                </TabPanel>
                {/* tab Payments end */}

                {/* tab Report start */}
                <TabPanel value={value} index={12} className="col-md-12">
                  <DealerReport active={value === 12} dealerId={params.id} />
                </TabPanel>
                {/* tab Report end */}

                {/* tab Notifications start */}
                <TabPanel value={value} index={13} className="col-md-12">
                  <Notify active={value === 13} dealerId={params.id} />
                </TabPanel>
                {/* tab Notifications end */}
            </div>
          </CardBody>
        </Card>
      </div>
      </Row>
    </AuthUser>
  );
};

export default CDetails;
