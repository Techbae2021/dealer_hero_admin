/** @format */

import React from 'react';
import { Link } from 'react-router-dom';
import { useHistory } from 'react-router-dom';

const TopBar = ({ id }) => {
  let history = useHistory();
  const Note = () => {
    history.push(`/dealer/note/${id}`);
  };
  const Reminder = () => {
    history.push(`/dealer/reminder/${id}`);
  };

  return (
    <>
      <div className='d-flex justify-content-end'>
        <Link to={`/dealer/mails/${id}`} className='btn btn-success mr-2 Button_Size'>
          Email
        </Link>
        <Link onClick={Note} className='btn btn-primary mr-2 Button_Size'>
          Note{' '}
        </Link>
        <Link onClick={Reminder} className='btn btn-warning mr-2 Button_Size'>
          Reminder{' '}
        </Link>
      </div>
    </>
  );
};

export default TopBar;
