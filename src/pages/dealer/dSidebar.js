import React from 'react';
import { Link } from 'react-router-dom';
import Instance from '../../Instance';

const CSidebar = (props) => {
  const [totalDealers, setDealers] = React.useState({dealers:0,delDealers:0});

  React.useEffect(()=>{
    Instance.get(
      `/api/admin/countDealers`,{
        headers: { authorization: `Bearer ${localStorage.getItem('token$')}` },
      }).then(({ data }) => {
        // console.log('asd',data)
        setDealers({dealers:data?.dealer,delDealers:data?.deletedDealer})
      }).catch((err) => {
        console.log('err', err);
      });
  },[props?.reload])

  return (
    <>
      <div>
        {/* Left sidebar */}
        <div className='p-3 card'>

          <div className='mail-list '>
            <Link to='/dealer' className='list-group-item border-0'>
              <i className='uil uil-receipt-alt font-size-15 mr-1' />
              Dealers
              <span className='badge badge-danger float-right ml-2 mt-1'>
                {totalDealers?.dealers}
              </span>
            </Link>

            <Link to='/dealer/deleted' className='list-group-item border-0'>
              <i className='uil uil-receipt-alt font-size-15 mr-1' /> Deleted
              <span className='badge badge-danger float-right ml-2 mt-1'>
                {totalDealers?.delDealers}
              </span>
            </Link>
          </div>
        </div>

        {/* End Left sidebar */}
      </div>
    </>
  );
};

export default CSidebar;
