/** @format */

export const PROFILE_DETAILS = 'PROFILE_DETAILS';
export const BRANCH_DETAILS = 'BRANCH_DETAILS';
export const LEAD_DETAILS = 'LEAD_DETAILS';
export const LEAD_NOTE_DETAILS = 'LEAD_NOTE_DETAILS';
export const LEAD_MAIL_DETAILS = 'LEAD_MAIL_DETAILS';
export const INVENTRY_DETAILS = 'INVENTRY_DETAILS';
export const EMP_DETAILS = 'EMP_DETAILS';
export const EMP_PERMISSION_DETAILS = 'EMP_PERMISSION_DETAILS';
export const INVENTORY_CURRENT_STEP = 'INVENTORY_CURRENT_STEP';
export const INVENTORY_VEHICLE = 'INVENTORY_VEHICLE';
export const INVENTORY_ACQUITION = 'INVENTORY_ACQUITION';
export const INVENTORY_ADVERTISING = 'INVENTORY_ADVERTISING';
export const INVENTORY_COST = 'INVENTORY_COST';
export const INVENTORY_DOCUMENT = 'INVENTORY_DOCUMENT';
export const INVENTORY_NOTE = 'INVENTORY_NOTE';
export const VALUATION_DETAILS = 'VALUATION_DETAILS';
export const PROPOSAL_CURRENT_STEP = 'PROPOSAL_CURRENT_STEP';
export const PROPOSAL_DETAILS = 'PROPOSAL_DETAILS';
export const PROPOSAL_TRADE_IN = 'PROPOSAL_TRADE_IN';
export const PROPOSAL_SELL_ITEM = 'PROPOSAL_SELL_ITEM';
export const PROPOSAL_NOTE = 'PROPOSAL_NOTE';
export const PROPOSAL_FINANCE_CURRENT_STEP = 'PROPOSAL_FINANCE_CURRENT_STEP';
export const BROKERED_CURRENT_STEP = 'BROKERED_CURRENT_STEP';
export const BROKERED_DETAILS = 'BROKERED_DETAILS';
export const BROKERED_TRADE_IN = 'BROKERED_TRADE_IN';
export const BROKERED_SELL_ITEM = 'BROKERED_SELL_ITEM';
export const BROKERED_NOTE = 'BROKERED_NOTE';
export const BROKERED_FINANCE_CURRENT_STEP = 'BROKERED_FINANCE_CURRENT_STEP';
export const RENTAL_DETAILS = 'RENTAL_DETAILS';
export const FINANCE_DETAILS = 'FINANCE_DETAILS';
export const FINANCE_NOTE = 'FINANCE_NOTE';
export const FINANCE_COMMENT = 'FINANCE_COMMENT';
export const CUSTOMER_DETAILS = 'CUSTOMER_DETAILS';
export const CUSTOMER_NOTE = 'CUSTOMER_NOTE';
export const MAIL_DETAILS = 'MAIL_DETAILS';
export const SETTING_RENTAL_DETAILS = 'SETTING_RENTAL_DETAILS';
export const SETTING_FINANCE_DETAILS = 'SETTING_FINANCE_DETAILS';
export const SETTING_HEADER_DETAILS = 'SETTING_HEADER_DETAILS';
export const SETTING_FOOTER_DETAILS = 'SETTING_FOOTER_DETAILS';
export const SETTING_TITLE_DETAILS = 'SETTING_TITLE_DETAILS';
export const SETTING_OFFER_DETAILS = 'SETTING_OFFER_DETAILS';
export const SETTING_SOCIAL_DETAILS = 'SETTING_SOCIAL_DETAILS';
export const SETTING_CONTACT_DETAILS = 'SETTING_CONTACT_DETAILS';
export const DASHBOARD_NOTE_DETAILS = 'DASHBOARD_NOTE_DETAILS';
