/** @format */

import {
  BRANCH_DETAILS,
  LEAD_MAIL_DETAILS,
  LEAD_NOTE_DETAILS,
  PROFILE_DETAILS,
  LEAD_DETAILS,
  INVENTORY_CURRENT_STEP,
  PROPOSAL_CURRENT_STEP,
  BROKERED_CURRENT_STEP,
  PROPOSAL_FINANCE_CURRENT_STEP,
  BROKERED_FINANCE_CURRENT_STEP,
  EMP_DETAILS,
  EMP_PERMISSION_DETAILS,
  INVENTORY_VEHICLE,
  INVENTORY_ACQUITION,
  INVENTRY_DETAILS,
  INVENTORY_ADVERTISING,
  INVENTORY_COST,
  INVENTORY_DOCUMENT,
  PROPOSAL_DETAILS,
  PROPOSAL_TRADE_IN,
  PROPOSAL_SELL_ITEM,
  INVENTORY_NOTE,
  PROPOSAL_NOTE,
  BROKERED_DETAILS,
  BROKERED_TRADE_IN,
  BROKERED_SELL_ITEM,
  BROKERED_NOTE,
  RENTAL_DETAILS,
  FINANCE_DETAILS,
  FINANCE_NOTE,
  FINANCE_COMMENT,
  CUSTOMER_DETAILS,
  CUSTOMER_NOTE,
  MAIL_DETAILS,
  SETTING_RENTAL_DETAILS,
  SETTING_FINANCE_DETAILS,
  SETTING_HEADER_DETAILS,
  SETTING_FOOTER_DETAILS,
  SETTING_TITLE_DETAILS,
  SETTING_OFFER_DETAILS,
  SETTING_SOCIAL_DETAILS,
  SETTING_CONTACT_DETAILS,
  VALUATION_DETAILS,
  DASHBOARD_NOTE_DETAILS,
} from './action.type';
// eslint-disable-next-line import/no-anonymous-default-export
export default (state, action) => {
  switch (action.type) {
    case BRANCH_DETAILS:
      return (state = action.payload);
    case PROFILE_DETAILS:
      return (state = action.payload);
    case LEAD_DETAILS:
      return (state = action.payload);
    case LEAD_NOTE_DETAILS:
      return (state = action.payload);
    case LEAD_MAIL_DETAILS:
      return (state = action.payload);
    case INVENTRY_DETAILS:
      return (state = action.payload);
    case EMP_DETAILS:
      return (state = action.payload);
    case EMP_PERMISSION_DETAILS:
      return (state = action.payload);
    case INVENTORY_CURRENT_STEP:
      return (state = action.payload);
    case INVENTORY_VEHICLE:
      return (state = action.payload);
    case INVENTORY_COST:
      return (state = action.payload);
    case INVENTORY_ACQUITION:
      return (state = action.payload);
    case INVENTORY_ADVERTISING:
      return (state = action.payload);
    case INVENTORY_DOCUMENT:
      return (state = action.payload);
    case INVENTORY_NOTE:
      return (state = action.payload);
    case VALUATION_DETAILS:
      return (state = action.payload);
    case PROPOSAL_CURRENT_STEP:
      return (state = action.payload);
    case PROPOSAL_DETAILS:
      return (state = action.payload);
    case PROPOSAL_TRADE_IN:
      return (state = action.payload);
    case PROPOSAL_SELL_ITEM:
      return (state = action.payload);
    case PROPOSAL_NOTE:
      return (state = action.payload);
    case PROPOSAL_FINANCE_CURRENT_STEP:
      return (state = action.payload);
    case BROKERED_CURRENT_STEP:
      return (state = action.payload);
    case BROKERED_DETAILS:
      return (state = action.payload);
    case BROKERED_TRADE_IN:
      return (state = action.payload);
    case BROKERED_SELL_ITEM:
      return (state = action.payload);
    case BROKERED_NOTE:
      return (state = action.payload);
    case BROKERED_FINANCE_CURRENT_STEP:
      return (state = action.payload);
    case RENTAL_DETAILS:
      return (state = action.payload);
    case FINANCE_DETAILS:
      return (state = action.payload);
    case FINANCE_NOTE:
      return (state = action.payload);
    case FINANCE_COMMENT:
      return (state = action.payload);
    case CUSTOMER_DETAILS:
      return (state = action.payload);
    case CUSTOMER_NOTE:
      return (state = action.payload);
    case MAIL_DETAILS:
      return (state = action.payload);
    case SETTING_RENTAL_DETAILS:
      return (state = action.payload);
    case SETTING_FINANCE_DETAILS:
      return (state = action.payload);
    case SETTING_HEADER_DETAILS:
      return (state = action.payload);
    case SETTING_FOOTER_DETAILS:
      return (state = action.payload);
    case SETTING_TITLE_DETAILS:
      return (state = action.payload);
    case SETTING_OFFER_DETAILS:
      return (state = action.payload);
    case SETTING_SOCIAL_DETAILS:
      return (state = action.payload);
    case SETTING_CONTACT_DETAILS:
      return (state = action.payload);
    case DASHBOARD_NOTE_DETAILS:
      return (state = action.payload);

    default:
      return state;
  }
};
