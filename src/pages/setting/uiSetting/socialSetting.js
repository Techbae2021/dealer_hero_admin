/** @format */
import React, { useState, useEffect, useContext } from 'react';
import SideNav from './sideNavUi';
import { Row, Col, Table } from 'reactstrap';
import { Link } from 'react-router-dom';
import Instance from '../../../Instance';
import { useHistory } from 'react-router-dom';
import moment from 'moment';
import { useAlert } from 'react-alert';
import Context from '../../context/Context';
import {  SETTING_SOCIAL_DETAILS } from '../../context/action.type';
import AuthUser from '../../../components/auth/AuthUser';

const SocialSetting = () => {

  const alert = useAlert();
  let history = useHistory();
  // const [profileview, setProfileView] = useState(null);
  const [socialView, setSocialView] = useState(null);
  const [refresh, setRefresh] = useState(false);
  const { dispatchDetails } = useContext(Context);

useEffect(() => {
    Instance.get(`/api/admin/settings/social/get`, {
      headers: { authorization: `Bearer ${localStorage.getItem('token$')}` },
    })
      .then(({ data }) => {
        // console.log('socialData', data);
        let temp = data?.result;
        temp.reverse();
        setSocialView(temp);
      })
      .catch((err) => {
        console.log('err', err);
      })
  }, [ refresh]);

  const socialDelete = (value) => {
    Instance.delete(`/api/admin/settings/social/delete/${value._id}`, {
      headers: { authorization: `Bearer ${localStorage.getItem('token$')}` },
    })
      .then(() => {
        // console.log('Success');
        alert.success('Social Deleted');
        setRefresh(!refresh);
      })
      .catch((err) => {
        console.log('Error', err);
      });
  };

  const socialEdit = (value) => {
    dispatchDetails({
      type:  SETTING_SOCIAL_DETAILS,
      payload: value,
    });
    history.push(`/setting/social/edit/${value._id}`);
  };



  return (
    <AuthUser>
      <Row className='page-title align-items-center'>
        <Col sm={4} xl={6}>
          <h4 className='mb-1 mt-0'>UI Setting</h4>
        </Col>
      </Row>

      <div className='row'>
        <div className='col-2 Lead_Board'>
          <div className='email-container bg-transparent'>
            {/* Left sidebar */}
            <SideNav/>
            {/* End Left sidebar */}
          </div>
        </div>
        <div className='col-10'>
          <div className='card'>
            <div className='p-3'>
              <div className='row'>
                <div className='col-11'>
                  <h4> Social Setting </h4>
                </div>
                {socialView && socialView.length===0?(
                  <div className='col-1'>
                  <Link
                    to='/setting/social/add'
                    className='btn btn-success  btn-sm Button_Size'>
                    Add
                  </Link>
                </div>
                ):null}
              </div>
              <div>
                <Table hover responsive className='mt-4'>
                  <tbody>
                  {socialView && socialView.map((value, i) => (
                    <tr key={i}>
                      <td>{i + 1 }</td>
                      <td>
                      {value.facebookUrl}<br/>
                      {value.twitterUrl}<br/>
                      {value.instagramUrl}<br/>
                      {value.linkedinUrl}
                      </td>
                      <td>
                      {moment(new Date(value.createdAt)).format(
                              'DD/MM/YYYY'
                            )}
                            ,{new Date(value.createdAt).toLocaleTimeString()}
                      </td>

                      <td>
                        <div className='button-list'>
                        <button
                                 onClick={() => socialEdit(value)}
                                className='btn btn-primary  btn-sm Button_Size'>
                                Edit{' '}
                              </button>
                              <button
                                    onClick={() => socialDelete(value)}
                                    type='button'
                                    className='btn btn-danger  btn-sm Button_Size'>
                                    Delete{' '}
                                  </button>
                        </div>
                      </td>
                    </tr>
                    ))}
                  </tbody>
                </Table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </AuthUser>
  );
};

export default SocialSetting;
