/** @format */

import React, { useState, useEffect } from "react";
import { Row, Col } from "reactstrap";
import Instance from "../../../Instance";
import { useHistory } from "react-router-dom";
import { useAlert } from "react-alert";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as Yup from "yup";
import CustomInput from "../../../components/CustomInput";
import AuthUser from "../../../components/auth/AuthUser";

const AddFooter = () => {
  const alert = useAlert();
  let history = useHistory();
  const [isLoading,setLoading] = useState(false)
  // const [profileview, setProfileView] = useState(null);

  // useEffect(() => {
  //   Instance.get(`/api/user/view`, {
  //     headers: { authorization: `Bearer ${localStorage.getItem("token$")}` },
  //   })
  //     .then(({ data }) => {
  //       console.log("profileData", data);
  //       setProfileView(data?.user);
  //     })
  //     .catch((err) => {
  //       console.log("err", err);
  //     });
  // }, []);

  const [footerDetails, setFooterDetails] = useState({
    footer: "",
  });

  const handelChange = (event) => {
    setFooterDetails({
      ...footerDetails,
      [event.target.id]: event.target.value,
    });

    if (event.target.value === "") {
      setErrors({ ...errors, [event.target.name]: true });
    } else {
      setErrors({ ...errors, [event.target.name]: false });
    }
  };

  const OnSubmit = async () => {
    //e.preventDefault();
    setLoading(true)
    Instance.post(`/api/admin/settings/footer/add`, {
      footer: footerDetails?.footer,
    }, {
      headers: { authorization: `Bearer ${localStorage.getItem("token$")}` },
    })
      .then(({ data }) => {
    setLoading(false)

        // console.log("save", data);
        alert.success("Footer Added");
        history.push(`/setting/footer`);
      })
      .catch((err) => {
    setLoading(false)

        console.log("Err", err);
      });
  };

  const [errors, setErrors] = useState({
    footer: false,
  });

  const HandleValidation = () => {
    if (footerDetails.footer == "") {
      setErrors({ footer: true });
    } else {
      setErrors({ footer: false });
      OnSubmit();
    }
  };
  return (
    <AuthUser>
      <Row className="page-title align-items-center">
        <Col sm={4} xl={6}>
          <h4 className="mb-1 mt-0">UI Setting</h4>
        </Col>
      </Row>

      <div className="row">
        <div className="col-12">
          <div className="card p-3">
            <h4 className="mb-4"> Add Footer </h4>
            <div className="form-row">
              <div className="form-group col-md-12">
                <CustomInput
                  type="text"
                  error={errors.footer}
                  name="footer"
                  id="footer"
                  value={footerDetails.footer}
                  label="footer"
                  handleChange={(e) => {
                    handelChange(e);
                  }}
                />
              </div>
            </div>
            <div className="row d-flex justify-content-center">
              <div className="form-group col-md-2">
              <button
                  disabled={isLoading}
                  className='btn btn-primary btn-block'
                  onClick={HandleValidation}>
                  {isLoading ? 'Adding...' : 'Add'}
                </button>
              </div>
            </div>
          </div>
        </div>{" "}
      </div>
    </AuthUser>
  );
};

export default AddFooter;
