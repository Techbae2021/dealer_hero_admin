/** @format */

import React, { useState, useEffect } from 'react';
import { Row, Col } from 'reactstrap';
import Instance from '../../../Instance';
import { useHistory} from 'react-router-dom';
import { useAlert } from 'react-alert';
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as Yup from 'yup';
import CustomInput from '../../../components/CustomInput';
import AuthUser from '../../../components/auth/AuthUser';

const AddContact = () => {

  const alert = useAlert();
  let history = useHistory();
  // const [profileview, setProfileView] = useState(null);
  
  // useEffect(() => {
  //   Instance.get(`/api/user/view`, {
  //     headers: { authorization: `Bearer ${localStorage.getItem('token$')}` },
  //   })
  //     .then(({ data }) => {
  //       console.log('profileData', data);
  //       setProfileView(data?.user);
  //     })
  //     .catch((err) => {
  //       console.log('err', err);
  //     });
  // }, []);
  const [isLoading, setLoading] = useState(false)
  const [contactDetails, setContactDetails] = useState({
    name: '',
    phone: '',
    email: '',
    address: ''
    
  });

  const handelChange = (event) => {
    setContactDetails({
      ...contactDetails,
      [event.target.id]: event.target.value,
    });

    if (event.target.value === "") {
      setErrors({ ...errors, [event.target.name]: true });
    } else {
      setErrors({ ...errors, [event.target.name]: false });
    }
  };

  const OnSubmit = async() => {
   // e.preventDefault();
   setLoading(true)
    Instance.post(`/api/admin/settings/contact/add`, {
      name: contactDetails?.name,  
      phone: contactDetails?.phone ,
      email: contactDetails?. email ,
      address: contactDetails?.address  
    }, {
      headers: { authorization: `Bearer ${localStorage.getItem('token$')}` },
    })
      .then(({ data }) => {
   setLoading(false)

        // console.log('save', data);
        alert.success('Contact Added');
        history.push(`/setting/contact`);
      })
      .catch((err) => {
   setLoading(false)

        console.log('Err', err);
      });
    
  };
  const [errors, setErrors] = useState({
    name: false,
    phone: false,
    email: false,
    address: false
  });

  const [isValid, setisValid] = useState(true);

  const HandleValidation = () => {
    let shouldsubmit = true;
    let tempError = errors;

    for (const [key, value] of Object.entries(contactDetails)) {
      if (value == "") {
        tempError[key] = true;
        if (shouldsubmit) {
          shouldsubmit = false;
        }
      } else {
        tempError[key] = false;
      }
    }
    // console.log(tempError);
    setisValid(shouldsubmit);
    setErrors(tempError);

    if (shouldsubmit) {
      OnSubmit();
    }
  };

  return (
    <AuthUser>
      <Row className='page-title align-items-center'>
        <Col sm={4} xl={6}>
          <h4 className='mb-1 mt-0'>UI Setting</h4>
        </Col>
      </Row>

      <div className='row'>
        <div className='col-12'>
          <div className='card p-3'>
            <h4 className='mb-4'> Add Contact </h4>
           <div className='form-row'>
           {[
              { label: "Name", name: "name",type:"text" },
              { label: "Phone", name: "phone",type:"text" },
              { label: "Email", name: "email",type:"text" },
              { label: "Address", name: "address",type:"text" }
            ].map((item, i) => (
              <div className='form-group col-md-12' key={i}>
                <CustomInput
                  label= {item.label} 
                  type={item.type}
                  error={errors[item.name]}
                  name={item.name}
                  id={item.name}
                  value={contactDetails[item.name]}
                  handleChange={(e) => {
                    handelChange(e);
                  }}
                />
              </div>
            ))}

            </div>
            <div className='row d-flex justify-content-center'>
              <div className='form-group col-md-2'>
              <button
                  disabled={isLoading}
                  className='btn btn-primary btn-block'
                  onClick={HandleValidation}>
                  {isLoading ? 'Adding...' : 'Add'}
                </button>
              </div>
            </div>
          </div>
        </div>{' '}
      </div>
    </AuthUser>
  );
};

export default AddContact;
