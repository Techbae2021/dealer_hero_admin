/** @format */

import React, { useState, useContext } from 'react';
import { Row, Col } from 'reactstrap';
import Instance from '../../../Instance';
import Context from '../../context/Context';
import { useHistory, Redirect } from 'react-router-dom';
import { useAlert } from 'react-alert';
import AuthUser from '../../../components/auth/AuthUser';

const EditSocial = () => {


  let history = useHistory();
  const { details } = useContext(Context);
  const alert = useAlert();

  const [socialDetails, setSocialDetails] = useState({
    facebookUrl: details != null ? details.facebookUrl : '',
    twitterUrl: details != null ? details.twitterUrl : '',
    instagramUrl: details != null ? details.instagramUrl : '',
    linkedinUrl: details != null ? details.linkedinUrl : ''
  });

  const handelChange = (event) => {
    setSocialDetails({
      ...socialDetails,
      [event.target.id]: event.target.value,
    });
  };

  const OnUpdated = (e) => {
    e.preventDefault();
    Instance.put(`/api/admin/settings/social/update/${details._id}`, {
      facebookUrl: socialDetails.facebookUrl,
      twitterUrl: socialDetails.twitterUrl,
      instagramUrl: socialDetails.instagramUrl,
      linkedinUrl: socialDetails.linkedinUrl
    }, {
      headers: { authorization: `Bearer ${localStorage.getItem('token$')}` },
    })
      .then(({ data }) => {
        // console.log('save', data);
        alert.success('Social Edited');
        history.push(`/setting/social`);
      })
      .catch((err) => {
        console.log('Err', err);
      });
  };

  if (details == null) {
    return <Redirect to='/setting/social' />;
  }


  return (
    <AuthUser>
      <Row className='page-title align-items-center'>
        <Col sm={4} xl={6}>
          <h4 className='mb-1 mt-0'>UI Setting</h4>
        </Col>
      </Row>

      <div className='row'>
        <div className='col-12'>
          <div className='card p-3'>
            <h4 className='mb-4'> Edit Social </h4>
            <div className='form-row'>
              <div className='form-group col-md-3'>
                <label>Facebook URL</label>
                <input
                  type='text'
                  className='form-control'
                  placeholder='facebookUrl'
                  id='facebookUrl'
                    value={socialDetails.facebookUrl}
                     onChange={handelChange}
                ></input>
              </div>
              <div className='form-group col-md-3'>
                <label>Tweeter URL</label>
                <input
                  type='text'
                  className='form-control'
                  placeholder='twitterUrl'
                  id='twitterUrl'
                    value={socialDetails.twitterUrl}
                     onChange={handelChange}
                ></input>
              </div>
              <div className='form-group col-md-3'>
                <label>Instagram URL</label>
                <input
                  type='text'
                  className='form-control'
                  placeholder='instagramUrl'
                  id='instagramUrl'
                    value={socialDetails.instagramUrl}
                    onChange={handelChange}
                ></input>
              </div>
              <div className='form-group col-md-3'>
                <label>LinkedIn URL</label>
                <input
                  type='text'
                  className='form-control'
                  placeholder='linkedinUrl'
                  id='linkedinUrl'
                   value={socialDetails.linkedinUrl}
                   onChange={handelChange}
                ></input>
              </div>
            </div>
            <div className='row d-flex justify-content-center'>
              <div className='form-group col-md-2'>
                <button
                  className='btn btn-primary btn-block'
                     onClick={OnUpdated}
                >
                  Update
                </button>
              </div>
            </div>
          </div>
        </div>{' '}
      </div>
    </AuthUser>
  );
};

export default EditSocial;
