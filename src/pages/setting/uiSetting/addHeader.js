/** @format */
import React, { useState} from 'react';
import { Row, Col } from 'reactstrap';
import Instance from '../../../Instance';
import { useHistory} from 'react-router-dom';
import { useAlert } from 'react-alert';

import CustomInput from '../../../components/CustomInput';
import AuthUser from '../../../components/auth/AuthUser';

const AddHeader = () => {
  const [isLoading,setLoading] = useState(false)

  const alert = useAlert();
  let history = useHistory();
  // const [profileview, setProfileView] = useState(null);
  
  // useEffect(() => {
  //   Instance.get(`/api/user/view`, {
  //     headers: { authorization: `Bearer ${localStorage.getItem('token$')}` },
  //   })
  //     .then(({ data }) => {
  //       console.log('profileData', data);
  //       setProfileView(data?.user);
  //     })
  //     .catch((err) => {
  //       console.log('err', err);
  //     });
  // }, []);

  const [headerDetails, setHeaderDetails] = useState({
    header: '',
  });

  const handelChange = (event) => {
    setHeaderDetails({
      ...headerDetails,
      [event.target.id]: event.target.value,
    });

    if (event.target.value === "") {
      setErrors({ ...errors, [event.target.name]: true });
    } else {
      setErrors({ ...errors, [event.target.name]: false });
    }
  };

  const OnSubmit = async() => {
   // e.preventDefault();
   setLoading(true)
    Instance.post(`/api/admin/settings/header/add`, {
      header: headerDetails?.header,   
    }, {
      headers: { authorization: `Bearer ${localStorage.getItem('token$')}` },
    })
      .then(({ data }) => {
   setLoading(false)

        // console.log('save', data);
        alert.success('Header Added');
        history.push(`/setting/header`);
      })
      .catch((err) => {
   setLoading(false)

        console.log('Err', err);
      });
    
  };

  const [errors, setErrors] = useState({
    header: false,
  });

  const HandleValidation = () => {
    if(headerDetails.header==""){
      setErrors({ header: true});
    }else{
      setErrors({ header: false});
      OnSubmit();
    }
  };

  return (
    <AuthUser>
      <Row className='page-title align-items-center'>
        <Col sm={4} xl={6}>
          <h4 className='mb-1 mt-0'>UI Setting</h4>
        </Col>
      </Row>

      <div className='row'>
        <div className='col-12'>
          <div className='card p-3'>
            <h4 className='mb-4'> Add Header </h4>
            <div className='form-row'>
              <div className='form-group col-md-12'>
              <CustomInput
                  type='text'
                  error={errors.header}
                  name='header'
                  id='header'
                  value={headerDetails.header}
                  label="Header"
                  handleChange={(e) => {
                    handelChange(e);
                  }}
                />
               
              </div>
            </div>
            <div className='row d-flex justify-content-center'>
              <div className='form-group col-md-2'>
              <button
                  disabled={isLoading}
                  className='btn btn-primary btn-block'
                  onClick={HandleValidation}>
                  {isLoading ? 'Adding...' : 'Add'}
                </button>
              </div>
            </div>
          </div>
        </div>{' '}
      </div>
    </AuthUser>
  );
};

export default AddHeader;
