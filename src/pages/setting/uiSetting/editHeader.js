/** @format */

import React, { useState, useContext } from 'react';
import { Row, Col } from 'reactstrap';
import Instance from '../../../Instance';
import Context from '../../context/Context';
import { useHistory, Redirect } from 'react-router-dom';
import { useAlert } from 'react-alert';
import AuthUser from '../../../components/auth/AuthUser';

const EditHeader = () => {

  let history = useHistory();
  const { details } = useContext(Context);
  const alert = useAlert();

  const [hDetails, setHDetails] = useState({
    header: details != null ? details.header : '',
  });

  const handelChange = (event) => {
    setHDetails({
      ...hDetails,
      [event.target.id]: event.target.value,
    });
  };

  const OnUpdated = (e) => {
    e.preventDefault();
    Instance.put(`/api/admin/settings/header/update/${details._id}`, {
      header: hDetails.header,
    }, {
      headers: { authorization: `Bearer ${localStorage.getItem('token$')}` },
    })
      .then(({ data }) => {
        // console.log('save', data);
        alert.success('Header Edited');
        history.push(`/setting/header`);
      })
      .catch((err) => {
        console.log('Err', err);
      });
  };

  if (details == null) {
    return <Redirect to='/setting/header' />;
  }

  return (
    <AuthUser>
      <Row className='page-title align-items-center'>
        <Col sm={4} xl={6}>
          <h4 className='mb-1 mt-0'>UI Setting</h4>
        </Col>
      </Row>

      <div className='row'>
        <div className='col-12'>
          <div className='card p-3'>
            <h4 className='mb-4'> Edit Header </h4>
            <div className='form-row'>
              <div className='form-group col-md-12'>
                <label>Header</label>
                <input
                  type='text'
                  className='form-control'
                  placeholder='header'
                  id='header'
                    value={hDetails.header}
                     onChange={handelChange}
                ></input>
              </div>
            </div>
            <div className='row d-flex justify-content-center'>
              <div className='form-group col-md-2'>
                <button
                  className='btn btn-primary btn-block'
                     onClick={OnUpdated}
                >
                 Update
                </button>
              </div>
            </div>
          </div>
        </div>{' '}
      </div>
    </AuthUser>
  );
};

export default EditHeader;
