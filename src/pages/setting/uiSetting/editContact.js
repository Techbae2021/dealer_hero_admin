/** @format */

import React, { useState, useContext } from 'react';
import { Row, Col } from 'reactstrap';
import Instance from '../../../Instance';
import Context from '../../context/Context';
import { useHistory, Redirect } from 'react-router-dom';
import { useAlert } from 'react-alert';
import AuthUser from '../../../components/auth/AuthUser';

const EditContact = () => {


  let history = useHistory();
  const { details } = useContext(Context);
  const alert = useAlert();

  const [contactDetails, setContactDetails] = useState({
    name: details != null ? details.name : '',
    phone: details != null ? details.phone : '',
    email: details != null ? details.email : '',
    address: details != null ? details.address : '',

  });

  const handelChange = (event) => {
    setContactDetails({
      ...contactDetails,
      [event.target.id]: event.target.value,
    });
  };

  const OnUpdated = (e) => {
    e.preventDefault();
    Instance.put(`/api/admin/settings/contact/update/${details._id}`, {
      name: contactDetails.name,
      phone: contactDetails.phone,
      email: contactDetails.email,
      address: contactDetails.address
    }, {
      headers: { authorization: `Bearer ${localStorage.getItem("token$")}` },
    })
      .then(({ data }) => {
        // console.log('save', data);
        alert.success('Contact Edited');
        history.push(`/setting/contact`);
      })
      .catch((err) => {
        console.log('Err', err);
      });
  };

  if (details == null) {
    return <Redirect to='/setting/contact' />;
  }


  return (
    <AuthUser>
      <Row className='page-title align-items-center'>
        <Col sm={4} xl={6}>
          <h4 className='mb-1 mt-0'>UI Setting</h4>
        </Col>
      </Row>

      <div className='row'>
        <div className='col-12'>
          <div className='card p-3'>
            <h4 className='mb-4'> Edit Contact </h4>
            <div className='form-row'>
              <div className='form-group col-md-3'>
                <label>Name</label>
                <input
                  type='text'
                  className='form-control'
                  placeholder='name'
                  id='name'
                     value={contactDetails.name}
                    onChange={handelChange}
                ></input>
              </div>
              <div className='form-group col-md-3'>
                <label>Phone</label>
                <input
                  type='text'
                  className='form-control'
                  placeholder='phone'
                  id='phone'
                    value={contactDetails.phone}
                     onChange={handelChange}
                ></input>
              </div>
              <div className='form-group col-md-3'>
                <label>Email</label>
                <input
                  type='text'
                  className='form-control'
                  placeholder='email'
                  id='email'
                    value={contactDetails.email}
                     onChange={handelChange}
                ></input>
              </div>
              <div className='form-group col-md-3'>
                <label>Address</label>
                <input
                  type='text'
                  className='form-control'
                  placeholder='address'
                  id='address'
                     value={contactDetails.address}
                    onChange={handelChange}
                ></input>
              </div>
            </div>
            <div className='row d-flex justify-content-center'>
              <div className='form-group col-md-2'>
                <button
                  className='btn btn-primary btn-block'
                     onClick={OnUpdated}
                >
                  Update
                </button>
              </div>
            </div>
          </div>
        </div>{' '}
      </div>
    </AuthUser>
  );
};

export default EditContact;
