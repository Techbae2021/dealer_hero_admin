/** @format */
import React, { useState } from 'react';
import { Row, Col } from 'reactstrap';
import Instance from '../../../Instance';
import { useHistory} from 'react-router-dom';
import { useAlert } from 'react-alert';
import CustomInput from '../../../components/CustomInput';
import AuthUser from '../../../components/auth/AuthUser';

const AddSocial = () => {
  const [isLoading,setLoading] = useState(false)

  const alert = useAlert();
  let history = useHistory();
  // const [profileview, setProfileView] = useState(null);
  
  // useEffect(() => {
  //   Instance.get(`/api/user/view`, {
  //     headers: { authorization: `Bearer ${localStorage.getItem('token$')}` },
  //   })
  //     .then(({ data }) => {
  //       console.log('profileData', data);
  //       setProfileView(data?.user);
  //     })
  //     .catch((err) => {
  //       console.log('err', err);
  //     });
  // }, []);

  const [socialDetails, setSocialDetails] = useState({
    facebookUrl: "",
    twitterUrl: "",
    instagramUrl: "",
    linkedinUrl: ""
    
  });

  const handelChange = (event) => {
    setSocialDetails({
      ...socialDetails,
      [event.target.id]: event.target.value,
    });

    if (event.target.value === "") {
      setErrors({ ...errors, [event.target.name]: true });
    } else {
      setErrors({ ...errors, [event.target.name]: false });
    }
  };

  const OnSubmit = async() => {
    //e.preventDefault();
    setLoading(true)
    Instance.post(`/api/admin/settings/social/add`, {
      facebookUrl: socialDetails?.facebookUrl, 
      twitterUrl: socialDetails?.twitterUrl ,
      instagramUrl: socialDetails?.instagramUrl ,
      linkedinUrl: socialDetails?.linkedinUrl   
    }, {
      headers: { authorization: `Bearer ${localStorage.getItem('token$')}` },
    })
      .then(({ data }) => {
    setLoading(false)

        // console.log('save', data);
        alert.success('Social Added');
        history.push(`/setting/social`);
      })
      .catch((err) => {
    setLoading(false)

        console.log('Err', err);
      });
    
  };

  const [errors, setErrors] = useState({
    facebookUrl:false,
    twitterUrl:false,
    instagramUrl:false,
    linkedinUrl:false
  });

  const [isValid, setisValid] = useState(true);

  const HandleValidation = () => {
    let shouldsubmit = true;
    let tempError = errors;

    for (const [key, value] of Object.entries(socialDetails)) {
      if (value == "") {
        tempError[key] = true;
        if (shouldsubmit) {
          shouldsubmit = false;
        }
      } else {
        tempError[key] = false;
      }
    }
    // console.log(tempError);
    setisValid(shouldsubmit);
    setErrors(tempError);

    if (shouldsubmit) {
      OnSubmit();
    }
  };

  return (
    <AuthUser>
      <Row className='page-title align-items-center'>
        <Col sm={4} xl={6}>
          <h4 className='mb-1 mt-0'>UI Setting</h4>
        </Col>
      </Row>

      <div className='row'>
        <div className='col-12'>
          <div className='card p-3'>
            <h4 className='mb-4'> Add Social </h4>
           <div className='form-row'>
            {[
              { label: "Facebook URL", name: "facebookUrl",type:"text" },
              { label: "Tweeter URL", name: "twitterUrl",type:"text" },
              { label: "Instagram URL", name: "instagramUrl",type:"text" },
              { label: "Linkedin URL", name: "linkedinUrl",type:"text" }
            ].map((item, i) => (
              <div className='form-group col-md-4' key={i}>
                <CustomInput
                  label= {item.label} 
                  type={item.type}
                  error={errors[item.name]}
                  name={item.name}
                  id={item.name}
                  value={socialDetails[item.name]}
                  handleChange={(e) => {
                    handelChange(e);
                  }}
                />
              </div>
            ))}
             
            </div>
            <div className='row d-flex justify-content-center'>
              <div className='form-group col-md-2'>
              <button
                  disabled={isLoading}
                  className='btn btn-primary btn-block'
                  onClick={HandleValidation}>
                  {isLoading ? 'Adding...' : 'Add'}
                </button>
              </div>
            </div>
          </div>
        </div>{' '}
      </div>
    </AuthUser>
  );
};

export default AddSocial;
