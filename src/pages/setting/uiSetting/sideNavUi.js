/** @format */

import React from "react";
import { Link } from "react-router-dom";
import Instance from "../../../Instance";

const SideNav = () => {
  const [summary,setSummary] = React.useState({contact:0,footer:0,header:0,offer:0,social:0,title:0})

  React.useEffect(()=>{
    Instance.get(`/api/admin/settings/uisummary`, {
      headers: { authorization: `Bearer ${localStorage.getItem('token$')}` },
    }).then(({ data }) => {
        // console.log('profileData', data);
        setSummary(data)
      })
      .catch((err) => {
        console.log('err', err);
      })
  },[])

  return (
    <>
      <div>
        {/* Left sidebar */}
        <div className="p-3 card">
          <div className="mail-list ">
            <Link to="/setting/header" className="list-group-item border-0 ">
              <i className="uil uil-envelope-alt font-size-15 mr-1" />
              Header Setting
              <span className="badge badge-danger float-right ml-2 mt-1">
                {summary?.header}
              </span>
            </Link>
            <Link to="/setting/title" className="list-group-item border-0 ">
              <i className="uil uil-envelope-alt font-size-15 mr-1" />
              Title Setting
              <span className="badge badge-danger float-right ml-2 mt-1">
                {summary?.title}
              </span>
            </Link>
            <Link to="/setting/footer" className="list-group-item border-0 ">
              <i className="uil uil-envelope-alt font-size-15 mr-1" />
              Footer Setting
              <span className="badge badge-danger float-right ml-2 mt-1">
                {summary?.footer}
              </span>
            </Link>
            <Link to="/setting/social" className="list-group-item border-0 ">
              <i className="uil uil-envelope-alt font-size-15 mr-1" />
              Social Setting
              <span className="badge badge-danger float-right ml-2 mt-1">
                {summary?.social}
              </span>
            </Link>
            <Link to="/setting/offer" className="list-group-item border-0 ">
              <i className="uil uil-envelope-alt font-size-15 mr-1" />
              Offer Setting
              <span className="badge badge-danger float-right ml-2 mt-1">
                {summary?.offer}
              </span>
            </Link>
            <Link to="/setting/contact" className="list-group-item border-0 ">
              <i className="uil uil-envelope-alt font-size-15 mr-1" />
              Contact Setting
              <span className="badge badge-danger float-right ml-2 mt-1">
                {summary?.contact}
              </span>
            </Link>
          </div>
        </div>
      </div>
    </>
  );
};

export default SideNav;
