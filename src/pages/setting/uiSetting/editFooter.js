/** @format */

import React, { useState, useContext } from 'react';
import { Row, Col } from 'reactstrap';
import Instance from '../../../Instance';
import Context from '../../context/Context';
import { useHistory, Redirect } from 'react-router-dom';
import { useAlert } from 'react-alert';
import AuthUser from '../../../components/auth/AuthUser';

const EditFooter = () => {

  let history = useHistory();
  const { details } = useContext(Context);
  const alert = useAlert();

  const [footerDetails, setFooterDetails] = useState({
    footer: details != null ? details.footer : '',
  });

  const handelChange = (event) => {
    setFooterDetails({
      ...footerDetails,
      [event.target.id]: event.target.value,
    });
  };

  const OnUpdated = (e) => {
    e.preventDefault();
    Instance.put(`/api/admin/settings/footer/update/${details._id}`, {
      footer: footerDetails.footer,
    }, {
      headers: { authorization: `Bearer ${localStorage.getItem('token$')}` },
    })
      .then(({ data }) => {
        // console.log('save', data);
        alert.success('Footer Edited');
        history.push(`/setting/footer`);
      })
      .catch((err) => {
        console.log('Err', err);
      });
  };

  if (details == null) {
    return <Redirect to='/setting/footer' />;
  }


  return (
    <AuthUser>
      <Row className='page-title align-items-center'>
        <Col sm={4} xl={6}>
          <h4 className='mb-1 mt-0'>UI Setting</h4>
        </Col>
      </Row>

      <div className='row'>
        <div className='col-12'>
          <div className='card p-3'>
            <h4 className='mb-4'> Edit Footer </h4>
           <div className='form-row'>
              <div className='form-group col-md-12'>
                <label>Footer</label>
                <input
                  type='text'
                  className='form-control'
                  placeholder='footer'
                  id='footer'
                    value={footerDetails.footer}
                    onChange={handelChange}
                ></input>
              </div>
             
            </div>
            <div className='row d-flex justify-content-center'>
              <div className='form-group col-md-2'>
                <button
                  className='btn btn-primary btn-block'
                    onClick={OnUpdated}
                >
                  Update
                </button>
              </div>
            </div>
          </div>
        </div>{' '}
      </div>
    </AuthUser>
  );
};

export default EditFooter;
