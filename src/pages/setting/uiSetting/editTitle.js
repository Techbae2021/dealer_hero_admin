/** @format */

import React, { useState, useContext } from 'react';
import { Row, Col } from 'reactstrap';
import Instance from '../../../Instance';
import Context from '../../context/Context';
import { useHistory, Redirect } from 'react-router-dom';
import { useAlert } from 'react-alert';
import AuthUser from '../../../components/auth/AuthUser';

const EditTitle = () => {


  let history = useHistory();
  const { details } = useContext(Context);
  const alert = useAlert();

  const [titleDetails, setTitleDetails] = useState({
    title: details != null ? details.title : '',
  });

  const handelChange = (event) => {
    setTitleDetails({
      ...titleDetails,
      [event.target.id]: event.target.value,
    });
  };

  const OnUpdated = (e) => {
    e.preventDefault();
    Instance.put(`/api/admin/settings/title/update/${details._id}`, {
      title: titleDetails.title,
    }, {
      headers: { authorization: `Bearer ${localStorage.getItem('token$')}` },
    })
      .then(({ data }) => {
        // console.log('save', data);
        alert.success('Title Edited');
        history.push(`/setting/title`);
      })
      .catch((err) => {
        console.log('Err', err);
      });
  };

  if (details == null) {
    return <Redirect to='/setting/title' />;
  }


  return (
    <AuthUser>
      <Row className='page-title align-items-center'>
        <Col sm={4} xl={6}>
          <h4 className='mb-1 mt-0'>UI Setting</h4>
        </Col>
      </Row>

      <div className='row'>
        <div className='col-12'>
          <div className='card p-3'>
            <h4 className='mb-4'> Edit Title </h4>
            <div className='form-row'>
              <div className='form-group col-md-12'>
                <label>Title</label>
                <input
                  type='text'
                  className='form-control'
                  placeholder='title'
                  id='title'
                    value={titleDetails.title}
                    onChange={handelChange}
                ></input>
              </div>
            </div>
            <div className='row d-flex justify-content-center'>
              <div className='form-group col-md-2'>
                <button
                  className='btn btn-primary btn-block'
                     onClick={OnUpdated}
                >
                  Update
                </button>
              </div>
            </div>
          </div>
        </div>{' '}
      </div>
    </AuthUser>
  );
};

export default EditTitle;
