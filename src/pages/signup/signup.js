/** @format */

import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import * as FeatherIcon from 'react-feather';
import DealerLogo from '../../assets/Dealer_Hero.png';
import { useHistory } from 'react-router-dom';
import Instance from './../../Instance';
import firebase from '../../firebaseConfig';
import { useAlert } from 'react-alert';
import PhoneInput from 'react-phone-input-2'
import 'react-phone-input-2/lib/style.css'

const Signup = () => {
  let history = useHistory();
  let alert = useAlert()
  const [userDetails, setUserDetails] = useState({
    firstName: '',
    lastName: '',
    mobile: '',
    email: '',
    password: '',
    img: 'noimage.png',
    emailOtp: '',
    phoneOtp: '',
    address:''
  });


  const [alertMessage1, setAlertMsg1] = useState(''); //forEmail alert Message
  const [alertMessage2, setAlertMsg2] = useState(''); //forPhone alert Message

  const [isEmailValid, setEmailValid] = useState(false);
  const [isPhoneValid, setPhoneValid] = useState(false);
  const [isLoading, setLoading] = useState(false)

  const handelChange = (event) => {
    setUserDetails({
      ...userDetails,
      [event.target.id]: event.target.value,
    });
  };

  const isValid = () => {
    for (let v in userDetails) {
      if (userDetails[v].toString().trim() === '') {
        return false
      }
    }
    return true
  }

  const OnSignUP = (e) => {
    e.preventDefault();
    if (isValid()) {
      setLoading(true)
      const requestF = {
        firstName: userDetails.firstName,
        lastName: userDetails.lastName,
        phone: "+"+userDetails.mobile,
        email: userDetails.email,
        password: userDetails.password,
        img: userDetails.img,
        address: userDetails.address
      }
      // console.log('requestF',requestF)
      Instance.post('/api/admin/signup', requestF).then(({ data }) => {
        setLoading(false)
        if(data?.success){
          alert.success(data?.message)
          history.push('/');
        }
      }).catch((err) => {
        setLoading(false)
        if(!err?.response?.data?.success){
          alert.error(err.response?.data?.err?.response?.data?.message);
          }else{
            alert.error("Something went wrong!")
          }
        console.log('Err', err);
      });
    } else {
      alert.error('Please fill all the details!')
    }
  };

  const requestForEmailCode = () => {
    if (userDetails.email) {
      setAlertMsg1('');
      setEmailValid(false);
      setUserDetails({
        ...userDetails,
        emailOtp: '',
      });
      Instance.post('/api/email/sendVerificationCode', {
        email: userDetails.email,
      }).then((res) => {
        if (res.data.success && res.data.message) {
          setAlertMsg1(res.data.message);
          alert.success(res.data.message)
        }
      }).catch((e) =>{
        alert.error('Failed to send email request!')
        console.log(e)
      });
    }
  };

  const requestVerifyEmail = () => {
    if (userDetails.emailOtp && userDetails.email) {
      setEmailValid(false);
      Instance.post('/api/email/verify', {
        email: userDetails.email,
        code: userDetails.emailOtp,
      })
        .then((res) => {
          if (res.data.success && res.data.message) {
            setAlertMsg1(res.data.message);
            alert.success(res.data.message)
            setEmailValid(true);
          }
        }).catch((e) =>{
          alert.error('Failed to verify email!')
          console.log(e);
        });
    }
  };


  const setUpRecaptcha = () => {
    window.recaptchaVerifier = new firebase.auth.RecaptchaVerifier(
      'recaptcha-container',
      {
        size: 'invisible',
        callback: (response) => {
          // reCAPTCHA solved, allow signInWithPhoneNumber.
          console.log('Captcha Resolved');
          // onPhoneOtp();
        },
      }
    );
  };

  const onPhoneOtp = () => {
    if (userDetails.mobile) {
      setUpRecaptcha();
      setPhoneValid(false);
      const phoneNumber = '+' + userDetails.mobile;
      // console.log(phoneNumber);
      const appVerifier = window.recaptchaVerifier;
      firebase
        .auth()
        .signInWithPhoneNumber(phoneNumber, appVerifier)
        .then((confirmationResult) => {
          // SMS sent. Prompt user to type the code from the message, then sign the
          // user in with confirmationResult.confirm(code).
          window.confirmationResult = confirmationResult;
          setAlertMsg2('OTP is sent');
          alert.success('OTP is sent, Please check!')
          // ...
        })
        .catch((error) => {
          if (error.message) setAlertMsg2(error.message);
          console.log(error);
          alert.error('Failed to send OTP!')
          // Error; SMS not sent
          // ...
        });
    }
  };

  const verifyPhoneNo = () => {
    let otpInput = userDetails.phoneOtp;
    let optConfirm = window.confirmationResult;
    // console.log(codee);
    optConfirm
      .confirm(otpInput)
      .then(function (result) {
        setPhoneValid(true);
        setAlertMsg2('Mobile No. verified successfully!');
        alert.success('Mobile No. verified successfully!')
      })
      .catch(function (error) {
        console.log(error);
        setPhoneValid(false);
        setAlertMsg2('Incorrect OTP');
        alert.error('Incorrect OTP')
      });
  };

  return (
    <>

      <div className='account-pages my-5'>
        <div className='container'>
          <div className='row justify-content-center'>
            <div className='col-xl-10'>
              <div className='card'>
                <div className='card-body p-0'>
                  <div className='row'>
                    <div className='col-lg-6 p-5'>
                      <div className='mx-auto mb-2 text-center'>
                        <a href='index.html'>
                          <img src={DealerLogo} alt='' height={60} />
                          {/* <h3 className='d-inline align-middle ml-1 text-logo'>
                            Shreyu
                          </h3> */}
                        </a>
                      </div>
                      <h6 className='h5 mb-2 mt-4 text-center'>ADMIN SINGUP</h6>
                      {/* <p className='text-muted mt-0 mb-4'>
                        Create a free account and start using Shreyu
                      </p> */}
                      <form
                        onSubmit={(e) => e.preventDefault()}
                        className='authentication-form'>
                        <div id='recaptcha-container'></div>
                        <div className='row m'>
                          <div className='col-md-6'>
                            <div className='form-group'>
                              <label className='form-control-label'>
                                First Name
                              </label>
                              <div className='input-group input-group-merge'>
                             
                                <div className='input-group-prepend'>
                                  <span className='input-group-text'>
                                    <FeatherIcon.User />
                                  </span>
                                </div>
                                <input
                                  type='text'
                                  className='form-control'
                                  id='firstName'
                                  placeholder='First Name'
                                  value={userDetails.firstName}
                                  onChange={handelChange}
                                  autoComplete='off'
                                />
                                
                              </div>
                            </div>
                          </div>
                          <div className='col-md-6'>
                            <div className='form-group'>
                              <label className='form-control-label'>
                                Last Name
                              </label>
                              <div className='input-group input-group-merge'>
                                <div className='input-group-prepend'>
                                  <span className='input-group-text'>
                                    <FeatherIcon.User />
                                  </span>
                                </div>
                                <input
                                  type='text'
                                  className='form-control'
                                  id='lastName'
                                  placeholder='Last Name'
                                  value={userDetails.lastName}
                                  onChange={handelChange}
                                  autoComplete='off'
                                />
                              </div>
                            </div>
                          </div>
                        </div>

                        <div className='form-group'>
                          <label className='form-control-label'>Mobile</label>
                          <div className='input-group input-group-merge'>
                            <div className='input-group-prepend'>
                            <PhoneInput
                              id='mobile'
                              placeholder='Mobile No.'
                              country={'au'}
                              value={userDetails.mobile}
                              onChange={phone => setUserDetails({...userDetails,mobile:phone})}/>
                            </div>
                            <button
                              onClick={onPhoneOtp}
                              disabled={
                                userDetails.mobile.trim() === '' || isPhoneValid
                              }
                              className='btn btn-success btn-xs'>
                              Send OTP
                            </button>
                          </div>
                          {alertMessage2 ? (
                            <div className='input-group input-group-merge mt-1'>
                              <div className='input-group-prepend'>
                                <span className='input-group-text'>
                                  <FeatherIcon.Lock />
                                </span>
                              </div>
                              <input
                                type='number'
                                className='form-control'
                                id='phoneOtp'
                                placeholder='Enter your one time password.'
                                value={userDetails.phoneOtp}
                                disabled={isPhoneValid}
                                onChange={handelChange}
                                autoComplete='off'
                              />
                              <button
                                onClick={verifyPhoneNo}
                                disabled={
                                  userDetails.phoneOtp.trim() === '' ||
                                  isPhoneValid
                                }
                                className='btn btn-success btn-xs'>
                                Verify OTP
                              </button>
                            </div>
                          ) : null}
                          {alertMessage2 ? (
                            <div
                              className='alert alert-success fade show mt-1 '
                              data-dismiss='alert'
                              aria-label='Close'>
                              {alertMessage2}
                            </div>
                          ) : null}
                        </div>
                        <div className='form-group'>
                          <label className='form-control-label'>
                            Email Address
                          </label>
                          <div className='input-group input-group-merge'>
                            <div className='input-group-prepend'>
                              <span className='input-group-text'>
                                <FeatherIcon.Mail />
                              </span>
                            </div>
                            <input
                              type='email'
                              className='form-control'
                              id='email'
                              placeholder='Email'
                              value={userDetails.email}
                              onChange={handelChange}
                              disabled={isEmailValid}
                              autoComplete='off'
                            />
                            <button
                              className='btn btn-success btn-xs'
                              onClick={requestForEmailCode}
                              disabled={
                                userDetails.email.trim() === '' || isEmailValid
                              }>
                              Send OTP
                            </button>
                          </div>
                          {alertMessage1 ? (
                            <div className='input-group input-group-merge mt-1'>
                              <div className='input-group-prepend'>
                                <span className='input-group-text'>
                                  <FeatherIcon.Lock />
                                </span>
                              </div>
                              <input
                                type='number'
                                className='form-control'
                                id='emailOtp'
                                placeholder='Enter your 6 digit otp'
                                value={userDetails.emailOtp}
                                disabled={isEmailValid}
                                onChange={handelChange}
                                autoComplete='off'
                              />
                              <button
                                className='btn btn-success btn-xs'
                                onClick={requestVerifyEmail}
                                disabled={
                                  userDetails.email.trim() === '' ||
                                  isEmailValid
                                }>
                                Verify OTP
                              </button>
                            </div>
                          ) : null}
                          {alertMessage1 ? (
                            <div
                              className='alert alert-success fade show mt-1 '
                              data-dismiss='alert'
                              aria-label='Close'>
                              {alertMessage1}
                            </div>
                          ) : null}
                        </div>
                        <div className='form-group '>
                          <label className='form-control-label'>Password</label>
                          <div className='input-group input-group-merge'>
                            <div className='input-group-prepend'>
                              <span className='input-group-text'>
                                <FeatherIcon.Lock />
                              </span>
                            </div>
                            <input
                              type='password'
                              className='form-control'
                              id='password'
                              placeholder='Password'
                              value={userDetails.password}
                              onChange={handelChange}
                              autoComplete='off'
                            />
                          </div>
                        </div>
                        <div className='form-group '>
                          <label className='form-control-label'>Address</label>
                          <div className='input-group input-group-merge'>
                            <div className='input-group-prepend'>
                              <span className='input-group-text'>
                                <FeatherIcon.MapPin />
                              </span>
                            </div>
                            <input
                              type='text'
                              className='form-control'
                              id='address'
                              placeholder='Address'
                              value={userDetails.address}
                              onChange={handelChange}
                              autoComplete='off'
                            />
                          </div>
                        </div>
                        {/* <div className='form-group mb-4'>
                          <div className='custom-control custom-checkbox'>
                            <input
                              type='checkbox'
                              className='custom-control-input'
                              id='checkbox-signup'
                              defaultChecked
                            />
                            <label
                              className='custom-control-label'
                              htmlFor='checkbox-signup'>
                              I accept{' '}
                              <a href='javascript: void(0);'>
                                Terms and Conditions
                              </a>
                            </label>
                          </div>
                        </div> */}
                        <div className='form-group mb-0 text-center'>
                          <button
                            disabled={isLoading}
                            className='btn btn-primary btn-block'
                            type='submit'
                            onClick={OnSignUP}>
                            {isLoading?'Signing up...':'Sign Up'}
                          </button>
                        </div>
                      </form>
                    </div>
                    <div className='col-lg-6 d-none d-md-inline-block'>
                      <div className='auth-page-sidebar'>
                        <div className='overlay' />
                        <div className='auth-user-testimonial'>
                          <p className='font-size-24 font-weight-bold text-white mb-1'>
                            DEALER HERO
                          </p>
                          <p className='lead'>
                            The dealership system that works.
                          </p>
                          <p>We've been helping dealers for over 20 years.</p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>{' '}
                {/* end card-body */}
              </div>
              {/* end card */}
              <div className='row mt-3'>
                <div className='col-12 text-center'>
                  <p className='text-muted'>
                    Already have account?
                    <Link to='/' className='text-primary font-weight-bold ml-1'>
                      Login
                    </Link>
                  </p>
                </div>{' '}
                {/* end col */}
              </div>
              {/* end row */}
            </div>{' '}
            {/* end col */}
          </div>
          {/* end row */}
        </div>
        {/* end container */}
      </div>
    </>
  );
};

export default Signup;
