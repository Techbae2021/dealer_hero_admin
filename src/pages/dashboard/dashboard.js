/** @format */

import React, { useState } from 'react';
import { Row, Col, Card, CardBody, Toast } from 'reactstrap';
import {
  UncontrolledButtonDropdown,
  DropdownMenu,
  DropdownItem,
  DropdownToggle,
} from 'reactstrap';
import './dashboard.scss';
import Flatpickr from 'react-flatpickr';
import {
  ChevronDown,
  Mail,
  Printer,
  File,
  Users,
  Image,
  ShoppingBag,
} from 'react-feather';
import OverviewWidget from '../../components/OverviewWidget';

import Statistics from './Statistics';
import RevenueChart from './RevenueChart';
import TargetChart from './TargetChart';
import SalesChart from './SalesChart';
import Orders from './Orders';
import Performers from './Performers';
import Tasks from './Tasks';
import ChatBox from './Chat';
// import ChatBox2 from '../../components/ChatUI';
import LeadBoard from './LeadBoard';
import { DragDropContext, Draggable, Droppable } from 'react-beautiful-dnd';
import Board from './Board';
import { getToken, onMessageListener } from '../../../src/firebase';

//import carImage from "../../assets/images/car.jpeg";
import { Button } from 'reactstrap';
import Calender from './Calender';
import Calendar2 from './calendar2';
import AuthUser from '../../components/auth/AuthUser';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Chat';

const Dashboard = () => {
  var oneWeekAgo = new Date();
  oneWeekAgo.setDate(oneWeekAgo.getDate() - 15);

  var filterDate = [oneWeekAgo, new Date()];

  const [showChat, setShowChat] = useState(false);
  // const [show, setShow] = useState(false);
  // const [notification, setNotification] = useState({ title: '', body: '' });
  // const [isTokenFound, setTokenFound] = useState(false);
  // getToken(setTokenFound);

  // onMessageListener()
  //   .then((payload) => {
  //     setShow(true);
  //     setNotification({
  //       title: payload.notification.title,
  //       body: payload.notification.body,
  //     });
  //     console.log(payload);
  //   })
  //   .catch((err) => console.log('failed: ', err));

  const Chat = () => {
    setShowChat(!showChat);
    // console.log({ showChat });
  };
  return (
    <AuthUser>
      <React.Fragment style={{zIndex:0}}>
        <Row className='page-title align-items-center'>
          <Col sm={4} xl={6}>
            <h4 className='mb-1 mt-0'>Dashboard</h4>
            {/* <Toast
              onClose={() => setShow(false)}
              show={show}
              delay={3000}
              autohide
              animation
              style={{
                position: 'absolute',
                top: 20,
                right: 20,
                minWidth: 200,
              }}>
              <Toast.Header>
                <img
                  src='holder.js/20x20?text=%20'
                  className='rounded mr-2'
                  alt=''
                />
                <strong className='mr-auto'>{notification.title}</strong>
                <small>just now</small>
              </Toast.Header>
              <Toast.Body>{notification.body}</Toast.Body>
            </Toast>
            <header className='App-header'>
              {isTokenFound && <h1> Notification permission enabled 👍🏻 </h1>}
              {!isTokenFound && <h1> Need notification permission ❗️ </h1>}

              <Button onClick={() => setShow(true)}>Show Toast</Button>
            </header> */}
          </Col>
          {/* <Col sm={8} xl={6}>
            <form className='form-inline float-sm-right mt-3 mt-sm-0'>
              <div className='form-group mb-sm-0 mr-2'>
                <Flatpickr
                  value={filterDate}
                  options={{ mode: 'range' }}
                  className='form-control'
                />
              </div>
              <UncontrolledButtonDropdown>
                <DropdownToggle color='primary' className='dropdown-toggle'>
                  <i className='uil uil-file-alt mr-1'></i>Download
                  <i className='icon ml-1'>
                    <ChevronDown />
                  </i>
                </DropdownToggle>
                <DropdownMenu right>
                  <DropdownItem>
                    <Mail className='icon-dual icon-xs mr-2'></Mail>
                    <span>Email</span>
                  </DropdownItem>
                  <DropdownItem>
                    <Printer className='icon-dual icon-xs mr-2'></Printer>
                    <span>Print</span>
                  </DropdownItem>
                  <DropdownItem divider />
                  <DropdownItem>
                    <File className='icon-dual icon-xs mr-2'></File>
                    <span>Re-Generate</span>
                  </DropdownItem>
                </DropdownMenu>
              </UncontrolledButtonDropdown>
            </form>
          </Col> */}
        </Row>

        {/* stats */}
        <Statistics></Statistics>

        <Row>
          <Col xl={12}>
            <Card className='Pipeline_Size'>
              <CardBody className='Pipeline_Size'>
                <Board />
              </CardBody>
            </Card>
          </Col>
        </Row>
       
        <div className='fixedsocial' style={{zIndex:9999}}>
       
          {showChat ? (
            <div className='showChat'>
              <ChatBox chatToggle={Chat} />
              {/* <ChatBox2/> */}
            </div>
          ) : (
            <>
            <Fab onClick={Chat} color="primary" aria-label="chat">
              <AddIcon />
            </Fab>
              {/* <svg
                xmlns='http://www.w3.org/2000/svg'
                width='32'
                height='32'
                fill='currentColor'
                class='bi bi-chat-right-dots'
                viewBox='0 0 16 16'>
                <path d='M2 1a1 1 0 0 0-1 1v8a1 1 0 0 0 1 1h9.586a2 2 0 0 1 1.414.586l2 2V2a1 1 0 0 0-1-1H2zm12-1a2 2 0 0 1 2 2v12.793a.5.5 0 0 1-.854.353l-2.853-2.853a1 1 0 0 0-.707-.293H2a2 2 0 0 1-2-2V2a2 2 0 0 1 2-2h12z' />
                <path d='M5 6a1 1 0 1 1-2 0 1 1 0 0 1 2 0zm4 0a1 1 0 1 1-2 0 1 1 0 0 1 2 0zm4 0a1 1 0 1 1-2 0 1 1 0 0 1 2 0z' />
              </svg> */}
            </>
          )}
        </div>
        {/* charts */}
        <Row>
          <Col xl={3}>
            <OverviewWidget
              items={[
                {
                  title: '121,000',
                  description: 'Total Visitors',
                  icon: Users,
                },
                { title: '21,000', description: 'Product Views', icon: Image },
                {
                  title: '$21.5',
                  description: 'Revenue Per Visitor',
                  icon: ShoppingBag,
                },
              ]}></OverviewWidget>
          </Col>

          <Col xl={9}>
            <RevenueChart />
          </Col>
          {/* <Col xl={3}></Col> */}
        </Row>

        {/* charts */}

        {/* charts */}
        <Row>
          <Col xl={5}>
            <SalesChart />
          </Col>
          <Col xl={7}>
            <LeadBoard />
          </Col>
        </Row>

        <Row>
          <Col xl={4}>{/* <Performers /> */}</Col>
          <Col xl={4}>{/* <Chat /> */}</Col>
          {/* <Col xl={4}>
            <TargetChart />
          </Col> */}
        </Row>
        <Row >
          <Calendar2 />
            {/* <Chat /> */}
            {/* <Tasks /> */}
           
            {/* <Orders /> */}
        </Row>
      </React.Fragment>
    </AuthUser>
  );
};

export default Dashboard;
