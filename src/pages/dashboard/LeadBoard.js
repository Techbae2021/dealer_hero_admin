/** @format */

import React, { useEffect, useState } from 'react';
import { Card, CardBody, Table } from 'reactstrap';
import './dashboard.scss';
import Instance from './../../Instance';
import { useHistory } from 'react-router-dom';

const LeadBoard = () => {
  let history = useHistory();
  const [data1, setData1] = useState([]);

  useEffect(() => {
    Instance.get(`/api/admin/dashboard/leadboards`, {
      headers: { authorization: `Bearer ${localStorage.getItem('token$')}` },
    })
      .then(({ data }) => {
        // console.log('leadBoard11', data);
        setData1(data?.leads);
      })
      .catch((err) => {
        console.log('err', err);
      });
  }, []);

  const LeadDetails = (value) => {
    Instance.put(
      `/api/user/changeLeadReadStatus/${value._id}`,
      {
        leadReadUnreadStutus: false,
      },
      {
        headers: { authorization: `Bearer ${localStorage.getItem('token$')}` },
      }
    )
      .then(() => {
        // console.log('Success');
        history.push(`/leads/detail/${value._id}`);
      })
      .catch((err) => {
        console.log('Error', err);
      });
  };

  return (
    <Card>
      <CardBody className='pb-0'>
        <h5 className='card-title mt-0 mb-0 header-title '>Lead Board</h5>
        <div
          className='Lead_Board_Style'
          style={{ maxHeight: '345px', overflow: 'auto' }}>
          <Table hover responsive className='mt-4'>
            <tbody>
              {data1 &&
                data1.map((value, i) => (
                  <tr key={i}>
                    <td onClick={() => LeadDetails(value)}>{i + 1}</td>
                    <td onClick={() => LeadDetails(value)}>
                      {value?.firstName}
                    </td>
                    <td onClick={() => LeadDetails(value)}>
                      {value?.lastName}
                    </td>
                    <td onClick={() => LeadDetails(value)}>{value?.phone}</td>
                    <td>
                      {value?.leadReadUnreadStutus === true ? (
                        <span className='badge badge-soft-danger py-1'>
                          NEW
                        </span>
                      ) : (
                        <span className='badge badge-soft-success py-1'>
                          READ
                        </span>
                      )}
                    </td>
                  </tr>
                ))}
            </tbody>
            ;
          </Table>
        </div>
      </CardBody>
    </Card>
  );
};

export default LeadBoard;
