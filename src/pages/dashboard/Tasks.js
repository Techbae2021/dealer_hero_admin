/** @format */

import React, { useState } from 'react';
import {
  Card,
  CardBody,
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
} from 'reactstrap';

import PerfectScrollbar from 'react-perfect-scrollbar';
import 'react-perfect-scrollbar/dist/css/styles.css';

import TaskList from '../../components/TaskList';
import TaskItem from '../../components/TaskItem';

const Tasks = () => {
  const [modal, setModal] = useState(false);

  const toggle = () => setModal(!modal);

  return (
    <Card>
      <CardBody className='pt-2 pb-3'>
        <Button
          className='float-right mt-2'
          size={'sm'}
          color='primary'
          onClick={toggle}>
          Add
        </Button>
        <h5 className='mb-4 header-title'>Tasks</h5>
        <PerfectScrollbar style={{ maxHeight: '270px' }}>
          <TaskList>
            <TaskItem
              title='Draft the new contract document for sales team'
              due_date='24 Aug, 2019'
            />
            <TaskItem
              title='iOS App home page'
              due_date='23 Aug, 2019'
              className='mt-2'
            />
            <TaskItem
              title='Write a release note for Shreyu'
              due_date='24 Aug, 2019'
              className='mt-2'
            />
            <TaskItem
              title='Invite Greeva to a project shreyu admin'
              due_date='22 Aug, 2019'
              className='mt-2'
            />
            <TaskItem
              title='Enable analytics tracking for main website'
              due_date='20 Aug, 2019'
              className='mt-2'
            />
            <TaskItem
              title='Invite user to a project'
              due_date='18 Aug, 2019'
              className='mt-2'
            />
            <TaskItem
              title='Write a release note'
              due_date='14 Aug, 2019'
              className='mt-2'
            />
          </TaskList>
        </PerfectScrollbar>

        <Modal isOpen={modal} toggle={toggle}>
          <ModalHeader toggle={toggle}>Task Add</ModalHeader>
          <ModalBody>
            <div className='row'>
              <div className='col-md-6'>
                <label for='exampleInputEmail1'>Task Date</label>
                <input
                  type='date'
                  class='form-control'
                  id='exampleInputEmail1'
                  aria-describedby='emailHelp'
                />
              </div>
              <div className='col-md-6'>
                <label for='exampleInputEmail1'>Task Time</label>
                <input
                  type='time'
                  class='form-control'
                  id='exampleInputEmail1'
                  aria-describedby='emailHelp'
                />
              </div>
            </div>
            <br />
            <div>
              <label for='exampleInputEmail1'>Task Name</label>
              <input
                type='text'
                class='form-control'
                id='exampleInputEmail1'
                aria-describedby='emailHelp'
              />
            </div>
            <br />
            <div>
              <label for='exampleInputEmail1'>Task Details</label>
              <textarea
                class='form-control'
                id='exampleFormControlTextarea1'
                rows='4'></textarea>
            </div>
          </ModalBody>
          <ModalFooter>
            <Button color='secondary' onClick={toggle}>
              Save
            </Button>
          </ModalFooter>
        </Modal>
      </CardBody>
    </Card>
  );
};

export default Tasks;
