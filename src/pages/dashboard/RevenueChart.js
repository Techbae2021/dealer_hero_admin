/** @format */

import React, { useEffect, useState } from 'react';
import Chart from 'react-apexcharts';
import { Card, CardBody, Nav, NavItem, NavLink } from 'reactstrap';
import Instance from './../../Instance';
import axios from 'axios';
import moment from 'moment';

const RevenueChart = () => {
  const [lebel, setLebel] = useState([]);
  const [count, setCount] = useState([]);

  useEffect(() => {
    axios
      .get(
        `https://mixpanel.com/api/2.0/segmentation/sum?project_id=2337984&event=Revinue&on=properties["amount"]&to_date=${moment(
          new Date().toISOString()
        ).format('YYYY-MM-DD')}&from_date=${moment(new Date().toISOString())
          .subtract(1, 'months')
          .format('YYYY-MM-DD')}`,
        {
          headers: {
            Authorization:
              'Basic DealerHero.223d8c.mp-service-account:R4eTjAMP953oYR1nvEd1CMWi8uPpAgZu',
            Accept: 'application/json',
          },
        }
      )
      .then(({ data }) => {
        // console.log('Revinue', data);

        let leadDetails1 = data?.results;

        let temp1 = [];
        let temp2 = [];

        Object.entries(leadDetails1)
          .sort()
          .forEach((value) => {
            temp1.push(value[0]);
            temp2.push(value[1]);
          });

        // console.log('Sort Count Values', temp1);
        // console.log('Sort Count Values2', temp2);
        setLebel(temp1);
        setCount(temp2);
      })
      .catch((err) => {
        console.log('err', err);
      });
  }, []);

  //   const getDaysInMonth = (month, year) => {
  //     var date = new Date(year, month, 1);
  //     var days = [];
  //     var idx = 0;
  //     while (date.getMonth() === month && idx < 15) {
  //       var d = new Date(date);
  //       days.push(
  //         d.getDate() + ' ' + d.toLocaleString('en-us', { month: 'short' })
  //       );
  //       date.setDate(date.getDate() + 1);
  //       idx += 1;
  //     }
  //     return days;
  //   };

  //   var now = new Date();
  var labels = lebel;

  const apexLineChartWithLables = {
    chart: {
      height: 296,
      type: 'area',
      toolbar: {
        show: false,
      },
      parentHeightOffset: 0,
    },
    grid: {
      padding: {
        left: 0,
        right: 0,
      },
    },
    dataLabels: {
      enabled: false,
    },
    stroke: {
      curve: 'smooth',
      width: 4,
    },
    zoom: {
      enabled: false,
    },
    legend: {
      show: false,
    },
    colors: ['#43d39e'],
    xaxis: {
      type: 'string',
      categories: labels,
      tooltip: {
        enabled: false,
      },
      axisBorder: {
        show: false,
      },
      labels: {},
    },
    yaxis: {
      labels: {
        formatter: function (val) {
          return val + 'k';
        },
      },
    },
    fill: {
      type: 'gradient',
      gradient: {
        type: 'vertical',
        shadeIntensity: 1,
        inverseColors: false,
        opacityFrom: 0.45,
        opacityTo: 0.05,
        stops: [45, 100],
      },
    },
    tooltip: {
      theme: 'dark',
      x: { show: false },
    },
  };

  const apexLineChartWithLablesData = [
    {
      name: 'Revenue',
      data: count,
    },
  ];

  return (
    <Card style={{ overflow: 'auto' }}>
      <CardBody className='pb-0'>
        <Nav className='card-nav float-right'>
          <NavItem>
            <NavLink className='text-muted' href='#'>
              Today
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink className='text-muted' href='#'>
              7d
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink className='' active href='#'>
              15d
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink className='text-muted' href='#'>
              1m
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink className='text-muted' href='#'>
              1y
            </NavLink>
          </NavItem>
        </Nav>

        <h5 className='card-title mb-0 header-title'>Revenue</h5>

        <Chart
          options={apexLineChartWithLables}
          series={apexLineChartWithLablesData}
          type='area'
          className='apex-charts mt-3'
          height={296}
        />
      </CardBody>
    </Card>
  );
};

export default RevenueChart;
