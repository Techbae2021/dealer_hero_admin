/** @format */

// @flow
import React, { useEffect } from 'react';
import { Row, Col } from 'reactstrap';
import './dashboard.scss';
import StatisticsChartWidget from '../../components/StatisticsChartWidget';
import Instance from './../../Instance';
import { Card, CardBody } from 'reactstrap';
import { useState } from 'react';
import axios from 'axios';
import moment from 'moment';
import { Link, useHistory } from 'react-router-dom';
const Statistics = () => {
  let history = useHistory();
  const [data, setData] = useState(null);
  // const [labels, setLabels] = useState([]);
  // const [labels2, setLabels2] = useState([]);
  const [leadValues, setLeadValues] = useState([]);
  const [dealerValue, setdealerValue] = useState([]);
  const [carsValue, setCars] = useState([]);
  const [soldValues, setSoldValues] = useState([]);

  useEffect(() => {
    Instance.get(`/api/admin/dashboard/summary`, {
      headers: { authorization: `Bearer ${localStorage.getItem('token$')}` },
    }).then(({ data }) => {
        // console.log('dd', data);
        setData(data);
      })
      .catch((err) => {
        console.log('err', err);
      });
  }, []);

  useEffect(() => {
    //Leads
    axios
      .get(
        `https://mixpanel.com/api/2.0/segmentation?project_id=2337984&event=Lead&to_date=${moment(
          new Date().toISOString()
        ).format('YYYY-MM-DD')}&from_date=${moment(new Date().toISOString())
          .subtract(1, 'months')
          .format('YYYY-MM-DD')}`,
        {
          headers: {
            Authorization:
              'Basic DealerHero.223d8c.mp-service-account:R4eTjAMP953oYR1nvEd1CMWi8uPpAgZu',
            Accept: 'application/json',
          },
        }
      )
      .then(({ data }) => {
        // console.log('Mix Panel Data Fetech', data);

        // setLabels(data?.data?.series);

        let leadDetails2 = data?.data?.values.Lead;
        // console.log('Sort lead Values A', Object.entries(leadDetails));
        let temp1 = [];

        Object.entries(leadDetails2)
          .sort()
          .forEach((value) => {
            temp1.push(value[1]);
          });

        // console.log('Sort lead Values', temp);
        setLeadValues(temp1);
      })
      .catch((error) => {
        console.log('Error in Mixpanel data fetch ', error);
      });

      //Dealer
    axios
      .get(
        `https://mixpanel.com/api/2.0/segmentation?project_id=2337984&event=Instock&to_date=${moment(
          new Date().toISOString()
        ).format('YYYY-MM-DD')}&from_date=${moment(new Date().toISOString())
          .subtract(1, 'months')
          .format('YYYY-MM-DD')}`,
        {
          headers: {
            Authorization:
              'Basic DealerHero.223d8c.mp-service-account:R4eTjAMP953oYR1nvEd1CMWi8uPpAgZu',
            Accept: 'application/json',
          },
        }
      )
      .then(({ data }) => {
        // console.log('Mix Panel Instock Data', data);

        // setLabels2(data?.data?.series);

        let leadDetails2 = data?.data?.values.Instock;

        // console.log('Sort333 instock Values', Object.entries(leadDetails));
        let temp2 = [];

        Object.entries(leadDetails2)
          .sort()
          .forEach((value) => {
            temp2.push(value[1]);
          });

        // console.log('Sort instock Values', temp);
        setdealerValue(temp2);
      })
      .catch((error) => {
        console.log('Error in Mixpanel data fetch ', error);
      });
    //Cars  
    axios
      .get(
        `https://mixpanel.com/api/2.0/segmentation?project_id=2337984&event=Proposal&to_date=${moment(
          new Date().toISOString()
        ).format('YYYY-MM-DD')}&from_date=${moment(new Date().toISOString())
          .subtract(1, 'months')
          .format('YYYY-MM-DD')}`,
        {
          headers: {
            Authorization:
              'Basic DealerHero.223d8c.mp-service-account:R4eTjAMP953oYR1nvEd1CMWi8uPpAgZu',
            Accept: 'application/json',
          },
        }
      )
      .then(({ data }) => {
        // console.log('Mix Panel Proposal Data', data);

        // setLabels2(data?.data?.series);

        let leadDetails3 = data?.data?.values.Proposal;

        // console.log('Sort333 instock Values', Object.entries(leadDetails));
        let temp3 = [];

        Object.entries(leadDetails3)
          .sort()
          .forEach((value) => {
            temp3.push(value[1]);
          });

        // console.log('Sort instock Values', temp);
        setCars(temp3);
      })
      .catch((error) => {
        console.log('Error in Mixpanel data fetch ', error);
      });

    axios
      .get(
        `https://mixpanel.com/api/2.0/segmentation?project_id=2337984&event=Sold&to_date=${moment(
          new Date().toISOString()
        ).format('YYYY-MM-DD')}&from_date=${moment(new Date().toISOString())
          .subtract(1, 'months')
          .format('YYYY-MM-DD')}`,
        {
          headers: {
            Authorization:
              'Basic DealerHero.223d8c.mp-service-account:R4eTjAMP953oYR1nvEd1CMWi8uPpAgZu',
            Accept: 'application/json',
          },
        }
      )
      .then(({ data }) => {
        console.log('Mix Panel Sold Data', data);

        // setLabels2(data?.data?.series);

        let leadDetails4 = data?.data?.values.Sold;

        // console.log('Sort333 instock Values', Object.entries(leadDetails));
        let temp4 = [];

        Object.entries(leadDetails4)
          .sort()
          .forEach((value) => {
            temp4.push(value[1]);
          });

        // console.log('Sort instock Values', temp);
        setSoldValues(temp4);
      })
      .catch((error) => {
        console.log('Error in Mixpanel data fetch ', error);
      });
  }, []);

  return (
    <React.Fragment>

      <Card>
            <CardBody className="p-3 d-flex" >
        <Col md={3} xl={3} >
          <Link to='/leads'>
            <StatisticsChartWidget
              description='Lead'
              title={data?.leads}
              data={leadValues}
              trend={{
                textClass: 'text-success',
                // icon: 'uil uil-arrow-up',
                // value: '10.21%',
              }}></StatisticsChartWidget>
          </Link>
        </Col>

        <Col md={3} xl={3}>
          <Link to='/dealer'>
            <StatisticsChartWidget
              description='Dealer'
              title={data?.dealers}
              colors={['#f77e53']}
              data={dealerValue}
              trend={{
                textClass: 'text-danger',
                // icon: 'uil uil-arrow-down',
                // value: '5.05%',
              }}></StatisticsChartWidget>
          </Link>
        </Col>

        <Col md={3} xl={3} >
          <Link to='/dealer'>
            <StatisticsChartWidget
              description='Car'
              title={data?.cars}
              colors={['#43d39e']}
              data={carsValue}
              trend={{
                textClass: 'text-success',
                // icon: 'uil uil-arrow-up',
                // value: '25.16%',
              }}></StatisticsChartWidget>
          </Link>
        </Col>

        <Col md={3} xl={3}>
          <Link to='/dealer'>
            <StatisticsChartWidget
              description='Car Sold'
              title={data?.carsolds}
              colors={['#ffbe0b']}
              data={soldValues}
              trend={{
                textClass: 'text-danger',
                // icon: 'uil uil-arrow-down',
                // value: '5.05%',
              }}></StatisticsChartWidget>
          </Link>
        </Col>

</CardBody>
</Card>
<Row>
        <Col md={6} xl={3} className='Card_Size'>
          <Link to='/inventory/sold'>
            <StatisticsChartWidget
              description='Payment'
              title={data?.solds}
              colors={['#ffbe0b']}
              data={soldValues}
              trend={{
                textClass: 'text-danger',
                // icon: 'uil uil-arrow-down',
                // value: '5.05%',
              }}></StatisticsChartWidget>
          </Link>
        </Col>
        <Col md={6} xl={3} className='Card_Size'>
          <Link to='/inventory/sold'>
            <StatisticsChartWidget
              description='Auction 1'
              title={data?.solds}
              colors={['#ffbe0b']}
              data={soldValues}
              trend={{
                textClass: 'text-danger',
                // icon: 'uil uil-arrow-down',
                // value: '5.05%',
              }}></StatisticsChartWidget>
          </Link>
        </Col>
        <Col md={6} xl={3} className='Card_Size'>
          <Link to='/inventory/sold'>
            <StatisticsChartWidget
              description='Auction 2'
              title={data?.solds}
              colors={['#ffbe0b']}
              data={soldValues}
              trend={{
                textClass: 'text-danger',
                // icon: 'uil uil-arrow-down',
                // value: '5.05%',
              }}></StatisticsChartWidget>
          </Link>
        </Col>
        <Col md={6} xl={3} className='Card_Size'>
          <Link to='/inventory/sold'>
            <StatisticsChartWidget
              description='Auction 3'
              title={data?.solds}
              colors={['#ffbe0b']}
              data={soldValues}
              trend={{
                textClass: 'text-danger',
                // icon: 'uil uil-arrow-down',
                // value: '5.05%',
              }}></StatisticsChartWidget>
          </Link>
        </Col>
      
      </Row>
    </React.Fragment>
  );
};

export default Statistics;
