import React, { Component, useState } from "react";
import { Calendar, momentLocalizer } from "react-big-calendar";
import moment from "moment";
import "react-big-calendar/lib/css/react-big-calendar.css";
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from "reactstrap";
import CustomInput from "../../components/CustomInput";
import { useEffect } from "react";
import Instance from "./../../Instance";
import { useAlert } from "react-alert";

const localizer = momentLocalizer(moment);

const Calendar2 = () => {
  const alert = useAlert();
  const [events, Setevents] = useState([]);
  const [modal, setModal] = useState(false);
  const [selectedEvent, setselectedEvent] = useState(null);
  const toggle = () => setModal(!modal);
  const [modal1, setModal1] = useState(false);
  const toggle1 = () => {
    setModal1(!modal1);
    setisEdit(false);
    setaddEventDetails({
      name: "",
      start: "",
      end: "",
    })
  };
  const [refresh, setrefresh] = useState(false);
  const [isEdit, setisEdit] = useState(false);

  const [addEventDetails, setaddEventDetails] = useState({
    name: "",
    start: "",
    end: "",
  });

  useEffect(() => {
    Instance.get(`/api/admin/taskevent/get`, {
      headers: { authorization: `Bearer ${localStorage.getItem("token$")}` },
    })
      .then(({ data }) => {
        if (Array.isArray(data?.data)) {
          let arr = [];
          data?.data.map((item, i) => {
            arr.push({
              id: item._id,
              title: item.name,
              allDay: false,
              start: new Date(item.startDateAndTime),
              end: new Date(item.endDateAndTime),
            });
          });
          Setevents(arr);
        }
      })
      .catch((err) => {
        console.log("err", err);
      });
  }, [refresh]);

  const addEvent = () => {
    Instance.post(
      `/api/admin/taskevent/add`,
      {
        name: addEventDetails.name,
        startDateAndTime: addEventDetails.start,
        endDateAndTime: addEventDetails.end,
      },
      {
        headers: { authorization: `Bearer ${localStorage.getItem("token$")}` },
      }
    )
      .then(({ data }) => {
        // console.log(data);
        alert.success("Event Added.");
        setrefresh(!refresh);
      })
      .catch((err) => {
        console.log("err", err);
      });
    setModal1(!modal1);
  };

  const DeleteEvent = (id) => {
    Instance.delete(`/api/admin/taskevent/delete/${id}`, {
      headers: { authorization: `Bearer ${localStorage.getItem("token$")}` },
    })
      .then(({ data }) => {
        // console.log(data);
        alert.success("Event Deleted.");
        setrefresh(!refresh);
      })
      .catch((err) => {
        console.log("err", err);
      });
    setModal(!modal);
  };

  const updateEvent = (id) => {
    Instance.put(
      `/api/admin/taskevent/update/${id}`,
      {
        name: addEventDetails.name,
        startDateAndTime: addEventDetails.start,
        endDateAndTime: addEventDetails.end,
      },
      {
        headers: { authorization: `Bearer ${localStorage.getItem("token$")}` },
      }
    )
      .then(({ data }) => {
        // console.log(data);
        alert.success("Event Updated.");
        setrefresh(!refresh);
      })
      .catch((err) => {
        console.log("err", err);
        alert.error(err?.response?.data?.error);
      });
    setModal1(!modal1);
    setisEdit(false);
  };

  const setUpdateEvent = () => {
    setModal(!modal);
    setModal1(!modal1);
    setaddEventDetails({
      name: selectedEvent.title,
      start: moment(new Date(selectedEvent?.start)).format("YYYY-MM-DDThh:mm"),
      end: moment(new Date(selectedEvent?.end)).format("YYYY-MM-DDThh:mm"),
    });
    setisEdit(true);
  };

  const handelChange = (event) => {
    setaddEventDetails({
      ...addEventDetails,
      [event.target.id]: event.target.value,
    });

    if (event.target.value === "") {
      setErrors({ ...errors, [event.target.name]: true });
    } else {
      setErrors({ ...errors, [event.target.name]: false });
    }
    // console.log(event.target.value);
  };

  const [errors, setErrors] = useState({
    name: false,
    start: false,
    end: false,
  });

  const [isValid, setisValid] = useState(true);

  const HandleValidation = () => {
    let shouldsubmit = true;
    let tempError = errors;

    for (const [key, value] of Object.entries(addEventDetails)) {
      if (value == "") {
        tempError[key] = true;
        if (shouldsubmit) {
          shouldsubmit = false;
        }
      } else {
        tempError[key] = false;
      }
    }
    // console.log(tempError);
    setisValid(shouldsubmit);
    setErrors(tempError);

    if (shouldsubmit) {
      if (isEdit) {
        updateEvent(selectedEvent?.id)
      } else {
        addEvent();
      }
    }
  };

  return (
    <div className="calendar_container_outer" style={{zIndex:0}}>
      <div className="calendar_container" style={{width:"100%"}}>
        <Calendar
          events={events}
          startAccessor="start"
          endAccessor="end"
          defaultDate={moment().toDate()}
          localizer={localizer}
          className="calendar_with_event"
          onSelectEvent={(e) => {
            // console.log(e);
            setselectedEvent(e);
            setModal(!modal);
          }}
        />
        <Button
          className="add_event"
          onClick={() => {
            setModal1(!modal1);
          }}
        >
          Add Event
        </Button>
      </div>
      <Modal isOpen={modal} toggle={toggle} className="centered">
        <ModalHeader toggle={toggle}>Event</ModalHeader>
        <ModalBody>
          <b>{selectedEvent?.title}</b>
          <p>
            Starts at:{" "}
            {moment(new Date(selectedEvent?.start)).format("DD/MM/YYYY")},
            {new Date(selectedEvent?.start).toLocaleTimeString()}
          </p>
          <p>
            Ends at: {moment(new Date(selectedEvent?.end)).format("DD/MM/YYYY")}
            ,{new Date(selectedEvent?.end).toLocaleTimeString()}
          </p>
        </ModalBody>
        <ModalFooter>
          <Button onClick={() => DeleteEvent(selectedEvent.id)}>Delete</Button>
          <Button color="primary" onClick={() => setUpdateEvent()}>
            Edit
          </Button>
        </ModalFooter>
      </Modal>
      <Modal isOpen={modal1} toggle={toggle1} className="centered">
        <ModalHeader toggle={toggle1}>Add Event</ModalHeader>
        <ModalBody>
          {[
            { label: "Name", name: "name", type: "text" },
            {
              label: "Start Date And Time",
              name: "start",
              type: "datetime-local",
            },
            { label: "End Date And Time", name: "end", type: "datetime-local" },
          ].map((item, i) => (
            <div className="" key={i}>
              <CustomInput
                label={item.label}
                type={item.type}
                error={errors[item.name]}
                name={item.name}
                id={item.name}
                value={addEventDetails[item.name]}
                handleChange={(e) => {
                  handelChange(e);
                }}
                style={{ width: "100%" }}
              />
            </div>
          ))}
        </ModalBody>
        <ModalFooter>
          <Button color="primary" onClick={HandleValidation}>
            {isEdit ? "Update" : "Add"}
          </Button>
        </ModalFooter>
      </Modal>
    </div>
  );
};

export default Calendar2;
