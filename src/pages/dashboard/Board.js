/** @format */

import React, { useState, useEffect, useContext } from 'react';
import {
  Row,
  Col,
  Card,
  CardBody,
  Button,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  UncontrolledDropdown,
} from 'reactstrap';
import classNames from 'classnames';
import Sortable from 'react-sortablejs';
import Instance from './../../Instance';
import { DragDropContext, Draggable, Droppable } from 'react-beautiful-dnd';
import uuid from 'uuid/v4';
import './dashboard.scss';
// import Instance from './../../Instance';
import Context from '../context/Context';
import { DASHBOARD_NOTE_DETAILS } from '../context/action.type';
import { useHistory, useParams } from 'react-router-dom';
import moment from 'moment';
import { useAlert } from 'react-alert';

const Board = () => {
  let history = useHistory();
  const params = useParams();
  const alert = useAlert();

  const [pipelineView, setPipelineView] = useState([]);
  const [noteDetails, setNoteDetails] = useState([]);
  const [columns, setColumns] = useState(null);
  const [loader, setLoader] = useState(false);
  const [refresh, seRefresh] = useState(false);
  const { dispatchDetails } = useContext(Context);

  useEffect(() => {
    setLoader(true);
    Instance.get(`/api/admin/leads/notes`, {
      headers: { authorization: `Bearer ${localStorage.getItem('token$')}` },
    }).then(({ data }) => {
        // console.log('piplinesData', data);
        setLoader(false);
        let dataTree = data?.data;
        setNoteDetails(data?.data);
        let obj = {};

        dataTree.forEach((value, i) => {
          obj[value._id] = {
            name: value.pipelineName,
            items: value.Notes,
          };
        });

        setColumns(obj);

        // console.log({ obj });
      }).catch((err) => {
        setLoader(false);
        console.log('err', err);
      });
  }, [refresh]);

  const itemsFromBackend = [
    { id: uuid(), content: 'First task' },
    { id: uuid(), content: 'Second task' },
    { id: uuid(), content: 'Third task' },
    { id: uuid(), content: 'Fourth task' },
    { id: uuid(), content: 'Fifth task' },
  ];

  const onDragEnd = (result, columns, setColumns) => {
    // console.log('result', result);
    setPipelineView(result);

    if (!result.destination) return;
    const { source, destination } = result;

    if (source.droppableId !== destination.droppableId) {
      const sourceColumn = columns[source.droppableId];
      const destColumn = columns[destination.droppableId];
      const sourceItems = [...sourceColumn.items];
      const destItems = [...destColumn.items];
      const [removed] = sourceItems.splice(source.index, 1);
      destItems.splice(destination.index, 0, removed);
      setColumns({
        ...columns,
        [source.droppableId]: {
          ...sourceColumn,
          items: sourceItems,
        },
        [destination.droppableId]: {
          ...destColumn,
          items: destItems,
        },
      });
    } else {
      const column = columns[source.droppableId];
      const copiedItems = [...column.items];
      const [removed] = copiedItems.splice(source.index, 1);
      copiedItems.splice(destination.index, 0, removed);
      setColumns({
        ...columns,
        [source.droppableId]: {
          ...column,
          items: copiedItems,
        },
      });
    }
  };

  useEffect(() => {
    if (pipelineView && pipelineView?.draggableId) {
      Instance.put(
        `/api/admin/leadNote/changeindex/${pipelineView?.draggableId}`,
        {
          newIndex: pipelineView?.destination?.index,
          pipeline: pipelineView?.destination?.droppableId,
        },
        {
          headers: {
            authorization: `Bearer ${localStorage.getItem('token$')}`,
          },
        }
      ).then(({ data }) => {
          // console.log('save', data);
        })
        .catch((err) => {
          console.log('Err', err);
        });
    }
  }, [pipelineView]);

  // const { dispatchDetails } = useContext(Context);
  const LeadNoteDelete = (item) => {
    Instance.delete(`/api/admin/leadNote/${item?._id}`, {
      headers: { authorization: `Bearer ${localStorage.getItem('token$')}` },
    })
      .then(() => {
        // console.log('Success');
        alert.success('Note Deleted');
        seRefresh(!refresh);
      })
      .catch((err) => {
        console.log('Error', err);
      });
  };

  const LeadNoteEdit = (item) => {
    // console.log({ item });
    dispatchDetails({
      type: DASHBOARD_NOTE_DETAILS,
      payload: item,
    });
    history.push(`/leadNoteEdit/${item._id}`);
  };

  const ViewDetails = (item) => {
    history.push(`/leads/detail/${item.lead_id}`);
  };

  // const LeadNoteEdit = () => {};

  // const LeadNoteDelete = () => {
  //   Instance.delete(`/api/user/leadNote/${value._id}`, {
  //     headers: { authorization: `Bearer ${localStorage.getItem('token$')}` },
  //   })
  //     .then(() => {
  //       console.log('Success');
  //       alert.success('Note Deleted');
  //       seRefresh(!refresh);
  //     })
  //     .catch((err) => {
  //       console.log('Error', err);
  //     });
  // };

  return (
    <React.Fragment>
      {loader ? (
        <>
          <div class='d-flex justify-content-center'>
            <div class='spinner-grow' role='status'>
              <span class='sr-only'>Loading...</span>
            </div>
          </div>
        </>
      ) : (
        <>
          <div
            style={{
              display: 'flex',
              // justifyContent: 'center',
              height: '100%',
              flexWrap: 'wrap',
            }}
            className='border mb-0 p-3'>
            <DragDropContext
              onDragEnd={(result) => onDragEnd(result, columns, setColumns)}>
              {columns != null &&
                Object.entries(columns).map(([columnId, column], index) => {
                  return (
                    <div
                      style={{
                        display: 'flex',
                        flexDirection: 'column',
                        alignItems: 'center',
                      }}
                      // className='row col-md-4'
                      key={columnId}>
                      {/* {console.log({ columnId })}
                  {console.log({ column })} */}
                      <h2 className='Pipeline_Size'>
                        {column.name} [{column && column.items.length}]
                      </h2>
                      <div style={{ margin: 8 }}>
                        <Droppable droppableId={columnId} key={columnId}>
                          {(provided, snapshot) => {
                            return (
                              <div
                                {...provided.droppableProps}
                                ref={provided.innerRef}
                                style={{
                                  background: snapshot.isDraggingOver
                                    ? 'lightblue'
                                    : '#43D39E',
                                  padding: 4,
                                  width: 250,
                                  // minHeight: 500,
                                }}
                                className='Pipeline_Size_In'>
                                {column.items.map((item, index) => {
                                  return (
                                    <Draggable
                                      key={item._id}
                                      draggableId={item._id}
                                      index={index}>
                                      {(provided, snapshot) => {
                                        return (
                                          <div
                                            ref={provided.innerRef}
                                            {...provided.draggableProps}
                                            {...provided.dragHandleProps}
                                            style={{
                                              userSelect: 'none',
                                              // padding: 16,
                                              margin: '0 0 8px 0',
                                              // minHeight: '50px',
                                              height: '100%',
                                              backgroundColor: snapshot.isDragging
                                                ? 'white'
                                                : 'white',
                                              color: 'black',
                                              borderRadius: '10px',
                                              ...provided.draggableProps.style,
                                            }}
                                            className='Pipeline_Size'>
                                            <Card>
                                              <CardBody>
                                                <UncontrolledDropdown className='float-right'>
                                                  <DropdownToggle
                                                    tag='a'
                                                    className='dropdown-toggle p-0 arrow-none cursor-pointer'>
                                                    <i className='uil uil-ellipsis-v font-size-14'></i>
                                                  </DropdownToggle>

                                                  <DropdownMenu>
                                                    <DropdownItem
                                                      onClick={() =>
                                                        LeadNoteEdit(item)
                                                      }>
                                                      <i className='uil uil-edit-alt mr-2'></i>
                                                      Edit
                                                    </DropdownItem>
                                                    {/* <DropdownItem>
                                                      <i className='uil uil-user-plus mr-2'></i>
                                                      Add People
                                                    </DropdownItem>
                                                    <DropdownItem className='text-warning'>
                                                      <i className='uil uil-exit mr-2'></i>
                                                      Leave
                                                    </DropdownItem> */}
                                                    <DropdownItem divider />
                                                    <DropdownItem
                                                      className='text-danger'
                                                      onClick={() =>
                                                        LeadNoteDelete(item)
                                                      }>
                                                      <i className='uil uil-trash mr-2'></i>
                                                      Delete
                                                    </DropdownItem>
                                                  </DropdownMenu>
                                                </UncontrolledDropdown>
                                                <a
                                                  onClick={() =>
                                                    ViewDetails(item)
                                                  }>
                                                  {item.details}
                                                </a>
                                                <br />
                                                <br />

                                                {/* <span
                                                  className={classNames(
                                                    'badge',
                                                    'badge-soft-danger'
                                                  )}>
                                                  High
                                                </span> */}
                                                <small className='float-right text-muted'>
                                                  {moment(
                                                    new Date(item.createdAt)
                                                  ).format('DD/MM/YYYY')}
                                                </small>
                                              </CardBody>
                                            </Card>
                                          </div>
                                        );
                                      }}
                                    </Draggable>
                                  );
                                })}
                                {provided.placeholder}
                              </div>
                            );
                          }}
                        </Droppable>
                      </div>
                    </div>
                  );
                })}
            </DragDropContext>
          </div>
        </>
      )}
      {/* <Row>
        <Col>
          {pipelineView != null &&
            pipelineView.map((value, i) => (
              <div className='tasks border'>
                <h5 className='mt-0 task-header header-title'>
                  {value?.pipelineName}
                  <span className='font-size-13'>
                    ({value && value.Notes.length})
                  </span>
                </h5>
                {value && (
                  <TaskContainer
                    id='task-list-one'
                    classNames='task-list-items'
                    tasks={value.Notes}></TaskContainer>
                )}
              </div>
            ))} */}
      {/* <div className='tasks border'>
            <h5 className='mt-0 task-header header-title'>
              In Progress{' '}
              <span className='font-size-13'>
                ({state.inProgessTasks.length})
              </span>
            </h5>
            <TaskContainer
              id='task-list-two'
              classNames='task-list-items'
              tasks={state.inProgessTasks}></TaskContainer>
          </div>

          <div className='tasks border'>
            <h5 className='mt-0 task-header header-title'>
              Review{' '}
              <span className='font-size-13'>({state.reviewTasks.length})</span>
            </h5>
            <TaskContainer
              id='task-list-three'
              classNames='task-list-items'
              tasks={state.reviewTasks}></TaskContainer>
          </div>

          <div className='tasks border'>
            <h5 className='mt-0 task-header header-title'>
              Done{' '}
              <span className='font-size-13'>
                ({state.completedTasks.length})
              </span>
            </h5>
            <TaskContainer
              id='task-list-four'
              classNames='task-list-items'
              tasks={state.completedTasks}></TaskContainer>
          </div> */}
      {/* </Col>
      </Row> */}
    </React.Fragment>
  );
};

export default Board;
