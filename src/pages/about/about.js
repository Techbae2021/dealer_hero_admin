/** @format */

import React, { useState, useEffect } from 'react';
import Script from 'react-load-script';
import axios from 'axios';

const About = () => {
  const [abc, setAbc] = useState(null);

  const handleScriptCreate = () => {
    setAbc({ scriptLoaded: false });
  };

  const handleScriptError = () => {
    setAbc({ scriptError: true });
  };

  const handleScriptLoad = () => {
    setAbc({ scriptLoaded: true });
  };

  // const ABC = () => {
  //   LatpayCheckout.open({
  //     merchantid: 'test_hps',
  //     publickey: 'querygfdsazar',
  //   });

  //   $('#customButton').click(function (e) {
  //     LatpayCheckout.processpayment({
  //       amount: '0.10',
  //       currency: 'AUD',
  //       // status:"0",
  //       // transtoken:"iRc2jMXATo\/UYXCTkjDLbbKb+Ai0eXx\/cahLmePGaYOVLAdbYrHoGngOoeldZo0V",
  //       // datakey:"jkhjhdashgau",
  //       // merchantuserid:"test_latpay",
  //       reference: '3811b2a16309ba9f20d8e51ebce782b8a09962eb',
  //       description: '3811b2a16309ba9f20d8e51ebce782b8a09962eb',

  //       status: function (token) {
  //         if (token == 'success') {
  //           console.log({ token });
  //           // Transtoken generated succesfully, you can proceed to form post after completing any custom work as necessary
  //           return true;
  //         } else if (token == 'failed') {
  //           // if transaction generation failed, merchant can complete any custom work and prevent form post
  //           e.preventDefault();
  //         }
  //       },
  //     });
  //   });
  // };

  return (
    <>
      <div>
        <Script
          url='https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js'
          // onCreate={this.handleScriptCreate.bind(this)}
          // onError={this.handleScriptError.bind(this)}
          onLoad={handleScriptLoad(abc)}
        />
        <button onClick={handleScriptLoad}>ABC</button>
      </div>
    </>
  );
};

export default About;
