/** @format */

import React, { useState, useEffect } from "react";
import { Row, Col, Table } from "reactstrap";
import Instance from "../../Instance";
import { useParams } from "react-router-dom";
//import moment from 'moment';
import { useAlert } from "react-alert";
import CustomInput from "../../components/CustomInput";
import moment from 'moment'
import AuthUser from "../../components/auth/AuthUser";

const CustomerReminder = () => {
  const alert = useAlert();
  const params = useParams();
  const [reminderView, setReminderView] = useState(null);
  const [refresh, setRefresh] = useState(false)
  const [loader, setLoader] = useState(null);
  const [isLoading, setLoading] = useState(null);

  useEffect(() => {
    setLoader(true)
    Instance.get(`/api/admin/customer/reminders/${params.id}`, {
      headers: { authorization: `Bearer ${localStorage.getItem('token$')}` },
    }).then(({ data }) => {
      setLoader(false)
      // console.log('reminderData', data);
      setReminderView(data?.customerReminders);
    }).catch((err) => {
      setLoader(false)
      console.log('err', err);
    });
  }, [refresh]);

  const [reminderDetails, setReminderDetails] = useState({
    message: "",
    time: "",
    day: "",
  });

  const handelChange = (event) => {
    setReminderDetails({
      ...reminderDetails,
      [event.target.id]: event.target.value,
    });

    if (event.target.value === "") {
      setErrors({ ...errors, [event.target.name]: true });
    } else {
      setErrors({ ...errors, [event.target.name]: false });
    }
  };

  const OnSubmit = () => {
    // e.preventDefault();
    setLoading(true)
    let time=""
    if(reminderDetails?.time){
      var [h,m] = reminderDetails?.time.split(":");
     time =  reminderDetails?.time+((h%12+12*(h%12==0))+":"+m, h >= 12 ? 'PM' : 'AM')
    }

    Instance.post(
      `/api/admin/customer/reminder/${params.id}`,
      {
        message: reminderDetails?.message,
        time: time,
        day: reminderDetails?.day,
      },
      {
        headers: { authorization: `Bearer ${localStorage.getItem("token$")}` },
      }
    ).then(({ data }) => {
      // console.log("save", data);
      setLoading(false)
      alert.success("Reminder Added");
      setReminderDetails({
        message: "",
        time: "",
        day: "",
      });
      setRefresh(!refresh)
    }).catch((err) => {
      setLoading(false)
      console.log("Err", err);
    });
  };

  const [errors, setErrors] = useState({
    message: false,
    time: false,
    day: false,
  });

  const [isValid, setisValid] = useState(true);

  const HandleValidation = () => {
    let shouldsubmit = true;
    let tempError = errors;

    for (const [key, value] of Object.entries(reminderDetails)) {
      if (value == "") {
        tempError[key] = true;
        if (shouldsubmit) {
          shouldsubmit = false;
        }
      } else {
        tempError[key] = false;
      }
    }
    // console.log(tempError);
    setisValid(shouldsubmit);
    setErrors(tempError);

    if (shouldsubmit) {
      OnSubmit();
    }
  };

  

  return (
    <AuthUser>
      <Row className="page-title align-items-center">
        <Col sm={4} xl={6}>
          <h4 className="mb-1 mt-0">Customer Reminder</h4>
        </Col>
      </Row>

      <div className="row">
        <div className="col-12">
          <div className="card p-3">
            <h4 className="mb-4"> Add Reminder </h4>
            <div className="form-row">
              <div className="form-group col-md-4">
                <CustomInput
                  label="New Reminder"
                  type="text"
                  error={errors.message}
                  name="message"
                  id="message"
                  value={reminderDetails.message}
                  handleChange={(e) => {
                    handelChange(e);
                  }}
                />
              </div>
              <div className="form-group col-md-4 mt-2">
                <label>Time</label>
                <input type="time"
                  className={`form-control ${errors.time ? "is-invalid" : ""}`}
                  name="time"
                  id="time"
                  value={reminderDetails.time}
                  onChange={handelChange}
                />
                {errors.time && (
                  <div className="invalid-feedback">Required</div>
                )}
              </div>
              <div className="form-group col-md-4 mt-2">
                <label>Day</label>
                <select
                  className={`form-control ${errors.day ? "is-invalid" : ""}`}
                  name="day"
                  id="day"
                  value={reminderDetails.day}
                  onChange={handelChange}
                >
                  <option value="">Select Day</option>
                  <option value="0">Today</option>
                  <option value="3">3 Day</option>
                  <option value="7">7 Day</option>
                  <option value="14">14 Day</option>
                  <option value="21">21 Day</option>
                  <option value="30">30 Day</option>
                </select>
                {errors.day && <div className="invalid-feedback">Required</div>}
              </div>
            </div>
            <div className="form-row">
              <div className="form-group col-md-2">
                <button
                  disabled={isLoading}
                  className="btn btn-primary btn-block"
                  onClick={HandleValidation}
                >
                  {isLoading ? 'Adding...' : 'Add'}
                </button>
              </div>
            </div>
          </div>


          <div className='card p-3'>
            <h4 className='mb-4'> Reminder Details </h4>

            <div>
            {
                loader ? (
                  <>
                    <div className='d-flex justify-content-center'>
                      <div className='spinner-border' role='status'>
                        <span className='sr-only'>Loading...</span>
                      </div>
                    </div>{' '}
                  </>
                ) : (
              <Table hover responsive className='mt-4'>
                <tbody>
                  {reminderView &&
                    reminderView.map((value, i) => (
                      <tr key={i}>
                        <td>{i + 1}</td>
                        <td>{value?.message}</td>
                        <td>{value?.time}</td>
                        <td>{value?.day}</td>
                        <td>
                          {moment(new Date(value?.createdAt)).format(
                            'DD/MM/YYYY'
                          )}
                          <br />
                          {new Date(value?.createdAt).toLocaleTimeString()}
                        </td>
                      </tr>
                    ))}
                </tbody>
              </Table>
                )}
            </div>
          </div>
        </div>
        {/* end Col */}
      </div>
    </AuthUser>
  );
};

export default CustomerReminder;