/** @format */

import React, { useEffect, useState, useContext } from 'react';
import {
  Row,
  Col,
  CardBody,
  Card,
  CustomInput,
  Modal,
  ModalHeader,
} from 'reactstrap';

import { useHistory, useParams } from 'react-router-dom';
import Context from '../context/Context';
import { CUSTOMER_DETAILS } from '../context/action.type';
//import { useAlert } from 'react-alert';
import Instance from '../../Instance';
import { ModalBody } from 'reactstrap';
import Context2 from '../../routes/Context';
import AuthUser from '../../components/auth/AuthUser';

const CustomerDetails = () => {
  const img = {
    borderRadius: '20px',
    border: '1px solid #ddd',
    padding: '5px',
    width: '300px',
    height: '200px',
  };
  let history = useHistory();
  const params = useParams();
  const { employee } = useContext(Context2);

  const [customerView, setCustomerView] = useState(null);
  const [financePersonalView, setFinancePersonalView] = useState(null);
  const [financeDeclarationView, setFinanceDeclarationView] = useState(null);
  const [financeIncomeView, setFinanceIncomeView] = useState(null);
  const [rentalView, setRentalView] = useState(null);
  const [rentalImage, setRentalImage] = useState([]);
  const [vehicleView, setVehicleView] = useState([]);
  const [loader, setLoader] = useState(false);
  const [refresh, seRefresh] = useState(false);
  const { dispatchDetails } = useContext(Context);

 
  const [modalOpen, setModalOpen] = useState(false);
  const [modalView, setModalView] = useState('');
  const modalToggle = (value) => {
    setModalOpen(!modalOpen);
    setModalView(value);
  };

  useEffect(() => {
    setLoader(true);
    Instance.get(`/api/user/cutomer/full/details/${params.id}`, {
      headers: { authorization: `Bearer ${localStorage.getItem('token$')}` },
    })
      .then(({ data }) => {
        // console.log('customerData', data);
        setCustomerView(data?.customerdetails);
        setFinancePersonalView(data?.financialDetails[0]);
        setFinanceDeclarationView(
          data?.financialDetails[0].financeDeclarationDetails[0]
        );
        setFinanceIncomeView(data?.financialDetails[0].financeIncomeDetails[0]);
        setRentalView(data?.rentalDetails[0]);
        setRentalImage(data?.rentalDetails[0].images);
        setVehicleView(data?.vehicleDetails[0]);
        setLoader(false);
      })
      .catch((err) => {
        console.log('err', err);
      });
  }, []);

  return (
    <AuthUser>
      <Row className='page-title align-items-center'>
        <Col sm={8}>
          <h4 className='mb-1 mt-0'>Customer</h4>
        </Col>
      </Row>

      <React.Fragment>
        <div className='row'>
          <div className='col-12'>
            <Card>
              <CardBody>
                <div className='row'>
                  <div className='col-md-6'>
                    <h4>
                      {' '}
                      <strong> Dealer Customer Details</strong>
                    </h4>
                  </div>
                </div>
                <div className='form-row'>
                  <div className='col-3 form-group'>
                    <label className='col-form-label'>
                      {' '}
                      <strong> Customer Code : </strong>
                      {customerView?.customerCode}
                    </label>
                  </div>
                  <div className='col-3 form-group'>
                    <label className='col-form-label'>
                      {' '}
                      <strong> First Name : </strong>
                      {customerView?.firstName}
                    </label>
                  </div>
                  <div className='col-3 form-group'>
                    <label className='col-form-label'>
                      {' '}
                      <strong> Last Name : </strong>
                      {customerView?.lastName}
                    </label>
                  </div>
                  <div className='col-3 form-group'>
                    <label className='col-form-label'>
                      {' '}
                      <strong> Email : </strong>
                      {customerView?.email}
                    </label>
                  </div>
                  <div className='col-3 form-group'>
                    <label className='col-form-label'>
                      {' '}
                      <strong> Country : </strong>
                      {customerView?.country}
                    </label>
                  </div>
                  <div className='col-3 form-group'>
                    <label className='col-form-label'>
                      {' '}
                      <strong> Address : </strong>
                      {customerView?.address}
                    </label>
                  </div>

                  <div className='col-3 form-group'>
                    <label className='col-form-label'>
                      {' '}
                      <strong> State: </strong>
                      {customerView?.state}
                    </label>
                  </div>
                  <div className='col-3 form-group'>
                    <label className='col-form-label'>
                      {' '}
                      <strong> Phone No: </strong>
                      {customerView?.mobile}
                    </label>
                  </div>
                  <div className='col-3 form-group'>
                    <label className='col-form-label'>
                      {' '}
                      <strong> Notes : </strong>
                      {customerView?.notes}
                    </label>
                  </div>
                  <div className='col-3 form-group'>
                    <label className='col-form-label'>
                      {' '}
                      <strong> GST No: </strong>
                      {customerView?.gstNo}
                    </label>
                  </div>
                </div>
              </CardBody>
            </Card>
          </div>
        </div>
      </React.Fragment>
    </AuthUser>
  );
};

export default CustomerDetails;
