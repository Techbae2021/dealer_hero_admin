/** @format */

import React from 'react';
import { Link } from 'react-router-dom';
import { useHistory } from 'react-router-dom';

const TopBar = ({ id }) => {
  let history = useHistory();
  const CustomerMail = () => {
    history.push(`/customer/mail/${id}`);
  };
  const Note = () => {
    history.push(`/customer/note/${id}`);
  };
  const Reminder = () => {
    history.push(`/customer/reminder/${id}`);
  };

  return (
    <>
      <div className='d-flex justify-content-end'>
        <Link onClick={CustomerMail} className='btn btn-success mr-2 Button_Size'>
          Email{' '}
        </Link>
        <Link onClick={Note} className='btn btn-primary mr-2 Button_Size'>
          Note{' '}
        </Link>
        <Link onClick={Reminder} className='btn btn-warning mr-2 Button_Size'>
          Reminder{' '}
        </Link>
      </div>
    </>
  );
};

export default TopBar;
