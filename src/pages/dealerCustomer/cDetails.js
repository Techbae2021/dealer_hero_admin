/** @format */

import React, { useEffect, useState, useContext } from 'react';
import {
  Row,
  Col,
  CardBody,
  Card,
  CustomInput,
  Modal,
  ModalHeader,
  CardImg,
} from 'reactstrap';
import TopBar from './topBar';
import { useHistory, useParams } from 'react-router-dom';
import Context from '../context/Context';
import { CUSTOMER_DETAILS } from '../context/action.type';
//import { useAlert } from 'react-alert';
import Instance from '../../Instance';
import { ModalBody } from 'reactstrap';
import Context2 from '../../routes/Context';
import AuthUser from '../../components/auth/AuthUser';
import config from '../../config';

const CDetails = () => {
  const img = {
    borderRadius: '20px',
    border: '1px solid #ddd',
    padding: '5px',
    width: '300px',
    height: '200px',
  };
  let history = useHistory();
  const params = useParams();
  const { employee } = useContext(Context2);

  const [customerView, setCustomerView] = useState(null);
  const [financePersonalView, setFinancePersonalView] = useState(null);
  const [financeDeclarationView, setFinanceDeclarationView] = useState(null);
  const [financeIncomeView, setFinanceIncomeView] = useState(null);
  const [rentalView, setRentalView] = useState(null);
  const [rentalImage, setRentalImage] = useState([]);
  const [vehicleView, setVehicleView] = useState([]);
  const [loader, setLoader] = useState(false);
  const [refresh, seRefresh] = useState(false);
  const { dispatchDetails } = useContext(Context);

  const [modalOpen, setModalOpen] = useState(false);
  const [modalView, setModalView] = useState('');
  const modalToggle = (value) => {
    setModalOpen(!modalOpen);
    setModalView(value);
  };

  useEffect(() => {
    setLoader(true);
    Instance.get(`/api/admin/customer/${params.id}`, {
      headers: { authorization: `Bearer ${localStorage.getItem('token$')}` },
    })
      .then(({ data }) => {
        // console.log('customerData', data);
        setCustomerView(data?.customerdetails);
        setFinancePersonalView(data?.financialDetails[0]);
        setFinanceDeclarationView(
          data?.financialDetails[0].financeDeclarationDetails[0]
        );
        setFinanceIncomeView(data?.financialDetails[0].financeIncomeDetails[0]);
        setRentalView(data?.rentalDetails[0]);
        setRentalImage(data?.rentalDetails[0].images);
        setVehicleView(data?.vehicleDetails[0]);
        setLoader(false);
      })
      .catch((err) => {
        console.log('err', err);
      });
  }, []);


  // const InventryDetails = () => {
  //   history.push(`/dealer/vehicle`);
  // };
  // const ProposalDetails = () => {
  //   history.push(`/dealer/proposal`);
  // };
  // const RentalDetails = (value) => {
  //   history.push(`/dealer/rental`);
  // };
  // const EmployeeDetails = (value) => {
  //   history.push(`/dealer/employee`);
  // };
  // const FinanceDetails = (value) => {
  //   history.push(`/dealer/finance`);
  // };
  // const SubscriptionDetails = (value) => {
  //   history.push(`/dealer/subscription`);
  // };

  return (
    <AuthUser>
      <Row className='page-title align-items-center'>
        <Col sm={8}>
          <h4 className='mb-1 mt-0'> Customer </h4>
        </Col>
        <Col sm={4}>
          <TopBar id={params.id} />
        </Col>
      </Row>

      <React.Fragment>
        <div className='row'>
        {loader?(
          <div className='col-12'>
             <div class='d-flex justify-content-center'>
                        <div class='spinner-border' role='status'>
                          <span class='sr-only'>Loading...</span>
                        </div>
             </div>{' '}
          </div>
        ):(

          <div className='col-12'>
            <Card>
              <CardBody>
                <div className='row'>
                  <div className='col-md-6'>
                    <h4>
                      {' '}
                      <strong> Customer Details</strong>
                    </h4>
                  </div>

                </div>

                <div className='form-row'>
                  <div className='col-3 form-group'>
                    <label className='col-form-label'>
                      {' '}
                      <strong> Customer Code : </strong>
                      {customerView?.customerCode}
                    </label>
                  </div>
                  <div className='col-3 form-group'>
                    <label className='col-form-label'>
                      {' '}
                      <strong> First Name : </strong>
                      {customerView?.firstName}
                    </label>
                  </div>
                  <div className='col-3 form-group'>
                    <label className='col-form-label'>
                      {' '}
                      <strong> Last Name : </strong>
                      {customerView?.lastName}
                    </label>
                  </div>
                  <div className='col-3 form-group'>
                    <label className='col-form-label'>
                      {' '}
                      <strong> Email : </strong>
                      {customerView?.email}
                    </label>
                  </div>
                  <div className='col-3 form-group'>
                    <label className='col-form-label'>
                      {' '}
                      <strong> Country : </strong>
                      {customerView?.country}
                    </label>
                  </div>
                  <div className='col-3 form-group'>
                    <label className='col-form-label'>
                      {' '}
                      <strong> Address : </strong>
                      {customerView?.address}
                    </label>
                  </div>

                  <div className='col-3 form-group'>
                    <label className='col-form-label'>
                      {' '}
                      <strong> State: </strong>
                      {customerView?.state}
                    </label>
                  </div>
                  <div className='col-3 form-group'>
                    <label className='col-form-label'>
                      {' '}
                      <strong> Phone No: </strong>
                      {customerView?.mobile}
                    </label>
                  </div>
                  <div className='col-3 form-group'>
                    <label className='col-form-label'>
                      {' '}
                      <strong> Notes : </strong>
                      {customerView?.notes}
                    </label>
                  </div>
                  <div className='col-3 form-group'>
                    <label className='col-form-label'>
                      {' '}
                      <strong> GST No: </strong>
                      {customerView?.gstNo}
                    </label>
                  </div>
                </div>
              </CardBody>
            </Card>
            <Card>
              <CardBody>
                <h4>
                  <strong> Customer Vehicle Details</strong>
                </h4>
                <Card>
                  <CardBody>
                    <h4> Vehicle Specifies </h4>
                    <div className='form-row'>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                          {' '}
                          <strong> Make : </strong>
                          {vehicleView?.make}
                        </label>
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                          {' '}
                          <strong> Model : </strong>
                          {vehicleView?.model}
                        </label>
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                          {' '}
                          <strong> Body : </strong>
                          {vehicleView?.vBody}
                        </label>
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                          {' '}
                          <strong> Transmission : </strong>
                          {vehicleView?.trasnmission}
                        </label>
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                          {' '}
                          <strong> Engine : </strong>
                          {vehicleView?.engine}
                        </label>
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                          {' '}
                          <strong> Compiled : </strong>
                          {vehicleView?.compiled}
                        </label>
                      </div>

                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                          {' '}
                          <strong> Varient & Seriez : </strong>
                          {vehicleView?.varientSeries}
                        </label>
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                          {' '}
                          <strong> Built : </strong>
                          {vehicleView?.built}
                        </label>
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                          {' '}
                          <strong> Vin : </strong>
                          {vehicleView?.vin}
                        </label>
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                          {' '}
                          <strong> Youtube URL : </strong>
                          {vehicleView?.youtubeUrl}
                        </label>
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                          {' '}
                          <strong> KMS : </strong>
                          {vehicleView?.kms}
                        </label>
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                          {' '}
                          <strong> Advertising Price : </strong>
                          {vehicleView?.advPrice}
                        </label>
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                          {' '}
                          <strong> Min Price : </strong>
                          {vehicleView?.minPrice}
                        </label>
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label' style={{ display: "flex", alignItems: "center" }}>
                          {vehicleView?.driveWay === true ?
                            <>
                              <span className="fa fa-toggle-on" style={{ fontSize: "17px", padding: "0 5px" }}></span>
                          Drive Away
                          </>
                            :
                            <>
                              <span className="fa fa-toggle-off" style={{ fontSize: "17px", padding: "0 5px" }}></span>
                          Drive Away
                          </>
                          }
                        </label>
                      </div>

                    </div>

                  </CardBody>
                </Card>

                <Card>
                  <CardBody>
                    <h4> Registration Details</h4>
                    <div className="form-row">
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                          {' '}
                          <strong> Registration State : </strong>
                          {vehicleView?.registrationState}
                        </label>
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                          {' '}
                          <strong> Registration No. : </strong>
                          {vehicleView?.registrationNo}
                        </label>
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                          {' '}
                          <strong> Registration Expiry : </strong>
                          {vehicleView?.registrationExpiry}
                        </label>
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                          {' '}
                          <strong> Registration Serial : </strong>
                          {vehicleView?.registrationSeriel}
                        </label>
                      </div>
                    </div>
                  </CardBody>
                </Card>


                <Card>
                  <CardBody>
                    <h4> Location & Status</h4>
                    <div className="form-row">
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                          {' '}
                          <strong> Yard : </strong>
                          {vehicleView?.yard}
                        </label>
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                          {' '}
                          <strong> Form Type : </strong>
                          {vehicleView?.fromType}
                        </label>
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                          {' '}
                          <strong> Condition : </strong>
                          {vehicleView?.condition}
                        </label>
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                          {' '}
                          <strong> Condition Rating : </strong>
                          {vehicleView?.conditionRating}
                        </label>
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                          {' '}
                          <strong> Keyboard : </strong>
                          {vehicleView?.keyboard}
                        </label>
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                          {' '}
                          <strong> Status : </strong>
                          {vehicleView?.status}
                        </label>
                      </div>
                    </div>
                  </CardBody>
                </Card>


                <Card>
                  <CardBody>
                    <h4> Extended Details</h4>
                    <div className="form-row">
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                          {' '}
                          <strong> Extended Color : </strong>
                          {vehicleView?.extendedcolor}
                        </label>
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                          {' '}
                          <strong> Int. Color : </strong>
                          {vehicleView?.intColor}
                        </label>
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                          {' '}
                          <strong> Doors : </strong>
                          {vehicleView?.doors}
                        </label>
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                          {' '}
                          <strong> Seats : </strong>
                          {vehicleView?.seats}
                        </label>
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                          {' '}
                          <strong> Drivetrain : </strong>
                          {vehicleView?.driveTrain}
                        </label>
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                          {' '}
                          <strong> Engine Size : </strong>
                          {vehicleView?.engineSize}
                        </label>
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                          {' '}
                          <strong> Engine Capacity : </strong>
                          {vehicleView?.engineCapicity}
                        </label>
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                          {' '}
                          <strong> Service History : </strong>
                          {vehicleView?.serviceHistory}
                        </label>
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                          {' '}
                          <strong> Country of Manufacture : </strong>
                          {vehicleView?.countryofmanufacture}
                        </label>
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                          {' '}
                          <strong> Redbook Code : </strong>
                          {vehicleView?.redbookCode}
                        </label>
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                          {' '}
                          <strong> Glasses Code : </strong>
                          {vehicleView?.glasscode}
                        </label>
                      </div>
                    </div>
                  </CardBody>
                </Card>

                <Card>
                  <CardBody>
                    <h4> Fuel Details</h4>
                    <div className="form-row">
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                          {' '}
                          <strong> Fuel Type : </strong>
                          {vehicleView?.fuelType}
                        </label>
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                          {' '}
                          <strong> Fuel Saver Rating : </strong>
                          {vehicleView?.fuelsavearRating}
                        </label>
                      </div>
                      <div className='col-3 form-group'>

                        <label className='col-form-label'>
                          {' '}
                          <strong> Safety Rating : </strong>
                          {vehicleView?.safetyRating}
                        </label>
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                          {' '}
                          <strong> Safety List : </strong>
                          {vehicleView?.safetyList}
                        </label>
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                          {' '}
                          <strong> Fuel Economy : </strong>
                          {vehicleView?.fuelEconomy}
                        </label>
                      </div>
                    </div>
                  </CardBody>
                </Card>

                <Card>
                  <CardBody>
                    <h4> Previous Details</h4>
                    <div className="form-row">
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                          {' '}
                          <strong> Previous Price : </strong>
                          {vehicleView?.previousPrice}
                        </label>
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                          {' '}
                          <strong> Prev Registration : </strong>
                          {vehicleView?.prevRegistration}
                        </label>
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                          {' '}
                          <strong> Year First Registered : </strong>
                          {vehicleView?.yearofFirstreg}
                        </label>
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                          {' '}
                          <strong> Suppliers Stock : </strong>
                          {vehicleView?.supplierStock}
                        </label>
                      </div>
                    </div>
                  </CardBody>
                </Card>

                <Card>
                  <CardBody>
                    <h4> Description </h4>
                    <div className="form-row">
                      <div className='col-3 form-group'>
                        <p>
                          {' '}
                          {vehicleView?.description}
                        </p>
                      </div>

                    </div>
                  </CardBody>
                </Card>
              </CardBody>
            </Card>
            <Card>
              <CardBody>
                <h4>
                  <strong> Customer Rental Details</strong>
                </h4>
                <Card>
                  <CardBody>
                    <h4>Check-In Details</h4>
                    <div className='form-row'>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                          {' '}
                          <strong> First Name : </strong>
                          {rentalView?.firstName}
                        </label>
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                          {' '}
                          <strong> Last Name : </strong>
                          {rentalView?.lastName}
                        </label>
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                          {' '}
                          <strong> Email : </strong>
                          {rentalView?.email}
                        </label>
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                          {' '}
                          <strong> Phone : </strong>
                          {rentalView?.phone}
                        </label>
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                          {' '}
                          <strong> Make : </strong>
                          {rentalView?.carMake}
                        </label>
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                          {' '}
                          <strong> Model : </strong>
                          {rentalView?.carModel}
                        </label>
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                          {' '}
                          <strong> Body : </strong>
                          {rentalView?.carBody}
                        </label>
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                          {' '}
                          <strong> Start Date : </strong>
                          {rentalView?.startDate}
                        </label>
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                          {' '}
                          <strong> End Date : </strong>
                          {rentalView?.endDate}
                        </label>
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                          {' '}
                          <strong> Price : </strong>
                          {rentalView?.rentalPrice}
                        </label>
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                          {' '}
                          <strong> Fuel  : </strong>
                          {rentalView?.fuel}
                        </label>
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                          {' '}
                          <strong> KM : </strong>
                          {rentalView?.km}
                        </label>
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                          {' '}
                          <strong> Odo Meter : </strong>
                          {rentalView?.odoMeter}
                        </label>
                      </div>
                    </div>
                  </CardBody>
                </Card>

                <Card>
                  <CardBody>
                    <h4> Description </h4>
                    <div className="form-row">
                      <div className='col-3 form-group'>
                        <p>
                          {' '}
                          {rentalView?.description}
                        </p>
                      </div>

                    </div>
                  </CardBody>
                </Card>

                <Card>
                  <CardBody>
                    <h4> Features</h4>
                    <div className="form-row">
                      <div className='col-3 form-group'>
                        {' '}
                        {rentalView?.childBooster === true ?
                          <h6 style={{ display: "flex", alignItems: "center" }}>
                            <span class="fa fa-toggle-on" style={{ padding: "0 6px", fontSize: "23px" }}> </span>
                            Child Booster
                          </h6>
                          :
                          <h6 style={{ display: "flex", alignItems: "center" }}>
                            <span class="fa fa-toggle-off" style={{ padding: "0 6px", fontSize: "23px" }}> </span>
                          Child Booster
                          </h6>
                        }
                      </div>
                      <div className='col-3 form-group'>
                        {' '}
                        {rentalView?.babySeat === true ?
                          <h6 style={{ display: "flex", alignItems: "center" }}>
                            <span class="fa fa-toggle-on" style={{ padding: "0 6px", fontSize: "23px" }}> </span>
                              Baby Seat
                          </h6>
                          :
                          <h6 style={{ display: "flex", alignItems: "center" }}>
                            <span class="fa fa-toggle-off" style={{ padding: "0 6px", fontSize: "23px" }}> </span>
                              Baby Seat
                          </h6>
                        }
                      </div>
                      <div className='col-3 form-group'>
                        {' '}
                        {rentalView?.gpsUnit === true ?
                          <h6 style={{ display: "flex", alignItems: "center" }}>
                            <span class="fa fa-toggle-on" style={{ padding: "0 6px", fontSize: "23px" }}> </span>
                              GPS Unit
                          </h6>
                          :
                          <h6 style={{ display: "flex", alignItems: "center" }}>
                            <span class="fa fa-toggle-off" style={{ padding: "0 6px", fontSize: "23px" }}> </span>
                              GPS Unit
                          </h6>
                        }
                      </div>
                      <div className='col-3 form-group'>
                        {' '}
                        {rentalView?.headLight === true ?
                          <h6 style={{ display: "flex", alignItems: "center" }}>
                            <span class="fa fa-toggle-on" style={{ padding: "0 6px", fontSize: "23px" }}> </span>
                              Head Light
                          </h6>
                          :
                          <h6 style={{ display: "flex", alignItems: "center" }}>
                            <span class="fa fa-toggle-off" style={{ padding: "0 6px", fontSize: "23px" }}> </span>
                              Head Light
                          </h6>
                        }
                      </div>
                      <div className='col-3 form-group'>
                        {' '}
                        {rentalView?.breakLight === true ?
                          <h6 style={{ display: "flex", alignItems: "center" }}>
                            <span class="fa fa-toggle-on" style={{ padding: "0 6px", fontSize: "23px" }}> </span>
                              Break Light
                          </h6>
                          :
                          <h6 style={{ display: "flex", alignItems: "center" }}>
                            <span class="fa fa-toggle-off" style={{ padding: "0 6px", fontSize: "23px" }}> </span>
                              Break Light
                          </h6>
                        }
                      </div>
                      <div className='col-3 form-group'>
                        {' '}
                        {rentalView?.spareTyre === true ?
                          <h6 style={{ display: "flex", alignItems: "center" }}>
                            <span class="fa fa-toggle-on" style={{ padding: "0 6px", fontSize: "23px" }}> </span>
                              Spare Tyre
                          </h6>
                          :
                          <h6 style={{ display: "flex", alignItems: "center" }}>
                            <span class="fa fa-toggle-off" style={{ padding: "0 6px", fontSize: "23px" }}> </span>
                              Spare Tyre
                          </h6>
                        }
                      </div>
                      <div className='col-3 form-group'>
                        {' '}
                        {rentalView?.lighter === true ?
                          <h6 style={{ display: "flex", alignItems: "center" }}>
                            <span class="fa fa-toggle-on" style={{ padding: "0 6px", fontSize: "23px" }}> </span>
                              Lighter
                          </h6>
                          :
                          <h6 style={{ display: "flex", alignItems: "center" }}>
                            <span class="fa fa-toggle-off" style={{ padding: "0 6px", fontSize: "23px" }}> </span>
                              Lighter
                          </h6>
                        }
                      </div>
                      <div className='col-3 form-group'>
                        {' '}
                        {rentalView?.jack === true ?
                          <h6 style={{ display: "flex", alignItems: "center" }}>
                            <span class="fa fa-toggle-on" style={{ padding: "0 6px", fontSize: "23px" }}> </span>
                              Jack
                          </h6>
                          :
                          <h6 style={{ display: "flex", alignItems: "center" }}>
                            <span class="fa fa-toggle-off" style={{ padding: "0 6px", fontSize: "23px" }}> </span>
                              Jack
                          </h6>
                        }
                      </div>
                      <div className='col-3 form-group'>
                        {' '}
                        {rentalView?.toolkit === true ?
                          <h6 style={{ display: "flex", alignItems: "center" }}>
                            <span class="fa fa-toggle-on" style={{ padding: "0 6px", fontSize: "23px" }}> </span>
                              Toolkit
                          </h6>
                          :
                          <h6 style={{ display: "flex", alignItems: "center" }}>
                            <span class="fa fa-toggle-off" style={{ padding: "0 6px", fontSize: "23px" }}> </span>
                              Toolkit
                          </h6>
                        }
                      </div>
                      <div className='col-3 form-group'>
                        {' '}
                        {rentalView?.floorMates === true ?
                          <h6 style={{ display: "flex", alignItems: "center" }}>
                            <span class="fa fa-toggle-on" style={{ padding: "0 6px", fontSize: "23px" }}> </span>
                              Floor Mates
                          </h6>
                          :
                          <h6 style={{ display: "flex", alignItems: "center" }}>
                            <span class="fa fa-toggle-off" style={{ padding: "0 6px", fontSize: "23px" }}> </span>
                              Floor Mates
                          </h6>
                        }
                      </div>
                      <div className='col-3 form-group'>
                        {' '}
                        {rentalView?.wiper === true ?
                          <h6 style={{ display: "flex", alignItems: "center" }}>
                            <span class="fa fa-toggle-on" style={{ padding: "0 6px", fontSize: "23px" }}> </span>
                              Wiper
                          </h6>
                          :
                          <h6 style={{ display: "flex", alignItems: "center" }}>
                            <span class="fa fa-toggle-off" style={{ padding: "0 6px", fontSize: "23px" }}> </span>
                              Wiper
                          </h6>
                        }
                      </div>
                      <div className='col-3 form-group'>
                        {' '}
                        {rentalView?.ac === true ?
                          <h6 style={{ display: "flex", alignItems: "center" }}>
                            <span class="fa fa-toggle-on" style={{ padding: "0 6px", fontSize: "23px" }}> </span>
                              AC
                          </h6>
                          :
                          <h6 style={{ display: "flex", alignItems: "center" }}>
                            <span class="fa fa-toggle-off" style={{ padding: "0 6px", fontSize: "23px" }}> </span>
                              AC
                          </h6>
                        }
                      </div>
                    </div>
                  </CardBody>
                </Card>

                <Card>
                  <CardBody>
                    <h4> Car Image</h4>
                    <div className="form-row">
                    {
                      rentalView?.images && rentalView?.images.map((img) => (
                            <Col xs="4">
                              <Card>
                                <CardBody>
                                  <CardImg width="100%" src={`${config.baseImgUrl}/rental/${img}`} alt="car image" />
                                </CardBody>
                              </Card>
                            </Col>
                          ))
                        }
                    </div>
                  </CardBody>
                </Card>

                <Card>
                  <CardBody>
                    <h4> Damage Car</h4>
                    <div className="form-row">
                      <div className='col-6 form-group'>
                        <label className='col-form-label' style={{ display: "flex", justifyContent: "center" }}>
                          Damage Car Pic 1
                        </label>
                        <div className="p-4" style={{ display: "flex", justifyContent: "center" }}>
                          <Card>
                            <CardBody>
                              <img src={`${config.baseImgUrl}/rental/${rentalView?.damageCar1}`} alt="img" style={{  width: "430px", height: "300px" }} />
                            </CardBody>
                          </Card>
                        </div>
                      </div>
                      <div className='col-6 form-group'>
                        <label className='col-form-label' style={{ display: "flex", justifyContent: "center" }}>
                          Damage Car Pic 2
                        </label>
                        <div className="p-4" style={{ display: "flex", justifyContent: "center" }}>
                          <Card>
                            <CardBody>
                              <img src={`${config.baseImgUrl}/rental/${rentalView?.damageCar2}`} alt="img" style={{  width: "430px", height: "300px" }} />
                            </CardBody>
                          </Card>
                        </div>
                      </div>
                    </div>
                  </CardBody>
                </Card>

                <Card>
                  <CardBody>
                    <h4> Signature</h4>
                    <div className="form-row">
                      <div className='col-6 form-group'>
                        <label className='col-form-label' style={{ display: "flex", justifyContent: "center" }}>
                        Customer Sign
                        </label>
                        <div className="p-4" style={{ display: "flex", justifyContent: "center" }}>
                          <Card>
                            <CardBody>
                              <img src={`${config.baseImgUrl}/rental/${rentalView?.customerSign}`} alt="img" style={{  width: "430px", height: "300px" }} />
                            </CardBody>
                          </Card>
                        </div>
                      </div>
                      <div className='col-6 form-group'>
                        <label className='col-form-label' style={{ display: "flex", justifyContent: "center" }}>
                        Authority Sign
                        </label>
                        <div className="p-4" style={{ display: "flex", justifyContent: "center" }}>
                          <Card>
                            <CardBody>
                              <img src={`${config.baseImgUrl}/rental/${rentalView?.authoritySign}`} alt="img" style={{  width: "430px", height: "300px" }} />
                            </CardBody>
                          </Card>
                        </div>
                      </div>
                    </div>
                  </CardBody>
                </Card>
              </CardBody>
            </Card>
            <Card>
              <CardBody>
                <h4>
                  <strong> Customer Finance Details</strong>
                </h4>
                <Card>
                  <CardBody>
                    <h4>Finance Personal</h4>
                    <div className='form-row'>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                          {' '}
                          <strong> First Name : </strong>
                          {financePersonalView?.firstName}
                        </label>
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                          {' '}
                          <strong> Last Name : </strong>
                          {financePersonalView?.lastName}
                        </label>
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                          {' '}
                          <strong> Email : </strong>
                          {financePersonalView?.email}
                        </label>
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                          {' '}
                          <strong> Phone : </strong>
                          {financePersonalView?.phone}
                        </label>
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                          {' '}
                          <strong> Address : </strong>
                          {financePersonalView?.address}
                        </label>
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                          {' '}
                          <strong> City : </strong>
                          {financePersonalView?.city}
                        </label>
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                          {' '}
                          <strong> State : </strong>
                          {financePersonalView?.state}
                        </label>
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                          {' '}
                          <strong> Postal Code : </strong>
                          {financePersonalView?.postalCode}
                        </label>
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                          {' '}
                          <strong> Marital Status : </strong>
                          {financePersonalView?.maratilStatus}
                        </label>
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                          {' '}
                          <strong> Residency : </strong>
                          {financePersonalView?.residency}
                        </label>
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                          {' '}
                          <strong> Dependents : </strong>
                          {financePersonalView?.depandents}
                        </label>
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                          {' '}
                          <strong> LicenseNo : </strong>
                          {financePersonalView?.licenseNo}
                        </label>
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                          {' '}
                          <strong> License Expiry : </strong>
                          {financePersonalView?.licenseExpiery}
                        </label>
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                          {' '}
                          <strong> Applying : </strong>
                          {financePersonalView?.applying}
                        </label>
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                          {' '}
                          <strong> Dob : </strong>
                          {financePersonalView?.dob}
                        </label>
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                          {' '}
                          <strong> Proposal Price : </strong>
                          {financePersonalView?.proposalPrice}
                        </label>
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                          {' '}
                          <strong> Deposit : </strong>
                          {financePersonalView?.deposit}
                        </label>
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                          {' '}
                          <strong> Finance Amount : </strong>
                          {financePersonalView?.financeAmount}
                        </label>
                      </div>
                    </div>
                  </CardBody>
                </Card>

                <Card>
                  <CardBody>
                    <h4>Finance Income</h4>
                    <div className='form-row'>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                          {' '}
                          <strong> Work : </strong>
                          {financeIncomeView?.work}
                        </label>
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                          {' '}
                          <strong> Employee FirstName : </strong>
                          {financeIncomeView?.empFirstName}
                        </label>
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                          {' '}
                          <strong> Employee LastName : </strong>
                          {financeIncomeView?.empLastName}
                        </label>
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                          {' '}
                          <strong> Employee PhoneNo : </strong>
                          {financeIncomeView?.empPhone}
                        </label>
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                          {' '}
                          <strong> Employee Year : </strong>
                          {financeIncomeView?.empYear}
                        </label>
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                          {' '}
                          <strong> Employee Occupation : </strong>
                          {financeIncomeView?.empOccupation}
                        </label>
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                          {' '}
                          <strong> SalaryType : </strong>
                          {financeIncomeView?.salaryType}
                        </label>
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                          {' '}
                          <strong> IncomeType : </strong>
                          {financeIncomeView?.incomeType}
                        </label>
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                          {' '}
                          <strong> IncomeBeforeTax : </strong>
                          {financeIncomeView?.incomeBeforeTax}
                        </label>
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                          {' '}
                          <strong> IncomeAmount : </strong>
                          {financeIncomeView?.incomeAmount}
                        </label>
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                          {' '}
                          <strong> CreditCard : </strong>
                          {financeIncomeView?.creditCard}
                        </label>
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                          {' '}
                          <strong> ExpanseType : </strong>
                          {financeIncomeView?.expanseType}
                        </label>
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                          {' '}
                          <strong> ExpanseBeforeTax : </strong>
                          {financeIncomeView?.expanseBeforeTax}
                        </label>
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                          {' '}
                          <strong> Expanse : </strong>
                          {financeIncomeView?.expanse}
                        </label>
                      </div>
                      <div className='col-3 form-group'>
                        <label className='col-form-label'>
                          {' '}
                          <strong> Comment : </strong>
                          {financeIncomeView?.comment}
                        </label>
                      </div>
                    </div>
                  </CardBody>
                </Card>

                <Card>
                  <CardBody>
                    <h4> Finance Declarations</h4>
                    <div className="form-row">
                      <div className='col-4 form-group'>
                        <label className='col-form-label'>
                          {' '}
                          <strong> LicenceFront</strong>
                          <br />
                          <img src={`${config.baseImgUrl}/finance/${financeDeclarationView?.licenseFront}`} alt="img" style={{ borderRadius: "20px", border: "1px solid rgb(221, 221, 221)", padding: "5px", width: "300px", height: "200px" }} />
                        </label>
                      </div>
                      <div className='col-4 form-group'>
                        <label className='col-form-label'>
                          {' '}
                          <strong> LicenseBack</strong>
                          <br />
                          <img src={`${config.baseImgUrl}/finance/${financeDeclarationView?.licenseBack}`} alt="img" style={{ borderRadius: "20px", border: "1px solid rgb(221, 221, 221)", padding: "5px", width: "300px", height: "200px" }} />
                        </label>
                      </div>
                      <div className='col-4 form-group'>
                        <label className='col-form-label'>
                          {' '}
                          <strong> Passport</strong>
                          <br />
                          <img src={`${config.baseImgUrl}/finance/${financeDeclarationView?.passport}`} alt="img" style={{ borderRadius: "20px", border: "1px solid rgb(221, 221, 221)", padding: "5px", width: "300px", height: "200px" }} />
                        </label>
                      </div>
                      <div className='col-4 form-group'>
                        <label className='col-form-label'>
                          {' '}
                          <strong> Medicare</strong>
                          <br />
                          <img src={`${config.baseImgUrl}/finance/${financeDeclarationView?.medicare}`} alt="img" style={{ borderRadius: "20px", border: "1px solid rgb(221, 221, 221)", padding: "5px", width: "300px", height: "200px" }} />
                        </label>
                      </div>
                      <div className='col-4 form-group'>
                        <label className='col-form-label'>
                          {' '}
                          <strong>BankStatement</strong>
                          <br />
                          <img src={`${config.baseImgUrl}/finance/${financeDeclarationView?.bankStatement}`} alt="img" style={{ borderRadius: "20px", border: "1px solid rgb(221, 221, 221)", padding: "5px", width: "300px", height: "200px" }} />
                        </label>
                      </div>
                      <div className='col-4 form-group'>
                        <label className='col-form-label'>
                          {' '}
                          <strong> OtherImage</strong>
                          <br />
                          <img src={`${config.baseImgUrl}/finance/${financeDeclarationView?.otherImage}`} alt="img" style={{ borderRadius: "20px", border: "1px solid rgb(221, 221, 221)", padding: "5px", width: "300px", height: "200px" }} />
                        </label>
                      </div>
                    </div>
                  </CardBody>
                </Card>

              </CardBody>
            </Card>
          </div>
        )}
        </div>

      </React.Fragment>
    </AuthUser>
  );
};

export default CDetails;
