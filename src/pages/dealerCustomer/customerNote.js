/** @format */

import React, { useEffect, useState, useContext } from 'react';
import { Row, Col, Table } from 'reactstrap';
//import Leftsidebar from './fNote';
import Instance from '../../Instance';
import { useHistory, useParams } from 'react-router-dom';
import { useAlert } from 'react-alert';
import moment from 'moment';
import Context from '../context/Context';
import { CUSTOMER_NOTE } from '../context/action.type';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as Yup from 'yup';
import AuthUser from '../../components/auth/AuthUser';

const CustomerNote = () => {
  const params = useParams();
  const history = useHistory();
  const alert = useAlert();
  const [noteView, setNoteView] = useState(null);
  const [isLoading, setLoading] = useState(false);
  const [loader, setLoader] = useState(false);
  const [refresh, setRefresh] = useState(null);
  const { dispatchDetails } = useContext(Context);
  const [noteDetails, setNoteDetails] = useState({
    note: '',
  });

  useEffect(() => {
    setLoader(true)
    Instance.get(`/api/admin/customer/notes/${params.id}`, {
      headers: { authorization: `Bearer ${localStorage.getItem('token$')}` },
    })
      .then(({ data }) => {
        setLoader(false)
        // console.log('noteData', data);
        setNoteView(data?.Notes);
      })
      .catch((err) => {
        setLoader(false)
        console.log('err', err);
      });
  }, [refresh]);

  const handelChange = (event) => {
    setNoteDetails({
      ...noteDetails,
      [event.target.id]: event.target.value,
    });

    if (event.target.value === '') {
      setErrors({ ...errors, [event.target.name]: true });
    } else {
      setErrors({ ...errors, [event.target.name]: false });
    }
  };

  const OnSubmit = async () => {
    // e.preventDefault();
    setLoading(true)
    Instance.post(
      `/api/admin/customer/note/${params.id}`,
      {
        note: noteDetails.note,
      },
      {
        headers: { authorization: `Bearer ${localStorage.getItem('token$')}` },
      }
    )
      .then(({ data }) => {
        setLoading(false)
        // console.log('save', data);
        alert.success('Note Added');
        setNoteDetails({note:''})
        setRefresh(!refresh);
        //history.push(`/inventory/stock`);
      })
      .catch((err) => {
        setLoading(false)
        console.log('Err', err);
      });
  };

  const CustomerNoteDelete = (value) => {
    setLoader(true)
    Instance.delete(`/api/admin/customer/note/${value._id}`, {
      headers: { authorization: `Bearer ${localStorage.getItem('token$')}` },
    }).then(() => {
    setLoader(false)
        // console.log('Success');
        alert.success('Note Deleted');
        setRefresh(!refresh);
      })
      .catch((err) => {
    setLoader(false)
        console.log('Error', err);
      });
  };

  const CustomerNoteEdit = (value) => {
    dispatchDetails({
      type: CUSTOMER_NOTE,
      payload: value,
    });
    history.push(`/customer/note/edit/${value._id}`);
  };

  const [errors, setErrors] = useState({
    note: false,
  });

  const HandleValidation = () => {
    if (noteDetails.note === '') {
      setErrors({ note: true });
    } else {
      OnSubmit();
    }
  };

  return (
    <AuthUser>
      <Row className='page-title align-items-center'>
        <Col sm={4} xl={6}>
          <h4 className='mb-1 mt-0'>Customer Note</h4>
        </Col>
      </Row>

      <div className='row'>
        <div className='col-12'>
          <div className='card p-3'>
            <h4 className='mb-4'> Add Note </h4>

            <div className='form-row'>
              <div className='form-group col-md-4'>
                <label>New Note</label>
                <input
                  type='text'
                  // className='form-control'
                  placeholder='Note'
                  id='note'
                  value={noteDetails.note}
                  onChange={handelChange}
                  name='note'
                  className={`form-control ${errors.note ? 'is-invalid' : ''}`}
                />
                {errors?.note && (
                  <div className='invalid-feedback'>Required</div>
                )}
              </div>
            </div>
            <div className='form-row'>
              <div className='form-group col-md-2'>
                <button
                 disabled={isLoading}
                  className='btn btn-primary btn-block'
                  onClick={HandleValidation}>
                    {isLoading?'Adding...':'Add'}
                </button>
              </div>
            </div>
          </div>
          <div className='card p-3'>
            <h4 className='mb-4'> Note Details </h4>

            <div>
            {
                loader ? (
                  <>
                    <div className='d-flex justify-content-center'>
                      <div className='spinner-border' role='status'>
                        <span className='sr-only'>Loading...</span>
                      </div>
                    </div>{' '}
                  </>
                ) : (
              <Table hover responsive className='mt-4'>
                <tbody>
                  {noteView &&
                    noteView.map((value, i) => (
                      <tr key={i}>
                        <td>{i + 1}</td>
                        <td>{value?.note}</td>
                        <td>
                          {moment(new Date(value?.createdAt)).format(
                            'DD/MM/YYYY'
                          )}
                          <br />
                          {new Date(value?.createdAt).toLocaleTimeString()}
                        </td>

                        <td>
                          <svg
                            xmlns='http://www.w3.org/2000/svg'
                            width='16'
                            height='16'
                            fill='currentColor'
                            class='bi bi-pencil-square'
                            viewBox='0 0 16 16'
                            onClick={() => CustomerNoteEdit(value)}>
                            <path d='M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456l-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z' />
                            <path
                              fill-rule='evenodd'
                              d='M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z'
                            />
                          </svg>

                          <i
                            className='uil uil-trash-alt ml-2'
                            onClick={() => CustomerNoteDelete(value)}
                          />

                          {/* <span className='badge badge-soft-warning py-1'>
                                  Pending
                                </span> */}
                        </td>
                      </tr>
                    ))}
                </tbody>
              </Table>
            
                )}
            </div>
          </div>
        </div>{' '}
        {/* end Col */}
      </div>
    </AuthUser>
  );
};

export default CustomerNote;
