/** @format */

import React, { useState, useContext, useEffect } from 'react';
import { Row, Col, CardBody, Card, Table, ModalHeader, ModalBody, Modal, CardImg } from 'reactstrap';
// import Leftsidebar from './leftsidebar';
import Instance from '../../Instance';
import { useHistory, useParams } from 'react-router-dom';
import Context from '../context/Context';
import { useAlert } from 'react-alert';
import axios from 'axios';
import moment from 'moment';
import AuthUser from '../../components/auth/AuthUser';
import config from "../../config"

const CustomerMail = () => {
  let history = useHistory();
  const alert = useAlert();
  const params = useParams()
  const [mailDetails, setMailDetails] = useState({
    subject: '',
    message: '',
    attactments: []
  });
  const [mails, setMails] = useState([])
  const [loader, setLoader] = useState(false)
  const [refresh, setRefresh] = useState(false)
    const [isLoading,setLoading] = useState(false)

    const [errors, setErrors] = useState({
      subject: false,
      message: false
    });

  useEffect(() => {
    setLoader(true);
    Instance.get(`/api/admin/customer/mails/${params.id}`, {
      headers: { authorization: `Bearer ${localStorage.getItem('token$')}` },
    }).then(({ data }) => {
      // console.log('customerData', data);
      setMails(data?.mails)
      setLoader(false);
    }).catch((err) => {
        setLoader(false);
        console.log('err', err);
      });
  }, [refresh])

  const handelChange = (event) => {
    if (event.target.id === 'attactments') {
      setMailDetails({
        ...mailDetails,
        [event.target.id]: event.target.files,
      });
    } else {
      setMailDetails({
        ...mailDetails,
        [event.target.id]: event.target.value,
      });
      if (event.target.value === '') {
        setErrors({ ...errors, [event.target.id]: true });
      } else {
        setErrors({ ...errors, [event.target.id]: false });
      }
    }
  };

  const HandleValidation = (e) => {
    e.preventDefault();
    let shouldsubmit = true;
    let tempError = errors;

    for (const [key, value] of Object.entries(mailDetails)) {
      if(key!=='attactments'){
      if (value === '') {
        // if (key != 'search') {
          tempError[key] = true;
          if (shouldsubmit) {
            shouldsubmit = false;
          }
        // }
        // else {
      //   tempError[key] = false;
      // }
      }
     }
    }
    // console.log(tempError);
    setErrors(tempError);

    if (shouldsubmit) {
      OnSubmit(e);
    }
  };

  const OnSubmit = (e) => {
    e.preventDefault();
    setLoading(true)
    var data = new FormData();
    data.append('subject', mailDetails.subject)
    data.append('message', mailDetails.message)

    if (mailDetails.attactments.length > 0) {
      [...mailDetails.attactments].map((file) => data.append("attachments", file))
    }

    var config = {
      method: 'post',
      url: `${process.env.REACT_APP_BACKEND_URL}api/admin/customer/sendMail/${params.id}`,
      headers: {
        authorization: `Bearer ${localStorage.getItem('token$')}`,
        'Content-Type': 'multipart/form-data'
      },
      data: data
    };
    axios(config).then(({ data }) => {
      // console.log('save', data);
      setLoading(false)
      alert.success('Mail Sent');
      setMailDetails({ subject: '', message: '', attactments: [] })
      document.getElementById('attactments').value = ''
      setRefresh(!refresh)
    }).catch((err) => {
      setLoading(false)
      console.log("Err", err);
    })
  };

  const [modalOpen, setModalOpen] = useState(false);
  const [modalView, setModalView] = useState('');

  const modalToggle = (value) => {
    setModalOpen(!modalOpen);
    setModalView(value);
  };


  return (
    <AuthUser>
      <Row className='page-title align-items-center'>
        <Col sm={4} xl={6}>
          <h4 className='mb-1 mt-0'>Customer Mail</h4>
        </Col>
      </Row>

      <div className='row'>

        <div className='col-10'>
          <div className='card p-3'>
            <h4 className='mb-4'> Send Mail </h4>
            <div className='form-row'>
              <div className='form-group col-md-8'>
                <label>Subject</label>
                <input
                  type='text'
                  className={`form-control ${errors.subject ? 'is-invalid' : ''}`}
                  placeholder='Subject'
                  id='subject'
                  value={mailDetails.subject}
                  onChange={handelChange}></input>
                   {errors.subject ? (
                  <div className='invalid-feedback'>Enter Your Subject</div>
                 ):null}
              </div>
            </div>
            <div className='form-row'>
              <div className='form-group col-md-8'>
                <label>Messege</label>
                <textarea
                  className={`form-control ${errors.message ? 'is-invalid' : ''}`}
                  placeholder='Messege'
                  rows='6'
                  id='message'
                  value={mailDetails.message}
                  onChange={handelChange}></textarea>
                   {errors.message ? (
                  <div className='invalid-feedback'>Enter Your Message</div>
                 ):null}
              </div>
            </div>
            <div className="form-row">
              <div className="form-group col-md-8">
                <label>Attactments</label>
                <input
                  type="file"
                  onChange={handelChange}
                  className={`form-control`}
                  id="attactments"
                  name="attactments"
                  multiple
                />
              </div>
            </div>

            <div className='form-row'>
              <div className='form-group col-md-2'>
              <button
                 disabled={isLoading}
                  className='btn btn-primary btn-block'
                  onClick={(e) => HandleValidation(e)}>
                    {isLoading?'Sending...':'Send'}
                </button>
              </div>
            </div>
          </div>
        </div>{' '}
        {/* end Col */}
      </div>

      <Row>
        <Col sm="12">
          <Card>
            <CardBody>
              <h4>All Mails</h4>
              {loader ? (
                <div class='d-flex justify-content-center'>
                  <div class='spinner-border' role='status'>
                    <span class='sr-only'>Loading...</span>
                  </div>
                </div>
              ) : (
                <Table hover responsive className='mt-4'>
                  <tbody>
                    {mails &&
                      mails.map((value, i) => (
                        <tr key={i}>
                          <td>{i + 1}</td>
                          <td>
                            <strong>Subject: </strong> {value?.subject}
                            <br />
                            <strong>Message: </strong> {value?.message}
                          </td>
                          <td>
                            {moment(new Date(value.createdAt)).format(
                              'DD/MM/YYYY'
                            )}
                              ,{new Date(value.createdAt).toLocaleTimeString()}
                          </td>

                          <td>
                            <div className='button-list'>
                              <button
                                onClick={() => modalToggle(value)}
                                className='btn btn-success  btn-sm Button_Size'>
                                View{' '}
                              </button>
                            </div>
                          </td>
                        </tr>
                      ))}
                  </tbody>
                </Table>
              )}
            </CardBody>
          </Card>
        </Col>
      </Row>
      <Modal isOpen={modalOpen} toggle={modalToggle} >
        <ModalHeader toggle={modalToggle}>Mail Details</ModalHeader>
        <ModalBody>
          <Card>
            <CardBody>
              <Row>

                <Col sm="12">
                  <label className='col-form-label'>
                    <strong> Mail To: </strong>
                  </label>{' '}
                  <span>{modalView?.to}</span>
                </Col>
              </Row>
              <Row>
                <Col sm="12">
                  <label className='col-form-label'>
                    <strong> Subject: </strong>
                  </label>{' '}
                  <span>{modalView?.subject}</span>
                </Col>
              </Row>
              <Row>
                <Col sm="12">
                  <label className='col-form-label'>
                    <strong> Message: </strong>
                  </label>{' '}
                  <span>{modalView?.message}</span>
                </Col>
              </Row>
              {
                (modalView?.attachments && modalView?.attachments.length>0) && (
                  <Card>
                    <CardBody>
                      <h4>Attachments:</h4>
                      <Row>
                        {
                          modalView?.attachments?.map((img) => (
                            <Col xs="4">
                              <Card>
                                <CardBody>
                                  <CardImg width="100%" style={{ height: 190 }} src={`${config?.baseImgUrl}/mailAttachments/${img.filename}`} alt="image" />
                                </CardBody>
                              </Card>
                            </Col>
                          ))
                        }
                      </Row>
                    </CardBody>
                  </Card>
                )
              }

            </CardBody>
          </Card>
        </ModalBody>
      </Modal>
    </AuthUser>
  );
};

export default CustomerMail;
