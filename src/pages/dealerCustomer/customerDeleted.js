import React, { useState, useEffect } from 'react';
import { Row, Col, Table} from 'reactstrap';
import CSidebar from './cSidebar';
import Instance from '../../Instance';
import moment from 'moment';
import AuthUser from '../../components/auth/AuthUser';

const CDeleted = () => {
  const [deletedCView, setDeletedCView] = useState(null);
  const [loader, setLoader] = useState(false);
  const [count, setCount] = useState(1);
  const [page, setPage] = useState(null);
  const [totalCount, settotalCount] = useState(0);

  useEffect(() => {
    setLoader(true);
    Instance.post(
      `/api/admin/deletedcustomers`,
      {
        page: count,
      },
      {
        headers: { authorization: `Bearer ${localStorage.getItem('token$')}` },
      }
    )
      .then(({ data }) => {
        // console.log('deletedCustomerData', data);
        let temp = data?.results?.DeletedCustomers;
        setPage(data?.results);
        setDeletedCView(temp);
        setLoader(false);
        settotalCount(data?.results?.totalDeletedCustomersdb)
      })
      .catch((err) => {
        console.log('err', err);
      });
  }, [count]);

  const NextPage = () => {
    setCount(count + 1);
  };
  const PrevPage = () => {
    setCount(count - 1);
  };


  return (
    <AuthUser>
      <Row className='page-title align-items-center'>
        <Col sm={4} xl={6}>
          <h4 className='mb-1 mt-0'>Customer</h4>
        </Col>
      </Row>

      <div className='row'>
        <div className='col-2 Lead_Board'>
          <div className='email-container bg-transparent'>
            {/* Left sidebar */}
            <CSidebar deletedCount={totalCount}/>
            {/* End Left sidebar */}
          </div>
        </div>
        <div className='col-10'>
          <div className='card'>
            <div className='card p-3'>
              <div className='row'>
                <div className='col-6'>
                  <h4>All Deleted Customer </h4>
                </div>
                <div className='col-md-6 text-right'>
                  <div className='d-inline-block align-middle float-lg-right'>
                    <div className='row'>
                      <div className='col-8 align-self-center' style={{ whiteSpace:"nowrap"}}>
                        Showing {page?.startOfItem} - {page?.endOfItem} of{' '}
                        {totalCount}
                      </div>
                      {/* end col*/}
                      <div className='col-4'>
                        <div className='btn-group float-right'>
                          <button
                            type='button'
                            className='btn btn-white btn-sm'
                            onClick={PrevPage}
                            style={{
                              display: page?.Previous === false ? 'none' : '',
                            }}>
                            <i
                              className='uil uil-angle-left'
                             
                            />
                          </button>
                          <button
                            type='button'
                            className='btn btn-primary btn-sm'
                            onClick={NextPage}
                            style={{
                              display: page?.Next === false ? 'none' : '',
                            }}>
                            <i
                              className='uil uil-angle-right'
                             
                            />
                          </button>
                        </div>
                      </div>{' '}
                      {/* end col*/}
                    </div>
                  </div>
                </div>
              </div>
              <div>
                {loader ? (
                  <>
                    <div class='d-flex justify-content-center'>
                      <div class='spinner-border' role='status'>
                        <span class='sr-only'>Loading...</span>
                      </div>
                    </div>{' '}
                  </>
                ) : (
                  <>
                    <Table hover responsive className='mt-4'>
                      <tbody>
                        {deletedCView &&
                          deletedCView.map((value, i) => (
                            <tr key={i}>
                              <td>{i + 1}</td>

                              <td>
                                {value.firstName}
                                <br />
                                {value.lastName}
                                <br />
                                {value.email}
                                <br />
                                {value.country}
                                <br />
                                {value.address}
                                <br />
                                {value.state}
                                <br />
                                {value.mobile}
                                <br />
                                {value.notes}
                                <br />
                                {value.gstNo}
                                <br />
                              </td>
                              <td>
                                {moment(new Date(value.createdAt)).format(
                                  'DD/MM/YYYY'
                                )}
                                ,
                                {new Date(value.createdAt).toLocaleTimeString()}
                              </td>
                            </tr>
                          ))}
                      </tbody>
                    </Table>
                  </>
                )}
              </div>
            </div>
          </div>
        </div>{' '}
        {/* end Col */}
      </div>
      {/* End row */}
    </AuthUser>
  );
};

export default CDeleted;
