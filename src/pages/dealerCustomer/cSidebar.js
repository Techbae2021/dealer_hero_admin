import React from 'react';
import { Link } from 'react-router-dom';
import Instance from '../../Instance';

const CSidebar = () => {
  const [count, setCount] = React.useState({ customers: 0, deletedCustomers: 0 })
  React.useEffect(() => {
    Instance.get(`/api/admin/customerCount`, {
      headers: { authorization: `Bearer ${localStorage.getItem('token$')}` },
    }).then(({ data }) => {
      setCount({
        customers: data?.customers,
        deletedCustomers: data?.deletedCustomers
      })
      // console.log('status', data)
    }).catch((e) => {
      console.log('err', e)
    })
  }, [])

  return (
    <>
      <div>
        {/* Left sidebar */}
        <div className='p-3 card'>
          <div className='mail-list '>
            <Link to='/customer' className='list-group-item border-0'>
              <i className='uil uil-receipt-alt font-size-15 mr-1' />
              Customer
              <span className='badge badge-danger float-right ml-2 mt-1'>
                {count?.customers}
              </span>
            </Link>

            <Link to='/customer/deleted' className='list-group-item border-0'>
              <i className='uil uil-receipt-alt font-size-15 mr-1' /> Deleted
              <span className='badge badge-danger float-right ml-2 mt-1'>
                {count?.deletedCustomers}
              </span>
            </Link>
          </div>
        </div>

        {/* End Left sidebar */}
      </div>
    </>
  );
};

export default CSidebar;
