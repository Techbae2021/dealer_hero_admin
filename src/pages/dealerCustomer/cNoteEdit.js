/** @format */

import React, { useState, useContext } from 'react';
import { Row, Col, Table } from 'reactstrap';
import Instance from '../../Instance';
import Context from '../context/Context';
import { useHistory, Redirect } from 'react-router-dom';
import { useAlert } from 'react-alert';
import AuthUser from '../../components/auth/AuthUser';

const CNoteEdit = () => {
  let history = useHistory();
  const { details } = useContext(Context);
  const alert = useAlert();
  const [isLoading,setLoading] = useState(false)
  const [noteDetails, setNoteDetails] = useState({
    note: details != null ? details.note : '',
  });


  const [errors, setErrors] = useState({
    note: false,
  });

  const HandleValidation = () => {
    if (noteDetails.note === '') {
      setErrors({ note: true });
    } else {
      OnUpdated();
    }
  };

  const handelChange = (event) => {
    setNoteDetails({
      ...noteDetails,
      [event.target.id]: event.target.value,
    });
    if (event.target.value === '') {
      setErrors({ ...errors, [event.target.name]: true });
    } else {
      setErrors({ ...errors, [event.target.name]: false });
    }
  };

  const OnUpdated = () => {
    setLoading(true)
    Instance.put(
      `/api/admin/customer/note/update/${details._id}`,
      {
        note: noteDetails.note,
      },
      {
        headers: { authorization: `Bearer ${localStorage.getItem('token$')}` },
      }
    )
      .then(({ data }) => {
        // console.log('save', data);
        setLoading(false)
        alert.success('Note Edited');
        history.goBack()
      })
      .catch((err) => {
        setLoading(false)
        console.log('Err', err);
      });
  };

  if (details == null) {
    return <Redirect to='/customer' />;
  }

  return (
    <AuthUser>
      <Row className='page-title align-items-center'>
        <Col sm={4} xl={6}>
          <h4 className='mb-1 mt-0'>Proposal Note</h4>
        </Col>
      </Row>

      <div className='row'>
        <div className='col-12'>
          <div className='card p-3'>
            <h4 className='mb-4'> Note Edit</h4>
            <div className='form-row'>
              <div className='form-group col-md-4'>
                <label>Note</label>
                <input
                  type='text'
                  // className='form-control'
                  placeholder='Note'
                  id='note'
                  value={noteDetails.note}
                  onChange={handelChange}
                  name='note'
                  className={`form-control ${errors.note ? 'is-invalid' : ''}`}
                />
                {errors?.note && (
                  <div className='invalid-feedback'>Required</div>
                )}
              </div>
            </div>
            <div className='form-row'>
              <div className='form-group col-md-2'>
              <button
                 disabled={isLoading}
                  className='btn btn-primary btn-block'
                  onClick={HandleValidation}>
                    {isLoading?'Updating...':'Update'}
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </AuthUser>
  );
};

export default CNoteEdit;
