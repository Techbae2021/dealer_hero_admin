/** @format */

import React from 'react';
import { Row, Col, Table } from 'reactstrap';
import { Link } from 'react-router-dom';
import AuthUser from '../../components/auth/AuthUser';
//import { useHistory } from 'react-router-dom';
// import PSidebar from './../purchase/pSidebar';

const Payment = () => {
  return (
    <AuthUser>
      <Row className='page-title align-items-center'>
        <Col sm={4} xl={6}>
          <h4 className='mb-1 mt-0'>Payment</h4>
        </Col>
      </Row>

      <div className='row' style={{justifyContent:"space-around"}}>
        <div className='col-10'>
          <div className='card'>
            <div className='card p-3'>
              <div className='row'>
                <div className='col-8'>
                  <h4> All Payments </h4>
                </div>
              </div>
              <div>
                <Table hover responsive className='mt-4'>
                  <thead>
                    <tr>
                      <th scope='col'>#</th>
                      <th scope='col'>First Name</th>
                      <th scope='col'>Last Name</th>
                      <th scope='col'>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>1</td>
                      <td>Call Him 10 A.M</td>
                      <td>22/01/2021</td>

                      <td>
                        <div className='button-list'>
                          <Link
                            to='/payment/kmfglklhk54'
                            className='btn btn-primary  btn-sm'>
                            View
                          </Link>
                          
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <td>2</td>
                      <td>Marco Lightweight Shirt</td>
                      <td>Mark P</td>

                      <td>
                        <div className='button-list'>
                          <Link to='#' className='btn btn-primary  btn-sm'>
                            View
                          </Link>
                          
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <td>3</td>
                      <td>Half Sleeve Shirt</td>
                      <td>Dave B</td>

                      <td>
                        <div className='button-list'>
                          <Link to='#' className='btn btn-primary  btn-sm'>
                            View
                          </Link>
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <td>4</td>
                      <td>Lightweight Jacket</td>
                      <td>Shreyu N</td>

                      <td>
                        <div className='button-list'>
                          <Link to='#' className='btn btn-primary  btn-sm'>
                            View
                          </Link>
                          
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <td>5</td>
                      <td>Marco Shoes</td>
                      <td>Rik N</td>

                      <td>
                        <div className='button-list'>
                          <Link to='#' className='btn btn-primary  btn-sm'>
                            View
                          </Link>
                          
                        </div>
                      </td>
                    </tr>
                  </tbody>
                </Table>
              </div>
            </div>
          </div>
        </div>{' '}
        {/* end Col */}
      </div>
    </AuthUser>
  );
};

export default Payment;
