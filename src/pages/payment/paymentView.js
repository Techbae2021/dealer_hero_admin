/** @format */

import React from 'react';
import { Row, Col, CardBody, Card } from 'reactstrap';
import AuthUser from '../../components/auth/AuthUser';
//import { useHistory } from 'react-router-dom';

const PaymentView = () => {
  return (
    <AuthUser>
      <Row className='page-title align-items-center'>
        <Col sm={8}>
          <h4 className='mb-1 mt-0'> Payment </h4>
        </Col>
        {/* <Col sm={4}>
          <PTopbar />
        </Col> */}
      </Row>

      <React.Fragment>
        <div className='row'>
          <div className='col-12'>
            <Card>
              <CardBody>
                <div className='row'>
                  <div className='col-md-6'>
                    <h4> Payment Details</h4>
                  </div>
                  {/* <div className='col-md-6 text-right'>
                    <svg
                      xmlns='http://www.w3.org/2000/svg'
                      width='2rem'
                      height='2rem'
                      fill='currentColor'
                      class='bi bi-pencil-square'
                      viewBox='0 0 16 16'
                      onClick={() => CustomerEdit()}>
                      <path d='M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456l-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z' />
                      <path
                        fill-rule='evenodd'
                        d='M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z'
                      />
                    </svg>
                  </div> */}
                </div>

                <div className='row'>
                  <div className='col-12'>
                    <div className='form-row'>
                      <div className='form-group col-md-4'>
                        <label>Customer Code</label>
                        <input
                          type='text'
                          className='form-control'
                          placeholder='First Name'
                          id='firstName'
                          //   value={leadDetails.firstName}
                          //   onChange={handelChange}
                        ></input>
                      </div>
                      <div className='form-group col-md-4'>
                        <label>First Name</label>
                        <input
                          type='text'
                          className='form-control'
                          placeholder='First Name'
                          id='firstName'
                          //   value={leadDetails.firstName}
                          //   onChange={handelChange}
                        ></input>
                      </div>
                      <div className='form-group col-md-4'>
                        <label>Last Name</label>
                        <input
                          type='text'
                          className='form-control'
                          placeholder='Last Name'
                          id='lastName'
                          //   value={leadDetails.lastName}
                          //   onChange={handelChange}
                        ></input>
                      </div>
                      <div className='form-group col-md-4'>
                        <label>Email</label>
                        <input
                          type='text'
                          className='form-control'
                          placeholder='Phone Number'
                          id='phone'
                          //   value={leadDetails.phone}
                          //   onChange={handelChange}
                        ></input>
                      </div>
                      <div className='form-group col-md-4'>
                        <label>Phone</label>
                        <input
                          type='text'
                          className='form-control'
                          placeholder='Email'
                          id='email'
                          //   value={leadDetails.email}
                          //   onChange={handelChange}
                        ></input>
                      </div>
                      <div className='form-group col-md-4'>
                        <label>Address</label>
                        <input
                          type='text'
                          className='form-control'
                          placeholder='Stock Id'
                          id='stockId'
                          //   value={leadDetails.stockId}
                          //   onChange={handelChange}
                        ></input>
                      </div>
                      <div className='form-group col-md-4'>
                        <label>State</label>
                        <input
                          type='text'
                          className='form-control'
                          placeholder='Stock Id'
                          id='stockId'
                          //   value={leadDetails.stockId}
                          //   onChange={handelChange}
                        ></input>
                      </div>
                      <div className='form-group col-md-4'>
                        <label>Country</label>
                        <input
                          type='text'
                          className='form-control'
                          placeholder='Stock Id'
                          id='stockId'
                          //   value={leadDetails.stockId}
                          //   onChange={handelChange}
                        ></input>
                      </div>
                      <div className='form-group col-md-4'>
                        <label>GST NO.</label>
                        <input
                          type='text'
                          className='form-control'
                          placeholder='Stock Id'
                          id='stockId'
                          //   value={leadDetails.stockId}
                          //   onChange={handelChange}
                        ></input>
                      </div>
                    </div>
                  </div>
                </div>
              </CardBody>
            </Card>
          </div>
        </div>
      </React.Fragment>
    </AuthUser>
  );
};

export default PaymentView;
