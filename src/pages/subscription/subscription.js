/** @format */
import React, { useState, useEffect } from "react";
import {
  Row,
  Col,
  CardBody,
  Card,
  ModalBody,
  Modal,
  ModalFooter,
  ModalHeader,
  CardText,
  CardTitle,
  CardSubtitle,
  Button,
} from "reactstrap";
//import moment from 'moment';
import { Link } from "react-router-dom";
import ViewSubscription from "./viewSubscription";
import Instance from "../../Instance";
import AuthUser from "../../components/auth/AuthUser";

const Subscription = () => {
  //let history = useHistory();

  const [subscriptionView, setSubscriptionView] = useState([
    {
      packageName: "abc",
      package: 32,
      packagePrice: "xyz",
      description: "asdfer",
    },
    {
      packageName: "abc",
      package: 32,
      packagePrice: "xyz",
      description: "asdfer",
    },
    {
      packageName: "abc",
      package: 32,
      packagePrice: "xyz",
      description: "asdfer",
    },
  ]);
  const [loader, setLoader] = useState(false);
  const [description, setdescription] = useState("");
  const [count, setCount] = useState(1);
  const [totalCount, settotalCount] = useState(0);
  const [page, setPage] = useState(null);

  useEffect(() => {
    setLoader(true);
    Instance.post(
      `/api/admin/subscriptions`,
      {
        page: count,
      },
      {
        headers: { authorization: `Bearer ${localStorage.getItem('token$')}` },
      }
    ).then(({ data }) => {
        // console.log('Data', data);
        let temp = data?.results?.subscriptions;
        setPage(data?.results);
        setSubscriptionView(temp);
        setLoader(false);
        settotalCount(data?.results?.totalIndb);
      }).catch((err) => {
        setLoader(false);
        console.log('err', err);
      });
  }, [count]);

   const NextPage = () => {
    setCount(count + 1);
  };
  const PrevPage = () => {
    setCount(count - 1);
  };

  const [viewmodalOpen, setViewModalOpen] = useState(false);
  const modalToggleView = (value) => {
    setViewModalOpen(!viewmodalOpen);
    setdescription(value.description);
  };

  return (
    <AuthUser>
      <Row className="page-title align-items-center">
        <Col sm={8}>
          <h4 className="mb-1 mt-0"> Subscription </h4>
        </Col>
        <Col sm={4}></Col>
      </Row>

      <React.Fragment>
        <div className="row" style={{ justifyContent: "space-around" }}>
          <div className="col-10">
            <Card>
              <CardBody>
                <div className="row">
                  <div className="col-6">
                    <h4>All Packages</h4>
                  </div>
                  <div className='col-md-6 text-right'>
                  <div className='d-inline-block align-middle float-lg-right'>
                    <div className='row'>
                      <div
                        className='col-8 align-self-center'
                        style={{ whiteSpace: 'nowrap' }}>
                        Showing {page?.startOfItem} - {page?.endOfItem} of{' '}
                        {totalCount}
                      </div>
                      {/* end col*/}
                      <div className='col-4'>
                        <div className='btn-group float-right'>
                          <button
                            type='button'
                            className='btn btn-white btn-sm'
                            onClick={PrevPage}
                            style={{
                              display: page?.Previous === false ? 'none' : '',
                            }}>
                            <i
                              className='uil uil-angle-left'
                             
                            />
                          </button>
                          <button
                            type='button'
                            onClick={NextPage}
                            className='btn btn-primary btn-sm'
                            style={{
                              display: page?.Next === false ? 'none' : '',
                            }}>
                            <i className='uil uil-angle-right'
                              
                            />
                          </button>
                        </div>
                      </div>{' '}
                      {/* end col*/}
                    </div>
                  </div>
                </div>
                </div>
                <div className="row">
                   <div className="col-6"></div>
                   <div className="col-6  text-right mb-2">
                   <Link
                      to="/subscription/add"
                      className="btn btn-success  btn-sm Button_Size"
                    >
                      Add
                    </Link>
                   </div>
                </div>

               
                {loader ? (
                    <>
                      <div className='d-flex justify-content-center'>
                        <div className='spinner-border' role='status'>
                          <span className='sr-only'>Loading...</span>
                        </div>
                      </div>{' '}
                    </>
                  ) : (
                    <div className="row">
                  {subscriptionView &&
                    subscriptionView.map((value, i) => (
                      <div className="col-4" key={i}>
                        <Card style={{ border: "2px solid silver" }}>
                          <CardBody style={{ textAlign: "center" }}>
                            <CardTitle tag="h5">{value.packageName}</CardTitle>
                            <hr />
                            <CardSubtitle tag="h6" className="mb-2 text-muted">
                              {value.packageType}
                              <br />
                              {value.package}
                              <br />
                              {value.packagePrice}
                            </CardSubtitle>
                            <CardText>{value.description}</CardText>
                            <Button
                              onClick={() => modalToggleView(value)}
                              className="btn btn-success Button_Size"
                            >
                              View
                            </Button>
                          </CardBody>
                        </Card>
                      </div>
                    ))}
                    </div>
                  )
                 }
              </CardBody>
            </Card>
          </div>
        </div>
      </React.Fragment>

      <Modal
        isOpen={viewmodalOpen}
        toggle={modalToggleView}
        className="modal-md mt-5"
      >
        <ModalHeader toggle={modalToggleView}>
          {" "}
          Package Description{" "}
        </ModalHeader>
        <ModalBody>
          <ViewSubscription description={description} />
        </ModalBody>
        <ModalFooter></ModalFooter>
      </Modal>
    </AuthUser>
  );
};

export default Subscription;
