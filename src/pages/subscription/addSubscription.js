/** @format */

import React, { useState } from 'react';
import { Row, Col } from 'reactstrap';
import Instance from '../../Instance';
import { useHistory } from 'react-router-dom';
import { useAlert } from 'react-alert';
import CustomInput from '../../components/CustomInput';
import AuthUser from '../../components/auth/AuthUser';

const AddSubscription = () => {
  const alert = useAlert();
  let history = useHistory();
  const [isLoading, setLoading] = useState(false)

  const [subInfo, setsubInfo] = useState({
    packageName: "",
    package: "",
    packagePrice: "",
    description: "",
  });

  const handelChange = (event) => {
    setsubInfo({
      ...subInfo,
      [event.target.id]: event.target.value,
    });
    if (event.target.value === "") {
      setErrors({ ...errors, [event.target.name]: true });
    } else {
      setErrors({ ...errors, [event.target.name]: false });
    }
  };

  const OnSubmit = async (e) => {
    // e.preventDefault();
    if(isValid){
      setLoading(true)
    Instance.post(
      `/api/admin/subscription`,
      {
        "packageName": subInfo?.packageName,
        "packageType": subInfo?.packageType,
        "description": subInfo?.description,
        "package": subInfo?.package,
        "packagePrice": subInfo?.packagePrice
      },
      {
        headers: { authorization: `Bearer ${localStorage.getItem('token$')}` },
      }
    ).then(({ data }) => {
      setLoading(false)

        // console.log('save', data);
        alert.success('Subscription Added');
        history.push(`/subscription`);
      })
      .catch((err) => {
      setLoading(false)

        // console.log('Err', err);
      });
    }
  };

  const [errors, setErrors] = useState({
    packageName: false,
    package: false,
    packagePrice: false,
    description: false,
  });

  const [isValid, setisValid] = useState(true);

  const HandleValidation = () => {
    let shouldsubmit = true;
    let tempError = errors;

    for (const [key, value] of Object.entries(subInfo)) {
      if (value === "") {
        tempError[key] = true;
        if (shouldsubmit) {
          shouldsubmit = false;
        }
      } else {
        tempError[key] = false;
      }
    }
    // console.log(tempError);
    setisValid(shouldsubmit);
    setErrors(tempError);

    if (shouldsubmit) {
      OnSubmit();
    }
  };


  return (
    <AuthUser>
      <Row className='page-title align-items-center'>
        <Col sm={4} xl={6}>
          <h4 className='mb-1 mt-0'>Subscription</h4>
        </Col>
      </Row>

      <div className='row'>
        <div className='col-12'>
          <div className='card p-3'>
            <h4 className='mb-4'> Add Subscription </h4>

            <div className='form-row'>
              {[
                { label: "Package Name", name: "packageName", type: "text" },
                { label: "Package", name: "package", type: "text" },
                { label: "Package Price", name: "packagePrice", type: "text" },
                { label: "Description", name: "description", type: "text" },
              ].map((item, i) => (
                <div className='form-group col-md-4' key={i}>
                  <CustomInput
                    label={item.label}
                    type={item.type}
                    error={errors[item.name]}
                    name={item.name}
                    id={item.name}
                    value={subInfo[item.name]}
                    handleChange={(e) => {
                      handelChange(e);
                    }}
                  />
                </div>
              ))}
            </div>

            <div className='form-row'>
              <div className='form-group col-md-2'>
              <button
                  disabled={isLoading}
                  className='btn btn-primary btn-block'
                  onClick={HandleValidation}>
                  {isLoading ? 'Adding...' : 'Add'}
                </button>
              </div>
            </div>
          </div>
        </div>{' '}
      </div>
    </AuthUser>
  );
};

export default AddSubscription;
