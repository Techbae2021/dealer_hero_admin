import React from "react";

import { CardBody, Card, CardText } from "reactstrap";

const ViewSubscription = (props) => {
  return (
    <>
      <Card>
        <CardBody>
          <CardText>
            {props.description}
          </CardText>
        </CardBody>
      </Card>
    </>
  );
};

export default ViewSubscription;
