/** @format */

import React, { useEffect, useState } from "react";
import { Row, Col, Table } from "reactstrap";
import Instance from "./../../Instance";
import moment from "moment";
import CustomInput from "../../components/CustomInput";
import AuthUser from "../../components/auth/AuthUser";

const Pipeline = () => {
  const [pipelineView, setPipelineView] = useState([]);
  const [refresh, seRefresh] = useState(false);
  const [isLoading, setLoading] = useState(false)
  const [loader, setLoader] = useState(false)

  useEffect(() => {
    setLoader(true)
    Instance.get(`/api/admin/getpipeline`, {
      headers: { authorization: `Bearer ${localStorage.getItem("token$")}` },
    })
      .then(({ data }) => {
    setLoader(false)

        // console.log("piplinesData", data);
        setPipelineView(data?.data);
      })
      .catch((err) => {
    setLoader(false)

        console.log("err", err);
      });
  }, [refresh]);

  const [pipelineDetails, setPipelineDetails] = useState({
    pipelineName: "",
  });

  const handelChange = (event) => {
    setPipelineDetails({
      ...pipelineDetails,
      [event.target.id]: event.target.value,
    });

    if (event.target.value === "") {
      setErrors({ ...errors, [event.target.name]: true });
    } else {
      setErrors({ ...errors, [event.target.name]: false });
    }
  };
  const OnSubmit = () => {
    // e.preventDefault();
    setLoading(true)
    Instance.post(
      `/api/admin/addpipeline`,
      {
        pipelineName: pipelineDetails.pipelineName,
      },
      {
        headers: { authorization: `Bearer ${localStorage.getItem("token$")}` },
      }
    )
      .then(({ data }) => {
    setLoading(false)

        // console.log("save", data);
        setPipelineDetails({
          pipelineName: "",
        });
        seRefresh(!refresh);
      })
      .catch((err) => {
    setLoading(false)

        console.log("Err", err);
      });
  };

  const PipeLineDelete = (value) => {
    setLoader(true)
    Instance.delete(`/api/admin/deletepipeline/${value._id}`, {
      headers: { authorization: `Bearer ${localStorage.getItem("token$")}` },
    })
      .then(() => {
    setLoader(false)

        // console.log("Success");
        seRefresh(!refresh);
      })
      .catch((err) => {
    setLoader(false)

        console.log("Error", err);
      });
  };

  const [errors, setErrors] = useState({
    pipelineName: false,
  });

  const [isValid, setisValid] = useState(true);

  const HandleValidation = () => {
    if (pipelineDetails.pipelineName == "") {
      setErrors({ pipelineName: true });
    } else {
      setErrors({ pipelineName: false });
      OnSubmit();
    }
  };

  return (
    <AuthUser>
      <Row className="page-title align-items-center">
        <Col sm={4} xl={6}>
          <h4 className="mb-1 mt-0">Pipeline</h4>
        </Col>
      </Row>

      <div className="row">
        <div className="col-md-6">
          <div className="card p-3">
            <h4 className="mb-4">Add Pipeline Status </h4>
            <div className="row mx-0  mb-3">
              <div className="col-md-7"></div>
              <div className="col-md-5"></div>
            </div>
            <label>Add Pipeline</label>
            <div className="row">
              <div className="form-group col-md-8">
                <CustomInput
                  type="text"
                  error={errors.pipelineName}
                  name="pipelineName"
                  id="pipelineName"
                  value={pipelineDetails.pipelineName}
                  handleChange={(e) => {
                    handelChange(e);
                  }}
                />
              </div>
              <div className="form-group col-md-3">
                <button
                  disabled={isLoading}
                  className='btn btn-primary btn-block'
                  onClick={HandleValidation}>
                  {isLoading ? 'Adding...' : 'Add'}
                </button>
              </div>
            </div>
          </div>
        </div>{" "}
        <div className="col-md-6">
          <div className="card p-3">
            <h4 className="mb-4">All Pipeline View </h4>
            <div className="row mx-0  mb-3">
              <div className="col-md-7"></div>
              <div className="col-md-5"></div>
            </div>

            <div>
            {loader ? (
                    <>
                      <div className='d-flex justify-content-center'>
                        <div className='spinner-border' role='status'>
                          <span className='sr-only'>Loading...</span>
                        </div>
                      </div>{' '}
                    </>
                  ) : (
              <Table hover responsive className="mt-4">
                {pipelineView != null &&
                  pipelineView.map((value, i) => (
                    <tbody key={i}>
                      <tr>
                        <td>{i + 1}</td>
                        <td>{value.pipelineName}</td>
                        <td>
                          {" "}
                          {moment(new Date(value?.createdAt)).format(
                            "DD/MM/YYYY"
                          )}
                          <br />
                          {new Date(value?.createdAt).toLocaleTimeString()}
                        </td>

                        <td>
                          <i
                            className="uil uil-trash-alt"
                            onClick={() => PipeLineDelete(value)}
                          />
                        </td>
                      </tr>
                    </tbody>
                  ))}
              </Table>
                  )}
            </div>
          </div>
        </div>{" "}
        {/* end Col */}
      </div>
      {/* End row */}
    </AuthUser>
  );
};

export default Pipeline;
