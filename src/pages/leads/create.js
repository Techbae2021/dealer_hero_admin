/** @format */

import React, { useState } from 'react';
import { Row, Col } from 'reactstrap';
import { useHistory } from 'react-router-dom';
import Instance from '../../Instance';
import { useAlert } from 'react-alert';
import Leftsidebar from './leftsidebar';
import './lead.css';
import mixpanel from 'mixpanel-browser';

const Creat = () => {
  let history = useHistory();

  const [seen, setSeen] = useState('password');
  const [isLoading,setLoading] = useState(false)
  const alert = useAlert();


  const PasswordSeen = () => {
    if (seen === 'password') {
      setSeen('text')
    }
    else {
      setSeen('password')
    }
  }

  const [leadDetails, setLeadDetails] = useState({
    firstName: '',
    lastName: '',
    phone: '',
    email: '',
    password: ''
  });

  const handelChange = (event) => {
    setLeadDetails({
      ...leadDetails,
      [event.target.id]: event.target.value,
    });
    if (event.target.value === '') {
      setErrors({ ...errors, [event.target.name]: true });
    } else {
      setErrors({ ...errors, [event.target.name]: false });
    }
  };



  const OnSubmit = async (e) => {
    e.preventDefault();
    setLoading(true)
    Instance.post(
      `/api/admin/addLead`,
      {
        firstName: leadDetails.firstName,
        lastName: leadDetails.lastName,
        mobile: leadDetails.phone,
        email: leadDetails.email,
        password: leadDetails.password,
        img: 'https://cdn2.iconfinder.com/data/icons/facebook-51/32/FACEBOOK_LINE-01-512.png'
      },
      {
        headers: { authorization: `Bearer ${localStorage.getItem('token$')}` },
      }
    )
      .then(({ data }) => {
        // console.log('save', data);
        setLoading(false)
        if(data?.success){
          alert.success(data?.message);
          mixpanel.track('Lead', {
            date: Date.now(),
          });
          history.push('/leads');
        }
      }).catch((err) => {
        setLoading(false)
        console.log('Err', err);
        if(!err?.response?.data?.success){
          alert.error(err.response?.data?.err?.response?.data?.message);
          }else{
            alert.error("Something went wrong!")
         }
      });
  
  };

  const [errors, setErrors] = useState({
    firstName: false,
    lastName: false,
    phone: false,
    email: false,
    password: false
  });

  const [isValid, setisValid] = useState(true);

  const HandleValidation = (e) => {
    e.preventDefault();
    let shouldsubmit = true;
    let tempError = errors;

    for (const [key, value] of Object.entries(leadDetails)) {
      if (value == '') {
        // if (key != 'search') {
          tempError[key] = true;
          if (shouldsubmit) {
            shouldsubmit = false;
          }
        // }
        // else {
      //   tempError[key] = false;
      // }
      }
    }
    // console.log(tempError);
    setisValid(shouldsubmit);
    setErrors(tempError);

    if (shouldsubmit) {
      OnSubmit(e);
    }
  };

  return (
    <>
      <Row className='page-title align-items-center'>
        <Col sm={4} xl={6}>
          <h4 className='mb-1 mt-0'>Leads </h4>
        </Col>
      </Row>

      <div className='row'>
        <div className='col-2 Lead_Board'>
          <Leftsidebar />
        </div>
        <div className='col-10'>
          <div className='card p-3'>
            <h4 className='mb-4'> Create Lead </h4>
            
            <div className='form-row'>
              <div className='form-group col-md-4'>
                <label>First Name</label>
                <input
                  type='text'
                  placeholder='First Name'
                  id='firstName'
                  value={leadDetails.firstName}
                  name='firstName'
                  onChange={handelChange}
                  // ref={register}
                  className={`form-control ${errors.firstName ? 'is-invalid' : ''
                    }`}
                />
                {errors.firstName && (
                  <div className='invalid-feedback'>Enter Your FirstName</div>
                )}
              </div>
              <div className='form-group col-md-4'>
                <label>Last Name</label>
                <input
                  type='text'
                  placeholder='Last Name'
                  id='lastName'
                  value={leadDetails.lastName}
                  name='lastName'
                  onChange={handelChange}
                  // ref={register}
                  className={`form-control ${errors.lastName ? 'is-invalid' : ''
                    }`}
                />
                {errors.lastName && (
                  <div className='invalid-feedback'>Enter Your LastName</div>
                )}
              </div>
              <div className='form-group col-md-4'>
                <label>Phone Number</label>
                <input
                  type='text'
                  placeholder='Phone Number'
                  id='phone'
                  value={leadDetails.phone}
                  name='phone'
                  onChange={handelChange}
                  // ref={register}
                  className={`form-control ${errors.phone ? 'is-invalid' : ''}`}
                />
                {errors.phone && (
                  <div className='invalid-feedback'>
                    Enter Your Phone Number
                  </div>
                )}
              </div>
              <div className='form-group col-md-4'>
                <label>Email</label>
                <input
                  type='email'
                  placeholder='Email'
                  id='email'
                  value={leadDetails.email}
                  name='email'
                  onChange={handelChange}
                  // ref={register}
                  className={`form-control ${errors.email ? 'is-invalid' : ''}`}
                />
                {errors.email && (
                  <div className='invalid-feedback'>Enter Your Email</div>
                )}
              </div>
              <div className='form-group col-md-4'>
                <label>Password</label>
                <div style={{ display: 'flex' }}>
                  <input
                    type={seen}
                    placeholder='Password'
                    id='password'
                    value={leadDetails.password}
                    name='password'
                    onChange={handelChange}
                    // ref={register}
                    className={`form-control ${errors.password ? 'is-invalid' : ''}`}
                  />
                  { seen === 'password' ?
                      <div class="input-group-prepend" style={{margin:'0 10px'}}>
                      <div className="input-group-text">  
                       <span className="fa fa-eye" style={{cursor:'pointer'}} onClick={() => PasswordSeen()}></span>
                      </div>
                    </div>
                    :
                    <div class="input-group-prepend" style={{margin:'0 10px'}}>
                    <div className="input-group-text">  
                     <span className="fa fa-eye-slash" style={{cursor:'pointer'}} onClick={() => PasswordSeen()}></span>
                    </div>
                  </div>
                  }
                 

                </div>

                {errors.password && (
                  <div className='invalid-feedback'>Enter Your Password</div>
                )}
              </div>

            </div>
            <div className='form-row' style={{ display: 'grid', placeItems: 'center' }}>
              <div className='form-group col-md-2' >
                <button
                  disabled={isLoading}
                  className='btn btn-primary btn-block'
                  onClick={(e) => HandleValidation(e)}>
                  {isLoading?'Creating lead...':'Create'}
                </button>
              </div>
            </div>
          </div>{' '}
          {/* end Col */}
        </div>
      </div>
      {/* End row */}
    </>
  );
};

export default Creat;
