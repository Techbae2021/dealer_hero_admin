/** @format */

import React, { useState, useContext } from 'react';
import { Row, Col } from 'reactstrap';
import Leftsidebar from './leftsidebar';
import Instance from './../../Instance';
import { useHistory } from 'react-router-dom';
import Context from '../context/Context';
import { useAlert } from 'react-alert';

const LeadMail = () => {
  let history = useHistory();
  const alert = useAlert();
  const { details } = useContext(Context);
  const [isLoading,setLoading] = useState(false)

  const [mailDetails, setMailDetails] = useState({
    to: details != null ? details?.email : '',
    subject: '',
    message: '',
  });

  const [errors, setErrors] = useState({
    to: false,
    subject: false,
    message: false
  });


  const HandleValidation = (e) => {
    e.preventDefault();
    let shouldsubmit = true;
    let tempError = errors;

    for (const [key, value] of Object.entries(mailDetails)) {
      if (value == '') {
        // if (key != 'search') {
          tempError[key] = true;
          if (shouldsubmit) {
            shouldsubmit = false;
          }
        // }
        // else {
      //   tempError[key] = false;
      // }
      }
    }
    // console.log(tempError);
    setErrors(tempError);

    if (shouldsubmit) {
      OnSubmit(e);
    }
  };

  const handelChange = (event) => {
    setMailDetails({
      ...mailDetails,
      [event.target.id]: event.target.value,
    });
    if (event.target.value === '') {
      setErrors({ ...errors, [event.target.id]: true });
    } else {
      setErrors({ ...errors, [event.target.id]: false });
    }
  };

  const OnSubmit = (e) => {
    e.preventDefault();
    setLoading(true)
    Instance.post(
      `/api/admin/sendLeadMail/${details._id}`,
      {
        to: details?.email,
        subject: mailDetails.subject,
        message: mailDetails.message,
      },
      {
        headers: { authorization: `Bearer ${localStorage.getItem('token$')}` },
      }
    ).then(({ data }) => {
        // console.log('save', data);
        setLoading(false)
        alert.success('Mail Sent');
        history.push(`/leads/detail/${details._id}`);
      })
      .catch((err) => {
        setLoading(false)
        console.log('Err', err);
      });
  };

  return (
    <>
      <Row className='page-title align-items-center'>
        <Col sm={4} xl={6}>
          <h4 className='mb-1 mt-0'>Lead Mail</h4>
        </Col>
      </Row>

      <div className='row'>
        <div className='col-2 Lead_Board'>
          <div className='email-container bg-transparent'>
            {/* Left sidebar */}
            <Leftsidebar />
            {/* End Left sidebar */}
          </div>
        </div>
        <div className='col-10'>
          <div className='card p-3'>
            <h4 className='mb-4'> Send Mail </h4>
            <div className='form-row'>
              <div className='form-group col-md-8'>
                <label>Subject</label>
                <input
                  type='text'
                  className={`form-control ${errors.subject ? 'is-invalid' : ''}`}
                  placeholder='Subject'
                  id='subject'
                  value={mailDetails.subject}
                  onChange={handelChange}></input>
                   {errors.subject?(
                  <div className='invalid-feedback'>Enter Your Subject</div>
                 ):null}
              </div>
            </div>
            <div className='form-row'>
              <div className='form-group col-md-8'>
                <label>Messege</label>
                <textarea
                   className={`form-control ${errors.message ? 'is-invalid' : ''}`}
                  placeholder='Messege'
                  rows='6'
                  id='message'
                  value={mailDetails.message}
                  onChange={handelChange}></textarea>
                   {errors.message?(
                  <div className='invalid-feedback'>Enter Your Message</div>
                 ):null}
              </div>
            </div>
            <div className='form-row'>
              <div className='form-group col-md-2'>
              <button
                disabled={isLoading}
                  className='btn btn-primary btn-block'
                  onClick={(e) => HandleValidation(e)}>
                  {isLoading?'Sending...':'Send'}
                </button>
              </div>
            </div>
          </div>
        </div>{' '}
        {/* end Col */}
      </div>
    </>
  );
};

export default LeadMail;
