/** @format */

import React, { useEffect, useState } from 'react';
import { Row, Col } from 'reactstrap';
import Leftsidebar from './leftsidebar';
import { Link } from 'react-router-dom';
import Instance from '../../Instance';
import './lead.css';
import moment from 'moment';

const LeadDelete = () => {
  const [leadview, setLeadView] = useState(null);
  const [loader, setLoader] = useState(false);
  const [count, setCount] = useState(1);
  const [page, setPage] = useState(null);

  useEffect(() => {
    setLoader(true);
    Instance.post(
      `/api/admin/deletedLeads`,
      {
        page: count,
      },
      {
        headers: { authorization: `Bearer ${localStorage.getItem('token$')}` },
      }
    )
      .then(({ data }) => {
        // console.log('leadData', data);
        let temp = data?.results?.DeletedLeads;
        setPage(data?.results);
        temp.reverse();
        setLeadView(temp);
        setLoader(false);
      })
      .catch((err) => {
        console.log('err', err);
      });
  }, [count]);

  const NextPage = () => {
    setCount(count + 1);
  };
  const PrevPage = () => {
    setCount(count - 1);
  };

  return (
    <>
      <Row className='page-title align-items-center'>
        <Col sm={4} xl={6}>
          <h4 className='mb-1 mt-0'>Deleted Lead</h4>
        </Col>
      </Row>

      <div className='row'>
        <div className='col-2 Lead_Board'>
          <div className='email-container bg-transparent'>
            {/* Left sidebar */}
            <Leftsidebar />
            {/* End Left sidebar */}
          </div>
        </div>
        <div className='col-10 Lead_Style'>
          <div className=''>
            {/* start right sidebar */}
            <div className=''>
              <div className='d-inline-block align-middle float-lg-right'>
                <div className='row'>
                  <div className='col-8 align-self-center'>
                    Showing {page?.startOfItem} - {page?.endOfItem} of{' '}
                    {page?.totalDeletedLeadsdb}
                  </div>
                  {/* end col*/}
                  <div className='col-4'>
                    <div className='btn-group float-right'>
                      <button
                        type='button'
                        className='btn btn-white btn-sm'
                        style={{
                          display: page?.Previous === false ? 'none' : '',
                        }}>
                        <i className='uil uil-angle-left' onClick={PrevPage} />
                      </button>
                      <button
                        type='button'
                        className='btn btn-primary btn-sm'
                        style={{ display: page?.Next === false ? 'none' : '' }}>
                        <i className='uil uil-angle-right' onClick={NextPage} />
                      </button>
                    </div>
                  </div>{' '}
                  {/* end col*/}
                </div>
              </div>
              <br />
              {loader ? (
                <>
                  <div class='d-flex justify-content-center'>
                    <div class='spinner-border' role='status'>
                      <span class='sr-only'>Loading...</span>
                    </div>
                  </div>{' '}
                </>
              ) : (
                <>
                  <div className='mt-2'>
                    <h5 className='mt-3 mb-2 font-size-16'>All</h5>
                    {leadview &&
                      leadview.map((value, i) => {
                        // if (value?.leadReadUnreadStutus === false) {
                          return (
                            <ul className='message-list' key={i}>
                              <li className='unread'>
                                <div className='col-mail col-mail-1'>
                                  {/* {value.done === true ? (
                                    <div className='checkbox-wrapper-mail'>
                                      <input
                                        type='checkbox'
                                        id='chk2'
                                        disabled
                                      />
                                      <label
                                        htmlFor='chk2'
                                        className='toggle'
                                        style={{ opacity: '1' }}
                                        disabled
                                        onClick={() => LeadDone(value)}
                                      />
                                    </div>
                                  ) : (
                                    <div className='checkbox-wrapper-mail'>
                                      <input
                                        type='checkbox'
                                        id='chk2'
                                        disabled
                                      />
                                      <label
                                        htmlFor='chk2'
                                        className='toggle'
                                        disabled
                                        onClick={() => LeadDone(value)}
                                      />
                                    </div>
                                  )} */}
                                  {/* {value.star === true ? (
                                    <span
                                      className='star-toggle uil uil-star text-warning'
                                      onClick={() => LeadStar(value)}
                                    />
                                  ) : (
                                    <span
                                      className='star-toggle uil uil-star'
                                      onClick={() => LeadStar(value)}
                                    />
                                  )} */}
                                  <a className='ml-4'>
                                    {value.firstName} {value.lastName}
                                  </a>
                                </div>
                                <div className='col-mail col-mail-2'>
                                  <Link to='#' href className='subject'>
                                    <a>
                                      {value.stockId}, {value.car}, {value.make}
                                      , {value.model}, {value.year}, ({' '}
                                      {moment(new Date(value.createdAt)).format(
                                        'DD/MM/YYYY'
                                      )}
                                      ,
                                      {new Date(
                                        value.createdAt
                                      ).toLocaleTimeString()}
                                      )
                                    </a>
                                  </Link>
                                  {/* <div className='date'>
                                    <button
                                      type='button'
                                      className='btn btn-light'
                                      data-toggle='tooltip'
                                      data-placement='top'
                                      title='Edit'
                                      onClick={() => LeadEdit(value)}>
                                      <svg
                                        xmlns='http://www.w3.org/2000/svg'
                                        width='16'
                                        height='16'
                                        fill='currentColor'
                                        class='bi bi-pencil-square'
                                        viewBox='0 0 16 16'>
                                        <path d='M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456l-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z' />
                                        <path
                                          fill-rule='evenodd'
                                          d='M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z'
                                        />
                                      </svg>
                                    </button>
                                    <button
                                      type='button'
                                      className='btn btn-light'
                                      data-toggle='tooltip'
                                      data-placement='top'
                                      title='Delete'
                                      onClick={() => LeadDelete(value)}>
                                      <i className='uil uil-trash-alt' />
                                    </button>
                                  </div> */}
                                </div>
                              </li>
                            </ul>
                          );
                        // }
                      })}
                  </div>
                </>
              )}
            </div>
            {/* end right sidebar */}
            <div className='clearfix' />
          </div>
        </div>{' '}
        {/* end Col */}
      </div>
      {/* End row */}
    </>
  );
};

export default LeadDelete;
