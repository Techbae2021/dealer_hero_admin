/** @format */

import React, { useEffect, useState, useContext } from 'react';
import { Row, Col, Table } from 'reactstrap';
import Leftsidebar from './leftsidebar';
import { useHistory, useParams } from 'react-router-dom';
import Instance from './../../Instance';
import moment from 'moment';
import Context from '../context/Context';
import { LEAD_MAIL_DETAILS, LEAD_NOTE_DETAILS } from '../context/action.type';
import { useAlert } from 'react-alert';
import Context2 from '../../routes/Context';
import AuthUser from '../../components/auth/AuthUser';

const Detail = (props) => {
  let history = useHistory();
  const params = useParams();
  const alert = useAlert();
  const { employee } = useContext(Context2);


  const [leadview, setLeadView] = useState(null);
  const [leadNoteView, setLeadNoteView] = useState(null);
  const [leadMailView, setLeadMailView] = useState(null);
  const [leadReminderView, setLeadReminderView] = useState(null);
  const [loader, setLoader] = useState(false);
  const [refresh, seRefresh] = useState(false);
  const { dispatchDetails } = useContext(Context);

  useEffect(() => {
    setLoader(true);
    Instance.get(`/api/admin/getLeadById/${params.id}`, {
      headers: { authorization: `Bearer ${localStorage.getItem('token$')}` },
    })
      .then(({ data }) => {
        // console.log('leadDetailsData', data);
        setLeadView(data?.results);
        setLoader(false);
      })
      .catch((err) => {
        console.log('err', err);
      });
  }, []);

  useEffect(() => {
    setLoader(true);
    if (leadview != null)
      Instance.get(`/api/admin/leadNotes/${leadview._id}`, {
        headers: { authorization: `Bearer ${localStorage.getItem('token$')}` },
      })
        .then(({ data }) => {
          // console.log('leadNoteData', data);
          setLeadNoteView(data?.leadNotes);
          setLoader(false);
        })
        .catch((err) => {
          console.log('err', err);
        });
  }, [leadview, refresh]);

  useEffect(() => {
    setLoader(true);
    if (leadview != null)
      Instance.get(`/api/admin/leadMails/${leadview._id}`, {
        headers: { authorization: `Bearer ${localStorage.getItem('token$')}` },
      })
        .then(({ data }) => {
          // console.log('leadMailData', data);
          setLeadMailView(data?.mails);
          setLoader(false);
        })
        .catch((err) => {
          console.log('err', err);
        });
  }, [leadview, refresh]);

  useEffect(() => {
    setLoader(true);
    if (leadview != null)
      Instance.get(`/api/admin/leadReminders/${leadview._id}`, {
        headers: { authorization: `Bearer ${localStorage.getItem('token$')}` },
      })
        .then(({ data }) => {
          // console.log('leadReminderData', data);
          setLeadReminderView(data?.reminders);
          setLoader(false);
        })
        .catch((err) => {
          console.log('err', err);
        });
  }, [leadview, refresh]);

  const LeadNote = (leadview) => {
    history.push(`/leadNote/${leadview._id}`);
  };

  const LeadNoteDelete = (value) => {
    Instance.delete(`/api/admin/leadNote/${value._id}`, {
      headers: { authorization: `Bearer ${localStorage.getItem('token$')}` },
    })
      .then(() => {
        // console.log('Success');
        alert.success('Note Deleted');
        seRefresh(!refresh);
      })
      .catch((err) => {
        console.log('Error', err);
      });
  };

  const LeadNoteEdit = (value) => {
    dispatchDetails({
      type: LEAD_NOTE_DETAILS,
      payload: value,
    });
    history.push(`/leadNoteEdit/${value._id}`);
  };

  const LeadReminder = (leadview) => {
    history.push(`/leadReminder/${leadview._id}`);
  };

  const LeadMail = (leadview) => {
    dispatchDetails({
      type: LEAD_MAIL_DETAILS,
      payload: leadview,
    });
    history.push(`/leadMail/${leadview._id}`);
  };

  return (
    <AuthUser>
      <Row className='page-title align-items-center'>
        <Col sm={4} xl={6}>
          <h4 className='mb-1 mt-0'>Details About Lead</h4>
        </Col>
      </Row>

      {loader ? (
        <>
          <div class='d-flex justify-content-center mt-4'>
            <div class='spinner-border' role='status'>
              <span class='sr-only'>Loading...</span>
            </div>
          </div>{' '}
        </>
      ) : (
        <>
          <div className='row'>
            <div className='col-2 Lead_Board'>
              <div className='email-container bg-transparent'>
                {/* Left sidebar */}
                <Leftsidebar />
                {/* End Left sidebar */}
              </div>
            </div>

            <div className='col-10'>
              <div className='card p-3'>
                <h4 className='mb-4'> Lead Detail </h4>
                <div className='row'>
                  <div className='col-md-3'>
                    <p>
                      <strong>First Name : </strong>
                      {leadview?.firstName}
                    </p>
                  </div>
                  <div className='col-md-3'>
                    <p>
                      <strong>Last Name : </strong>
                      {leadview?.lastName}
                    </p>
                  </div>
                  <div className='col-md-3'>
                    <p>
                      <strong>Email : </strong> {leadview?.email}
                    </p>
                  </div>
                  <div className='col-md-3'>
                    <p>
                      <strong>Phone Number : </strong> {leadview?.mobile}
                    </p>
                  </div>
                </div>
              </div>

              <div className='card'>
                <div className='card p-3'>
                  <div className='row'>
                    <div className='col-8'>
                      <h4> Notes </h4>
                    </div>
                    <div className='col-4 text-right'>
                     
                        <button
                          type='button'
                          className='btn btn-danger btn-rounded'
                          data-toggle='modal'
                          data-target='#noteModal'
                          onClick={() => LeadNote(leadview)}>
                          {' '}
                          Add Note{' '}
                        </button>
                     
                    </div>
                  </div>
                  <div>
                    <Table hover responsive className='mt-4'>
                    <tr>
                      <th>#</th>
                      <th>Details</th>
                      <th>CreatedAt</th>
                      <th>Assign To</th>
                      <th>Actions</th>
                    </tr>
                      <tbody>
                        {leadNoteView != null &&
                          leadNoteView.map((value, i) => (
                            <tr key={i}>
                              <td>{i + 1}</td>
                              <td>{value.details}</td>
                              <td>
                                {' '}
                                {moment(new Date(value.createdAt)).format(
                                  'DD/MM/YYYY'
                                )}
                                <br />
                                {new Date(value.createdAt).toLocaleTimeString()}
                              </td>
                              <td>{value.assignTo?.firstName} {value.assignTo?.lastName}</td>
                              <td>
                                <svg
                                  xmlns='http://www.w3.org/2000/svg'
                                  width='16'
                                  height='16'
                                  fill='currentColor'
                                  class='bi bi-pencil-square'
                                  viewBox='0 0 16 16'
                                  onClick={() => LeadNoteEdit(value)}>
                                  <path d='M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456l-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z' />
                                  <path
                                    fill-rule='evenodd'
                                    d='M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z'
                                  />
                                </svg>

                                <i
                                  className='uil uil-trash-alt ml-2'
                                  onClick={() => LeadNoteDelete(value)}
                                />

                                {/* <span className='badge badge-soft-warning py-1'>
                                  Pending
                                </span> */}
                              </td>
                            </tr>
                          ))}
                      </tbody>
                    </Table>
                  </div>
                </div>
              </div>

              <div className='card'>
                <div className='card p-3'>
                  <div className='row'>
                    <div className='col-8'>
                      <h4> Email </h4>
                    </div>
                    <div className='col-4 text-right'>
                        <button
                          type='button'
                          className='btn btn-danger btn-rounded'
                          onClick={() => LeadMail(leadview)}>
                          {' '}
                          Send Email{' '}
                        </button>
                    </div>
                  </div>
                  <div>
                    <Table hover responsive className='mt-4'>
                    <tr>
                      <th>#</th>
                      <th>CreatedAt</th>
                      <th>Subject</th>
                      <th>Message</th>
                    </tr>
                      <tbody>
                        {leadMailView != null &&
                          leadMailView.map((value, i) => (
                            <tr key={i}>
                              <td>{i + 1}</td>

                              <td>
                                {' '}
                                {moment(new Date(value.createdAt)).format(
                                  'DD/MM/YYYY'
                                )}
                                <br />
                                {new Date(value.createdAt).toLocaleTimeString()}
                              </td>
                              <td>{value.subject}</td>
                              <td>{value.message}</td>
                            </tr>
                          ))}
                      </tbody>
                    </Table>
                  </div>
                </div>
              </div>

              <div className='card'>
                <div className='card p-3'>
                  <div className='row'>
                    <div className='col-8'>
                      <h4> Reminder </h4>
                    </div>
                    <div className='col-4 text-right'>
                        <button
                          type='button'
                          className='btn btn-danger btn-rounded'
                          onClick={() => LeadReminder(leadview)}>
                          {' '}
                          Set Reminder{' '}
                        </button>
                      
                    </div>
                  </div>
                  <div>
                    <Table hover responsive className='mt-4'>
                    <tr>
                      <th>#</th>
                      <th>Message</th>
                      <th>Time</th>
                      <th>Day</th>
                      <th>CreatedAt</th>
                    </tr>
                      <tbody>
                        {leadReminderView != null &&
                          leadReminderView.map((value, i) => (
                            <tr key={i}>
                              <td>{i + 1}</td>
                              <td>{value?.message}</td>
                              <td>{value?.time}</td>
                              <td>
                                {value?.day === 0
                                  ? 'Today'
                                  : value?.day === 3
                                  ? 'After 3 Days'
                                  : value?.day === 7
                                  ? 'After 7 Days'
                                  : value?.day === 14
                                  ? 'After 14 Days'
                                  : value?.day === 21
                                  ? 'After 21 Days'
                                  : value?.day === 30
                                  ? 'After 30 Days'
                                  : ''}
                              </td>
                              <td>
                                {' '}
                                {moment(new Date(value.createdAt)).format(
                                  'DD/MM/YYYY'
                                )}
                                <br />
                                {new Date(value.createdAt).toLocaleTimeString()}
                              </td>
                            </tr>
                          ))}
                      </tbody>
                    </Table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </>
      )}
    </AuthUser>
  );
};

export default Detail;
