/** @format */

import React, { useState, useContext, useEffect } from 'react';
import { Row, Col } from 'reactstrap';
import Leftsidebar from './leftsidebar';
import { useHistory, Redirect } from 'react-router-dom';
import Instance from '../../Instance';
import Context from '../context/Context';
import { useAlert } from 'react-alert';

const LeadEdit = () => {
  let history = useHistory();
  const alert = useAlert();
  const { details } = useContext(Context);
  const [isUpdate, setUpdate] = useState(false)
  const [isLoading, setLoading] = useState(false)
  const [seen, setSeen] = useState('password');

  const [leadDetails, setLeadDetails] = useState({
    firstName: '',
    lastName: '',
    phone: '',
    email: '',
    password: ''
  });

  useEffect(() => {
    setLoading(true)
    Instance.get(`/api/admin/getLeadById/${details?._id}`,
      {
        headers: { authorization: `Bearer ${localStorage.getItem('token$')}` },
      }).then(({ data }) => {
        setLoading(false)
        if (data.success) {
          setLeadDetails({
            firstName: data?.results?.firstName,
            lastName: data?.results?.lastName,
            phone: data?.results?.mobile,
            email: data?.results?.email,
            password: data?.results?.password
          })
        }
      }).catch((err) => {
        setLoading(false)
        console.log('err', err)
      })
  }, [])

  const PasswordSeen = () => {
    if (seen === 'password') {
      setSeen('text')
    }
    else {
      setSeen('password')
    }
  }


  const [errors, setErrors] = useState({
    firstName: false,
    lastName: false,
    phone: false,
    email: false,
    password: false
  });

  const handelChange = (event) => {
    setLeadDetails({
      ...leadDetails,
      [event.target.id]: event.target.value,
    });

    if (event.target.value === '') {
      setErrors({ ...errors, [event.target.id]: true });
    } else {
      setErrors({ ...errors, [event.target.id]: false });
    }
  };

  const OnUpdated = (e) => {
    e.preventDefault();
    setUpdate(true)
    Instance.put(
      `/api/admin/updateLead/${details._id}`,
      {
        firstName: leadDetails.firstName,
        lastName: leadDetails.lastName,
        mobile: leadDetails.phone,
        email: leadDetails.email,
        password: leadDetails.password
      },
      {
        headers: { authorization: `Bearer ${localStorage.getItem('token$')}` },
      }
    ).then(({ data }) => {
      setUpdate(false)
      if (data?.success) {
        // console.log('save', data);
        alert.success(data?.message);
        history.push('/leads');
      }
    })
      .catch((err) => {
        setUpdate(false)
        console.log('Err', err);
      });
  };

  if (details == null) {
    return <Redirect to='/leads' />;
  }

  const HandleValidation = (e) => {
    e.preventDefault();
    let shouldsubmit = true;
    let tempError = errors;

    for (const [key, value] of Object.entries(leadDetails)) {
      if (value == '') {
        // if (key != 'search') {
        tempError[key] = true;
        if (shouldsubmit) {
          shouldsubmit = false;
        }
        // }
        // else {
        //   tempError[key] = false;
        // }
      }
    }
    setErrors(tempError);

    if (shouldsubmit) {
      OnUpdated(e);
    }
  };

  return (
    <>
      <Row className='page-title align-items-center'>
        <Col sm={4} xl={6}>
          <h4 className='mb-1 mt-0'>Leads </h4>
        </Col>
      </Row>

      <div className='row'>
        <div className='col-2 Lead_Board'>
          <div className='email-container bg-transparent'>
            {/* Left sidebar */}
            <Leftsidebar />
            {/* End Left sidebar */}
          </div>
        </div>
        <div className='col-10'>
          <div className='card p-3'>
            <h4 className='mb-4'> Edit Lead </h4>

            {
              isLoading ? (
                <>
                  <div class='d-flex justify-content-center mt-4'>
                    <div class='spinner-border' role='status'>
                      <span class='sr-only'>Loading...</span>
                    </div>
                  </div>{' '}
                </>
              ) : (
                <>
                  <div className='form-row'>
                    <div className='form-group col-md-4'>
                      <label>First Name</label>
                      <input
                        type='text'
                        placeholder='First Name'
                        id='firstName'
                        value={leadDetails.firstName}
                        name='firstName'
                        onChange={handelChange}
                        // ref={register}
                        className={`form-control ${errors.firstName ? 'is-invalid' : ''
                          }`}
                      />
                      {errors.firstName && (
                        <div className='invalid-feedback'>Enter Your FirstName</div>
                      )}
                    </div>
                    <div className='form-group col-md-4'>
                      <label>Last Name</label>
                      <input
                        type='text'
                        placeholder='Last Name'
                        id='lastName'
                        value={leadDetails.lastName}
                        name='lastName'
                        onChange={handelChange}
                        // ref={register}
                        className={`form-control ${errors.lastName ? 'is-invalid' : ''
                          }`}
                      />
                      {errors.lastName && (
                        <div className='invalid-feedback'>Enter Your LastName</div>
                      )}
                    </div>
                    <div className='form-group col-md-4'>
                      <label>Phone Number</label>
                      <input
                        type='text'
                        placeholder='Phone Number'
                        id='phone'
                        value={leadDetails.phone}
                        name='phone'
                        onChange={handelChange}
                        // ref={register}
                        className={`form-control ${errors.phone ? 'is-invalid' : ''}`}
                      />
                      {errors.phone && (
                        <div className='invalid-feedback'>
                          Enter Your Phone Number
                        </div>
                      )}
                    </div>
                    <div className='form-group col-md-4'>
                      <label>Email</label>
                      <input
                        type='email'
                        placeholder='Email'
                        id='email'
                        value={leadDetails.email}
                        name='email'
                        onChange={handelChange}
                        // ref={register}
                        className={`form-control ${errors.email ? 'is-invalid' : ''}`}
                      />
                      {errors.email && (
                        <div className='invalid-feedback'>Enter Your Email</div>
                      )}
                    </div>
                    <div className='form-group col-md-4'>
                      <label>Password</label>
                      <div style={{ display: 'flex' }}>
                        <input
                          type={seen}
                          placeholder='Password'
                          id='password'
                          value={leadDetails.password}
                          name='password'
                          onChange={handelChange}
                          // ref={register}
                          className={`form-control ${errors.password ? 'is-invalid' : ''}`}
                        />
                        {seen === 'password' ?
                          <div class="input-group-prepend" style={{ margin: '0 10px' }}>
                            <div className="input-group-text">
                              <span className="fa fa-eye" style={{ cursor: 'pointer' }} onClick={() => PasswordSeen()}></span>
                            </div>
                          </div>
                          :
                          <div class="input-group-prepend" style={{ margin: '0 10px' }}>
                            <div className="input-group-text">
                              <span className="fa fa-eye-slash" style={{ cursor: 'pointer' }} onClick={() => PasswordSeen()}></span>
                            </div>
                          </div>
                        }


                      </div>

                      {errors.password && (
                        <div className='invalid-feedback'>Enter Your Password</div>
                      )}
                    </div>
                  </div>

                  <div className='form-row'>
                    <div className='form-group col-md-2'>
                      <button
                        disabled={isUpdate}
                        className='btn btn-primary btn-block'
                        onClick={(e) => HandleValidation(e)}>
                        {isUpdate ? 'Updating lead...' : 'Update'}
                      </button>
                    </div>
                  </div>

                </>
              )
            }


          </div>
        </div>{' '}
        {/* end Col */}
      </div>
      {/* End row */}
    </>
  );
};

export default LeadEdit;
