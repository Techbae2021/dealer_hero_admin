/** @format */

import React, { useState, useContext } from 'react';
import { Row, Col } from 'reactstrap';
import Leftsidebar from './leftsidebar';
import Instance from './../../Instance';
import { useHistory, useParams } from 'react-router-dom';

const LeadReminder = () => {
  let history = useHistory();
  const params = useParams();
  const [isLoading, setLoading] = useState(false)
  const [reminderDetails, setReminderDetails] = useState({
    message: '',
    time: '',
    day: '',
  });

  const [errors, setErrors] = useState({
    message: false,
    time: false,
    day: false
  });

  const HandleValidation = (e) => {
    e.preventDefault();
    let shouldsubmit = true;
    let tempError = errors;

    for (const [key, value] of Object.entries(reminderDetails)) {
      if (value == '') {
        // if (key != 'search') {
        tempError[key] = true;
        if (shouldsubmit) {
          shouldsubmit = false;
        }
        // }
        // else {
        //   tempError[key] = false;
        // }
      }
    }
    // console.log(tempError);
    setErrors(tempError);

    if (shouldsubmit) {
      OnSubmit(e);
    }
  };

  const handelChange = (event) => {
    setReminderDetails({
      ...reminderDetails,
      [event.target.id]: event.target.value,
    });
    if (event.target.value === '') {
      setErrors({ ...errors, [event.target.id]: true });
    } else {
      setErrors({ ...errors, [event.target.id]: false });
    }
  };

  const OnSubmit = (e) => {
    e.preventDefault();
    setLoading(true)

    let time = ""
    if (reminderDetails?.time) {
      var [h, m] = reminderDetails?.time.split(":");
      time = reminderDetails?.time + ((h % 12 + 12 * (h % 12 == 0)) + ":" + m, h >= 12 ? 'PM' : 'AM')
    }

    Instance.post(
      `/api/admin/leadReminder/${params.id}`,
      {
        message: reminderDetails?.message,
        time: time,
        day: reminderDetails?.day,
      },
      {
        headers: { authorization: `Bearer ${localStorage.getItem('token$')}` },
      }
    ).then(({ data }) => {
      // console.log('save', data);
      setLoading(false)
      history.push(`/leads/detail/${params.id}`);
    }).catch((err) => {
      setLoading(false)
      console.log('Err', err);
    });
  };

  return (
    <>
      <Row className='page-title align-items-center'>
        <Col sm={4} xl={6}>
          <h4 className='mb-1 mt-0'>Lead Reminder</h4>
        </Col>
      </Row>

      <div className='row'>
        <div className='col-2 Lead_Board'>
          <div className='email-container bg-transparent'>
            {/* Left sidebar */}
            <Leftsidebar />
            {/* End Left sidebar */}
          </div>
        </div>
        <div className='col-10'>
          <div className='card p-3'>
            <h4 className='mb-4'> Add Reminder </h4>
            <div className='form-row'>
              <div className='form-group col-md-4'>
                <label>New Reminder</label>
                <input
                  type='text'
                  className={`form-control ${errors.message ? 'is-invalid' : ''}`}
                  placeholder='Reminder'
                  id='message'
                  value={reminderDetails.message}
                  onChange={handelChange}></input>
                {errors.message ? (
                  <div className='invalid-feedback'>Enter Your message</div>
                ) : null}
              </div>
              <div className='form-group col-md-4'>
                <label>Time</label>
                <input type="time"
                  className={`form-control ${errors.time ? 'is-invalid' : ''}`}
                  id='time'
                  value={reminderDetails.time}
                  onChange={handelChange}
                />

                {errors.time ? (
                  <div className='invalid-feedback'>Enter Your Time</div>
                ) : null}
              </div>
              <div className='form-group col-md-4'>
                <label>Day</label>
                <select
                  className={`form-control ${errors.day ? 'is-invalid' : ''}`}
                  id='day'
                  value={reminderDetails.day}
                  onChange={handelChange}>
                  <option value=''>Select Day</option>
                  <option value='0'>Today</option>
                  <option value='3'>3 Day</option>
                  <option value='7'>7 Day</option>
                  <option value='14'>14 Day</option>
                  <option value='21'>21 Day</option>
                  <option value='30'>30 Day</option>
                </select>
                {errors.day ? (
                  <div className='invalid-feedback'>Enter Your Day</div>
                ) : null}
              </div>
            </div>
            <div className='form-row'>
              <div className='form-group col-md-2'>
                <button
                  disabled={isLoading}
                  className='btn btn-primary btn-block'
                  onClick={(e) => HandleValidation(e)}>
                  {isLoading ? 'Adding...' : 'Add'}
                </button>
              </div>
            </div>
          </div>
        </div>
        {/* end Col */}
      </div>
    </>
  );
};

export default LeadReminder;
