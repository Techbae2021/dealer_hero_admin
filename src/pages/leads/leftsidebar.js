/** @format */

import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import Instance from '../../Instance';
import './lead.css';

const Leftsidebar = (props) => {
  const [leadCount, setLeadCount] = useState(null);
  const [refresh, seRefresh] = useState(false);


  useEffect(() => {
    Instance.get(`/api/admin/lead/summary`, {
      headers: { authorization: `Bearer ${localStorage.getItem('token$')}` },
    }).then(({ data }) => {
        // console.log('leadCounting', data);
        setLeadCount(data);
        seRefresh(!refresh);
      })
      .catch((err) => {
        console.log('err', err);
      });
  }, [props?.reload]);

  return (
    <>
      <div className=''>
        {/* Left sidebar */}
        <div className='p-3 card'>
            <Link to='/leads/create' className='btn btn-danger btn-block mb-3 '>
              Create Lead
            </Link>
         

          <div className='mail-list '>
            <Link to='/leads' className='list-group-item border-0'>
              <>
              <i className='uil uil-envelope-alt font-size-15 mr-1' />
              Leads
              <span className='badge badge-danger float-right ml-2 mt-1'>
                {leadCount?.leads}
              </span>
              </>
            </Link>
            <Link to='/leads/star' className='list-group-item border-0'>
              <>
              <i className='uil uil-envelope-star font-size-15 mr-1' />
              Star
              <span className='badge badge-info float-right ml-2 mt-1'>
                {leadCount?.starLeads}
              </span>
              </>
            </Link>
            <Link to='/leads/done' className='list-group-item border-0'>
              <>
              <i className='uil uil-envelope-edit font-size-15 mr-1' />
              Done
              <span className='badge badge-info text-right ml-2 mt-1'>
                {leadCount?.doneLeads}
              </span>
              </>
            </Link>
            <Link to='/leads/delete' className='list-group-item border-0'>
              <>
              <i className='uil uil-trash font-size-15 mr-1' />
              Deleted{' '}
              <span className='text-right badge badge-info  mt-1'>
                {leadCount?.deletedLeads}
              </span>
              </>
            </Link>
          </div>
        </div>
        {/* End Left sidebar */}
      </div>
    </>
  );
};

export default Leftsidebar;
