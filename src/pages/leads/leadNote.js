/** @format */

import React, { useEffect, useState } from 'react';
import { Row, Col } from 'reactstrap';
import Leftsidebar from './leftsidebar';
import Instance from './../../Instance';
import { useHistory, useParams } from 'react-router-dom';
import { useAlert } from 'react-alert';

const LeadNote = () => {
  const [pipelineView, setPipelineView] = useState(null);
  const params = useParams();
  let history = useHistory();
  const alert = useAlert();
  const [isLoading,setLoading] = useState(false)
  const [empList,setEmpList] = useState([])

  useEffect(() => {
    Instance.get(`/api/admin/getpipeline`, {
      headers: { authorization: `Bearer ${localStorage.getItem('token$')}` },
    })
      .then(({ data }) => {
        // console.log('piplinesData', data);
        setPipelineView(data?.data);
      })
      .catch((err) => {
        console.log('err', err);
      });
  }, []);

  useEffect(() => {
    Instance.get(`/api/admin/getAllEmployeeList`, {
      headers: { authorization: `Bearer ${localStorage.getItem('token$')}` },
    })
      .then(({ data }) => {
        // console.log('asd', data?.EmployeeList);
        setEmpList(data?.EmployeeList);
      })
      .catch((err) => {
        console.log('err', err);
      });
  }, []);

  const [noteDetails, setNoteDetails] = useState({
    details: '',
    assignTo: '',
    pipeline: ''
  });

  const [errors, setErrors] = useState({
    details: false,
    assignTo: false,
    pipeline: false
  });

  const handelChange = (event) => {
    setNoteDetails({
      ...noteDetails,
      [event.target.id]: event.target.value,
    });
    if (event.target.value === '') {
      setErrors({ ...errors, [event.target.id]: true });
    } else {
      setErrors({ ...errors, [event.target.id]: false });
    }
  };

  const OnSubmit = (e) => {
    e.preventDefault();
    setLoading(true)
    // console.log({noteDetails})
    Instance.post(
      `/api/admin/lead/note/${params.id}`,
      {
        details: noteDetails.details,
        assignTo: noteDetails.assignTo,
        pipeline: noteDetails.pipeline,
      },
      {
        headers: { authorization: `Bearer ${localStorage.getItem('token$')}` },
      }
    ).then(({ data }) => {
        // console.log('save', data)
        setLoading(false)
        alert.success('Note Added');
        history.push(`/leads/detail/${params.id}`);
      }).catch((err) => {
        setLoading(false)
        console.log('Err', err);
      });
  };

  const HandleValidation = (e) => {
    e.preventDefault();
    let shouldsubmit = true;
    let tempError = errors;

    for (const [key, value] of Object.entries(noteDetails)) {
      if (value == '') {
        // if (key != 'search') {
          tempError[key] = true;
          if (shouldsubmit) {
            shouldsubmit = false;
          }
        // }
        // else {
      //   tempError[key] = false;
      // }
      }
    }
    // console.log(tempError);
    setErrors(tempError);

    if (shouldsubmit) {
      OnSubmit(e);
    }
  };

  return (
    <>
      <Row className='page-title align-items-center'>
        <Col sm={4} xl={6}>
          <h4 className='mb-1 mt-0'>Lead Note</h4>
        </Col>
      </Row>

      <div className='row'>
        <div className='col-2 Lead_Board'>
          <div className='email-container bg-transparent'>
            {/* Left sidebar */}
            <Leftsidebar />
            {/* End Left sidebar */}
          </div>
        </div>
        <div className='col-10'>
          <div className='card p-3'>
            <h4 className='mb-4'> Add Note </h4>
            <div className='form-row'>
              <div className='form-group col-md-4'>
                <label>New Note</label>
                <input
                  type='text'
                  className={`form-control ${errors.details ? 'is-invalid' : ''}`}
                  placeholder='Note'
                  id='details'
                  value={noteDetails.details}
                  onChange={handelChange}></input>
                 {errors.details && (
                  <div className='invalid-feedback'>Enter Your Details</div>
                )}
              </div>
              <div className='form-group col-md-4'>
                <label>Assign To</label>
                <select
                   className={`form-control ${errors.assignTo ? 'is-invalid' : ''}`}
                  id='assignTo'
                  value={noteDetails.assignTo}
                  onChange={handelChange}>
                  <option value=''>Select Employee</option>
                  {
                    empList && empList.map((emp,i)=><option key={i} value={emp?._id}>{emp?.firstName} {emp?.lastName} -  {emp?.email} </option>)
                  }
                </select>
                {errors.assignTo && (<div className='invalid-feedback'>Select Your AssignTo</div>)}
              </div>
              <div className='form-group col-md-4'>
                <label>Pipeline</label>
                <select
                className={`form-control ${errors.pipeline ? 'is-invalid' : ''}`}
                  id='pipeline'
                  value={noteDetails.pipeline}
                  onChange={handelChange}>
                  <option value=''>Select Pipeline</option>
                  {pipelineView != null &&
                    pipelineView.map((value, i) => (
                      <option value={value._id}>{value.pipelineName}</option>
                    ))}
                </select>
                {errors.pipeline && (
                  <div className='invalid-feedback'>Select Your Pipeline</div>
                )}
              </div>
            </div>
            <div className='form-row'>
              <div className='form-group col-md-2'>
                <button
                 disabled={isLoading}
                  className='btn btn-primary btn-block'
                  onClick={(e) => HandleValidation(e)}>
                    {isLoading?'Adding...':'Add'}
                </button>
              </div>
            </div>
          </div>
        </div>{' '}
        {/* end Col */}
      </div>
      {/* End row */}
    </>
  );
};

export default LeadNote;
