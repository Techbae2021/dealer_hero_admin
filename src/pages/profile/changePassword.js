/** @format */

import React, { useContext, useState } from 'react';
import { Row, Col } from 'reactstrap';
import Instance from '../../Instance';
import Context from '../context/Context';
import { useHistory } from 'react-router-dom';
import { useAlert } from 'react-alert';
import AuthUser from '../../components/auth/AuthUser';

const ChangePassword = () => {
  let history = useHistory();
  const { details } = useContext(Context);
  const alert = useAlert();

  const [passwordchange, setPasswordChange] = useState({
    password: '',
    oldPassword: '',
  });

  const handelChange = (event) => {
    setPasswordChange({
      ...passwordchange,
      [event.target.id]: event.target.value,
    });
  };

  const OnUpdated = (e) => {
    e.preventDefault();
    if(passwordchange.oldPassword.trim()!=='' && passwordchange.password.trim()!==''){
    Instance.put(
      `/api/admin/changePassword`,
      {
        oldPassword: passwordchange.oldPassword,
        newPassword: passwordchange.password,
      },
      {
        headers: { authorization: `Bearer ${localStorage.getItem('token$')}` },
      }
    ).then(({ data }) => {
        // console.log('save', data);
        alert.success('Password Changed');
        history.push('/myprofile');
      })
      .catch((err) => {
        // alert
        console.log('Err', err);
        if(!err?.response?.data?.success){
          alert.error(err.response?.data?.err?.response?.data?.message);
        }else{
            alert.error("Something went wrong!")
        }
      });
    }else{
      alert.error('Please enter both correct passwords!');
    }
  };

  return (
    <AuthUser>
      <Row className='page-title align-items-center'>
        <Col sm={4} xl={6}>
          <h4 className='mb-1 mt-0'>Change Password</h4>
        </Col>
      </Row>
      <div className='col-12'>
        <div className='card p-3'>
          {/* <h4 className='mb-4'> Create Lead </h4> */}
          <div className='form-row'>
            <div className='form-group col-md-4'>
              <label>Old Password</label>
              <input
                type='password'
                className='form-control'
                placeholder='Old Password'
                id='oldPassword'
                value={passwordchange.oldPassword}
                onChange={handelChange}></input>
            </div>
            <div className='form-group col-md-4'>
              <label>New Password</label>
              <input
                type='password'
                className='form-control'
                placeholder='New Password'
                id='password'
                value={passwordchange.password}
                onChange={handelChange}></input>
            </div>
          </div>

          <div
            className='form-row'
            style={{ display: 'grid', placeItems: 'center' }}>
            <div className='form-group col-md-2'>
              <button className='btn btn-primary btn-block' onClick={OnUpdated}>
                {' '}
                Save{' '}
              </button>
            </div>
          </div>
        </div>
      </div>{' '}
      {/* end Col */}
    </AuthUser>
  );
};

export default ChangePassword;
