/** @format */

import React, { useState, useEffect, useContext } from 'react';
import './profile.css';
import * as FeatherIcon from 'react-feather';
import moment from 'moment';
import Instance from '../../Instance';
import { useHistory } from 'react-router-dom';
import Context from '../context/Context';
import { PROFILE_DETAILS } from '../context/action.type';
import { useAlert } from 'react-alert';
import Context2 from '../../routes/Context';
import AuthUser from '../../components/auth/AuthUser';
import config from '../../config';

const Profile = () => {
  let history = useHistory();
  const alert = useAlert();
  const [profileview, setProfileView] = useState(null);
  const [refresh, setRefresh] = useState(false);
  const [loader, setLoader] = useState(true);
  const [img, setImg] = useState(null);
  const [imgbase, setImgeBase] = useState(null);
  const { dispatchDetails } = useContext(Context);
  const { setEmployee } = useContext(Context2);
  useEffect(() => {
    Instance.get(`/api/admin/getProfileDetails`, {
      headers: { authorization: `Bearer ${localStorage.getItem('token$')}` },
    }).then(({ data }) => {
        setEmployee(data?.data)
        setProfileView(data?.data);
        setLoader(false);
      }).catch((err) => {setLoader(false);console.log('err', err);});
  }, [refresh]);

  const handleImage = async (e) => {
    try {
      e.preventDefault();
      let file = e.target.files[0];
      setImg(file);

      if (file) {
        let base64Image = await new Promise((resolve, reject) => {
          const fileReader = new FileReader();
          fileReader.readAsDataURL(file);

          fileReader.onload = () => {
            resolve(fileReader.result);
          };
          fileReader.onerror = (err) => {
            reject(err);
          };
        });

        if (base64Image !== undefined) {
          setImgeBase(base64Image);
        }
      } else {
        setImg(file);
      }
    } catch (err) {
      console.log('Errror in image upload ', err);
    }
  };

  const PicUpdate = (e) => {
    e.preventDefault();
    let data = new FormData();
    data.append('Profile', img);
    Instance.put(`/api/admin/changeProfilePic`, data, {
      headers: { authorization: `Bearer ${localStorage.getItem('token$')}` },
    }).then(({ data }) => {
        // console.log('save', data);
        setRefresh(!refresh);
        setImg(null);
        alert.success('Profile Pic Updated');
      })
      .catch((err) => {
        console.log('Err', err);
      });
  };

  const ProfileEdit = (profileview) => {
    dispatchDetails({
      type: PROFILE_DETAILS,
      payload: profileview,
    });
    // console.log("====>")
    history.push("/editprofile");
  };

  const ChangePassword = (profileview) => {
    dispatchDetails({
      type: PROFILE_DETAILS,
      payload: profileview,
    });
    history.push(`/changepassword`);
  };

  return (
    <AuthUser>
      {loader ? (
        <>
          <div class='d-flex justify-content-center mt-2'>
            <div class='spinner-border' role='status'>
              <span class='sr-only'>Loading...</span>
            </div>
          </div>{' '}
        </>
      ) : (
        <>
          <div className='page-content page-container' id='page-content'>
            <div className='padding'>
              <div className='row container d-flex justify-content-center'>
                <div className='col-xl-6 col-md-12'>
                  <div className='card user-card-full'>
                    <div className='row m-l-0 m-r-0'>
                      <div className='col-sm-4 bg-c-lite-green user-profile'>
                        <div className='card-block text-center text-white'>
                          <div className='m-b-25'>
                            <label
                              htmlFor='image'
                              style={{ objectFit: 'cover' }}>
                              <input
                                type='file'
                                name='image'
                                id='image'
                                style={{ display: 'none' }}
                                onChange={handleImage}
                              />
                              <img
                                style={{ height: 100, width: 100 }}
                                objectFit='cover'
                                src={
                                  img != null
                                    ? imgbase
                                    : `${config.baseImgUrl}/SuperAdminProfile/${profileview?.profile}`
                                }
                                className='rounded-circle mx-auto'
                                alt='logo'
                              />
                            </label>

                            <button
                              onClick={PicUpdate}
                              type='button'
                              className={
                                img != null
                                  ? 'btn btn-primary btn-sm mt-4'
                                  : 'btn btn-primary btn-sm mt-4 profileImage'
                              }>
                              Save
                            </button>
                          </div>
                          <h6 className='f-w-600'>
                            {profileview?.firstName} {profileview?.lastName}
                          </h6>
                          {/* <p>Web Designer</p>{' '} */}
                          <a
                            onClick={() => ProfileEdit(profileview)}
                            className='text-center'>
                            <FeatherIcon.Edit />
                          </a>
                          <a
                            onClick={() => ChangePassword(profileview)}
                            className='text-center'>
                            <FeatherIcon.Lock />
                          </a>
                          <i className=' mdi mdi-square-edit-outline feather icon-edit m-t-10 f-16' />
                        </div>
                      </div>
                      <div className='col-sm-8'>
                        <div className='card-block'>
                          <h6 className='m-b-20 p-b-5 b-b-default f-w-600'>
                            Personal Information
                          </h6>
                          <div className='row'>
                            <div className='col-sm-6'>
                              <p className='m-b-10 f-w-600'>Email</p>
                              <h6 className='text-muted f-w-400'>
                                {profileview?.email}
                              </h6>
                            </div>
                            <div className='col-sm-6'>
                              <p className='m-b-10 f-w-600'>Phone</p>
                              <h6 className='text-muted f-w-400'>
                                {profileview?.phone}
                              </h6>
                            </div>
                            <div className='col-sm-12'>
                              <p className='m-b-10 f-w-600'>Address</p>
                              <h6 className='text-muted f-w-400'>
                                {profileview?.address}
                              </h6>
                            </div>
                          </div>
                          <h6 className='m-b-20 m-t-40 p-b-5 b-b-default f-w-600'>
                            Other Information
                          </h6>
                          <div className='row'>
                            <div className='col-sm-6'>
                              <p className='m-b-10 f-w-600'>Join Date</p>
                              <h6 className='text-muted f-w-400'>
                                {moment(
                                  new Date(profileview?.createdAt)
                                ).format('DD/MM/YYYY')}
                              </h6>
                            </div>
                            <div className='col-sm-6'>
                              <p className='m-b-10 f-w-600'>Join Time</p>
                              <h6 className='text-muted f-w-400'>
                                {new Date(
                                  profileview?.createdAt
                                ).toLocaleTimeString()}
                              </h6>
                            </div>
                          </div>
                          <ul className='social-link list-unstyled m-t-40 m-b-10'>
                            <li>
                              <a
                                href='#!'
                                data-toggle='tooltip'
                                data-placement='bottom'
                                title
                                data-original-title='facebook'
                                data-abc='true'>
                                <i
                                  className='mdi mdi-facebook feather icon-facebook facebook'
                                  aria-hidden='true'
                                />
                              </a>
                            </li>
                            <li>
                              <a
                                href='#!'
                                data-toggle='tooltip'
                                data-placement='bottom'
                                title
                                data-original-title='twitter'
                                data-abc='true'>
                                <i
                                  className='mdi mdi-twitter feather icon-twitter twitter'
                                  aria-hidden='true'
                                />
                              </a>
                            </li>
                            <li>
                              <a
                                href='#!'
                                data-toggle='tooltip'
                                data-placement='bottom'
                                title
                                data-original-title='instagram'
                                data-abc='true'>
                                <i
                                  className='mdi mdi-instagram feather icon-instagram instagram'
                                  aria-hidden='true'
                                />
                              </a>
                            </li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </>
      )}
    </AuthUser>
  );
};

export default Profile;
