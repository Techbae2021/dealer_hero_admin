/** @format */

import React, { useContext, useState } from 'react';
import { Row, Col } from 'reactstrap';
import Instance from '../../Instance';
import Context from '../context/Context';
import { Redirect, useHistory } from 'react-router-dom';
import { useAlert } from 'react-alert';
import AuthUser from '../../components/auth/AuthUser';

const EditProfile = () => {
  let history = useHistory();
  const alert = useAlert();

  const { details } = useContext(Context);
  const [userDetails, setUserDetails] = useState({
    firstName: details != null ? details.firstName : '',
    lastName: details != null ? details.lastName : '',
    phone: details != null ? details.phone : '',
    email: details != null ? details.email : '',
    address: details != null ? details.address : '',
  })

  const handelChange = (event) => {
    setUserDetails({
      ...userDetails,
      [event.target.id]: event.target.value,
    });
  };

  const OnUpdated = (e) => {
    e.preventDefault();
    Instance.put(
      `/api/admin/ProfileDetailsUpdate`,
      {
        firstName: userDetails.firstName,
        lastName: userDetails.lastName,
        phone: userDetails.phone,
        address: userDetails.address,
      },
      {
        headers: { authorization: `Bearer ${localStorage.getItem('token$')}` },
      }
    ).then(({ data }) => {
        // console.log('save', data);
        alert.success('Profile Edited');
        history.push('/myprofile');
      })
      .catch((err) => {
        console.log('Err', err);
      });
  };

  // if (details == null) {
  //   return <Redirect to='/myprofile' />;
  // }

  return (
    <AuthUser>
      <Row className='page-title align-items-center'>
        <Col sm={4} xl={6}>
          <h4 className='mb-1 mt-0'>Edit Profile</h4>
        </Col>
      </Row>
      <div className='col-12'>
        <div className='card p-3'>
          <div className='form-row'>
            <div className='form-group col-md-4'>
              <label>First Name</label>
              <input
                type='text'
                className='form-control'
                placeholder='First Name'
                id='firstName'
                value={userDetails.firstName}
                onChange={handelChange}></input>
            </div>
            <div className='form-group col-md-4'>
              <label>Last Name</label>
              <input
                type='text'
                className='form-control'
                placeholder='Last Number'
                id='lastName'
                value={userDetails.lastName}
                onChange={handelChange}></input>
            </div>

            <div className='form-group col-md-4'>
              <label>Email</label>
              <input
                type='text'
                className='form-control'
                placeholder='Email'
                id='email'
                value={userDetails.email}
                onChange={handelChange}
                disabled></input>
            </div>
            <div className='form-group col-md-4'>
              <label>phone</label>
              <input
                type='text'
                className='form-control'
                placeholder='phone'
                id='phone'
                disabled
                value={userDetails.phone}
                onChange={handelChange}></input>
            </div>
            <div className='form-group col-md-4'>
              <label>Address</label>
              <input
                type='text'
                className='form-control'
                placeholder='address'
                id='address'
                value={userDetails.address}
                onChange={handelChange}></input>
            </div>
          </div>

          <div
            className='form-row'
            style={{ display: 'grid', placeItems: 'center' }}>
            <div className='form-group col-md-2'>
              <button className='btn btn-primary btn-block' onClick={OnUpdated}>
                {' '}
                Save{' '}
              </button>
            </div>
          </div>
        </div>
      </div>{' '}
      {/* end Col */}
    </AuthUser>
  );
};

export default EditProfile;
