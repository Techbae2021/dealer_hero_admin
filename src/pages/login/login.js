/** @format */

import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import * as FeatherIcon from 'react-feather';
import DealerLogo from '../../assets/Dealer_Hero.png';
import { useHistory } from 'react-router-dom';
import Instance from './../../Instance';
import { useAlert } from 'react-alert';
import mixpanel from 'mixpanel-browser';

const Login = () => {
  let history = useHistory();
  const alert = useAlert();
  const [isLoading, setLoading] = useState(false)

  useEffect(() => {
    let token = localStorage.getItem('token$')
    // console.log('token',token)
    if (token) {
      history.push('/dashboard')
    } 
    mixpanel.track('Login', {
      date: Date.now(),
    });

  }, []);

  const [userDetails, setUserDetails] = useState({
    email: '',
    password: '',
  });

  const handelChange = (event) => {
    setUserDetails({
      ...userDetails,
      [event.target.id]: event.target.value,
    });
  };

  const OnLogIn = () => {
    // e.preventDefault();
    setLoading(true)
    Instance.post('/api/admin/login', {
      email: userDetails.email,
      password: userDetails.password,
    }).then(({ data }) => {
      setLoading(false)
      if(data?.success){
        alert.success(data?.message)
      // console.log('save', data);
      localStorage.setItem('token$', data ? data.token : '');
      history.push('/dashboard')
      }
    }).catch((err) => {
      setLoading(false)
        console.log('Err', err?.response?.data?.err?.response?.data?.message);
        if(!err?.response?.data?.success){
          alert.error(err.response?.data?.err?.response?.data?.message);
          }else{
            alert.error("Something went wrong!")
          }
      });
  };

  const [errors, setErrors] = useState({
    userNameError: false,
    passwordError: false,
  });

  const HandleValidation = () => {
    // e.preventDefault();
    if (userDetails.email === '') {
      setErrors((prevError) => {
        return {
          ...prevError,
          userNameError: true,
          passwordError: false,
        };
      });

      if (userDetails.password === '') {
        setErrors((prevError) => {
          return {
            ...prevError,
            passwordError: true,
          };
        });
      }
    } else if (userDetails.password === '') {
      setErrors((prevError) => {
        return {
          ...prevError,
          userNameError: false,
          passwordError: true,
        };
      });
    } else {
      OnLogIn();
    }
  };

  return (
    <>
      <div className='account-pages my-5'>
        <div className='container'>
          <div
            className='row justify-content-center'
            style={{ marginTop: '8rem' }}>
            <div className='col-xl-10'>
              <div className='card'>
                <div className='card-body p-0'>
                  <div className='row'>
                    <div className='col-md-6 p-5'>
                      <div className='mx-auto mb-5 text-center'>
                        <a href='index.html'>
                          <img src={DealerLogo} alt='' height={60} />
                          {/* <h3 className='d-inline align-middle ml-1 text-logo'>
                            Shreyu
                          </h3> */}
                        </a>
                      </div>
                      <h6 className='h5 mb-0 mt-4 text-center'>
                        <strong> ADMIN LOGIN </strong>
                      </h6>
                      {/* <p className='text-muted mt-1 mb-4'>
                        Enter your email address and password to access admin
                        panel.
                      </p> */}
                      <div className='authentication-form'>
                        <div className='form-group'>
                          <label className='form-control-label'>
                            Email Address
                          </label>
                          <div className='input-group input-group-merge'>
                            <div className='input-group-prepend'>
                              <span className='input-group-text'>
                                {/* <i className="icon-dual" data-feather="mail" /> */}
                                <FeatherIcon.Mail />
                              </span>
                            </div>
                            <input
                              type='email'
                              className='form-control'
                              id='email'
                              placeholder='Email'
                              value={userDetails.email}
                              onChange={handelChange}
                              autoComplete='off'
                            />
                          </div>
                          {errors.userNameError && (
                            <label style={{ color: 'red' }}>
                              Enter Your Email
                            </label>
                          )}
                        </div>
                        <div className='form-group mt-4'>
                          <label className='form-control-label'>Password</label>

                          <div className='input-group input-group-merge'>
                            <div className='input-group-prepend'>
                              <span className='input-group-text'>
                                {/* <i className="icon-dual" data-feather="lock" /> */}
                                <FeatherIcon.Lock />
                              </span>
                            </div>
                            <input
                              type='password'
                              className='form-control'
                              id='password'
                              placeholder='Password'
                              value={userDetails.password}
                              onChange={handelChange}
                              autoComplete='off'
                            />
                          </div>
                          {errors.passwordError && (
                            <label style={{ color: 'red' }}>
                              Enter Your Password
                            </label>
                          )}
                        </div>
                        <div className='form-group mb-4'>
                          <div className='custom-control custom-checkbox'>
                            <Link
                              to='/forgotpassword'
                              className='float-right text-muted text-unline-dashed ml-1'>
                              Forgot your password?
                            </Link>
                          </div>
                        </div>

                        <div className='form-group mb-0 text-center'>
                          {/* <button className="btn btn-primary btn-block" type="submit"> Log In
		                    </button> */}

                          <input
                            disabled={isLoading}
                            type='submit'
                            className='btn btn-primary btn-block'
                            value={isLoading?'Please wait...':'Login'}
                            onClick={HandleValidation}
                          />
                        </div>
                      </div>
                      {/* <div className="py-3 text-center"><span className="font-size-16 font-weight-bold">Or</span></div>
		                <div className="row">
		                  <div className="col-6">
		                    <Link to="" className="btn btn-white"><i className="uil uil-google icon-google mr-2" />With Google</Link>
		                  </div>
		                  <div className="col-6 text-right">
		                    <Link to="" className="btn btn-white"><i className="uil uil-facebook mr-2 icon-fb" />With Facebook</Link>
		                  </div>
		                </div> */}
                    </div>
                    <div className='col-lg-6 d-none d-md-inline-block'>
                      <div className='auth-page-sidebar'>
                        <div className='overlay' />
                        <div className='auth-user-testimonial'>
                          <p className='font-size-24 font-weight-bold text-white mb-1'>
                            DEALER HERO
                          </p>
                          <p className='lead'>
                            The dealership system that works.
                          </p>
                          <p>We've been helping dealers for over 20 years.</p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>{' '}
                {/* end card-body */}
              </div>
              {/* end card */}
              <div className='row mt-3'>
                <div className='col-12 text-center'>
                  <p className='text-muted'>
                    Don't have an account?
                    <Link
                      to='/signup'
                      className='text-primary font-weight-bold ml-1'>
                      Sign Up{' '}
                    </Link>{' '}
                  </p>
                </div>{' '}
                {/* end col */}
              </div>
              {/* end row */}
            </div>{' '}
            {/* end col */}
          </div>
          {/* end row */}
        </div>
        {/* end container */}
      </div>
    </>
  );
};

export default Login;
