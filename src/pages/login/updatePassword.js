/** @format */

import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import * as FeatherIcon from 'react-feather';
import DealerLogo from '../../assets/Dealer_Hero.png';
import Instance from '../../Instance';
import { useHistory, useParams } from 'react-router-dom';
import { useAlert } from 'react-alert';

const UpdatePassword = () => {
  let history = useHistory();
  const params = useParams();
  const alert = useAlert();

  // useEffect(() => {
  //   console.log('id', params.id);
  // }, []);

  const [userDetails, setUserDetails] = useState({
    newPassword: '',
  });

  const handelChange = (event) => {
    setUserDetails({
      ...userDetails,
      [event.target.id]: event.target.value,
    });
  };

  const OnUpdatePassword = () => {
    // e.preventDefault();
    Instance.put(`/api/admin/reset/${params.id}`, {
      newPassword: userDetails.password,
    })
      .then(({ data }) => {
        if(data?.success){
          alert.success(data?.message)
          setTimeout(()=>history.push('/'),2000)
          }
      })
      .catch((err) => {
        console.log('Err', err);
        if(!err?.response?.data?.success){
          alert.error(err.response?.data?.err?.response?.data?.message);
          }else{
            alert.error("Something went wrong!")
          }
      });
  };

  const [errors, setErrors] = useState({
    passwordError: false,
  });

  const HandleValidation = () => {
    // e.preventDefault();
    if (userDetails.password === '') {
      setErrors((prevError) => {
        return {
          ...prevError,
          passwordError: true,
        };
      });
    } else {
      OnUpdatePassword();
    }
  };

  return (
    <>
      <div className='account-pages my-5'>
        <div className='container'>
          <div
            className='row justify-content-center'
            style={{ marginTop: '8rem' }}>
            <div className='col-xl-10'>
              <div className='card' style={{ width: '50rem' }}>
                <div className='card-body p-0'>
                  <div className='row'>
                    <div className='col-md-12 p-5'>
                      <div className='mx-auto mb-5 text-center'>
                        <a href='index.html'>
                          <img src={DealerLogo} alt='' height={60} />
                          {/* <h3 className='d-inline align-middle ml-1 text-logo'>
                            Shreyu
                          </h3> */}
                        </a>
                      </div>
                      {/* <h6 className='h5 mb-0 mt-4 text-center'>
                        Welcome back!
                      </h6> */}
                      {/* <p className='text-muted mt-1 mb-4'>
                        Enter your email address and password to access admin
                        panel.
                      </p> */}
                      <div className='authentication-form'>
                        <div className='form-group'>
                          <label className='form-control-label'>
                            New Password
                          </label>
                          <div className='input-group input-group-merge'>
                            <div className='input-group-prepend'>
                              <span className='input-group-text'>
                                {/* <i className="icon-dual" data-feather="mail" /> */}
                                <FeatherIcon.Lock />
                              </span>
                            </div>
                            <input
                              type='password'
                              className='form-control'
                              id='password'
                              placeholder='Password'
                              value={userDetails.password}
                              onChange={handelChange}
                              autoComplete='off'
                            />
                          </div>
                          {errors.passwordError && (
                            <label style={{ color: 'red' }}>
                              Enter Your Password
                            </label>
                          )}
                        </div>

                        <div className='form-group mb-0 text-center'>
                          {/* <button className="btn btn-primary btn-block" type="submit"> Log In
		                    </button> */}
                          <Link
                            to='#'
                            className='btn btn-primary btn-block'
                            onClick={HandleValidation}>
                            Submit
                          </Link>
                        </div>
                      </div>
                      {/* <div className="py-3 text-center"><span className="font-size-16 font-weight-bold">Or</span></div>
		                <div className="row">
		                  <div className="col-6">
		                    <Link to="" className="btn btn-white"><i className="uil uil-google icon-google mr-2" />With Google</Link>
		                  </div>
		                  <div className="col-6 text-right">
		                    <Link to="" className="btn btn-white"><i className="uil uil-facebook mr-2 icon-fb" />With Facebook</Link>
		                  </div>
		                </div> */}
                    </div>
                    {/* <div className='col-lg-6 d-none d-md-inline-block'>
                      <div className='auth-page-sidebar'>
                        <div className='overlay' />
                        <div className='auth-user-testimonial'>
                          <p className='font-size-24 font-weight-bold text-white mb-1'>
                            DEALER HERO
                          </p>
                          <p className='lead'>
                            The dealership system that works.
                          </p>
                          <p>We've been helping dealers for over 20 years.</p>
                        </div>
                      </div>
                    </div> */}
                  </div>
                </div>{' '}
                {/* end card-body */}
              </div>
              {/* end card */}
              <div className='row mt-3'>
                <div className='col-12 text-center'>
                  <p className='text-muted'>
                    Already have account?
                    <Link to='/' className='text-primary font-weight-bold ml-1'>
                      Login
                    </Link>{' '}
                  </p>
                </div>{' '}
                {/* end col */}
              </div>
              {/* end row */}
            </div>{' '}
            {/* end col */}
          </div>
          {/* end row */}
        </div>
        {/* end container */}
      </div>
    </>
  );
};

export default UpdatePassword;
