/** @format */

import React, { useState } from "react";
import { Row, Col } from "reactstrap";
import { useHistory } from "react-router-dom";
import { useAlert } from "react-alert";
import Leftsidebar from "./leftsidebar";
import ReactChipInput from "react-chip-input";
import axios from 'axios'
import AuthUser from "../../components/auth/AuthUser";

const CreateMail = () => {
  let history = useHistory();
  const alert = useAlert();
  const [chips, setChips] = useState([])
  const [toError, setToError] = useState(false)
  const [isLoading, setLoading] = useState(false)

  const [mailDetails, setMailDetails] = useState({
    subject: "",
    text: "",
    attactments:[]
  });

  const [errors, setErrors] = useState({
    subject: false,
    text: false,
  });

  const handelChange = (event) => {
    if(event.target.id==='attactments'){
      setMailDetails({
        ...mailDetails,
        [event.target.id]: event.target.files,
      });
    }else{
    setMailDetails({
      ...mailDetails,
      [event.target.id]: event.target.value,
    });
  }
    if (event.target.value === "") {
      setErrors({ ...errors, [event.target.name]: true });
    } else {
      setErrors({ ...errors, [event.target.name]: false });
    }
  };

  const OnSubmit = () => {
    // e.preventDefault();
    if(isValid){
      setLoading(true)
    var data = new FormData();
    if(chips.length>0){
      chips.map((to)=>data.append("to",to))
    }

    if(mailDetails.attactments.length>0){
      [...mailDetails.attactments].map((file)=>data.append("attachments",file))
    }

    data.append('subject',mailDetails.subject)
    data.append('message',mailDetails.text)

     var config = {
      method: 'post',
      url: `${process.env.REACT_APP_BACKEND_URL}api/admin/sendMail`,
      headers: { 
        authorization: `Bearer ${localStorage.getItem('token$')}`,
        'Content-Type': 'multipart/form-data'
      },
      data : data
    };
     axios(config).then(({ data }) => {
        // console.log("save", data);
      setLoading(false)

        alert.success("Mail Created");
        history.push("/email");
      })
      .catch((err) => {
      setLoading(false)

        console.log("Err", err);
      });
    }
  };



  const [isValid, setisValid] = useState(true);

  const HandleValidation = () => {
    let shouldsubmit = true;
    let tempError = errors;

    for (const [key, value] of Object.entries(mailDetails)) {
      if(key!=='attactments'){
      if (value == "") {
        tempError[key] = true;
        if (shouldsubmit) {
          shouldsubmit = false;
        }
      } else {
        tempError[key] = false;
      }
     }
    }
    // console.log(tempError);
    setisValid(shouldsubmit);
    setErrors(tempError);

    if(chips.length===0){
      setToError(true)
      shouldsubmit=false
    }

    if (shouldsubmit) {
      OnSubmit();
    }
  };

  const addChip = value => {
    if(value.toString().trim()!==''){
    const v = chips.slice()
    v.push(value);
    setChips(v)
    }
  }

  const removeChip = index => {
    const v = chips.slice()
    v.splice(index, 1)
    setChips(v)
  }

  return (
    <AuthUser>
      <Row className="page-title align-items-center">
        <Col sm={4} xl={6}>
          <h4 className="mb-1 mt-0">Mails</h4>
        </Col>
      </Row>

      <div className="row">
        <div className="col-2 Lead_Board">
          <div className="email-container bg-transparent">
            {/* Left sidebar */}
            <Leftsidebar/>
            {/* End Left sidebar */}
          </div>
        </div>
        <div className="col-10">
          <div className="card p-3">
            <h4 className="mb-4"> Create Mail </h4>
            <div className="row mx-0  mb-3">
              <div className="col-md-7"></div>
              <div className="col-md-5">
              </div>
            </div>
            <div className="form-row">
              <div className="form-group col-md-8">
                <label>To <span>(Type & press enter)</span> </label>
                <ReactChipInput
                  classes="class1 class2"
                  className={`form-control ${
                    toError ? "is-invalid" : ""
                  }`}
                  style={{borderWidth:1}}
                  chips={chips}
                  onSubmit={value => addChip(value)}
                  onRemove={index => removeChip(index)}
                />
             
                 {toError && (
                  <div className="invalid-feedback">Required</div>
                )}
              </div>
            </div>
            <div className="form-row">
              <div className="form-group col-md-8">
                <label>Subject</label>
                <input
                  type="text"
                  className={`form-control ${
                    errors.subject ? "is-invalid" : ""
                  }`}
                  placeholder="Subject"
                  id="subject"
                  name="subject"
                  value={mailDetails.subject}
                  onChange={handelChange}
                ></input>
                {errors.subject && (
                  <div className="invalid-feedback">Required</div>
                )}
              </div>
            </div>

            <div className="form-row">
              <div className="form-group col-md-8">
                <label>Attactments</label>
                <input
                  type="file"
                  onChange={handelChange}
                  className={`form-control`}
                  id="attactments"
                  name="attactments"
                  multiple
                />
                {errors.attactments && (
                  <div className="invalid-feedback">Required</div>
                )}
              </div>
            </div>

            <div className="form-row">
              <div className="form-group col-md-8">
                <label>Messege</label>
                <textarea
                  className={`form-control ${errors.text ? "is-invalid" : ""}`}
                  id="text"
                  name="text"
                  rows="5"
                  value={mailDetails.text}
                  onChange={handelChange}
                ></textarea>
                {errors.text && (
                  <div className="invalid-feedback">Required</div>
                )}
              </div>
            </div>

            <div className="form-row">
              <div className="form-group col-md-2">
              <button
                 disabled={isLoading}
                  className='btn btn-primary btn-block'
                  onClick={HandleValidation}>
                    {isLoading?'Sending...':'Send'}
                </button>
              </div>
            </div>
          </div>
        </div>{" "}
      </div>
    </AuthUser>
  );
};

export default CreateMail;
