/** @format */

import React, { useEffect, useState } from 'react';
import { Row, Col } from 'reactstrap';
import Leftsidebar from './leftsidebar';
import { Link } from 'react-router-dom';
import Instance from '../../Instance';
import moment from 'moment';
import AuthUser from '../../components/auth/AuthUser';

const StarMail = () => {

  const [emailview, setEmailView] = useState(null);
  const [loader, setLoader] = useState(false);
  const [count, setCount] = useState(1);
  const [page, setPage] = useState(null);

  useEffect(() => {
    setLoader(true);
    Instance.post(
      `/api/admin/getStarredMails`,
      {
        page: count,
      },
      {
        headers: { authorization: `Bearer ${localStorage.getItem('token$')}` },
      }
    ).then(({ data }) => {
        // console.log('starMailData', data);
        let temp = data?.results?.EmailStarred;
        setPage(data.results);
        setEmailView(temp);
        setLoader(false);
      }).catch((err) => {
        setLoader(false);
        console.log('err', err);
      });
  }, [count]);

  const NextPage = () => {
    setCount(count + 1);
  };
  const PrevPage = () => {
    setCount(count - 1);
  };

  return (
    <AuthUser>
      <Row className='page-title align-items-center'>
        <Col sm={4} xl={6}>
          <h4 className='mb-1 mt-0'>Mails</h4>
        </Col>
      </Row>

      <div className='row'>
        <div className='col-2 Lead_Board'>
          <div className='email-container bg-transparent'>
            {/* Left sidebar */}
            <Leftsidebar/>
            {/* End Left sidebar */}
          </div>
        </div>
        <div className='col-10'>
          <div className=''>
            {/* start right sidebar */}
            <div className=''>
              <div className='btn-group mb-2'>
              </div>
              <div className='d-inline-block align-middle float-lg-right'>
                <div className='row'>
                  <div className='col-8 align-self-center' style={{ whiteSpace:"nowrap"}}>
                    Showing {page?.startOfItem} - {page?.endOfItem} of{' '}
                    {page?.totalEmailWithStarreddb}
                  </div>
                  {/* end col*/}
                  <div className='col-4'>
                    <div className='btn-group float-right'>
                      <button
                        type='button'
                        className='btn btn-white btn-sm'
                        onClick={PrevPage}
                        style={{
                          display: page?.Previous === false ? 'none' : '',
                        }}>
                        <i className='uil uil-angle-left' />
                      </button>
                      <button
                        type='button'
                        className='btn btn-primary btn-sm'
                        onClick={NextPage}
                        style={{
                          display: page?.Next === false ? 'none' : '',
                        }}>
                        <i className='uil uil-angle-right'  />
                      </button>
                    </div>
                  </div>{' '}
                  {/* end col*/}
                </div>
              </div>
              {loader ? (
                  <div class='d-flex justify-content-center'>
                    <div class='spinner-border' role='status'>
                      <span class='sr-only'>Loading...</span>
                    </div>
                  </div>
              ) : (
                <>
                  <div className='mt-2'>
                    <h5 className='mt-3 mb-2 font-size-16'>Star Mail </h5>
                    {emailview &&
                      emailview.map((value, i) => (
                        <ul className='message-list' key={i}>
                          <li>
                            <div className='col-mail col-mail-1'>
                            <Link className="title" to={`/email/details/${value._id}`}>{value.to}</Link>
                            </div>
                            <div className='col-mail col-mail-2'>
                              <a href className='subject'>
                                <span className='teaser'>
                                  {value.subject} ,{value.text}, ({' '}
                                  {moment(new Date(value.createdAt)).format(
                                    'DD/MM/YYYY'
                                  )}
                                  ,
                                  {new Date(
                                    value.createdAt
                                  ).toLocaleTimeString()}
                                  )
                                </span>
                              </a>
                              <div className='date'></div>
                            </div>
                          </li>
                        </ul>
                      ))}
                  </div>
                </>
              )}
            </div>
            {/* end right sidebar */}
            <div className='clearfix' />
          </div>
        </div>{' '}
        {/* end Col */}
      </div>
      {/* End row */}
    </AuthUser>
  );
};

export default StarMail;
