/** @format */

import React, { useEffect, useState } from 'react';
import {
  Row,
  Col,
  CardBody,
  Card,
  CardImg
} from 'reactstrap';
import { useHistory, useParams } from 'react-router-dom';
import Instance from '../../Instance';
import AuthUser from '../../components/auth/AuthUser';

const EmailDetails = () => {

  const params = useParams();
  const [emailView, setEmailView] = useState(null);
  const [loader, setLoader] = useState(false);

  useEffect(() => {
    setLoader(true);
    Instance.get(`/api/admin/mailDetailds/${params.id}`, {
      headers: { authorization: `Bearer ${localStorage.getItem('token$')}` },
    }).then(({ data }) => {
      setEmailView(data?.mailDetails);
      setLoader(false);
    }).catch((err) => {
      setLoader(false);
      console.log('err', err);
    });
  }, []);


  return (
    <AuthUser>
      <Row className='page-title align-items-center'>
        <Col sm={8}>
          <h4 className='mb-1 mt-0'> Email Details</h4>
        </Col>
      </Row>

      <React.Fragment>
      {loader ? (
                  <>
                    <div className='d-flex justify-content-center'>
                      <div className='spinner-border' role='status'>
                        <span className='sr-only'>Loading...</span>
                      </div>
                    </div>{' '}
                  </>
                ) : (
        <Card>
          <CardBody>
            <Row>
            
              <Col sm="12">
                <label className='col-form-label'>
                  <strong> Mail To: </strong>
                </label>{' '}
                <span>{emailView?.to?.join(", ")}</span>
              </Col>
            </Row>
            <Row>
              <Col sm="12">
                <label className='col-form-label'>
                  <strong> Subject: </strong>
                </label>{' '}
                <span>{emailView?.subject}</span>
              </Col>
            </Row>
            <Row>
              <Col sm="12">
                <label className='col-form-label'>
                  <strong> Message: </strong>
                </label>{' '}
                <span>{emailView?.message}</span>
              </Col>
            </Row>
            {
                (emailView?.attachments && emailView?.attachments.length>0) && (
                  <Card>
                    <CardBody>
                      <h4>Attachments:</h4>
                      <Row>
                        {
                          emailView?.attachments?.map((img) => (
                            <Col xs="4">
                              <Card>
                                <CardBody>
                                  <CardImg width="100%" style={{ height: 190 }} src={`${window?.env?.base_imgUrl}/mailAttachments/${img.filename}`} alt="image" />
                                </CardBody>
                              </Card>
                            </Col>
                          ))
                        }
                      </Row>
                    </CardBody>
                  </Card>
                )
              }
          </CardBody>
        </Card>
        )}
      </React.Fragment>
    </AuthUser>
  );
};

export default EmailDetails;
