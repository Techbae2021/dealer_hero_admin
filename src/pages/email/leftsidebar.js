import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import Instance from '../../Instance';

const Leftsidebar = () => {
  const [totalMail, setMails] = useState({ inbox: 0, star: 0 });

  useEffect(() => {
    Instance.get(
      `/api/admin/mail/summary`, {
      headers: { authorization: `Bearer ${localStorage.getItem('token$')}` },
    }).then(({ data }) => {
      setMails({ inbox: data?.inbox, star: data?.star })
    }).catch((err) => {
      console.log('err', err);
    })
  }, []);

  return (
    <>
      <div>
        {/* Left sidebar */}
        <div className='p-3 card'>
          <Link to='/email/create' className='btn btn-danger btn-block mb-3'>
            {' '}
            Create Mail
          </Link>
          <div className='mail-list '>
            <Link to='/email' className='list-group-item border-0 '>
              <i className='uil uil-envelope-alt font-size-15 mr-1' /> Inbox{' '}
              <span className='badge badge-danger float-right ml-2 mt-1'>
                {totalMail?.inbox}
              </span>
            </Link>
            <Link to='/email/star' className='list-group-item border-0'>
              <i className='uil uil-envelope-star font-size-15 mr-1' />
              Star
              <span className='badge badge-info float-right ml-2 mt-1'>
                {totalMail?.star}
              </span>
            </Link>
          </div>
        </div>
      </div>
    </>
  );
};

export default Leftsidebar;
