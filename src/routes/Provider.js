/** @format */

import React, { useState } from 'react';
import Context from './Context';

export default function EmployeeProvider(props) {
  const [employee, setEmployee] = useState(null);

  return (
    <Context.Provider
      value={{
        employee,
        setEmployee,
      }}>
      {props.children}
    </Context.Provider>
  );
}
