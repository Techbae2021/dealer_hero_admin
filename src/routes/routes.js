/** @format */

import React, { useContext, useEffect } from 'react';
import { Route, Switch } from 'react-router-dom';

import Login from '../pages/login/login';
import Signup from '../pages/signup/signup';

import Dashboard from '../pages/dashboard/dashboard';

import Leads from '../pages/leads/leads';
import LeadsCreate from '../pages/leads/create';
import LeadsDetail from '../pages/leads/detail';

import Email from '../pages/email/email';
import ForgotPassword from '../pages/login/forgotPassword';
import UpdatePassword from './../pages/login/updatePassword';

import Profile from '../pages/profile/profile';
import EditProfile from './../pages/profile/editProfile';
import ChangePassword from './../pages/profile/changePassword';
import LeadEdit from '../pages/leads/leadEdit';
import LeadStar from '../pages/leads/leadStar';
import LeadDone from '../pages/leads/leadDone';
import LeadDelete from '../pages/leads/leadDelete';
import LeadNote from '../pages/leads/leadNote';
import LeadReminder from '../pages/leads/leadReminder';
import LeadMail from '../pages/leads/leadMail';
import LeadNoteEdit from '../pages/leads/leadNoteEdit';
import Pipeline from '../pages/pipeline/pipeline';
import LeadEmp from '../pages/leads/leadEmp';

import Dealer from '../pages/dealer/dealer';
import DDetails from '../pages/dealer/dDetails';

import CDetails from './../pages/dealerCustomer/cDetails';
import CustomerNote from './../pages/dealerCustomer/customerNote';
import CNoteEdit from './../pages/dealerCustomer/cNoteEdit';
import CustomerReminder from './../pages/dealerCustomer/customerReminder';

import CreateMail from './../pages/email/CreateMail';
import StarMail from './../pages/email/StarMail';
import HeaderSetting from './../pages/setting/uiSetting/headerSetting';
import FooterSetting from './../pages/setting/uiSetting/footerSetting';
import TitleSetting from './../pages/setting/uiSetting/titleSetting';
import SocialSetting from './../pages/setting/uiSetting/socialSetting';
import OfferSetting from './../pages/setting/uiSetting/offerSetting';
import ContactSetting from './../pages/setting/uiSetting/contactSetting';
import EditHeader from './../pages/setting/uiSetting/editHeader';
import AddHeader from './../pages/setting/uiSetting/addHeader';
import AddFooter from './../pages/setting/uiSetting/addFooter';
import EditFooter from './../pages/setting/uiSetting/editFooter';
import AddOffer from './../pages/setting/uiSetting/addOffer';
import EditOffer from './../pages/setting/uiSetting/editOffer';
import AddTitle from './../pages/setting/uiSetting/addTitle';
import EditTitle from './../pages/setting/uiSetting/editTitle';
import EditSocial from './../pages/setting/uiSetting/editSocial';
import AddSocial from './../pages/setting/uiSetting/addSocial';
import EditContact from './../pages/setting/uiSetting/editContact';
import AddContact from './../pages/setting/uiSetting/addContact';
import Instance from './../Instance';
import Context from './Context';
import Payment from '../pages/payment/payment';
import PaymentView from '../pages/payment/paymentView';
import Subscription from '../pages/subscription/subscription';
import AddSubscription from '../pages/subscription/addSubscription';
import DealerCustomer from '../pages/dealerCustomer/dealerCustomer';
import EmailDetails from '../pages/email/emailDetails'
import CustomerMail from '../pages/dealerCustomer/customerMail';
import DeletedCustomer from '../pages/dealerCustomer/customerDeleted';
import Notification from '../pages/notification/notification';

const Routes = () => {
  const { setEmployee } = useContext(Context);

  useEffect(() => {
    let token = localStorage.getItem('token$')
    if (token) {
      Instance.get(`/api/admin/getProfileDetails`, {
        headers: { authorization: `Bearer ${token}` },
      }).then(({ data }) => {
        setEmployee(data?.data)
      }).catch((err) => {
        console.log('err', err)
      })

      let ftoken = localStorage.getItem('ftoken$')
      if (ftoken) {
        Instance.put(`/api/admin/updatedevicetoken`, { devicetoken: ftoken }, {
          headers: { authorization: `Bearer ${token}` },
        }).catch((e) => {
          console.log('Error', e)
        })
      }
    }
  }, [setEmployee]);

  return (
    <>
      <Switch>
        <Route exact path='/' component={Login} />

        <Route exact path='/dashboard' component={Dashboard} />
        <Route exact path='/signup' component={Signup} />
        <Route exact path='/forgotpassword' component={ForgotPassword} />
        <Route exact path='/updatepassword/:id' component={UpdatePassword} />

        <Route exact path='/myprofile' component={Profile} />
        <Route exact path='/editprofile' component={EditProfile} />
        <Route exact path='/changepassword' component={ChangePassword} />

        <Route exact path='/leads' component={Leads} />
        <Route exact path='/leads/create' component={LeadsCreate} />
        <Route exact path='/leads/detail/:id' component={LeadsDetail} />
        <Route exact path='/leads/edit/:id' component={LeadEdit} />
        <Route path='/leadNote/:id' component={LeadNote} />
        <Route path='/leadNoteEdit/:id' component={LeadNoteEdit} />
        <Route path='/leadReminder/:id' component={LeadReminder} />
        <Route path='/leadMail/:id' component={LeadMail} />
        <Route exact path='/leads/done' component={LeadDone} />
        <Route exact path='/leads/star' component={LeadStar} />
        <Route exact path='/leads/delete' component={LeadDelete} />
        <Route exact path='/leads/emp/:id' component={LeadEmp} />

        <Route exact path='/pipeline' component={Pipeline} />

        <Route exact path='/dealer' component={Dealer} />
        <Route exact path='/dealer/view/:id' component={DDetails} />

        <Route exact path='/customer' component={DealerCustomer} />
        <Route exact path='/customer/note/:id' component={CustomerNote} />
        <Route path='/customer/note/edit/:id' component={CNoteEdit} />
        <Route path='/customer/reminder/:id' component={CustomerReminder} />
        <Route path='/customer/details/:id' component={CDetails} />
        <Route path='/customer/mail/:id' component={CustomerMail} />
        <Route path='/customer/deleted' component={DeletedCustomer} />


        <Route exact path='/email' component={Email} />
        <Route exact path='/email/create' component={CreateMail} />
        <Route exact path='/email/star' component={StarMail} />
        <Route exact path='/email/details/:id' component={EmailDetails} />

        <Route exact path='/setting/header' component={HeaderSetting} />
        <Route exact path='/setting/header/add' component={AddHeader} />
        <Route exact path='/setting/header/edit/:id' component={EditHeader} />
        <Route exact path='/setting/footer' component={FooterSetting} />
        <Route exact path='/setting/footer/add' component={AddFooter} />
        <Route exact path='/setting/footer/edit/:id' component={EditFooter} />
        <Route exact path='/setting/title' component={TitleSetting} />
        <Route exact path='/setting/title/add' component={AddTitle} />
        <Route exact path='/setting/title/edit/:id' component={EditTitle} />
        <Route exact path='/setting/social' component={SocialSetting} />
        <Route exact path='/setting/social/add' component={AddSocial} />
        <Route exact path='/setting/social/edit/:id' component={EditSocial} />
        <Route exact path='/setting/offer' component={OfferSetting} />
        <Route exact path='/setting/offer/add' component={AddOffer} />
        <Route exact path='/setting/offer/edit/:id' component={EditOffer} />
        <Route exact path='/setting/contact' component={ContactSetting} />
        <Route exact path='/setting/contact/add' component={AddContact} />
        <Route exact path='/setting/contact/edit/:id' component={EditContact} />

        <Route exact path='/payment' component={Payment} />
        <Route exact path='/payment/:id' component={PaymentView} />
        <Route exact path='/subscription' component={Subscription} />
        <Route exact path='/subscription/add' component={AddSubscription} />
        <Route exact path='/notification' component={Notification} />
      </Switch>
    </>
  );
};

export default Routes;
