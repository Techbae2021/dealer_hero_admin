/** @format */

importScripts('https://www.gstatic.com/firebasejs/3.5.0/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/3.5.0/firebase-messaging.js');

firebase.initializeApp({
  messagingSenderId: '545296404168',
});

const initMessaging = firebase.messaging();
